# STAnalysis

This package provides a script to perform static analysis on C source code,
in order to detect potential cache-timing side channel leaks.


# Installation

This package can be installed from [https://gitlab.telecom-paris.fr/sen/stanalysis/](https://gitlab.telecom-paris.fr/sen/stanalysis/). 
The easiest way to install this tool is via pipx. The installation procedure for pipx can be found [here](https://pipxproject.github.io/pipx/installation/).
This package can then be installed by running
### pipx >= 15.0.0

    pipx install git+https://gitlab.telecom-paris.fr/sen/stanalysis/

### pipx < 15.0.0

    pipx install stanalysis --spec git+https://gitlab.telecom-paris.fr/sen/stanalysis/

Alternatively, if pipx cannot be installed (it requires Python 3.6 which may not be available everywhere), stanalysis can be installed directly via pip. 
After creating and activating a virtual environment whith Python>=3.5, simpy run

    pip install git+https://gitlab.telecom-paris.fr/sen/stanalysis/

Note that you must activate the virtual environment before running this tool, which is not needed when installing via pipx.

# Manual installation

To install all required packages, run:

    pip install argcomplete python-ctags3 pycparserext

# Issues with the lexer
Once you have run the stanalyzer.py script once, it might create lextab.py / yacctab.py files
in the directory you run the script. Because they are re-generated on each run, it slows down
the analysis (and pollutes your directories). To avoid this, copy them in the installation 
directory of pycparserext, which should be (on Linux, if you installed this package in a virtual environment) at:

    ~/.virtualenvs/VIRTUALENVNAME/lib/pythonPYTHONVERSIONNUMBER/site-packages/pycparserext/

They should stop re-appearing after that.

# Optional: Installation of Jupyter Lab and extensions for interactive result analysis

We also provide a script to interactively explore the potential leakage sources. This requires an
installation of Jupyter Lab and is therefore optional. The easiest way to install the required packages is
to use pip and replace the installation command with

    pip install git+https://gitlab.telecom-paris.fr/sen/stanalysis/#egg=stanalysis[notebook] 

Installing via pipx does not seem possible, because editing jupyter notebook is required. This might be improved in a future version.

After installing, you need to run the follwing commands:

    jupyter labextension install @jupyter-widgets/jupyterlab-manager
    jupyter nbextension enable --py --sys-prefix widgetsnbextension
    jupyter nbextension enable --py --sys-prefix qgrid


If you do not use virtual environments, you do not need the --sys-prefix positional argument.

# Documentation

In progress.
