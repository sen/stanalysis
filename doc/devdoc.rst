Developper documentation of STAnalysis.
===========================================
.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. automodule:: stanalysis.leakageanalysis
   :members:

.. automodule:: stanalysis.scopes
   :members:

.. automodule:: stanalysis.common
   :members: