.. STAnlysis documentation master file, created by
   sphinx-quickstart on Thu Jan  4 15:54:24 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the documentation of STAnalysis.
===========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   intro
   tutorial
   devdoc

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
