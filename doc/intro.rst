==============
 Introduction
==============

Implementing cryptographic algorithms is not an easy task. Classical programming
errors such as buffer overflows, missing input sanitizing, etc. may lead to 
catastrophic failures. However, even implementations that are correct, even
proven correct, from a functional perspective might still get broken due to
so-called side-channel attacks. These kinds of attacks use information that
are not directly related to the functional description of the cryptographic 
algorithm, such as running time, power consumption, cache-behavior, in order
to break the security assumptions. This tool is designed to catch potential
oversights concerning two kinds of side-channel leaks that can arise in
software implementations: timing-attacks and more precisely, cache-timing
attacks.

Cache-timing attacks have been developed since the late 2010's, when measurement
techniques such as PRIME+PROBE [OsShTr05]_, FLUSH+RELOAD [YaFa14]_ and 
FLUSH+FLUSH [GrMaWaMa16]_ have been formalized and successfully implemented in 
order to break implementations of cryptographic algorithms such as AES, RSA and ECC. 
This is possible because these implementations have different cache behaviors
depending on secret inputs such as the secret key or the randomness used during
sensitive operations. Therefore, there is a simple (?) way to make an
implementation resilient against cache-timing attacks: the cache-behavior of 
the program must me independent of sensitive variables. In order to achieve
this, no branch instruction should depend on sensitive variables (so that
the instruction cache behavior is secret-independent) and no memory-address
computation for data retrieval should depend on sensitive variables (that is,
then index value for array accesses should not depend on sensitive variables,
and neither should the offset used for pointer arithmetics). These properties
*guarantee* that the implementation is protected against cache-timing attacks,
and probably also against timing attacks (some operations, such as divisions,
might seem to take a different number of cycles depending on the size of the
operands, but no practical use of this potential leak seems to have been
exploited yet). However, an implementation that does not follow these guidelines
is not necessarily broken, as the implementation of an actual attack is not
necessarily easy to do, it might require HyperThreading to work, the measurements
might be too noisy, etc. However, it signals that an attack *might* be possible.

In order to guarantee a protection against cache-timing attacks, the aforementioned
properties need to be verified in the assembly code. However, most of the time,
the leaks are introduced by the C code and not the compiler. This tool therefore
scans C code for potential violations of these properties. Thus, the code can
be corrected in order to avoid cache-timing attacks. 

.. [YaFa14] Yarom, Y., and Falkner, K., "FLUSH+ RELOAD: A High Resolution, Low Noise, L3 Cache Side-Channel Attack." USENIX Security Symposium. Vol. 1. 2014.
.. [OsShTr05] Osvik, D.A., Shamir, A., and Tromer, E., "Cache attacks and countermeasures: the case of AES," http://www.cs.tau.ac.il/~tromer/papers/cache.pdf, Nov 2005.
.. [GrMaWaMa16] Gruss,  D.,  Maurice,  C.,  Wagner,  K.,  Mangard,  S., "Flush+Flush:  a  fast  and stealthy  cache  attack",  DIMVA 2016. LNCS, vol. 9721, pp. 279–299.(2016)