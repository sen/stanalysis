===========
 Tutorial
===========

In order to use this tool, one needs:
 * One ore more files of C code implementing the algorithm to be tested for side-channel leaks,
 * Some indication about which variables are sensitive, and
 * Some (not necessarily functionally correct) implementation of all functions being called by the implementation being tested.

And that's it !


Installation
------------

TBD (links will depend on the GitLab links)

Setup
-----

In order to perform the static analysis of the C program, a few extra steps are
necessary after installation.

Faked header files
^^^^^^^^^^^^^^^^^^
The folder containing "faked" header files needs to be accessible. It contains 
mainly #define directives that are needed in order to preprocess your C files
and thus build an Abstract Syntactic Tree (AST) from it. Copy it somewhere
that is either accessible to your whole system or close to the C projects
you might want to use this tool on, as you wish. Note the path to this folder
somewhere.

Configuration file
^^^^^^^^^^^^^^^^^^
In the directory containing the C files, create a file named ".stanalyzer-config".
It will contain the options for the C parser, and it should contain at least
the following content::

   -nostdinc -E -IABSOLUTE_PATH_TO_FAKE_LIBC_FOLDER -I. -Doffsetof(x,y)=1

Those are actually the options passed to gcc for doing the preprocessing of
you C files (note the "-E" option for preprocessing only). You can add other
-I directives to add header files from other directories, or -D directives
to define constants needed in your program.

Tagging sensitive variable
^^^^^^^^^^^^^^^^^^^^^^^^^^
The C files need to be annotated, so that the list of sensitive variables is
passed to the static analysis tool. In order to do this, add a pragma directive
after the variable declaration, as such:

.. code-block:: C

    //file bar.c
    int foo(int s) {
		return s+1;

    int main() {
            int s = 0;
            #pragma STA secret s
            // s is now considered to be a sensitive variable
            return foo(s);
    }		


Simple Usage
------------
Once the setup is completed, you can launch an analysis using the ``stanalyzer.py`` script.
Several documented operations are possible, but the easiest way to use it the following::
    
    stanalyzer.py analyze --function-names main --output out.txt *.c

This will perform a static analysis of the ``main`` function and output the report
in out.txt. If the C files containing the function definitions are located elsewhere,
this can be specified by replacing the ``*.c`` part by their actual location.

It is very likely that for some functions, for example functions from the
standard library, the static analysis tool will not be able to find a
corresponding definition. In order to circumvent this problem, you can
re-write an implementation of these functions and pass the corresponding
file to the script. This has already be done for several functions, so in
addition to the C code implementation of the cryptographic algorithms to be
analyzed, you can also pass the fake_lib_implem.c file to the stanalyzer.py script,
like this: ::

    stanalyzer.py analyze --function-names main --output out.txt SOME_FOLDER/fake_lib_implem.c *.c


Missing function definition prompt
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
If you forgot to pass C files containing the definition of some function to the script,
you will be prompted to do so during the analysis. However, this causes the analysis
to be started from scratch again.

Reading the generated report
----------------------------
When specifying the ``output`` command line parameter, the tool will generate
three files: a text file with the name specified by this command line parameter 
(``out.txt`` in the given example), a CSV file with an auto-generated name, and a
file called ``leaks.gv`` containing a call graph of the functions leading to potential
leakages, as well as the instructions causing the leakages.

Text file
^^^^^^^^^^^
The text file contains the list of all leaks reported by the tools, as well as
all the information explaining the reasons of these leaks. This is best
explained by an example : ::

    Leakage of sk{1}[] (reason: LeakageType.CondBranch) at:
    (crypto_kem_keypair) dme.c:660:7
        -> (parse_skey) dme.c:620:9
        -> dme.c:596:5 (p{0}[] secret)
        -> dme.c:596:5 (p{1}[] secret)
        -> dme.c:619:9 (skey.L3{1}[] secret)
            -> (fq_matrix_inverse) dme.c:171:11
            -> dme.c:139:5 (b{0}[] secret)
            -> dme.c:139:5 (b{1}[] secret)
            -> dme.c:150:7 (c{1}[] secret)
                -> (fq_inv) dme.c:55:3
                -> dme.c:51:9 (a{0}[] secret)
                -> dme.c:51:9 (a{1}[] secret)
    .
    Dependencies leading to a secret variable: 
        dme.c:659:3 (sk{1}[] secret)

- The notation for variables is ``variable_name{scope}[struct_type]``. This
  is mainly for debugging reasons. A negative scope corresponds to a global
  variable, 0 and 1 to function arguments (again, for technical reasons), and
  everything above 1 is a function-local variable. The struct type corresponds to
  the name of the structure for variables that do not have a primitive type
  such as `int`, `double`, etc. For structure members (such as ``skey.L3`` in
  this example), dot notation is used to denote the member field.

- The leakage can have three different reasons:
    - CondBranch (conditional branch): This corresponds to a branching condition
      that depends on sensitive variables (If condition, switch statement or loop)
    - PointerDeref (pointer dereference): This corresponds to dereferencing a
      pointer at an address that depends on sensitive variables (for instance,
      indexing into an array where the index depends on sensitive variables)
    - Other: Sometimes, it is not possible to determine the behavior of the
      functions being called. This happens, mainly, for functions pointers that
      are passed as arguments to other functions. In this case, the analysis
      pessimistically supposes that all arguments passed to that function leak.

- The next block explains why the leakage happens, from the top-level function
  call (here ``parse_skey``), to the function that causes the leakage (here
  ``fq_inv``). The intermediate lines explain the dependencies between variables
  that cause the final leaking variable (here, ``a`` in ``fq_inv``), and the 
  variable that was originally considered sensitive (here, ``sk`` in
  ``crypto_kem_keypair``), as well as the intermediate function calls.


CSV file
^^^^^^^^
This file contains a summary of the information contained in the text file, 
in a format to be exploited by the companion JupyterLab script (tutorial
on this script to come !).

Advanced options
----------------

Other #pragma directives
^^^^^^^^^^^^^^^^^^^^^^^^
In addition to the ``secret`` directive, other pragma directives can be added
in the source code to be analyzed to modify the behavior of the analysis tool.
All of these are always prefixed with ``STA``, as shown in the source code example
(line ``#pragma STA secret s``)

:secret: Probably the most important pragma directive. It is followed by one or
         more variable that are considered sensitives for the rest of the analysis.

:safe: This is the reverse pragma of the ``secret`` directive. It is followed by
       a variable or a list of variables that are considered safe, i.e. not sensitive,
       for the rest of the analysis (to reduce the number of false positives, for example).

:safe_branch: This pragma should be declared just before a branching instruction. 
              Any warning caused by this branch will be suppressed.

:independent_branch: By default, every operation inside a branch adds dependencies
                     between the variables being modified and the variables that
                     influence the branching operation (for instance, in the code::

                        int a = 1;
                        int b = 0;
                        if (a > 0) b = -1;

                     the value of the variable ``b`` depends on the variable ``a``). In some cases,
                     this might lead to numerous false positives, and this behavior can be suppressed
                     with the ``independent_branch`` pragma, that is used in a similar fashion as the
                     ``safe_branch`` pragma.

:import: This pragma directive is useful if you wish to analyze code that uses function pointers, or
         if you do not want to specify too many source code files as command line arguments to
         the stanalyzer.py script. There are two ways to use this pragma:
         
         - Specify only a filename and a function file. In this case, the definition
           of the specified function will be imported from the specified source file
           and you do not need to pass the source file as a command line argument to
           the stanalyzer.py script. For example,

           .. code-block:: C

               #pragma STA import somefile.c foo

           will import the definition of the function ``foo`` from the file ``somefile.c``. Thus, 
           the behavior, in terms of leakages, of a call to ``foo`` can be correctly analyzed.

         - By also specifying the name of the function, or function pointer, that the definition
           needs to be bound to. For instance, suppose that your program defines a structure which
           contains a member named ``foo``, which is a function pointer. If this structure
           is used as an argument to some function, the analysis script will not be able to determine
           the behavior of the function that will be bound to this structure member, since every
           function is analyzed independently. You can use an ``import`` pragma to tell the
           script where to look for the definition of the function that will be bound to this member: 

           .. code-block:: C

               void f(struct baz bar*){
                   #pragma STA import somefile.c somefunction bar.foo
                   bar->foo(...);
               }

:array_declaration: Used in conjunction with the ``--strict-pointers`` command line option
                    (see below), it allows to fine-tune the behavior applied to pointers. When
                    the option  ``--strict-pointers`` is present, pointers will not be considered
                    as arrays. Thus, if some pointer is in fact used as an array, it is possibly
                    that the dependency set for this array be underestimated. This behavior can
                    be corrected if this pragma is used prior to the array declaration, followed
                    by the name (or names) of the variable(s) to be considered as arrays,
                    for example:

                    .. code-block:: C

                      void foo() {
                        #pragma STA array_declaration bar
                        int* bar = malloc(sizeof(int) * 10);
                        //...
                      }



Advanced command-line arguments
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
The ``stanalyzer.py`` script can take several other command-line arguments. These
are commented and can be displayed by executing ::
    
    stanalyzer.py analyze -h

However, some explanation for the most useful arguments might be useful.

--ignored-variables  You can list variables that should be excluded from the analysis.
                     As such, they will never be considered sensitive. There are three
                     ways to specify the variables to be excluded:
                    
                     - By just specifying the name. Every variable with this name will
                       be excluded from the analysis.
                     - By specifying the function name and the variable name, using the 
                       syntax ``function_name:variable_name``. Only the variables in the
                       specified function and with the specified name will be excluded.
                     - By also specifying the file name and function name, using the syntax
                       ``file_name:function_name:variable_name``. This will only exclude
                       the variables in the specified file and function, with the 
                       specified variable name.
                     - You can use ``*`` as a joker if you wish to exclude variables from
                       all functions in a given file, or all variable from a given file 
                       and / or function. Partial matching of variable names, functions or
                       files is **not** supported.
--cond-move  If this option is present, we suppose that ternary operations are
             implemented as conditional move operations by the compiler and do,
             therefore, not trigger a leakage.
--tags  If you are not sure were the functions that are called by you program,
        are defined, you can use `ctags` to generate a definition file for your
        project. If you pass it to the Python script using the ``tags`` command
        line argument, it will automatically look up the function definitions
        and you don't have to manually pass the corresponding file names via
        the command line options.

        .. warning::

		For the time being, only functions are being imported. Global variables,
		for instance, are being ignored, and this might cause issues with the analysis.
		The solution is to manually specify the .c files containing the declaration of
		the global variables as command line arguments for the stanalyzer.py script.

--strict-pointers  If this option is present, all pointers are considered as pointers
                   and not arrays. This distinction is important: because this
                   static analysis tool does *not* try to estimate the value
                   of any variable, it is not possible to determine which
                   index is accessed by an array access operation (except in
                   trivial cases, but even these are ignored for the sake
                   of simplicity). Therefore, all elements of an array share
                   the same dependencies. When applying the same reasoning to
                   pointers, the dependency set might be unnecessarily 
                   overestimated. When this option is present, this "dependency
                   accumulation" will not be enabled for pointers, but only
                   for explicitly declared arrays.
