import os
from subprocess import run, PIPE

directory = "/home/alexander/Documents/Télécom/Thèse/sca-plugin/cache_eval/static_analysis/PQCR2/"
util_directory = "/home/alexander/Documents/Télécom/Thèse/sca-plugin/cache_eval/static_analysis/stanalysis/utils/"
lib_keccak = "/home/alexander/Documents/Télécom/Thèse/sca-plugin/cache_eval/static_analysis/libkeccak.a.headers"
script_src = "/home/alexander/Documents/Télécom/Thèse/sca-plugin/cache_eval/static_analysis/stanalysis/stanalyzer.py"

fake_libc_implem = os.path.join(util_directory, "fake_lib_implem.c")
fake_libc_include = os.path.join(util_directory, "fake_libc_include")
functions_xml = os.path.join(util_directory, "functions.xml")
rng_file = os.path.join(directory, "rng.c")


extra_options = {
    "RQC/Reference_Implementation/rqc128": {
        "make_target": "rqc128"
    },
    "ntruprime/Reference_Implementation/kem/sntrup653": {
        "extra_files": ["kem.c"]
    },
    "ntruprime/Reference_Implementation/kem/sntrup761": {
        "extra_files": ["kem.c"]
    },
    "ntruprime/Reference_Implementation/kem/sntrup857": {
        "extra_files": ["kem.c"]
    },
    "ntruprime/Reference_Implementation/kem/ntrulpr653": {
        "extra_files": ["kem.c"]
    },
    "ntruprime/Reference_Implementation/kem/ntrulpr761": {
        "extra_files": ["kem.c"]
    },
    "ntruprime/Reference_Implementation/kem/ntrulpr857": {
        "extra_files": ["kem.c"]
    },
    "FrodoKEM/Reference_Implementation/reference/FrodoKEM-640": {
        "extra_files": ["tests/PQCtestKAT_kem.c", "frodo640.c"]
    },
    "FrodoKEM/Reference_Implementation/reference/FrodoKEM-976": {
        "extra_files": ["tests/PQCtestKAT_kem.c", "frodo976.c"]
    },
    "FrodoKEM/Reference_Implementation/reference/FrodoKEM-1344": {
        "extra_files": ["tests/PQCtestKAT_kem.c", "frodo1344.c"]
    },
    "LEDAcrypt/Reference_Implementation/KEM-LT": {
        "make_target": "SL=1 DFR_SL_LEVEL=0"
    },
    "LEDAcrypt/Reference_Implementation/KEM": {
        "make_target": "SL=1 N0=2"
    },
    "LEDAcrypt/Reference_Implementation/PKC": {
        "make_target": "SL=1 DFR_SL_LEVEL=0"
    },
    "NTSKEM/Reference_Implementation/kem/nts_kem_13_80": {
        "extra_files": ["nist/aes_drbg.c"]
    },
    "NTSKEM/Reference_Implementation/kem/nts_kem_13_136": {
        "extra_files": ["nist/aes_drbg.c"]
    },
    "NTSKEM/Reference_Implementation/kem/nts_kem_12_64": {
        "extra_files": ["nist/aes_drbg.c"]
    },
    "Falcon-Round2/Reference_Implementation/falcon512": {
        "extra_files": ["falcon-sign.c"]
    },
    "Falcon-Round2/Reference_Implementation/falcon768": {
        "extra_files": ["falcon-sign.c"]
    },
    "Falcon-Round2/Reference_Implementation/falcon1024": {
        "extra_files": ["falcon-sign.c"]
    },
    "rainbow/Reference_Implementation": {
        "extra_files": ["Ia_Classic/*.c"]
    }

}

for submission_type in "kem", "encrypt":
    local_directory = os.path.join("Round5_submission", "Reference_Implementation", submission_type)
    for name in os.listdir(os.path.join(directory, local_directory)):
        extra_options[os.path.join(local_directory, name)] = {
            "extra_files": ["drbg.c"]
        }

local_directory = os.path.join("mceliece", "Reference_Implementation", "kem")
for name in os.listdir(os.path.join(directory, local_directory)):
    extra_options[os.path.join(local_directory, name)] = {"extra_files": ["operations.c"]}

local_directory = os.path.join("ThreeBears", "Reference_Implementation", "crypto_kem")
for name in os.listdir(os.path.join(directory, local_directory)):
    extra_options[os.path.join(local_directory, name)] = {"extra_files": ["threebears.c", "ring.c"]}

skips = ["RQC", "ROLLO", "nist-hqc", "GeMSS-Round2", "BIKE-Round2"]
# Reasons:
# - RQC, ROLLO, HQC,GeMSS-Round2: uses C++
# - BIKE: not ANSI C, uses anonymous unions (C11 feature) which make the analysis much harder
# - rainbow: do later, directory structure confusing <- done, seems to be almost CT (some checks still to do).
# there were functions missings in the provided source code.

already_done = ["LUOV-master", "LUOV-master-2.1", "ntruprime", "NTRU", "Round5_submission", "qTesla",
                "FrodoKEM", "MQDSS", "NewHope", "LEDAcrypt",
                "CRYSTALS-Kyber-Round2", "picnic", "mceliece", "Dilithium", "NTSKEM", "qTesla-v2",
                "LAC", "LAC-v3a", "SPHINCS", "ThreeBears", "SABER_KEM_(Round_2)", "rainbow", "Falcon-Round2",
                "falcon-20190918"]


def find_files(path: str, filename: str = "Makefile"):
    if filename in os.listdir(path):
        yield os.path.abspath(path)
    else:
        for fn in os.listdir(path):
            fn = os.path.join(path, fn)
            if os.path.isdir(fn) and not os.path.islink(fn):
                yield from find_files(fn, filename)


for submission in os.listdir(directory):
    abs_path = os.path.join(directory, submission)
    if not os.path.isdir(abs_path):
        continue
    if submission in skips + already_done:
        continue

    print(submission)
    base_path = os.path.join(abs_path, "Reference_Implementation")
    os.chdir(base_path)

    # Find all subdirectories that contain a Makefile
    directories = find_files(".", "Makefile")
    if not directories:
        exit("No Makefile found for %s !" % submission)

    for dir in directories:
        os.chdir(dir)
        rel_path = os.path.relpath(".", directory)

        # Execute the Makefile to determine which include directories / #defines are needed
        cmd = "make -n {target}".format(target=extra_options.get(rel_path, {}).get("make_target", ""))
        print(rel_path)
        make_run = run(cmd, shell=True, stdout=PIPE).stdout.decode("utf-8")\
            .replace(" -D ", " -D")\
            .replace(" -I ", " -I")

        flags = {token for token in make_run.split() if token.startswith(("-I", "-D"))}

        # Create the config file
        config_content = "-E -nostdinc -I. -I%s %s -DKAT -DKATNUM=4  -Dalignas(x)= -DNIST_randombytes=randombytes -DNIST_randombytes_init=randombytes_init" % (fake_libc_include, ' '.join(flags))
        with open(".stanalyzer-config", "w") as f:
            f.write(config_content)

        # Copy the function definition file
        with open(functions_xml, "r") as f1:
            with open("functions.xml", "w") as f2:
                f2.write(f1.read())

        # Add a symlink to the libkeccak directory
        if not "libkeccak.a.headers" in os.listdir("."):
            os.symlink(lib_keccak, "libkeccak.a.headers", target_is_directory=True)

        # Modify all rng.c / randombytes.c files to replace them with an annotated versions
        if submission not in ["LEDAcrypt", "NTSKEM"]:
            with open(rng_file) as f:
                rng_content = f.read()

            for filename in "rng.c", "randombytes.c":
                if submission in ["Dilithium"] and filename == "randombytes.c":
                    continue
                for dir_name in find_files(".", filename):
                    fn = os.path.join(dir_name, filename)
                    with open(fn, "w") as f:
                        f.write(rng_content)

        # Generate a ctags file
        run("ctags -R --languages=C --c-kinds=fs *", shell=True)

        # Find the function name and file to analyze
        from stanalysis.common import TagsHandler
        th = TagsHandler("tags")
        filenames = th.get_function_files("main")
        filename = ""
        if not filenames:
            # No main function found !
            exit("No main found for %s !" % submission)
        elif len(filenames) > 1:
            refined_candidates = [fn for fn in filenames if os.path.basename(fn).startswith("PQCgen")]
            if len(refined_candidates) == 1:
                filename = refined_candidates[0]

        res = run("{script} analyze {fake_implem} {filename}  {extra_filenames} --tags tags --out out.txt "
                  "--function-names main --ignored-variables pk mlen len logn"
                  .format(script=script_src, fake_implem=fake_libc_implem, filename=filename,
                          extra_filenames=' '.join(extra_options.get(rel_path, {}).get("extra_files", []))
                          ),
                  shell=True)
        if res.returncode != 0:
            exit("Failed submission %s" % submission)

        os.chdir(base_path)
    os.chdir(directory)





