import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="stanalysis",
    version="0.0.11",
    author="Alexander Schaub",
    author_email="alexander-schaub@telecom-paristech.fr",
    description="A package for performing static detection of cache-timing leaks in C-code.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/pypa/sampleproject",
    scripts=["stanalyzer.py", "timeres.py"],
    install_requires=[
        "pycparserext>=2019.1",
        "argcomplete>=1.9.2",
        "python-ctags3",
        "pyelftools"
    ],
    extras_require={
        "notebook": ["PyQt5>=5.10.1", "ipython>=6.2.1", "qgrid>=1.0.1", "pandas>=0.22.0", "widgetsnbextension>=3.1.0"],
    },
    include_package_data=True,
    packages=setuptools.find_packages(exclude=['tests', '*.tests', '*.tests.*']),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
