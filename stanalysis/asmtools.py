from elftools.elf.elffile import ELFFile
from elftools.elf.sections import SymbolTableSection


def get_functions(filename):
    """Returns the list of functions declared in the ELF-file `filename`"""
    res = []
    with open(filename, "rb") as f:
        elf = ELFFile(f)
        for section in elf.iter_sections():
            if isinstance(section, SymbolTableSection):
                for symbol in section.iter_symbols():
                    if symbol["st_info"].get("type", None) == "STT_FUNC":
                        res.append(symbol.name)
    return res

