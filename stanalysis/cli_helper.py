import html
from collections import OrderedDict
from typing import Iterable, List, Dict, Optional
import os

from pycparser import CParser
from pycparser.c_ast import NodeVisitor, Node, ID, StructRef
from pycparser.c_generator import CGenerator

from .common import OrderedSet, get_codeline, Variable
from .leakageanalysis import LeakageAnalysis
from .scopes import LeakageTrace, LeakageType, LeakageCoord


class ColoredCGenerator(CGenerator):
    """Generates C code from AST and colors a given set of variables."""

    def __init__(self, variables: Iterable[str]):
        super().__init__()
        self.variables = variables
        self.should_color = True

    def visit_ID(self, n: ID):
        return self.color(n.name) if n.name in self.variables and self.should_color else n.name

    def visit_StructRef(self, n: StructRef):
        should_color_after = self.should_color
        self.should_color = False
        full_name = super().visit_StructRef(n)
        self.should_color = should_color_after

        return self.color(full_name) if full_name.replace("->", ".") in self.variables and self.should_color \
            else full_name

    def color(self, value: str):
        return "{FONT color='red'}%s{/FONT}" % value


class ExtractLineVisitor(NodeVisitor):
    """Return the first node with the correct filename and line number."""

    def __init__(self, filename: str, line_no: int):
        super().__init__()
        self.coord = "%s:%d:" % (filename, line_no)

    def visit(self, node: Node):
        if hasattr(node, "coord") and str(node.coord).startswith(self.coord):
            return node

        method = 'visit_' + node.__class__.__name__

        return getattr(self, method, self.generic_visit)(node)

    def generic_visit(self, node: Node):
        # ~ print('generic:', type(node))
        if node is None:
            return None
        else:
            for c_name, c in node.children():
                res = self.visit(c)
                if res is not None:
                    return res
            return None


def get_colored_codeline(node: Node, filename: str, line_no: int, variable_names: Iterable[str]) -> str:
    if not node:
        return ""

    inst_node = ExtractLineVisitor(filename, line_no).visit(node)
    colored_str = ColoredCGenerator(variable_names).visit(inst_node)
    return colored_str.split("\n")[0]


# Here an example on how to subclass LeakageAnalysis to change the behaviour concerning
# emitted leakage warnings.
class SilentLeakageAnalysis(LeakageAnalysis):
    leakages = {}

    def __init__(self, parser=CParser()):
        super(SilentLeakageAnalysis, self).__init__(parser)
        self.leakages = OrderedSet([])
        self.leakagescoord = set()
        self.leakages_df = []
        self.verbose = False
        self.count = 1
        self.structured_leakages = []

    def on_leakage(self, variable: Variable, leakage_type: LeakageType, leakage_coord: LeakageCoord,
                   dependency_path: List[str]):
        if leakage_coord.to_str() not in self.leakagescoord:
            self.leakagescoord.add(leakage_coord.to_str())

            text = self.get_leakage_text(variable, leakage_type, leakage_coord, dependency_path)
            self.leakages.add(text)

            secret = ""
            for l in self.tagged_variables['secret']:
                secret = secret + l[0] + " [" + l[-1] + "],"

            # Parse the leakage traces
            lt = LeakageTrace(leakage_coord, dependency_path)
            lt.type = leakage_type
            self.structured_leakages.append(lt)

            # Get the codeline
            filename, line, _ = lt.trace[-1].inst_coord.split(":")
            code = get_codeline(filename, int(line))
            leaking_variable_dependency = lt.trace[-1].dependency_chain

            # Mainly when the leakage type is Other, there is no leaking variable
            # at the last level, because we could not determine the code being
            # executed. Thus, we mark the leaking variable as unknown.
            if not leaking_variable_dependency or leakage_type == LeakageType.Other:
                leaking_variable = "?"
            else:
                leaking_variable = leaking_variable_dependency[-1][1]

            # We already have the remaining information
            row = orderd_dict_from_tuple_list(*[
                ('', int(self.count)),
                ('Function', lt.trace[0].name),
                ('Reference', lt.trace[-1].inst_coord),
                ('Code', code),
                ('Leaking variable', leaking_variable),
                ('Leakage Type', str(leakage_type.name)),
                ('Top Variable', lt.trace[0].dependency_chain[0][1] if lt.trace[0].dependency_chain else "<None>"),
                ('Root Reference', lt.trace[0].inst_coord.rsplit(",", 1)[0]),
                ('Dependency Chain', lt.get_dependency_chain()),
                ('Max Depth', len(lt.trace) - 1),
                ('Secret', secret),
            ])

            # Save to list for later exploitation
            if row not in self.leakages_df:
                self.leakages_df.append(row)
                self.count = self.count + 1

    def get_leakage_dot_file(self):
        """Return the dot file representing the leakage graph."""
        res = """strict digraph leak {
            size="8,4"; ratio=fill;
            graph[rankdir=UD, center=true, margin=0.2, nodesep=0.1, ranksep=0.3]
            node[fontname="Courier-Bold", fontsize=10]
            edge[arrowsize=0.6, arrowhead=vee]       
        """
        inst_nodes = {}

        # This dictionary maps tuples of the form (filename, line_no) to the set of variable names
        # that are leaking and need to be highlighted
        leakage_edges = set()
        for leak in self.structured_leakages:
            for (i, f_call) in enumerate(leak.trace[:-1]):
                # For the first elements, just trace an edge between the function of f_call and the next one
                res += "\t\"{caller}\" -> \"{callee}\" [label=\"{call_coord}\", edgetooltip=\"{dependency_chain}\"];\n".format(
                    caller=f_call.name,
                    callee=leak.trace[i + 1].name,
                    call_coord=f_call.inst_coord,
                    dependency_chain=" \\n ".join(f_call.get_dependency_chain()))

            # Last element: edge to the code line that causes the leakage
            f_call = leak.trace[-1]
            file_name, line, _ = f_call.inst_coord.split(":")
            line = int(line)

            inst_nodes.setdefault(f_call.name, {})

            inst_nodes[f_call.name].setdefault((file_name, line), set())
            if f_call.get_dependency_chain():
                inst_nodes[f_call.name][(file_name, line)].add(
                    f_call.get_dependency_chain()[-1].replace("&", "").replace("*", ""))

            res += "\t\"{caller}\" -> \"{caller}___node\" [label=\"{dependency_chain}\"];\n".format(
                caller=f_call.name,
                dependency_chain=" \\n ".join(f_call.get_dependency_chain()))

        table_style = "font-family:monospace;padding-left:none;border:0px;"
        # Now, handle the function-name___node nodes and add the code in them
        for (fun_name, inst_list) in inst_nodes.items():
            # We first prune the instruction list: there may be duplicates, and we might want to
            # order them by increasing line number
            node_text = ""

            for (filename, line_no), variable_names in sorted(inst_list.items()):
                leaking_inst = html.escape(get_colored_codeline(self.code_analysis.func_defs.get(fun_name, None),
                                                                filename, line_no, variable_names) or
                                           get_codeline(filename, line_no),
                                           quote=False)
                leaking_inst = leaking_inst.replace("{FONT color='red'}", "<FONT color='red'>")
                leaking_inst = leaking_inst.replace("{/FONT}", "</FONT>")

                cell_content = "<FONT FACE=\"monospace\">%s</FONT>" % leaking_inst if leaking_inst else ""
                node_text += "<TR><TD>%s</TD><TD ALIGN=\"LEFT\">%s </TD></TR>" % (line_no, cell_content)
            res += "\"{fun_name}___node\" [label = <<TABLE CELLSPACING=\"0\">{node_text}</TABLE>>, shape=rectangle];\n".format(
                fun_name=fun_name, node_text=node_text, table_style=table_style)
        res += "}"
        return res


def write_leakages_to_csv(leakages: List[Dict], filename: str, **fmtargs):
    import csv

    with open(filename, 'w', newline='') as csvfile:
        if leakages:
            fieldnames = list(leakages[0].keys())
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames, **fmtargs)
            writer.writeheader()
            for leakage in leakages:
                writer.writerow(leakage)


def orderd_dict_from_tuple_list(*tuple_list):
    # Creates an ordered dictionary from a list of (key, value) tuples
    res = OrderedDict()
    for t in tuple_list:
        res[t[0]] = t[1]
    return res


def generate_function_call_graph(d: Dict[str, Dict[str, str]]):
    # d is a dict of the form caller_function: {coords: callee_function}.
    # This functions transforms this into a graphviz file
    res = """strict digraph f_call_graph {
                size="8,4"; ratio=fill;
                graph[rankdir=UD, center=true, margin=0.2, nodesep=0.1, ranksep=0.3]
                node[fontname="Courier-Bold", fontsize=10]
                edge[arrowsize=0.6, arrowhead=vee]       
            """
    for caller_function in d:
        for coord, callee_function in d[caller_function].items():
            res += """ \t"%s" -> "%s" [label="%s"];\n """ % (caller_function, callee_function, coord)
    res += "}"

    return res


def get_cpp_args_for(filename: str, cpp_command: Optional[List[str]],
                     compilation_database: Optional[Dict[str, List[str]]]) -> Optional[List[str]]:
    # Return the command to replace the default command with, if any.
    # If the cmd line argument cpp_command is specified, or if a compilation database
    # file containing an entry for `filename` is given, then this replaces the default
    # command. An entry in the compilation database overrides the cpp_command argument.
    # If neither is present, returns None. Else, returns the new base command as a list
    # of arguments
    if compilation_database is not None:
        filename_abs_path = os.path.abspath(filename)
        res = compilation_database.get(filename_abs_path)
        if res is not None:
            return res
    # If not compilation database or no entry for file: use the cpp_command if specified
    if cpp_command is not None:
        return cpp_command + [filename]

    return None
