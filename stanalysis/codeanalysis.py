import os
import shlex
from itertools import chain
from typing import List, Dict, Set, Optional

from pycparser.c_ast import *
from pycparser import CParser
from pycparserext.ext_c_parser import GnuCParser

from .common_analysis import get_ast, UnknownFunctionDefError
from .common import TagsHandler, Variable, VariableType, ControlDependenciesHandling
from .dependencygraph import DependencyGraph
from .leakageanalysis import LeakageAnalysis

from .cli_helper import get_cpp_args_for


def parse_variable_pattern_list(pattern_list: List[str]) -> Dict[str, Dict[str, Set[str]]]:
    """
    A variable pattern list is a list of strings formatted according to
    one of these formats:
     - variable_name
     - func_name:variable_name
     - file_name:func_name:variable_name
    The `file_name` or `func_name` part can be replaced by a wildcard `*` which means "any function"
    or "any file", depending on the format. Thus, for instance, variable_name is shorthand for
    *:*:variable_name.

    The variable_name part is of the following format:
       fieldNameOrStructTypeOrVariableName.fieldNameOrStructType. ... fieldNameOrStructType

    A struct type is given between brackets ([]).

    This list is transformed into a dictionary of the format
    {
        file_name => {
                        func_name => {
                                        {variable_name, variable_name, ...}
                                     }
                     }
    }
    Note that file_name or func_name can be equal to "*".
    """
    res = {}  # type: Dict[str, Dict[str, Set[str]]]

    for v in pattern_list:
        count = v.count(":")
        var_name = "*:" * (2 - count) + v  # add jokers if only variable or filename + variable

        (filename, func_name, var_name) = var_name.split(":", 3)

        res.setdefault(filename, {})
        file_dict = res[filename]

        file_dict.setdefault(func_name, set())
        func_set = file_dict[func_name]

        func_set.add(var_name)

    return res


class CodeAnalysis(object):
    """
    This class provides high-level functionality for :obj:`LeakageAnalysis`.
    """

    def __init__(self, parser: CParser = GnuCParser(), verbosity: int = 1,
                 functions_file: str = "functions.xml", save_funcs: bool = False,
                 ignored_variables: Optional[List[str]] = None,
                 sensitive_variables: Optional[List[str]] = None, la_class=LeakageAnalysis,
                 assume_cond_move: bool = False, report_spectre: bool = True, tags_file: str = "",
                 strict_pointers: bool = False,
                 cpp_command: Optional[str] = None,
                 cpp_extra_args: str = "",
                 compilation_database: Optional[Dict[str, List[str]]] = None,
                 control_dependencies=ControlDependenciesHandling.FULL
                 ):
        """
        Create a :obj:`CodeAnalysis` object, with the following options:
        Args:
            functions_file (:obj:`str`): Name of the file containing all known
                function definitions.
            save_funcs (:obj:`bool`): If true, parsed functions are exported
                into `filename` after analysis is complete.
            ignored_variables (:obj:`List[str]`): List of variables to ignore, in the format
                `file_name:function_name:variable_name`. The star (*) can be used as joker for the file
                name or function name.
            sensitive_variables (:obj:`List[str]`): List of variables to always consider as
                sensitive, in the format `file_name:function_name:variable_name`. The star (*) can
                be used as joker for the file name or function name.
            la_class (:obj:`LeakageAnalysis`): The leakage analyser to use. :obj:`LeakageAnalysis`
                can be subclassed if needed. Only the constructor and `on_leakage` should need
                a re-definition in general.
            assume_cond_move (bool): If set to `True`, ternary expression (``cond ? exp_a : exp_b``)
                are assumed to be implemented with conditional move expressions, and thus do not
                leak. Set to `False` in order to report leaks involving ternary expressions the same
                way as regular `if` statements (default: `False`).
            report_spectre (bool): When set to `True`, Spectre type vulnerabilities will also be
                reported (experimental, requires tagging variables controlled by the attacker
                as user input) (default: `True`).
            tags_file (str): The filename of the ctags-generated file to look for the file names
                of unknown functions (default: "").
            strict_pointers (bool): If set to True, all pointers are considered "true" pointers by
                default, as opposed to pointer notation being used for arrays. In this case, the
                pragma directive ``STA array_declaration``, followed by one ore more variable names,
                used just before these variables are declared, allows a more fine-grained control.
                These variables will then be considered arrays and not pointers. (default: `False`).
            cpp_command (str or None): overwrites the default cpp command
            cpp_extra_args (str): appends additional arguments to the cpp command
            compilation_database (:obj:`Dict[str,str]` or None): overwrites cpp commands with those
                specified by the corresponding compilation database file (usually named compile_commands.json)
            control_dependencies: Determines how control dependencies are handled: full (FULL, by default), only on safe
                branches (LESS), or never (NONE)
        """
        self.parser = parser
        self.verbosity = verbosity
        self.functions_file = functions_file
        self.save_funcs = save_funcs

        self.compilation_database = compilation_database
        self.cpp_extra_args = shlex.split(cpp_extra_args)
        self.cpp_command = None if cpp_command is None else shlex.split(cpp_command)

        # parse the ignored variables into a usable structure
        self.ignored_variables = parse_variable_pattern_list(ignored_variables or [])
        self.sensitive_variables = parse_variable_pattern_list(sensitive_variables or [])

        self.control_dependencies = control_dependencies

        if strict_pointers:
            DependencyGraph.set_strict_pointers()
        self.la_class = la_class
        self.assume_cond_move = assume_cond_move
        self.report_spectre = report_spectre
        self.tagsHandler = None
        if tags_file != "":
            if os.path.exists(tags_file):
                self.tagsHandler = TagsHandler(tags_file)
            else:
                raise ValueError("Tags file " + tags_file + " doesn't exist")

        self.func_defs = {}
        self.analyzed_files = []
        self.la = None

    def prepare_leakage_object(self):
        la = self.la_class(self.parser)  # type: LeakageAnalysis

        if self.verbosity == 0:  # Default is True. Later we can use finer verbosity config
            la.set_option("verbose", False)
            print("Verbose mode OFF")

        # TODO la does not need to know about this
        la.set_option("functions_file", self.functions_file)
        la.set_option("cond_move", self.assume_cond_move)
        la.set_option("spectre_vulnerability", self.report_spectre)

        la.set_option("ignored_variables", self.ignored_variables)
        la.set_option("sensitive_variables", self.sensitive_variables)

        if self.functions_file:
            la.import_functions(self.functions_file)

        la.code_analysis = self
        self.la = la

        return la

    def _get_cpp_args_for(self, filename: str) -> Optional[List[str]]:
        return get_cpp_args_for(filename, cpp_command=self.cpp_command, compilation_database=self.compilation_database)


    def import_global_scope(self, nodes: List[Node]):
        # Import globaly defined symbols, as well as pragmas

        with self.la.state.saved_scopes():
            for decl in nodes:
                if isinstance(decl, (Decl, Typedef, Pragma)):
                    # Handles global variables, typedefs, structs and pragmas
                    self.la.depends_on(decl)
                # Import function prototypes
                if isinstance(decl, FuncDef):
                    self.la.state.add_var(Variable.new(decl.decl.name)._replace(var_type=VariableType.FUNCTION_PTR,
                                                                                scope=-1))


    def import_file(self, filename: str) -> bool:
        # Add the func_defs to the known list
        if filename in self.analyzed_files:
            return False

        ast = get_ast(filename, cpp_extra_args=self.cpp_extra_args, override_cpp_args=self._get_cpp_args_for(filename))

        self.analyzed_files.append(filename)

        for node in ast.ext:
            if isinstance(node, FuncDef) and node.decl.name not in self.func_defs:
                self.func_defs[node.decl.name] = node

        self.import_global_scope(ast.ext)

        return True

    def get_func_definition(self, func_name: str, filename: str = ""):
        if func_name in self.func_defs:
            return self.func_defs[func_name]

        if filename:
            if not self.import_file(filename):
                raise UnknownFunctionDefError(func_name)

            if func_name in self.func_defs:
                return self.func_defs[func_name]
            else:
                raise UnknownFunctionDefError(func_name)
        else:
            # Get the filename with the TagsHandler
            if not self.tagsHandler:
                raise UnknownFunctionDefError(func_name)
            filenames = self.tagsHandler.get_function_files(func_name)

            if len(filenames) > 1:
                raise ValueError("Multiple matches for functions %s, please run again by adding one of the "
                                 "following source files to parse: \n - %s" % (func_name, "\n - ".join(filenames)))
            if filenames:
                return self.get_func_definition(func_name, filenames[0])
            else:
                raise UnknownFunctionDefError(func_name)

    def analyze(self, filenames, def_name=None):
        """
        Analyze the function(s) `def_name` in the file(s) `filename`. Warnings are issued when
        sensitive variables are leaked or unexpected behaviour is encountered. If `filename`
        (resp `def_name`) is a list or a set, analyze all the filenames (resp. functions).

        Args:
            filenames (:obj:`Union[str, List[str]]`): Name(s) of the file(s) to analyze.
            def_name (:obj:`Union[str, List[str]]`): If set, only the functions with their name in
                this list might be parsed (as well as the functions they call).

        Returns:
            :obj:`LeakageAnalysis`: The object used for leakage analysis.
        """
        if isinstance(filenames, str):
            filenames = [filenames]
        elif isinstance(filenames, set):
            filenames = list(filenames)

        if def_name is None:
            def_name = []
        elif isinstance(def_name, str):
            def_name = [def_name]

        la = self.prepare_leakage_object()

        # TODO LeakageAnalysis does not need to know about this
        la.set_option("filenames", filenames)

        ast_nodes = list(chain.from_iterable([
            get_ast(f, cpp_extra_args=self.cpp_extra_args, override_cpp_args=self._get_cpp_args_for(f)).ext
            for f in filenames]))

        func_defs = [d for d in ast_nodes if isinstance(d, FuncDef)]
        la.func_defs = func_defs

        for f in func_defs:
            self.func_defs[f.decl.name] = f

        # First, import all global variables and structs
        self.analyzed_files.extend(filenames)
        self.import_global_scope(ast_nodes)


        # Then, import all functions
        if def_name:
            for name in def_name:
                la.depends_on(self.get_func_definition(name))
        else:
            for f_def in func_defs:
                # print("Trying to parse definition of " + f_def.decl.name)
                la.depends_on(f_def)
                # print("Finished parsing definition of " + f_def.decl.name)

        # if "__warningregistry__" in globals():
        #     __warningregistry__.clear()

        if self.save_funcs:
            la.export_functions(self.functions_file)

        return la
