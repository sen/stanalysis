# ---------------------------------------------------------------------
# cache_eval: common.py
#
# Defines common structured used during leakage analysis, most
# importantly the namedtuple Variable representing a C variable.
#
# Alexander Schaub
# License: BSD
# ---------------------------------------------------------------------
"""
common
======
The module :mod:`common` contains structures used throughout the analysis script.
The main class is :class:`VariableSet`, but this module also contains other useful classes
and functions.
"""

from collections import namedtuple, OrderedDict
from collections.abc import MutableSet
import enum
import re
import os
import readline
import warnings
from xml.dom.minidom import getDOMImplementation
from typing import List, Union, Iterable, Set, Dict, Tuple, Optional

from pycparser.plyparser import Coord


class LeakageWarning(UserWarning):
    pass


def get_codeline(filename: str, lineno: int) -> str:
    fn = os.path.join(os.getcwd(), filename) if os.path.dirname(filename) == "" else filename
    with open(fn, "r") as f:
        for i, line in enumerate(f):
            if i + 1 == lineno:
                return line.strip()


def convert_tag_name(name: str) -> str:
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()


class VariableType(enum.Enum):
    """Enumeration for the type a variable can be:

        * `SCALAR`: A simple variable.
        * `ARRAY`: An array of anything.
        * `PTR`: A pointer on anything.
        * `STRUCT`: A structure with known definition.
        * `OPAQUE_STRUCT`: A structure with unknown definition.
        * `FUNCTION_PTR`: A function pointer.
        * `ENUM_VALUE`: An enumeration item.
    """
    SCALAR = 1
    ARRAY = 2
    PTR = 3
    STRUCT = 4
    OPAQUE_STRUCT = 5
    FUNCTION_PTR = 6
    ENUM_VALUE = 7


class TypedefInfo(namedtuple("TypedefInfo", ["struct_type", "indirection", "var_type"])):
    """
    A tuple representing information about a typedef statement. It contains the bare
    information needed to do a typedef translation. As such, the fields in this
    tuple mirror those of :obj:`Variable`, but the fields from :obj:`Variable`
    that are not useful have been removed.

    Attributes:
        struct_type (:obj:`str`): The name of the structure being typedef'd, if any.
        indirection (:obj:`int`): The indirection level, 0 for a scalar, 1 for a pointer or array,
            2 for a double pointer (or a 2d-array), etc.
        var_type (:obj:`VariableType`): The type of the variable.
    """
    pass


class XmlExportMixin:
    """Useful mixin that makes importing/exporting objects as XML easier."""
    tag_to_class = {}

    @staticmethod
    def element_content(node) -> str:
        """Retrieves the value of given XML node parameter:
        Args:
            node: node object containing the tag element produced by minidom

        Returns:
            Content of the tag element as string
        """

        return ''.join([el.toxml() for el in node.childNodes])

    def asXml(self, fields_to_export=None):
        impl = getDOMImplementation()
        doc = impl.createDocument(None, convert_tag_name(self.__class__.__name__), None)

        def export_field(field, element):
            if isinstance(field, enum.Enum):
                field = str(field)
            if isinstance(field, str):
                element.appendChild(doc.createTextNode(field))
            elif isinstance(field, int):
                element.appendChild(doc.createTextNode(str(field)))
            elif hasattr(field, "asXml") and callable(getattr(field, "asXml")):
                element.appendChild(field.asXml())
            else:
                if isinstance(field, dict):
                    for key in field.keys():
                        entry = doc.createElement("entry")
                        of = doc.createElement("of")
                        export_field(key, of)
                        entry.appendChild(of)

                        values = doc.createElement("values")
                        export_field(field[key], values)
                        entry.appendChild(values)
                        element.appendChild(entry)
                elif isinstance(field, tuple):
                    el = doc.createElement("tuple")
                    for i, val in enumerate(field):
                        el.setAttribute("v%d" % i, str(val))
                    element.appendChild(el)
                else:  # we assume it is an iterable
                    for v in field:
                        export_field(v, element)

        if fields_to_export is None:
            if hasattr(self, "_fields"):
                fields_to_export = self._fields
            else:
                fields_to_export = []
        for f in fields_to_export:
            field = getattr(self, f)
            field_element = doc.createElement(f)
            export_field(field, field_element)
            doc.documentElement.appendChild(field_element)

        return doc.documentElement

    def compactXml(self):
        impl = getDOMImplementation()
        doc = impl.createDocument(None, "root", None)
        tuple_element = doc.createElement(self.__class__.__name__.lower())
        if hasattr(self, "_fields"):
            for f in self._fields:
                field = getattr(self, f)
                if isinstance(field, bool):
                    if field:
                        tuple_element.setAttribute(f, f)
                else:
                    if getattr(self, f):
                        tuple_element.setAttribute(f, str(getattr(self, f)))
        return tuple_element

    @classmethod
    def importXmlDict(cls, element, field_name, list_transform=lambda x: x, key_transform=None,
                      import_types=None):
        try:
            field_element = element.getElementsByTagName(field_name)[0]
        except IndexError:
            raise ValueError("No tag named %s found." % field_name)
        res = {}
        for entry in field_element.getElementsByTagName("entry"):
            try:
                of = entry.getElementsByTagName("of")[0]
                # values_element = entry.getElementsByTagName("values")[0]
                key_element = [n for n in of.childNodes if n.nodeName != "#text"][0]
                if key_transform:
                    key = key_transform(key_element)
                else:
                    key = cls.tag_to_class[key_element.nodeName].fromXml(key_element)
                if not import_types:
                    res[key] = list_transform(cls.importXmlList(entry, "values"))
                elif import_types[0] == cls.importXmlDict:
                    res[key] = cls.importXmlDict(entry, "values", list_transform, key_transform, import_types[1:])
                else:
                    res[key] = import_types[0](entry, "values")

                # for v in filter(lambda x: x.nodeName != "#text", values_element.childNodes):
                #    res[key].append(cls.tag_to_class[v.nodeName].fromXml(v))
                # res[key] = res[key])

            except IndexError:
                raise ValueError("Malformed exported dictionnary : %s" % entry.toprettyxml())
        return res

    @staticmethod
    def importXmlStringSplit(sep):
        def importXmlFun(element, field_name):
            return XmlExportMixin.importXmlString(element, field_name).split(sep)

        return importXmlFun

    @staticmethod
    def importXmlString(element, field_name):
        try:
            field_element = element.getElementsByTagName(field_name)[0]
        except IndexError:
            return ""
        if field_element.firstChild:
            return XmlExportMixin.element_content(field_element)
        else:
            return ""

    @classmethod
    def importXmlList(cls, element, field_name):
        res = []
        try:
            field_element = element.getElementsByTagName(field_name)[0]
        except IndexError:
            raise ValueError("No tag named %s" % field_name)

        for v in [x for x in field_element.childNodes if x.nodeName != "#text"]:
            res.append(cls.tag_to_class[v.nodeName].fromXml(v))

        return res

    @classmethod
    def importXmlStrtingList(cls, element, field_name):
        res = []
        try:
            field_element = element.getElementsByTagName(field_name)[0]
        except IndexError:
            raise ValueError("No tag named %s" % field_name)
        for v in [x for x in field_element.childNodes if x.nodeName != "#text"]:
            res.append(XmlExportMixin.element_content(v))
        return res

    @classmethod
    def importXmlCompactField(cls, element, field_name, default, modifier=lambda x: x):
        attribute = element.attributes.get(field_name)
        if attribute is not None:
            return modifier(attribute.value)
        return default

    @classmethod
    def enum_modifier(cls, enum_class):
        def modifier(x):
            enum_value = x.split(".")[-1]
            try:
                enum_type = getattr(enum_class, enum_value)
            except AttributeError:
                raise ValueError("Unknown enum type: %s" % x)
            return enum_type

        return modifier


VariableTupleClass = namedtuple("Variable", "name var_type scope indirection struct_type secret user_input")
VariableTupleClass.__doc__ = ""


class Variable(VariableTupleClass, XmlExportMixin):
    """
    A truly immutable class representing a variable encountered in the C code.

    Attributes:
        name (:obj:`str`): Variable name. The "."(dot) character is used for structure reference.
        var_type (:obj:`VariableType`): The type of the variable (scalar, pointer, matrix, etc.)
        scope (:obj:`int`): The scope depth where the variable was defined (-1 for global variables,
            0 for global function arguments, 1 and more for function-local variables).
        indirection (:obj:`int`): The indirection level, 0 for a scalar, 1 for a pointer or array,
            2 for a double pointer (or a 2d-array), etc.
        struct_type (:obj:`str`): The name of the structure for the variable, if any.
        secret (:obj:`bool`): True for secret dependencies. Note that only some special
            variables are defined as secret, for other variables, the dependency
            graph defines whether there are secret or not.
        user_input (:obj:`bool`): True for user supplied values. Same remarks as for `secret`.
    """
    __slots__ = ()

    def indirect(self, indirection_change: int) -> "Variable":
        return self._replace(indirection=self.indirection + indirection_change)

    def is_terminal(self) -> bool:
        return self.secret or self.indirection > 0 or self.user_input or self.scope < 0

    def is_global(self) -> bool:
        return self.scope < 0 and not self.secret and not self.user_input

    def is_global_function(self) -> bool:
        return self.scope < 0 and self.var_type == VariableType.FUNCTION_PTR

    def __str__(self) -> str:
        prefix = ("&" if self.indirection > 0 else "*") * abs(self.indirection)
        return "%s%s{%d}%s[%s]" % (prefix, self.name, self.scope,
                                   " (secret)" if self.secret else "", self.struct_type)

    def __repr__(self) -> str:
        return self.__str__()

    def __pos__(self) -> "Variable":
        return self.indirect(1)

    def __neg__(self) -> "Variable":
        return self.indirect(-1)

    def __rmul__(self, other: int) -> "Variable":
        if not isinstance(other, int):
            raise ValueError("Must be an int !")
        return self._replace(indirection=other)

    def __rand__(self, other):
        if not isinstance(other, int):
            raise ValueError("Must be an int !")
        return self._replace(indirection=-other)

    def asXml(self, fields_to_export=None):
        return self.compactXml()

    @classmethod
    def fromXml(cls, element):
        var_type = cls.importXmlCompactField(element, "var_type", VariableType.SCALAR,
                                             cls.enum_modifier(VariableType))
        scope = cls.importXmlCompactField(element, "scope", 0, int)
        indirection = cls.importXmlCompactField(element, "indirection", 0, int)
        secret = cls.importXmlCompactField(element, "secret", False, bool)
        name = cls.importXmlCompactField(element, "name", "")
        struct_type = cls.importXmlCompactField(element, "struct_type", "")
        user_input = cls.importXmlCompactField(element, "user_input", False, bool)

        return Variable(name=name, var_type=var_type, scope=scope, indirection=indirection,
                        struct_type=struct_type, secret=secret, user_input=user_input)

    def __eq__(self, v):
        return super(Variable, self._replace(var_type=1)).__eq__(v._replace(var_type=1))

    def __hash__(self):
        return super(Variable, self._replace(var_type=1)).__hash__()

    @classmethod
    def new(cls, name, var_type=VariableType.SCALAR, scope=1, indirection=0, struct_type="",
            secret=False, user_input=False):
        return Variable(name=name, var_type=var_type, scope=scope,
                        indirection=indirection, struct_type=struct_type,
                        secret=secret, user_input=user_input)

    def get_immediate_member(self, member_var: "Variable") -> "Variable":
        """Return the member corresponding to `member_var` of `self`."""
        return self._replace(name=self.name + "." + member_var.name, struct_type=member_var.struct_type,
                             indirection=member_var.indirection, var_type=member_var.var_type)

    def get_member(self, member_name: str, structs: Dict[str, List["Variable"]],
                   coords: Union[Coord, str] = "") -> "Variable":
        """
        Return the field defined by `member_name` with correct type and indirection. The struct
        definition definition is taken from the dictionary `structs`. If `member_name` contains
        dot characters, the variable corresponding to the correct sub-field is returned. If `self`
        is not a structure, or `member_name` is empty, return self instead.

        Args:
            member_name (:obj:`str`): name of the (sub-)field to return.
            structs (:obj:`Dict[str, List[Variable]]`): Structure definitions
            coords (:obj:`Union[Coord, str]`): Coordinates for this function call

        Returns:
            :obj:`Variable`: The variable corresponding to the subfield, or `self` if `self` is
                not a structure.
        """

        # Implement a longest prefix matching on self.get_all_members()
        def prefix_overlap(s1: str, s2: str) -> int:
            if len(s1) < len(s2):
                return prefix_overlap(s2, s1)
            # Now we can suppose that len(s1) >= len(s2)
            for (i, c) in enumerate(s2):
                if s1[i] != c:
                    return i
            return len(s2)

        if not member_name:
            return self

        if self.struct_type == "":
            # Probably an opaque structure. Treat as such
            return self

        all_members = self.get_all_members(structs)
        member_name = self.name + "." + member_name
        res = None
        res_overlap = 0
        for member in all_members:
            # If member_name is a strict prefix of member.name: ignore
            if len(member_name) < len(member.name) and member.name.startswith(member_name):
                continue
            overlap = prefix_overlap(member_name, member.name)
            if overlap > res_overlap:
                res = member
                res_overlap = overlap

        return res

    def get_all_members(self, structs: Dict[str, List["Variable"]]) -> List["Variable"]:
        """
        Return all member variables, with correct indirection and struct type.
        Args:
            structs (:obj:`Dict[str, List[Variable]]`): Struct definitions

        Returns:
            List[Variable]: All member variables, including itself.
        """
        res = []

        def _get_member_vars_rec(var, branch_struct_types):
            if var.struct_type and var.struct_type in structs and var.struct_type in branch_struct_types:
                res.append(var._replace(struct_type=""))
            else:
                res.append(var)
            if var.struct_type and var.struct_type in structs:
                if var.struct_type in branch_struct_types:
                    warnings.warn("Recursive structure for variable %s !" % str(var))
                else:
                    for member_var in structs[var.struct_type]:
                        if member_var.name:
                            new_var = var.get_immediate_member(member_var)
                            _get_member_vars_rec(new_var, branch_struct_types.union([var.struct_type]))

        _get_member_vars_rec(self, set())

        return res

    def get_members_as_odict(self, structs: Dict[str, List["Variable"]]) -> OrderedDict:
        """
        Returns all member variables as a tree, where leaves are "simple" variables and intermediary
        nodes are structures. Links are labeled with the field names.
        The tree is implemented using ordered dictionaries.
        """
        res = OrderedDict()

        def _get_member_vars_rec(var, result, branch_struct_types):
            next_name = var.name  # if "." not in var.name else var.name.split(".", 1)[1]
            result[next_name] = OrderedDict()
            if var.struct_type and var.struct_type in structs:
                if var.struct_type in branch_struct_types:
                    warnings.warn("Recursive structure for variable %s !" % str(var))
                else:
                    for member_var in structs[var.struct_type]:
                        if member_var.name:
                            new_var = var.get_immediate_member(member_var)
                            _get_member_vars_rec(new_var, result[next_name],
                                                 branch_struct_types.union([var.struct_type]))

        _get_member_vars_rec(self, res, set())
        return res[self.name]

    def get_possible_next_fields(self, odict: OrderedDict, var_name: str) \
            -> Tuple[Optional[str], Optional[str]]:
        if not var_name:
            # To get the first field name:
            parent_dict = odict
            field1 = list(parent_dict.keys())[0]
            field_dict = parent_dict[field1]
        else:
            # var_name = self.name+"."+var_name
            # prefix, _ = var_name.split(".", 1)
            prefix = self.name
            cur_dict = OrderedDict({prefix: odict})
            parent_dict = OrderedDict({"": cur_dict})
            parent_name = ""
            cur_name = self.name
            field1 = None
            field_dict = None
            for field in [""] + var_name.split("."):
                cur_name = cur_name + "." + field if field else cur_name
                # print("cur_name", cur_name)
                # print("parent_keys", list(cur_dict.keys()))
                list1 = list(cur_dict.keys())
                index1 = list1.index(cur_name)
                # First field: direct neighbor from var_name if present
                if index1 < len(list1) - 1:
                    field1 = list1[index1 + 1]
                    field_dict = cur_dict[field1]
                parent_dict, cur_dict = parent_dict[parent_name], cur_dict[cur_name]
                parent_name = cur_name
                # print(parent_dict, cur_dict)

        # Second field: follow field1 until a scalar field is found
        if field1 is not None:
            res = field1
            while field_dict.keys():
                res = list(field_dict.keys())[0]
                field_dict = field_dict[res]
            field2 = res
        else:
            field2 = None

        # Just return the field names
        skip_chars = len(self.name) + 1
        if field1:
            field1 = field1[skip_chars:]
        if field2:
            field2 = field2[skip_chars:]
        return field1, field2

    def get_ignored_members(self, structured_ignored_variables: Dict[str, List[str]],
                            structs: Dict[str, List["Variable"]]) -> List["Variable"]:
        """
        Return the list of member variables that are to be ignored, according to the dict
        `structured_ignored_variables`, indexed by the variable `struct_type`, where each
        entry contains the list of member names to be ignored.

        Args:
            structured_ignored_variables (:obj:`Dict[str, List[str]]`): Dictionary containing\
                the member variables to be ignored.
            structs (:obj:`Dict[str, List[Variable]]`): Struct definitions

        Returns:
            List[Variable]: Member variables to be ignored in the dependency analysis.
        """
        if not self.struct_type:
            return []

        return [self.get_member(member_name, structs)
                for member_name in structured_ignored_variables.get(self.struct_type, [])]

    def get_all_indirections(self, reverse: bool = False) -> List["Variable"]:
        """Returns the list of all indirection levels for this variable, starting with itself. If
         reverse is set to True, starts with the highest indrection instead."""
        if not reverse:
            return [self.indirect(-i) for i in range(self.indirection + 1)]
        else:
            return [self.indirect(-i) for i in range(self.indirection, -1, -1)]


# noinspection PyProtectedMember
SECRET = Variable.new("\\SECRET")._replace(secret=True, scope=-1)
# noinspection PyProtectedMember
USER_INPUT = Variable.new("\\USER_INPUT")._replace(user_input=True, scope=-1)
# noinspection PyProtectedMember
SPECTRE_LEAKAGE = Variable.new("\\SPECTRE_LEAKAGE")._replace(secret=True, scope=-1)


class FileNameInput:
    @staticmethod
    def complete(text, state):
        return (FileNameInput.get_file_names(text) + [None])[state]

    def __init__(self, prompt_msg=None):
        self.prompt_msg = prompt_msg
        readline.set_completer_delims(' \t\n;')
        readline.parse_and_bind("tab: complete")
        readline.set_completer(self.complete)

    @staticmethod
    def get_file_names(prefix):
        res = []
        prefix = os.path.expanduser(prefix)
        path = os.path.dirname(prefix) or "."
        name_prefix = os.path.basename(prefix)
        for entry in os.scandir(path):
            if entry.name.startswith(name_prefix) and \
                    (not entry.name.startswith(".") or name_prefix.startswith(".")):
                if entry.is_dir():
                    to_add = entry.name + "/"
                else:
                    to_add = entry.name

                res.append(os.path.join(os.path.dirname(prefix), to_add))
        return res

    @staticmethod
    def change_autocomplete(f):
        readline.set_completer(lambda text, state: (f(text) + [None])[state])

    def revert_autocomplete(self):
        readline.set_completer(self.complete)

    def get_input(self, prompt_msg=None):
        if prompt_msg is None:
            prompt_msg = self.prompt_msg
        return input(prompt_msg)


class VariadicFunctionCall:
    def __init__(self, arg_list):
        self.args = arg_list

    def default_dependencies(self):
        res = {}
        for v in self.args:
            for i in range(v.indirection + 1):
                res[v.indirect(-i)] = [v.indirect(-i)]
        return res

    def dependency_set(self):
        return self.default_dependencies()

    def return_values(self):
        return [v._replace(indirection=0) for v in self.args]

    def leaking_vars(self):
        return [v._replace(indirection=0) for v in self.args]


VariadicFunctionDefinitionClass = namedtuple("variadic_function",
                                             "name code definition is_complete")


class VariadicFunctionDefinition(VariadicFunctionDefinitionClass, XmlExportMixin):
    __slots__ = ()

    @classmethod
    def fromXml(cls, element):
        name = cls.importXmlString(element, "name")
        code = cls.importXmlString(element, "code")
        local_vars = {}

        # pylint: disable=w0122
        exec(code, None, local_vars)
        definition = list(local_vars.values())[0]
        return VariadicFunctionDefinition(name=name, code=code, definition=definition,
                                          is_complete={True})

    def asXml(self, fields_to_export=None):
        return super(VariadicFunctionDefinition, self).asXml(fields_to_export=["name", "code"])

    @staticmethod
    def new(name: str, leaking: bool = True, dependencies: bool = True):
        return VariadicFunctionDefinition(name=name, code="""
class %s(VariadicFunctionCall):
    def return_values(self):
        return %s
    def leaking_vars(self):
        return %s
""" % ("variadicFunction_" + name.replace(".", "_"),
            "[v._replace(indirection=0) for v in self.args]" if dependencies else "[]",
            "[v._replace(indirection=0) for v in self.args]" if leaking else "[]"),
            definition=VariadicFunctionCall, is_complete={True})


class VariableSet:
    """
    Represents a variable set, that is returned by most of the _handle_xxx functions
    in :obj:`LeakageAnalysis`. In most cases, works exactly like set, but it can also used
    to propagate dependencies for member fields.
    """

    # TODO clarify indirection_mod. Should be the indirection level instead ?
    def __init__(self, iterable: Iterable = ()):

        self.vars = {0: set(iterable)}
        self.struct_vars = {}
        self.ignored_dependent_vars = set()

    def add(self, var: Variable, indirection_mod: int = 0):
        """Add `var` to `self.vars` with indirection modifier `indirection_mod`."""
        if not isinstance(indirection_mod, int):
            raise ValueError("Expected integer for indirection, got {} instead."
                             .format(str(indirection_mod)))
        self.vars.setdefault(indirection_mod, set())
        self.vars[indirection_mod].add(var)

    def get_vars(self, indirection_mod: int = 0) -> Iterable[Variable]:
        """Get the set of  variables at indirection `indirection_mod`."""
        if not isinstance(indirection_mod, int):
            raise ValueError("Expected integer for indirection_mod, got {} instead."
                             .format(str(indirection_mod)))
        return self.vars.get(indirection_mod, set())

    def add_vars_to_field(self, s: Union[set, "VariableSet"], field_name: str, indirection: int = 0):
        """
        Add the variables in the set `s` to the field `field_name`. If `s` is a
        :obj:`VariableSet`, all child fields are also added.

        Args:
            s (:obj:`Union[set, VariableSet]`): Elements to add.
            field_name (:obj:`str`): Field that indicates where to add `s`.
            indirection (int): Indirection level of the variable to add, or `None` for
                automatic inference.

        """
        var_set = self
        if field_name:
            for field in field_name.split("."):
                var_set.struct_vars.setdefault(field, VariableSet())
                var_set = var_set.struct_vars[field]

        var_set.update(s, indirection)

    def update(self, s: Union[set, "VariableSet"], indirection_mod: int = 0):
        """
        If `s` is a set, update `self.vars` with s, at indirection level
        `indirection`. Else, combine two :obj:`VariableSet` objects.

        Args:
            s (:obj:`Union[set, VariableSet]`): Elements to add to `self`
            indirection_mod(int): Indirection modifier level at which to add the variables
                in `s`, if `s` is a set or list. Ignored if `s` is a :obj:`VariableSet`.

        Returns:
            None
        """

        if isinstance(s, (set, list)):
            self.vars.setdefault(indirection_mod, set())
            self.vars[indirection_mod].update(s)
        elif isinstance(s, dict):
            for indirection, var_set in s.items():
                indirection += indirection_mod
                self.vars.setdefault(indirection, set())
                self.vars[indirection].update(var_set)
        else:
            for indirection, var_set in s.vars.items():
                indirection += indirection_mod
                self.vars.setdefault(indirection, set())
                self.vars[indirection].update(var_set)

            for key in s.struct_vars:
                if key not in self.struct_vars:
                    self.struct_vars[key] = VariableSet()
                    self.struct_vars[key].update(s.struct_vars[key])
            self.ignored_dependent_vars.update(s.ignored_dependent_vars)

    def union(self, s: Union[set, "VariableSet"]) -> "VariableSet":
        """Return a new object that contains all the variables of `self` and  `s`."""
        res = VariableSet()
        res.update(self)
        res.update(s)

        return res

    def indirect(self, indirection_level):
        """Return a new object"""
        raise NotImplementedError()

    def map(self, f):
        # applies transformation f to all variables in self
        for key in self.keys():
            for indir_level in self.struct_vars[key]:
                self.struct_vars[key].vars[indir_level] = set(map(f, self.struct_vars[key].vars[indir_level]))
        for indir_level in self.vars:
            self.vars[indir_level] = set(map(f, self.vars[indir_level]))
        # Do not forget the ignored variables !
        self.ignored_dependent_vars = set(map(f, self.ignored_dependent_vars))

        return self

    def __getitem__(self, item: str) -> "VariableSet":
        vs = self

        for field in item.split("."):
            if field not in vs.struct_vars:
                raise KeyError("Field %s not found !" % field)
            vs = vs.struct_vars[field]

        return vs

    def __contains__(self, item: str) -> bool:
        return item in self.keys()

    def __str__(self):
        res = "{\n"
        for i in sorted(self.vars):
            res += "vars[%d]: %s\n" % (i, ", ".join([str(v) for v in self.vars[i]]))
        if self.struct_vars:
            res += "Fields:\n"
            for field in self.struct_vars:
                res += "\t[" + field + "]\n"
                field_res = "\t" + "\n\t".join(str(self[field]).split("\n")) + "\n"
                res += field_res
        return res + "}"

    def __repr__(self):
        return self.__str__()

    def __iter__(self):
        # TODO maybe needs to be improved. indirection level ?
        if not self.vars:
            return {}.__iter__()
        return self.vars[0].__iter__()

    def keys(self) -> Iterable[str]:
        """Iterate over all possible (sub-)fields of `self`."""

        def _keys(vs, prefix=""):
            for k in vs.struct_vars:
                to_yield = k if not prefix else prefix + "." + k
                yield to_yield
                for val in _keys(vs.struct_vars[k], to_yield):
                    yield val

        return _keys(self, "")

    def items(self) -> Iterable[Tuple[str, Set[Variable]]]:
        """
        Iterate over the pairs (member, variables) for all struct members, including itself
        (in this case, member is None).
        """
        # TODO how to handle indirection levels for self.vars ?
        if self.vars.keys():
            for k in range(min(self.vars.keys()), max(self.vars.keys()) + 1):
                if k in self.vars:
                    yield (None, self.vars[k])
        for k in self.keys():
            yield (k, self[k].vars)

    def __len__(self) -> int:
        res = 0
        for (_, v) in self.items():
            res += len(v)
        return res


# Thanks SO

class OrderedSet(OrderedDict, MutableSet):

    def update(self, *args, **kwargs):
        if kwargs:
            raise TypeError("update() takes no keyword arguments")

        for s in args:
            for e in s:
                self.add(e)

    def add(self, elem):
        self[elem] = None

    def discard(self, elem):
        self.pop(elem, None)

    def __le__(self, other):
        return all(e in other for e in self)

    def __lt__(self, other):
        return self <= other and self != other

    def __ge__(self, other):
        return all(e in self for e in other)

    def __gt__(self, other):
        return self >= other and self != other

    def __repr__(self):
        return 'OrderedSet([%s])' % (', '.join(map(repr, self.keys())))

    def __str__(self):
        return '{%s}' % (', '.join(map(repr, self.keys())))

    difference = property(lambda self: self.__sub__)
    difference_update = property(lambda self: self.__isub__)
    intersection = property(lambda self: self.__and__)
    intersection_update = property(lambda self: self.__iand__)
    issubset = property(lambda self: self.__le__)
    issuperset = property(lambda self: self.__ge__)
    symmetric_difference = property(lambda self: self.__xor__)
    symmetric_difference_update = property(lambda self: self.__ixor__)
    union = property(lambda self: self.__or__)


# Wrapper for ctags. The API for ctags-python3 is not Pythonic at all, and has weird quirks where
# str objects are not accepted, only byte objects (which is a pain in Python3)
try:
    import ctags

    class TagsHandler:
        @staticmethod
        def _to_bytes(s: Union[str, bytes]):
            if isinstance(s, str):
                return s.encode("utf-8")
            return s

        def __init__(self, filename: str):
            self.tag_filename = filename
            filename = self._to_bytes(filename)
            self.tag_file = ctags.CTags(filename)
            self.tag_dir = os.path.dirname(filename)

        def get_files_for(self, name: Union[str, bytes], kinds: Tuple[bytes, ...]) -> List[str]:
            """
            Returns the source code files in which `name` of a kind among those in `kinds` was declared,
            using a ctags file.
            """
            entry = ctags.TagEntry()
            matching_results = set()

            if self.tag_file.find(entry, self._to_bytes(name), ctags.TAG_FULLMATCH):
                if entry["kind"].lower() in kinds:
                    return_value = os.path.join(self.tag_dir, entry["file"]).decode("utf-8")
                    matching_results.add(return_value)

            while self.tag_file.findNext(entry):
                if entry["kind"].lower() in kinds:
                    matching_results.add(os.path.join(self.tag_dir, entry["file"]).decode("utf-8"))

            return list(matching_results)

        def get_function_files(self, f_name: Union[str, bytes]) -> List[str]:
            """
            Returns the source code files in which a function named `f_name` was declared, using a
            ctags file.
            """
            print("Searching for definition of function " + str(f_name) + "...")
            return self.get_files_for(f_name, (b"f", b"function"))

        def get_struct_files(self, struct_name: Union[str, bytes]) -> List[str]:
            """
            Returns the source code files in which a structure named `f_name` was declared, using a
            ctags file.
            """
            print("Searching for definition of structure " + str(struct_name) + "...")
            return self.get_files_for(struct_name, (b"s", b"struct"))

except ModuleNotFoundError:
    # Provide a fallback implementation

    class Entry:
        name: str
        file: str
        kind: Optional[str]

        def __init__(self, line):
            (name, file, rest) = line.split("\t", maxsplit=2)
            (_, extended) = rest.split(';"', maxsplit=1)
            kind = None
            for tag in extended.split("\t"):
                if tag.startswith("kind"):
                    kind = tag[5:]
                elif ":" not in tag:
                    kind = tag
            self.name = name
            self.file = file
            self.kind = kind

    def find_entry(name: str, tagfile: List[str]) -> List[Entry]:
        left = 0
        right = len(tagfile)
        mid = (left + right) // 2
        while left < right:
            mid = (left + right) // 2
            (mid_entry, _) = tagfile[mid].split("\t", maxsplit=1)
            if mid_entry < name:
                if left == mid:
                    break
                left = mid
            elif mid_entry > name:
                if right == mid:
                    break
                right = mid
            else:
                break

        res = []
        # Get all entries
        pos = mid
        while pos < len(tagfile):
            line = tagfile[pos]
            if line.startswith(name+"\t"):
                res.append(Entry(line))
            else:
                break
            pos += 1

        pos = mid - 1
        while pos >= 0:
            line = tagfile[pos]
            if line.startswith(name+"\t"):
                res.append(Entry(line))
            else:
                break
            pos -= 1
        return res

    class TagsHandler:
        @staticmethod
        def _to_bytes(s: Union[str, bytes]):
            if isinstance(s, str):
                return s.encode("utf-8")
            return s

        @staticmethod
        def _to_str(s: Union[str, bytes]):
            if isinstance(s, bytes):
                return s.decode("utf-8")
            return s

        def __init__(self, filename: str):
            self.tag_filename = filename
            filename = self._to_bytes(filename)
            self.tag_file = open(filename, mode="r").read().splitlines()
            self.tag_dir = os.path.dirname(filename)

        def get_files_for(self, name: Union[str, bytes], kinds: Tuple[bytes, ...]) -> List[str]:
            """
            Returns the source code files in which `name` of a kind among those in `kinds` was declared,
            using a ctags file.
            """
            kinds = [kind.decode() for kind in kinds]

            return [
                os.path.join(self.tag_dir, entry.file.encode("utf-8")).decode()
                for entry in find_entry(self._to_str(name), self.tag_file)
                if entry.kind in kinds
            ]

        def get_function_files(self, f_name: Union[str, bytes]) -> List[str]:
            """
            Returns the source code files in which a function named `f_name` was declared, using a
            ctags file.
            """
            print("Searching for definition of function " + str(f_name) + "...")
            return self.get_files_for(f_name, (b"f", b"function"))

        def get_struct_files(self, struct_name: Union[str, bytes]) -> List[str]:
            """
            Returns the source code files in which a structure named `f_name` was declared, using a
            ctags file.
            """
            print("Searching for definition of structure " + str(struct_name) + "...")
            return self.get_files_for(struct_name, (b"s", b"struct"))


class ControlDependenciesHandling(enum.Enum):
    """Determines how control dependencies are handled."""
    FULL = 1
    """
    Default: track all control dependencies (except when suppressed via annotation). 
    Might increase the number of false positives
    """
    LESS = 2
    """
    Track control dependencies only for branches marked as safe branches.
    Should not cause false negatives (but be careful !)
    """
    NONE = 3
    """
    Never track control dependencies. Can cause false negatives.
    """
