import os
import shlex
import tempfile
from subprocess import Popen, PIPE
from typing import Optional, List

from pycparser.plyparser import ParseError
from pycparserext.ext_c_parser import GnuCParser

CONFIG_FN = ".stanalyzer-config"


class PreProcessingError(RuntimeError):
    pass


class UnknownFunctionDefError(ValueError):
    pass


def parse_file(filename, use_cpp=True, cpp_path='cpp', cpp_args='', parser=None, rel_filename=None, append_filename=True):
    """Usage is the same as for parse_file defined in pycparser, but errors cause an exception."""
    if use_cpp:
        path_list = [cpp_path]
        if isinstance(cpp_args, list):
            path_list += cpp_args
        elif cpp_args != '':
            path_list += [cpp_args]
        if append_filename:
            path_list += [filename]

        try:
            # Note the use of universal_newlines to treat all newlines
            # as \n for Python's purpose

            pipe = Popen(path_list,
                         stdout=PIPE,
                         universal_newlines=True,
                         )
            text = pipe.communicate()[0]
            # In case of pre-processing errors, raise an exception
            if pipe.returncode != 0:
                raise PreProcessingError()

        except OSError as e:
            raise RuntimeError("Unable to invoke cpp command for " + filename + " :  " + str(path_list) +
                               '\nMake sure its path was passed correctly\n' +
                               ('Original error: %s' % e))
    else:
        with open(filename, "r") as f:
            text = f.read()
    if parser is None:
        parser = GnuCParser()

    return parser.parse(text, rel_filename or filename)


def get_ast(filename, cpp_extra_args: Optional[List[str]] = None, override_cpp_args=None):
    """
    Parse the C file `filename` and return the parsed Abstract Syntactic Tree (AST)

    Args:
        filename (:obj:`str`): The name of the C file containing the code to parse.
        cpp_extra_args (Optional[List[str]]): A list of options to add to the preprocessing command
        override_cpp_args (None or List[str]): Override the cpp arguments by this value
    Returns:
        :obj:`FileAST`: The parsed AST.
    """

    cpp_extra_args = "" if cpp_extra_args is None else cpp_extra_args
    original_filename = filename
    dirname, basename = os.path.split(filename)
    current_wd = os.getcwd()
    config_file = os.path.join(dirname, CONFIG_FN)

    if override_cpp_args is None:
        cpp_path = "/usr/bin/gcc"
        if os.path.isfile(config_file):
            print("Getting AST for %s, using file at %s." % (filename, config_file))
            with open(config_file) as f:
                args = shlex.split(f.read())
                os.chdir(dirname or ".")
                filename = basename
        elif os.path.isfile(CONFIG_FN):
            print("Getting AST for %s, using file at %s." % (filename, CONFIG_FN))
            with open(CONFIG_FN) as f:
                args = shlex.split(f.read())
        else:
            print("No config file")
            args = ['-nostdinc', '-E', r'-Iutils/fake_libc_include']
        args += [original_filename]
    else:
        args = override_cpp_args[1:]
        cpp_path = override_cpp_args[0]

    # If we have not returned, no parser managed to parse the file. Raise the error
    # for debugging
    try:
        return parse_file(filename, use_cpp=True, parser=GnuCParser(),
                          cpp_path=cpp_path, cpp_args=args + cpp_extra_args, rel_filename=original_filename,
                          append_filename=False)

    # except ParseError as e:
    #     # Try to understand what is going on: run preprocessor, then do a syntax check
    #     with tempfile.TemporaryDirectory(".") as tmp_dir_name:
    #         function_name = os.path.join(tmp_dir_name, "pp.c")
    #         # First, run preprocessor
    #         with open(function_name, "w+") as pp_file:
    #             Popen([cpp_path] + args + [filename], stdout=pp_file)
    #
    #         # Then, do a syntax checkw
    #         with Popen([cpp_path, "-nostdinc", "-fsyntax-only", function_name], stderr=PIPE) as process:
    #             err_msg = process.stderr.read().decode("utf-8")
    #             if err_msg:
    #                 print("GCC syntax check returned an non-empty error message:")
    #                 print(err_msg)
    #                 # print("Something might be off in the source file. GCC output below.\n")
    #             # print(err_msg)
    #     raise e
    except PreProcessingError:
        print("gcc pre-processing failed. See above for error messages.")
        exit(1)
    finally:
        os.chdir(current_wd)
