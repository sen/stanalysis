# ---------------------------------------------------------------------
# cache_eval: dependencygraph.py
#
# Implements dependency semantic between the different variables
# encountered during leakage analysis.
#
# Alexander Schaub
# License: BSD
# ---------------------------------------------------------------------
from copy import deepcopy
from typing import List, Dict, Union, Tuple, Iterable, Set, NamedTuple
import itertools
import logging
import warnings
from xml.dom.minidom import getDOMImplementation

from pycparser.plyparser import Coord

from .common import VariableType, Variable, XmlExportMixin, VariableSet, SECRET

VariableGraph = Dict[Variable, Dict[Variable, List[str]]]

logger = logging.getLogger(__name__)

ch = logging.StreamHandler()

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


# logger.setLevel(logging.DEBUG)


def graph_modifier(func):
    def decorated(inst, *args, **kwargs):
        if not inst.modifiable:
            # print(inst.dependency_set)
            # print(inst)
            raise RuntimeError("Tried to modify non-modifiable graph.")
        else:
            inst.dependency_set.clear()
            inst.pointed_set.clear()
            func(inst, *args, **kwargs)

    return decorated


class DependencyGraph(XmlExportMixin):
    ARRAY_TYPES = (VariableType.ARRAY, VariableType.PTR)

    graph_id = 0

    @classmethod
    def set_strict_pointers(cls):
        cls.ARRAY_TYPES = (VariableType.ARRAY,)

    @classmethod
    def set_array_pointers(cls):
        cls.ARRAY_TYPES = (VariableType.ARRAY, VariableType.PTR)

    class GraphLink(NamedTuple("GraphLink", [("variable", Variable), ("coords", tuple)]), XmlExportMixin):
        __slots__ = ()

        def __str__(self):
            return "-> " + str(self.variable) + "@" + str(self.coords)

        def asXml(self, fields_to_export=None):
            impl = getDOMImplementation()
            element = impl.createDocument(None, "a", None).createElement("at")
            element.setAttribute("coords", "|".join(self.coords))
            element.appendChild(self.variable.asXml())
            return element

        @staticmethod
        def fromXml(element):
            coords = element.getAttribute("coords")
            try:
                var_el = element.getElementsByTagName("variable")[0]
            except IndexError:
                raise ValueError("<at> node does not have a <variable> child.")
            variable = Variable.fromXml(var_el)  # type: Variable
            return DependencyGraph.GraphLink(variable=variable, coords=tuple(coords.split("|")))

    tag_to_class = {
        "variable": Variable,
        "at": GraphLink
    }

    def __init__(self, verbosity=0):
        self.pointer_graph = {}  # type: VariableGraph
        self.value_dependencies = {}  # type: VariableGraph
        self.base_variable = {}  # type: Dict[Tuple[str, int], Variable]

        # Computed dependency set. Used as cache, but also sole "source of truth" once the graph
        # has been finalized
        self.dependency_set = {}  # type: Dict[Variable, Set["DependencyGraph.GraphLink"]]
        # Used as a cache
        self.pointed_set = {}  # type: Dict[Tuple[Variable, bool], Set[Tuple[Variable, Tuple[str]]]]

        self.modifiable = True
        self.verbosity = verbosity
        self._fields = ["dependency_set"]
        self.structs = {}  # type: Dict[str, List[Variable]]

        # Enables us to filter out variables from the dependency graph
        self.ignored_variables = set()
        self.ignored_variable_names = set()

        # Enables us to define certain variables as sensitive
        self.sensitive_variables = set()
        self.sensitive_variable_names = set()
        # self.set_log_level()
        self.grafted_variables = set()

        logger.debug("Created graph.")
        DependencyGraph.graph_id += 1

        self.state_id = 0

    def set_log_level(self):
        # Translate between verbosity and log-level
        log_level = {
            0: logging.ERROR,
            1: logging.INFO,
            2: logging.DEBUG
        }
        logger.setLevel(log_level.get(self.verbosity, logging.ERROR))

    def copy(self):
        res = DependencyGraph(verbosity=self.verbosity)
        res.pointer_graph = deepcopy(self.pointer_graph)
        res.value_dependencies = deepcopy(self.value_dependencies)
        res.base_variable = deepcopy(self.base_variable)
        res.dependency_set = deepcopy(self.dependency_set)
        res.ignored_variable_names = set(self.ignored_variable_names)
        res.ignored_variables = set(self.ignored_variables)

        res.sensitive_variable_names = set(self.ignored_variable_names)
        res.sensitive_variables = set(self.ignored_variables)

        res.structs = self.structs
        res.grafted_variables = set(self.grafted_variables)
        return res

    def clear(self):
        for s in [self.pointer_graph, self.value_dependencies, self.base_variable, self.dependency_set,
                  self.ignored_variable_names, self.ignored_variables, self.grafted_variables,
                  self.sensitive_variables, self.sensitive_variable_names]:
            s.clear()

    @graph_modifier
    def merge(self, dep_graph: "DependencyGraph"):
        """Merges dep_graph with itself"""
        self.dependency_set.clear()
        self.pointed_set.clear()
        self.merge_graph_pair((self.pointer_graph, self.value_dependencies),
                              (dep_graph.pointer_graph, dep_graph.value_dependencies))
        # Do not forget the base variables
        for key in dep_graph.base_variable:
            self.base_variable[key] = dep_graph.base_variable[key]
        # Also, ignored variables
        self.ignored_variable_names.update(dep_graph.ignored_variable_names)
        self.ignored_variables.update(dep_graph.ignored_variables)

        self.sensitive_variable_names.update(dep_graph.sensitive_variable_names)
        self.sensitive_variables.update(dep_graph.sensitive_variables)

        # Take care of newly grafted variables
        self.graft_extra_variables(dep_graph)

    @graph_modifier
    def set_secret(self, var: Variable, coords: str):
        if var.name in self.sensitive_variable_names:
            self.value_dependencies.setdefault(0& var, {})
            self.value_dependencies[0& var][SECRET] = ["%s (%s secret)" % (coords, var.name)]

    def get_state(self):
        return {v: self.get_dependency_set(v) for v in list(self.variables())}

    def graft_global_variable(self, global_variable: Variable):
        """Adds a global variable to a potentially closed graph."""
        if global_variable.scope >= 0:
            raise ValueError("Only global variables should be grafted !")
        if global_variable.is_global_function():
            return

        self.grafted_variables.add(global_variable)
        self.add_local_variable_dependencies(global_variable, "")

        # If the graph is closed, also update the cache
        for member_var in global_variable.get_all_members(self.structs):
            for indirected_member_var in member_var.get_all_indirections():
                self.dependency_set[indirected_member_var] = \
                    {DependencyGraph.GraphLink(variable=indirected_member_var, coords=tuple([""]))}

    @graph_modifier
    def add_function_argument_dependencies(self, variable: Variable, coords: Union[Coord, str]):
        """Add initial pointers and value dependencies between function arguments. The corresponding
        graph looks like this: (|+- pointer graph dependency, : value graph dependency)
        ``
        &&a0   &&a              a0:a0  a:a0
          |     |
          |     |       or
         &a0 ---+
          |
          a0: a0``

        For structures, it is more complicated:
        ``
        &f0   &f   or       f0                    f
         |    |           /    \               /    \
         f0---+         &f.a0  f.b0: f.b0     &f.a  f.b:f.b0
         / \                |                      |
        .....           f.a0:f.a0 -----------------+
        ``
        """
        if variable.is_global_function():
            return

        callee_variable = variable._replace(scope=0)  # type: Variable
        variable = variable._replace(scope=1)  # type: Variable

        # For the callee variable, the dependencies are identical to those of local variables
        self.add_local_variable_dependencies(callee_variable, coords)
        coords = str(coords)

        # But for the corresponding local variable, it can be more complicated if structures are involved
        # If variable is a pointer, then the local variable points on the pointee of the corresponding base variable
        # If it is a scalar (not a pointer, not a structure), then the value dependency of the local variable is the
        # corresponding base variable
        # If it is a structure (and not a pointer on a structure), the process is repeated on the member variables

        def _add_local_parameter_dependencies(v: Variable):
            if v.indirection > 0:
                self.pointer_graph[v] = {-v._replace(scope=0): ["%s (%s secret)" % (coords, str(v))]} \
                    if v.name not in self.ignored_variable_names else {}
            elif v.indirection == 0 and (not v.struct_type or v.struct_type not in self.structs):
                self.value_dependencies[v] = {v._replace(scope=0): ["%s (%s secret)" % (coords, str(v))]} \
                    if v.name not in self.ignored_variable_names else {}
                self.set_secret(v._replace(scope=0), coords)
            else:
                # v is a structure of known definition. Perform recursive calls
                for member_var in self.structs[v.struct_type]:
                    # print("Added %s" % str(member_var))
                    immediate_member = v.get_immediate_member(member_var)
                    _add_local_parameter_dependencies(immediate_member)

        _add_local_parameter_dependencies(variable)

        # And also add the base variable for the local variable
        self.base_variable[variable.name, variable.scope] = variable

        # print("Initial graph: ")
        # print(variable)
        # print(self)

    def add_local_variable_dependencies(self, variable: Variable, coords: Union[Coord, str]):
        """Add the local variable dependencies for `variable`, that is a link in the pointer graph
        between the variable and its pointee, an entry in the value dependencies for non-pointers,
        and the same for struct members. The resulting structure then looks like:
        ``
                &&f
                 |
                 &f
                 |
                 f
                /  \
              /     \
          f.a:f.a   &f.b
                      |
                    f.b:f.b
        ``

        Args:
            variable: the variable to include in the dependency graph.
            coords: the coordinates of this inclusion.

        Returns: None

        """
        if variable.var_type == VariableType.ENUM_VALUE or variable.is_global_function():
            return

        # logger.error("Adding variable %s to the graph at %s" % (str(variable), str(coords)))

        coords = str(coords)
        for member_var in variable.get_all_members(self.structs):
            # get_all_members also returns the variable itself, as well as all members,
            # for any depth. This therefore also works for embedded structures.

            for i in range(member_var.indirection):
                self.pointer_graph[member_var.indirect(-i)] = \
                    {member_var.indirect(-i - 1): ["%s (%s secret)" % (coords, str(member_var.indirect(-i)))]}

            if not member_var.struct_type or member_var.struct_type not in self.structs:
                if member_var.name not in self.ignored_variable_names:
                    self.value_dependencies[member_var._replace(indirection=0)] = \
                        {member_var._replace(indirection=0):
                             ["%s (%s secret)" % (coords, str(member_var._replace(indirection=0)))]} \
                            if not member_var.name.startswith("\\RET") else {}
                    self.set_secret(member_var, coords)

        # Also set the base_variable for `variable`
        self.base_variable[(variable.name, variable.scope)] = variable

    def is_pointer(self, variable: Variable):
        return bool(self.pointer_graph.get(variable, {}))

    def is_value(self, variable: Variable):
        return variable in self.value_dependencies

    def merge_graphs(self, d1: VariableGraph, d2: VariableGraph):
        """Merges the two graphs `d1` and `d2` by adding new
        dependencies from `d2` to `d1` and replacing longer dependency chains
        with shorter ones."""
        for v in d2:
            if v.name in self.ignored_variable_names or "*" in self.ignored_variable_names:
                continue

            d1.setdefault(v, {})
            for dependencee in d2[v]:
                if dependencee not in d1[v]:
                    d1[v][dependencee] = d2[v][dependencee]
                elif len(d1[v][dependencee]) > len(d2[v][dependencee]):
                    d1[v][dependencee] = d2[v][dependencee]

    def merge_graph_pair(self, g1: Tuple[VariableGraph, VariableGraph], g2: Tuple[VariableGraph, VariableGraph]):
        """Utility function to merge pairs of graphs at the same time, i.e. a pair of pointer graph
        and value dependencies."""
        (d1a, d1b) = g1
        (d2a, d2b) = g2

        return self.merge_graphs(d1a, d2a), self.merge_graphs(d1b, d2b)

    def graft_extra_variables(self, dep_graph: "DependencyGraph"):
        for global_variable in dep_graph.grafted_variables:
            if global_variable not in self.variables():
                self.graft_global_variable(global_variable)

    def replace_with_closed_graph(self, dep_graph: "DependencyGraph"):
        """
        Replace the graph with a copy from `dep_graph`
        """
        for k in dep_graph.base_variable:
            self.base_variable[k] = dep_graph.base_variable[k]
        for v in dep_graph.dependency_set:
            self.dependency_set[v] = deepcopy(dep_graph.dependency_set[v])
        self.modifiable = False

    def get_base_variables(self, v: Variable) -> Set[Variable]:
        """
        Returns the base variables for `v`. If `v` is not a structure member, this is simply `v` with
        an indirection level corresponding to that of the variable that defined it (removing any dereferences
        and indirection operators). For structure members, this corresponds to the members, at the right
        structure accesses,
        Args:
            v: The variable for which the base variables are to be determined

        Returns: The set of base variables for `v`.


        """
        struct_member_names = v.name.split(".")
        # base_variable = None
        # for scope in range(v.scope, -2, -1):  # = [v.scope, v.scope-1, ..., -1]
        #     base_variable = base_variable or self.base_variable.get((struct_member_names[0], scope), None)
        #

        if v.secret or v.user_input or v.var_type == VariableType.ENUM_VALUE:
            return {v}

        if v.is_global_function():
            return {v}

        base_variable = self.base_variable.get((struct_member_names[0], v.scope))

        if not base_variable:
            import pdb; pdb.set_trace()
            raise ValueError("No base variable for name %s and scope <= %d" % (struct_member_names[0], v.scope))

        to_explore = {base_variable}

        for member_name in struct_member_names[1:]:
            new_to_explore = set()
            for var in to_explore:
                new_to_explore.update(self.breadth_first_search({var}, var.indirection))
            # try to determine the correct indirection, in case anonymous structs / void * fields are present
            # ~~~~~~~~~~~~~~~~~~~~~~~~~~
            # ??????????????????????????
            # Determine why this was necessary. It screws everything over in an *unpredictable*
            # way if some pointed variables do not have the same indirection.
            # ~~~~~~~~~~~~~~~~~~~~~~~~~

            # indirection = None
            # for variable in new_to_explore:
            #     if member_name in [v.name for v in self.structs.get(variable.struct_type, [])]:
            #         indirection = variable.get_member(member_name, self.structs, "").indirection
            # if indirection is None and new_to_explore:
            #     raise ValueError("Could not determine the indirection for the base variable of %s " % str(v))

            to_explore = {variable[0].get_member(member_name, self.structs, "") for variable in new_to_explore}

        return to_explore

    def breadth_first_search(self, starting_v: Set[Variable], max_depth: int, writing: bool = True) -> Set[Tuple[Variable, Tuple[str]]]:
        """Perform a breadth_first search in the pointer graph, starting from `starting_v`, for a
        depth of `max_depth`.

        Args:
            starting_v: starting points for the graph search
            max_depth: depth, from `starting_v`, after which the search is stopped
            writing: when set to True, compute variable sets for writing operations (exclude variables
            that would have negative indirection level), else those for reading operations (change the
            negative indirection levels to indirection = 0)

        Returns:
            The set of variables obtained by performing that search.
        """
        to_explore = set(starting_v)
        new_to_explore = set([])
        # Add some links from max indirection to the variables in starting_v
        if max_depth < 0:
            for var in starting_v:
                for i in range(1, -max_depth + 1):
                    self.pointer_graph[var.indirect(i)] = {var.indirect(i - 1): []}
            return {(var.indirect(-max_depth), ()) for var in starting_v}

        extra_dependencies = set()  # type: Set[Tuple[Variable, Tuple[str]]]

        for _ in range(max_depth):
            new_to_explore.clear()
            for v in to_explore:
                new_to_explore.update(self.pointer_graph.get(v, {}).keys())
                if not writing:
                    extra_dependencies.update([
                        (v, tuple(deps))
                        for v, deps in self.value_dependencies.get(v, {}).items()
                    ])
            to_explore.clear()

            to_explore.update(new_to_explore)

        return {(v, ()) for v in to_explore}.union(extra_dependencies)

    def get_pointed_variables(self, v: Variable, writing: bool = True) -> Set[Tuple[Variable, Tuple[str]]]:
        """Returns the variables that correspond to v in the dependency graph
        Because of pointer aliasing, and array operations, this might not be
        `v` (there might be other variables, for example for arrays), or the set might
        not even contain `v` (if values are swapped for instance).
        Algorithm:
         - Determine the "base variable", that is, the variable with highest indirection
         level and the same name as v.
         - Do a breadth first search with a depth corresponding to the difference of
         indirection level between the base variable and v.
         - Return all the variables discovered in the last round of the breadth first search.
        """
        if not self.modifiable:
            raise RuntimeError("Request for pointed set of unknown variable %s " % str(v))

        if (v, writing) in self.pointed_set:
            return self.pointed_set[(v, writing)]

        base_v = self.get_base_variables(v)  # type: Set[Variable]

        if not base_v:
            if v.name in self.ignored_variable_names:
                return set()
            import ipdb; ipdb.set_trace()
            raise RuntimeError("Request for pointed set of unknown variable %s " % str(v))

        for bv in base_v:
            max_depth = bv.indirection - v.indirection
            res = self.breadth_first_search({bv}, max_depth, writing)
            self.pointed_set.setdefault((v, writing), set())
            self.pointed_set[(v, writing)].update(res)
        return self.pointed_set[(v, writing)]

    def get_pointed_list_of_variables(self, it: Iterable[Variable], writing=True) -> Set[Variable]:
        """Utility function to get the set of of variables that point to any of the variables
        in the iterable `it`."""
        res = set()
        for var in it:
            res.update({
                t[0] for t in self.get_pointed_variables(var, writing)
            })
        return res

    def _get_extra_dependencies(self, variable: Variable, depends_on: Iterable[Variable], coords: Union[Coord, str]):
        """Returns the extra dependencies, in terms of pointer graph and value dependencies,
        created by adding a dependency between `variable` and all variables in `depends_on`."""

        already_done_variable = set()
        already_done_dependency = set()

        def __get_extra_dependencies(variable: Variable, depends_on: Iterable[Variable], coords: Union[Coord, str]):
            res_pointer_graph = {}  # type: VariableGraph
            res_value_dependencies = {}  # type: VariableGraph
            coords = str(coords)
            dependencies = self.get_pointed_list_of_variables(depends_on, writing=False)
            logger.debug("Dependencies: %s", dependencies)
            for v, _ in self.get_pointed_variables(variable, writing=True):
                if not dependencies:
                    if v.indirection == 0:
                        res_value_dependencies.setdefault(v, {})

                for d in dependencies:  # type: Variable
                    if d.secret or d.user_input:
                        for member_var in v.get_all_members(self.structs):
                            member_var = 0 & member_var  # on pragma STA secret, the dereferenced variable is deemed secret
                            if d not in res_value_dependencies.get(member_var, {}):
                                res_value_dependencies.setdefault(member_var, {})
                                res_value_dependencies[member_var][d] = ["%s (%s secret)" % (coords, str(v))]
                                continue
                    elif d.is_global_function():
                        res_pointer_graph.setdefault(v, {})
                        res_pointer_graph[v][0 & d] = ["_"]
                        continue

                    if d not in self.pointer_graph:
                        # This is probably due to the fact that d is a pointer on a known
                        # variable, but with higher level of indirection than the base variable. We need to create
                        # the right pointer chain, and then set the dependency chain. This is automatically done
                        # by get_pointed_variables
                        self.get_pointed_variables(d)
                    if v.indirection > 0:
                        for dependency in self.pointer_graph.get(d, {}):
                            if dependency not in res_pointer_graph.get(v, {}):
                                res_pointer_graph.setdefault(v, {})
                                res_pointer_graph[v][dependency] =\
                                    self.pointer_graph[d][dependency] + ["%s (%s secret)" % (coords, str(v))]

                    for dependency in self.value_dependencies.get(d, {}):
                        if not d.struct_type or d.struct_type not in self.structs:
                            for var_member in v.get_all_members(self.structs):
                                if v.struct_type:
                                    var_member = 0 & var_member
                                if dependency not in res_value_dependencies.get(var_member, {}):
                                    res_value_dependencies.setdefault(var_member, {})
                                    res_value_dependencies[var_member][dependency] = \
                                        self.value_dependencies[d][dependency] + ["%s (%s secret)" % (coords, str(var_member))]
                        else:
                            if dependency not in res_value_dependencies.get(v, {}):
                                res_value_dependencies.setdefault(v, {})
                                res_value_dependencies[v][dependency] =\
                                    self.value_dependencies[d][dependency] + ["%s (%s secret)" % (coords, str(v))]

                    if d.indirection == 0 and d.struct_type and d.struct_type in self.structs:
                        # Create a dependency between structs. The easiest is just to recurse on the fields
                        if not v.struct_type or v.struct_type not in self.structs:
                            warnings.warn("Non structure to structure dep at %s" % str(coords))
                        # print("v = %s" % str(v))
                        # print("d = %s" % str(d))
                        for member_field in self.structs[d.struct_type]:
                            # print("@ %s / field %s" % (str(coords), member_field.name))
                            # print("extra : %s / %s" % (v.get_member(member_field.name, self.structs, ""), d.get_immediate_member(member_field)))
                            dependent_var = v.get_member(member_field.name, self.structs, "")
                            dependency = d.get_member(member_field.name, self.structs, "")
                            if dependent_var not in already_done_variable or dependency not in already_done_dependency:
                                already_done_variable.add(dependent_var)
                                already_done_dependency.add(dependency)
                                self.merge_graph_pair((res_pointer_graph, res_value_dependencies),
                                                      __get_extra_dependencies(dependent_var, [dependency], coords))

            return res_pointer_graph, res_value_dependencies
        res = __get_extra_dependencies(variable, depends_on, coords)
        return res

    def should_add_only(self, v: Variable) -> bool:
        if v.name.startswith("\\RET"):
            return True
        if v.var_type == VariableType.FUNCTION_PTR:
            return False

        for parent_member_var_name in itertools.accumulate(v.name.split("."), lambda a, b: a + "." + b):
            for parent_member_var in self.get_base_variables(v._replace(name=parent_member_var_name)):
                if parent_member_var.var_type in self.ARRAY_TYPES:
                    return True
        return False

    @graph_modifier
    def add_multiple_dependencies(self, variable_dependencies: Dict[Variable, Iterable[Variable]],
                                  coords: Union[Coord, str], add_only: bool = False, extra_dependencies: set = None):

        logger.debug("Add dependency between variables : %s", str(variable_dependencies))
        if not extra_dependencies:
            extra_dependencies = set()
        coords = str(coords)
        # Compute the new dependencies
        res = {}, {}
        for v in variable_dependencies:
            #if v.var_type == VariableType.FUNCTION_PTR:
            #    warnings.warn("Adding function pointer %s to graph at %s !" % (str(v), str(coords)))

            if v.name in self.ignored_variable_names or "*" in self.ignored_variable_names:
                continue

            extra_dependencies_graph = {
                v: {
                    extra_dep: coords for extra_dep in extra_dependencies
                }
            }
            self.merge_graph_pair(res, self._get_extra_dependencies(v, variable_dependencies[v], coords))
            self.merge_graphs(res[1], extra_dependencies_graph)

        # Clear the old dependencies
        if not add_only:
            for v in res[0]:
                if not self.should_add_only(v) and v in self.pointer_graph:
                    for d in set(self.pointer_graph[v].keys()):
                        if d not in res[0].get(v, {}):
                            del (self.pointer_graph[v][d])
            for v in res[1]:
                if not self.should_add_only(v) and v in self.value_dependencies:
                    for d in set(self.value_dependencies[v].keys()):
                        if d not in res[1].get(v, {}):
                            del (self.value_dependencies[v][d])

        # TODO: understand how a non-pointer function pointer made its way into the pointer graph
        if [key for key in res[0] if key.indirection <= 0 and key.var_type != VariableType.FUNCTION_PTR]:  # type: List[Variable]
            import ipdb; ipdb.set_trace()
            raise ValueError("Non pointer in pointer graph at %s" % coords)

        for v in res[1]:
            self.set_secret(v, coords)

        # And add the newly computed dependencies
        self.merge_graph_pair((self.pointer_graph, self.value_dependencies), res)

        if logger.getEffectiveLevel() <= logging.INFO:
            fn = "graph_{}_{}.gv".format(DependencyGraph.graph_id, self.state_id)
            with open(fn, "w+") as f:
                f.write(self.get_graph_as_dot())
            logger.info("New graph saved as %s" % fn)
            self.state_id += 1

        if self.verbosity > 1:
            print(self)

    @graph_modifier
    def clear_dependencies(self, v: Variable, coords: Union[Coord, str]):
        """Clears all the dependencies for variable `v`. If it is a pointer, the indirection graph is
        modified so that the pointer does not point to other values."""
        # Reset the pointer graph
        base_v = self.get_base_variables(v)
        for var in base_v:  # type: Variable
            for indir in range(var.indirection):
                indir_var = var.indirect(-indir)
                self.pointer_graph[indir_var] = {-indir_var: [str(coords)]}

        # Clear the value dependencies
        self.value_dependencies.get(v.indirect(-v.indirection), {}).clear()

    @graph_modifier
    def add_dependencies(self, variable: Variable, depends_on: Union[List[Variable], Variable, VariableSet],
                         coords: Union[Coord, str], add_only=False, extra_dependencies=None):
        if isinstance(depends_on, Variable):
            depends_on = [depends_on]

        self.add_multiple_dependencies({variable: depends_on}, coords, add_only, extra_dependencies)

    def compute_dependency_set(self, v: Variable):
        if v in self.dependency_set:
            return self.dependency_set[v]
        elif not self.modifiable:
            # Probably a return variable with a struct type, or SECRET
            if (v.name == "\\RET" and v.struct_type) or v.secret or v.user_input or v.is_global_function():
                return {DependencyGraph.GraphLink(variable=v, coords="")}
            else:
                raise RuntimeError("Asking for non-existent variable %s on non-modifiable graph." % str(v))
        res = set()
        if v.name not in self.ignored_variable_names:
            for variable, deps in self.get_pointed_variables(v, writing=False):
                if variable.secret:
                    res.add(DependencyGraph.GraphLink(variable=variable, coords=tuple(deps)))
                for pointer in self.pointer_graph.get(variable, {}):
                    res.add(DependencyGraph.GraphLink(variable=+pointer,
                                                      coords=tuple(self.pointer_graph[variable][pointer])))
                for value in self.value_dependencies.get(variable, {}):
                    res.add(DependencyGraph.GraphLink(variable=value,
                                                      coords=tuple(self.value_dependencies[variable][value])))

        self.dependency_set[v] = res

        return res

    def get_dependency_set(self, v: Variable):
        return {gl.variable for gl in self.compute_dependency_set(v)}

    def get_var_member(self, var: Variable, member_name: str):
        # Returns the variable var.member_name, with correct struct_type and indirection
        return var.get_member(member_name, self.structs)

    def get_related_vars(self, var: Variable):
        # If var is NOT an explicit struct, returns var
        # If it is, returns var as well as all variables belonging
        # to the structure var, with maximum indirection value
        res = {}
        prefix = var.name + "."
        for v in self.variables():
            if v.name.startswith(prefix):
                if v.name not in res or res[v.name].indirection < v.indirection:
                    res[v.name] = v
        return set([res[key] for key in res]).union({var})

    def get_related_equivalent_vars(self, var: Variable, var2: Variable):
        for pointed in self.get_related_vars(var2):
            for right in pointed.get_all_members(self.structs):
                suffix = right.name[len(var2.name) + 1:]
                left = self.get_var_member(var, suffix)
                for i in range(max(right.indirection, left.indirection) + 1):
                    yield (left.indirect(-min(i, left.indirection)),
                           right.indirect(-min(i, right.indirection)))

    def variables(self) -> Iterable[Variable]:
        if not self.modifiable:
            for k in self.dependency_set:
                yield k
        else:
            for k in self.pointer_graph:
                yield k
            for k in self.value_dependencies:
                yield k

    @classmethod
    def fromXml(cls, element, scope=1):
        G = DependencyGraph()
        G.dependency_set = cls.importXmlDict(element, "dependency_set")
        #                                     import_types=[cls.importXmlDict, cls.importXmlStringSplit("|")])
        for var in G.dependency_set:
            G._add_base_variable_if_necessary(var)
        G.modifiable = False
        return G

    def eliminate_local_vars(self, local_scope: int = 0, close_graph: bool = True):
        # if local_scope > 0:
        #    return
        res_dependency_set = {}

        # Compute the dependency set for the variables still visible.
        for (name, scope) in self.base_variable:
            if scope <= local_scope:
                base_var = self.base_variable[name, scope]
                for member_var in base_var.get_all_members(self.structs):
                    member_var = self.base_variable.get((member_var.name, member_var.scope), member_var)
                    for indir in range(member_var.indirection + 1):
                        v = member_var.indirect(-indir)
                        res_dependency_set[v] = {dep for dep in self.compute_dependency_set(v) if
                                                 dep.variable.scope <= local_scope}

        # Remove any variables, values and pointers of scope less than local_scope
        for graph in self.value_dependencies, self.pointer_graph:
            for key in list(graph.keys()):
                if key.scope > local_scope:
                    del graph[key]
                else:
                    for value in list(graph[key].keys()):
                        if value.scope > local_scope:
                            del graph[key][value]

        # Do the same for the cache
        for key, writing in list(self.pointed_set.keys()):  # type: Variable, bool
            if key.scope > local_scope:
                del self.pointed_set[(key, writing)]
            else:
                self.pointed_set[(key, writing)] =\
                    set(filter(lambda var: var[0].scope <= local_scope, self.pointed_set[(key, writing)]))

        # Update the dependency set
        self.dependency_set.clear()
        self.dependency_set.update(res_dependency_set)

        # Also, remove unnecessary base variables
        # Could be a problem for statement-expressions, but an even bigger problem for
        # regular use-cases
        for name, scope in set(self.base_variable.keys()):
            if scope > local_scope:
                del self.base_variable[name, scope]

        # print("New variables (local_scope = %d): %s" % (local_scope, str(self.base_variable)))

        if close_graph:
            # If the option is set, the graph cannot be modified anymore and is ready to be exported
            # This is the case if the current function being parsed has been fully analyzed.

            self.pointer_graph.clear()
            self.value_dependencies.clear()
            self.pointed_set.clear()

            self.modifiable = False

    @staticmethod
    def from_dict(d: Dict[Variable, Iterable[Variable]], coord: Union[Coord, str]):
        g = DependencyGraph()
        for var in d:
            g._add_base_variable_if_necessary(var)
            g.dependency_set[var] = set()
            for dep_var in d[var]:
                g.dependency_set[var].add(DependencyGraph.GraphLink(variable=dep_var, coords=(str(coord),)))
        g.modifiable = False
        return g

    def _add_base_variable_if_necessary(self, v: Variable):
        if not (v.name, v.scope) in self.base_variable or \
                self.base_variable[v.name, v.scope].indirection < v.indirection:
            self.base_variable[v.name, v.scope] = v

    def get_graph_as_dot(self) -> str:
        """Return a representation of the value graph as a dot file."""
        res = "strict digraph G {\n"
        for var in self.value_dependencies.keys():
            if var.indirection == 0:
                self.compute_dependency_set(var)

        for var, gls in self.dependency_set.items():
            for gl in gls:
                if gl.variable.scope == 0 or gl.variable.secret:
                    if not gl.variable.secret:
                        res += "\t\"{s}_0\"[label=<{s}<SUB>0</SUB>>];\n".format(s=gl.variable.name)
                    else:
                        res += "\t\"{s}_0\"[label=<{s}>];\n".format(s=gl.variable.name)
                    res += "\t\"%s\" -> \"%s_0\";\n" % (var.name, gl.variable.name)
        res += "}"

        return res

    def __str__(self) -> str:
        res = "Pointer graph (v=%d):\n" % self.verbosity
        for v in self.pointer_graph:
            # if v.scope >= 0 or self.dependency_set[v]:
            res += "    %s : %s\n" % (str(v), ", ".join(map(str, self.pointer_graph[v])))
        res += "Dependency sets :\n"
        for v in self.dependency_set:
            res += "    " + str(v) + ": " + ", ".join(map(str, self.dependency_set[v])) + "\n"
        res += "Value dependencies :\n"
        for v in sorted(self.value_dependencies.keys(), key=lambda v: v.name):
            res += "Of " + str(v) + ": " + ", ".join(map(str, self.value_dependencies[v])) + "\n"
        res += "Base variables :\n"
        for (var_name, scope) in self.base_variable:
            res += "\t%s(%d): %s\n" % (var_name, scope, self.base_variable[var_name, scope])
        return res
