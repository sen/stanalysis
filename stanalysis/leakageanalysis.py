# -*- coding: utf-8 -*-
# ---------------------------------------------------------------------
# cache_eval: leakageanalysis.py
#
# Implements the main class used to analyze C source code and detect
# potential side channel leakage.
#
# Alexander Schaub
# License: BSD
# ---------------------------------------------------------------------
# pylint: disable-msg=c0103

"""
LeakageAnalysis
===============
The module :mod:`leakageanalysis` analyzes C code and detects any potential cache timing leakage.
In order to use its functionality, three operations need to be performed beforehand:

    #. A file named `.stanalyzer-config` needs to be created in the same directory\
    as the C file to be analyzed. This file needs to contain the gcc options necessary for\
    the preprocession step of the compilation. It mainly needs to contain all the include\
    flags (``-Isomelib``). It can also contain extra definitions (``-D'something='``) in\
    order to hide unresolved naming issues.
    #. Pragma directives that are specific to this module need to be added to the C code, in\
    order to enable this tool to lookup relevant function specifications, as well as to\
    determine which variables are sensitive. Only the leakage of those variables will be\
    reported. The following example shows how to use those pragma directives.

        .. code-block:: C

            //file bar.c
            int foo(int); // defined in foo.c
            int main() {
                #pragma STA import foo.c
                // All functions declared in foo.c are now imported
                // The import pragma directives can be placed anywhere, before
                // a call to an imported function is performed.
                int s = 0;
                #pragma STA secret s
                // s is now considered to be a sensitive variable
                return foo(s);
            }

    #. Analyze the C code. This is pretty straightforward using this module:

    >>> from stanalysis.codeanalysis import CodeAnalysis
    >>> CodeAnalysis().analyze("bar.c")

"""

from __future__ import print_function

import itertools
import logging
from typing import Set, List, Iterable, Union as TypeUnion, Any, Tuple, Dict, Optional
import warnings

from collections import OrderedDict

from pycparser.c_ast import *
from pycparser.c_parser import CParser
from pycparser.plyparser import Coord
from pycparserext.ext_c_parser import FuncDeclExt, TypeOfExpression, Asm

from .common import VariableType, Variable, LeakageWarning, VariadicFunctionDefinition, VariableSet, \
    TypedefInfo, SECRET, USER_INPUT, SPECTRE_LEAKAGE, ControlDependenciesHandling
from .dependencygraph import DependencyGraph
from .scopes import AnalysisState, FunctionDefinition, LeakageEvents, LeakageEvent, LeakageType, \
    UnknownVariableError, LeakageCoord
from .simple_c_generator import SimpleCGenerator

# This is not required if you've installed pycparser into
# your site-packages/ with setup.py
sys.path.extend(['.', '..'])

logger = logging.getLogger(__name__)
ch = logging.StreamHandler()

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


# Monkeypatch warnings.showwarning to avoid messy warnings
def _showwarning(message, category, filename, lineno, file=None, line=None):
    if file is None:
        file = sys.stderr
    if category == LeakageWarning:
        file.write("LeakageWarning: %s\n" % message)
    else:
        file.write(warnings.formatwarning(message, category, filename, lineno, line))


warnings.showwarning = _showwarning

SECRET_PREFIX = '____'


def is_void_0(expr: Node) -> bool:
    """Returns True if expr corresponds to (void *) 0 (ie NULL)."""
    if not isinstance(expr, Cast):
        return False

    if not is_constant_0(expr.expr):
        return False

    cast_type = expr.to_type.type

    if not isinstance(cast_type, PtrDecl):
        return False

    if not isinstance(cast_type.type, TypeDecl) or not isinstance(cast_type.type.type, IdentifierType):
        return False

    if cast_type.type.type.names == ["void"]:
        return True
    else:
        return False


def is_constant_0(expr: Node) -> bool:
    """Returns True if expr is the constant 0."""
    return isinstance(expr, Constant) and expr.type == "int" and expr.value == "0"


def is_null_check(expr: Node) -> bool:
    return is_void_0(expr) or is_constant_0(expr)


def dict_diff(d1: Dict[Variable, Set[Variable]], d2: Dict[Variable, Set[Variable]]):
    # returns the difference between the dictionaries d1 and d2
    res = {}
    for k in d1:
        if k not in d2 or d1[k] != d2[k]:
            res[k] = (d1[k] - d2[k], d2[k] - d1[k] if k in d2 else None)
    for k in d2:
        if k not in d1:
            res[k] = (None, d2[k])

    return res


class LeakageAnalysis:
    """The main class used for leakage analysis. It does dependency analysis and
    keeps track of leaking variables.

    Attributes:
        state (:obj:`AnalysisState`): The analysis state, keeping track of
            declared variables, functions and dependency graphs, as well as
            a couple of options.
    """

    def __init__(self, parser=CParser()):
        self.state = AnalysisState()
        self.parser = parser

        # Set default options
        self.set_option("spectre_vulnerability", True)
        self.set_option("verbose", True)

        # Keep track of which goto labels where already taken, in
        # order to avoid infinite recursions
        self.gotos_taken = []

        # Keep track of the tagged variables, for further analysis
        self.tagged_variables = {"secret": set(), "user_input": set()}

        # Used to hold options for the leakage analysis, such as:
        # safe_branch (bool): indicates that the next branch will be ignored for leakage
        # array_pointers (set[String]): these variables, although declared as pointers, should
        #   be in fact considered as arrays instead.

        self.state_modifiers = {}

        # List of functions being imported, to better detect recursive function calls
        self.importing_functions = set()

        # CodeAnalysis handler to more cleanly separate concerns
        self.code_analysis = None  # type: "CodeAnalysis"

        self.function_call_graph = {}

    def set_option(self, option_name: str, option_value: Any):
        """
        Change option `option_name` to take value `option_value`.

        Args:
            option_name (:obj:`str`): Name of the option.
            option_value: Value it will take.

        Returns:
            None
        """
        self.state.set_option(option_name, option_value)

    def get_option(self, option_name: str):
        """
        Return the value of option `option_name`.

        Args:
            option_name (:obj:`str`): Name of the option.

        Returns:
            Value of option `option_name`.
        """
        return self.state.get_option(option_name)

    def import_functions(self, filename: str):
        """
        Import all functions defined in `filename` into the current state.

        Args:
            filename (:obj:`str`): Name of the file.
        """
        self.state.import_functions(filename)

    def export_functions(self, filename: str):
        """
        Export all functions defined in `self.state` into file `filename`.

        Args:
            filename (:obj:`str`): Name of the  file.
        """
        self.state.export_functions(filename)

    def add_function(self, f_def: FunctionDefinition):
        """
        Add the function defined by `f_def` to the functions in `self.state`.

        Args:
            f_def (:obj:`FunctionDefinition`): The object defining the function to be added.
        """
        self.state.add_function(f_def)

    @property
    def functions(self) -> List[FunctionDefinition]:
        return self.state.functions

    def import_global_scope(self, nodes: List[Node]):
        # Import globaly defined symbols, as well as pragmas
        for decl in nodes:
            if isinstance(decl, (Decl, Typedef, Pragma)):
                # Handles global variables, typedefs, structs and pragmas
                self.depends_on(decl)
            # Import function prototypes
            if isinstance(decl, FuncDef):
                self.state.add_var(Variable.new(decl.decl.name)._replace(var_type=VariableType.FUNCTION_PTR,
                                                                         scope=-1))

    def _get_variable_by_name(self, expr: Node) -> Variable:
        """Return the variable corresponding to the expression expr."""
        if isinstance(expr, StructRef):
            self._handle_struct_ref(expr)
            expr = ID(self._parse_struct_ref(expr), coord=expr.coord)
        try:
            res = self.state.get_variable_by_name(expr.name)  # type: Variable

        except UnknownVariableError as e:
            # print("Known variables: ")
            # print({v for v in self.state.vars if v.var_type != VariableType.FUNCTION_PTR})
            raise UnknownVariableError("%s: %s" % (expr.coord, e.args[0]))
        return res

    def _get_function_by_name(self, expr: Node) -> FunctionDefinition:
        """Return the function corresponding to the expression expr."""
        if isinstance(expr, ID):
            function_name = expr.name
        elif isinstance(expr, (UnaryOp, Cast)):  # possible optional & prefix or Cast
            function_name = expr.expr.name
        else:
            raise ValueError("Could not determine the function addressed by %s" % expr)
        funcs = self.state.functions
        if function_name in funcs and funcs[function_name].is_complete:
            return funcs[function_name]
        else:
            return self._import_function(function_name)

    def check_safe_branch(self) -> bool:
        """Returns `True` if the branch is to be considered safe, and sets the option to `False`."""
        if self.state_modifiers.get("safe_branch", False):
            self.state_modifiers["safe_branch"] = False
            return True
        return False

    def _add_leaking_vars(self, variables: Iterable[Variable], coord: TypeUnion[str, Coord, LeakageCoord],
                          leakage_type: LeakageType):
        if not self.state.function_stack:
            return

        if not isinstance(coord, LeakageCoord):
            coord = LeakageCoord.from_coord(str(coord))

        for var in variables:
            if var.var_type in [VariableType.FUNCTION_PTR, VariableType.ENUM_VALUE]:
                continue

            dependencies = {gl for gl in self.state.dependency_graph.compute_dependency_set(var)}
            #                            if gl.variable.indirection >= 0}

            if not dependencies:
                continue

            # Do not add the same kind of event (variable, type and leaking line) several times
            should_add = True
            to_add = LeakageEvent(variable=var, dependencies=dependencies, coord=coord,
                                  leakage_type=leakage_type)

            dependency_vars = {gl.variable for gl in dependencies}

            equivalent_leakages = self.state.leaking_vars.get_equivalent_leakage_events(to_add)
            for le in equivalent_leakages or []:  # type: LeakageEvent
                old_dependency_vars = {gl.variable for gl in le.dependencies}
                # The related dependencies might have varied. If new dependencies
                # are added to the leakage, we need to add them
                if dependency_vars.issubset(old_dependency_vars):
                    # In this case, the new dependencies are just a subset of the old
                    # dependencies: no need to add a redundant leakage event.
                    should_add = False
                    break

            if should_add:
                self.state.leaking_vars.append(to_add)

        # Logging change of leakage set
        if logger.getEffectiveLevel() <= logging.INFO:
            leaked_vars = set()
            for (v, deps, coord, leakage_type) in self.state.leaking_vars:
                for gl in deps:
                    if gl.variable.scope == 0 or gl.variable.secret:
                        leaked_vars.add((gl.variable, coord))
            log_list = ["%s%s%s: %s" % ("&" * v.indirection, v.name, "_0" if v.scope == 0 else "", str(c)) for (v, c) in
                        leaked_vars]
            log_text = "Leakages: " + ", ".join(log_list)
            logger.info(log_text)

        # self._check_leakage()

    def _check_leakage(self):
        """Check if a variable in state.leaking_vars is secret and if so, emit a warning."""
        if not self.state.function_stack:
            return

        for (v, deps, coord, leakage_type) in self.state.leaking_vars:
            secret_dep = None
            for dep in deps:
                # Get the shortest dependency path to SECRET
                if dep.variable.secret:
                    if secret_dep is None or \
                            len(dep.coords) < len(secret_dep.coords):
                        secret_dep = dep

            if secret_dep is not None:
                if secret_dep.variable == SPECTRE_LEAKAGE:
                    leakage_type = LeakageType.SpectreVulnerability
                self.on_leakage(v, leakage_type, coord.finalize(self.state.get_function().name), secret_dep.coords)
                # path = "\n\t".join(secret_dep.coords.split("|"))
                # if secret_dep.variable == SPECTRE_LEAKAGE:
                #     leakage_type = LeakageType.SpectreVulnerability
                # warnings.warn("Leakage of %s (reason: %s) at:\n\t %s.\n Dependencies leading to a secret variable: %s"
                #               % (v, leakage_type, coord.replace("|", "\n\t-> "), "\n\t" + path), LeakageWarning)

    @staticmethod
    def get_leakage_text(variable: Variable, leakage_type: LeakageType, leakage_coord: LeakageCoord,
                         dependency_path: List[str]) -> str:
        path = "\n    ".join(dependency_path)

        coord_str = ""
        for (depth, fun_dependencies) in enumerate(reversed(leakage_coord)):
            whitespace = " " * (4 * depth)
            prefix = whitespace + "-> " if depth > 0 else ""
            join_str = "\n" + whitespace + "-> "
            coord_str += prefix + fun_dependencies[0] + \
                         (join_str + join_str.join(fun_dependencies[1]) if fun_dependencies[1] else "") + "\n"
            # coord_str += prefix + fun_dependencies.replace("+", "\n"+whitespace+"-> ") + "\n"
        return "Leakage of %s (reason: %s) at:\n%s.\n" \
               "Dependencies leading to a secret variable: \n    %s\n" \
               % (variable, leakage_type, coord_str, path)

    def on_leakage(self, variable: Variable, leakage_type: LeakageType, leakage_coord: LeakageCoord,
                   dependency_path: List[str]):
        text = self.get_leakage_text(variable, leakage_type, leakage_coord, dependency_path)
        warnings.warn(text, LeakageWarning)

    def _is_user_dependent(self, variables: Iterable[Variable]):
        """Return True if a variable in `variables` depends on user input, else False."""
        for v in variables:
            s = self.state.dependency_graph.get_dependency_set(v)
            for v_s in s:
                if v_s.user_input:
                    return True
        return False

    def _handle_array_ref(self, expr: ArrayRef) -> VariableSet:
        """
        Handle an array reference. The subscript leaks, and the return value depends
        on the array being referenced.
        """
        array_vars = self.depends_on(expr.name)
        index_vars = self.depends_on(expr.subscript)
        self._add_leaking_vars(index_vars, expr.coord, LeakageType.PointerDeref)
        # The array adress itself might also depend on sensitive values - take this into account

        self._add_leaking_vars(array_vars, expr.coord, LeakageType.PointerDeref)

        # ---- Spectre Detection start ----
        if self.get_option("spectre_vulnerability"):
            if self._is_user_dependent(index_vars):
                for v in array_vars:
                    self.state.add_dependencies(-v, VariableSet({SPECTRE_LEAKAGE}), expr.coord)
        # ---- Spectre Detection end ----

        # This handles ternary expressions for expr.name. Simply return several variables
        # This is enough if this expression is an rvalue. If it is an lvalue,
        # get_dependent_var needs to be handled with care.
        dependent_var = self._get_dependent_var(array_vars)
        if len(dependent_var) > 1:
            warnings.warn("Got multiple arrays for ArrayReference at %s" % expr.coord)
            # print(dependent_var)

        res = VariableSet([-v for v in dependent_var]).union(index_vars)
        res.ignored_dependent_vars.update(index_vars)
        res.ignored_dependent_vars.difference_update(
            {-v for v in dependent_var}.difference(array_vars.ignored_dependent_vars))

        return res

    @staticmethod
    def _get_dependent_var(exp_vars: Iterable[Variable]) -> Set[Variable]:
        """
        Return the "dependent" variable from a set of variables, that is, the variable
        with the highest indirection level. If several variables have the same highest
        indirection level, ambiguous is set to true.

        Args:
            exp_vars : set of Variables

        Returns:
            dependent_var (:obj:`Variable`):  Variable with the highest indirection level
            ambiguous (:obj:`bool`): True if more than one element in exp_vars has the highest
            indirection level
        """

        dependent_vars = set()
        indirection = -10
        for var in exp_vars:
            if not dependent_vars:
                dependent_vars.add(var)
                indirection = var.indirection
            elif indirection < var.indirection:
                dependent_vars = {var}
                indirection = var.indirection
            elif indirection == var.indirection:
                dependent_vars.add(var)

        # remove variables to ignore
        if isinstance(exp_vars, VariableSet):
            dependent_vars.difference_update(exp_vars.ignored_dependent_vars)

        return dependent_vars

    def _handle_asm(self, expr: Asm) -> VariableSet:
        warnings.warn("Asm expression at %s. Not supported yet." % str(expr.coord))
        return VariableSet()

    def _handle_assignment(self, expr: Assignment) -> VariableSet:
        """Add the dependencies of expr.rvalue to the dependent variable of expr.lvalue."""
        exp_vars = self.depends_on(expr.lvalue)
        # TODO handle multiple dependent_var here
        dependent_var = self._get_dependent_var(exp_vars)

        if not dependent_var:
            raise ValueError("Try to assign constant value at %s" % expr.coord)
        ambiguous = len(dependent_var) > 1
        if ambiguous:
            raise ValueError("Not clear ! %s %s" % (str(dependent_var), expr.coord))

        dependent_var = list(dependent_var)[0]
        dependencies = self.depends_on(expr.rvalue)

        if dependent_var.var_type != VariableType.FUNCTION_PTR:
            dependencies.update(self.state.extra_dependencies)
        if expr.op != "=":
            dependencies.add(dependent_var)
        if expr.op in ("|=", "&="):
            leaking_vars = exp_vars.union(dependencies)
            self._add_leaking_vars(leaking_vars, expr.coord, LeakageType.PotentialBranchBitwise)

        if not ambiguous:
            if dependent_var.var_type == VariableType.FUNCTION_PTR:
                self._get_function_by_name(expr.rvalue)


            self.state.add_dependencies(dependent_var, dependencies, expr.coord)
        # else:
        #     # TODO: clone the dependency graph instead, cf. loop handling
        #     # old_dep_graph = state.dependency_graph.copy()
        #     with self.state.dependencies_add_only():
        #         for v in dependent_var:
        #             self.state.add_dependencies(v, dependencies, expr.coord)
        #     return dependencies
        return VariableSet({dependent_var})

    def _handle_binary_op(self, expr: BinaryOp) -> VariableSet:
        """
        Handle a binary operation, by adding the dependencies from both sides of the
        operation.
        """
        affected_left = self.depends_on(expr.left)
        res = self.depends_on(expr.right).union(affected_left)
        if not self.state.potential_leakages_already_covered:
            if expr.op == "|" or expr.op == "&":
                self._add_leaking_vars(res, expr.coord, LeakageType.PotentialBranchBitwise)
            elif expr.op == "||" or expr.op == "&&":
                self._add_leaking_vars(res, expr.coord, LeakageType.PotentialBranchShortcut)
        return res

    def _handle_case(self, expr: Case) -> VariableSet:
        """Handle a case statement, by iterating over all expressions of that case."""
        res = VariableSet()
        if isinstance(expr, Case):
            self.state.add_extra_dependencies(self.depends_on(expr.expr),
                                              safe_branch=False,
                                              control_dependencies=self.code_analysis.control_dependencies)
        for e in expr.stmts:
            res.update(self.depends_on(e))
        return res

    def _handle_compound(self, expr: Compound) -> VariableSet:
        with self.state.new_scope():
            return self._handle_list(expr)

    def _handle_compound_literal(self, expr: CompoundLiteral) -> VariableSet:
        """Handle a compound literal, initializing an array (WARNING: might fail for structures)."""
        # WARNING: may not work correctly in all cases.
        # TODO does not work correctly when a structure is initialized without using NamedInitializers
        # TODO correct this, but this might require a bit more work (because we do not know which
        # TODO type is being initialized.)

        return self._handle_list(expr.init)

    def _parse_decl(self, expr: Decl) -> Tuple[Variable, FunctionDefinition]:
        """
        Parse a declaration statement. The function determines the correct variable type from
        the declaration (pointer, array, structure type, etc.). Returns a variable and a
        (Variadic)FunctionDefinition (that might be None).
        """
        indirection = 0
        expr_type = expr.type
        var_type = None
        struct_type = ""
        f_def = None

        while isinstance(expr_type, (ArrayDecl, PtrDecl)):
            indirection += 1
            var_type = var_type or \
                       (VariableType.ARRAY if isinstance(expr_type, ArrayDecl) else VariableType.PTR)
            expr_type = expr_type.type

        # Let's check if this variable should be considered as an array instead of a pointer
        if var_type == VariableType.PTR and expr.name in self.state_modifiers.get("array_pointers", set()):
            # remove this name: we don't need to consider it anymore
            self.state_modifiers["array_pointers"].remove(expr.name)
            # and change the variable type to Array
            expr_type = VariableType.ARRAY

        if isinstance(expr_type, TypeDecl):
            # Either a struct/typedef or a scalar type
            # Problem : typedef and scalar type parsed in a similar fashion, but
            # we want to associate a very different semantic
            if isinstance(expr_type.type, Struct):
                struct_type = expr_type.type.name or expr_type.declname
                self._handle_struct(expr_type.type, name=struct_type)
                var_type = var_type \
                           or (VariableType.STRUCT if struct_type in self.state.structs else
                               VariableType.OPAQUE_STRUCT)
            elif isinstance(expr_type.type, Enum):
                self._handle_enum(expr_type.type)
                var_type = var_type or VariableType.SCALAR
            elif isinstance(expr_type.type, Union):
                if expr_type.type.decls:
                    union_member_var, _ = self._parse_decl(expr_type.type.decls[0])
                    indirection += union_member_var.indirection
                var_type = var_type or VariableType.OPAQUE_STRUCT
            elif isinstance(expr_type.type, TypeOfExpression):
                # Should give information about the type of expr_type.type.expr
                # If depends_on(expr_type.type.expr) returns a variable, the new variable
                # type should be of that.
                try:
                    s = list(self.depends_on(expr_type.type.expr))
                except ValueError:
                    # Very likely to be a function of which we only know the prototype
                    # TODO check and verify this
                    var_type = VariableType.FUNCTION_PTR
                else:
                    # can be the typeof of a constant. In this case, ignore it, it's a simple type
                    if not s:
                        pass
                    else:
                        var = s[0]
                        # A function pointer cannot be anything else than a function pointer
                        # Let's just pretend pointers on function pointers do not exist
                        if var.var_type == VariableType.FUNCTION_PTR:
                            var_type = VariableType.FUNCTION_PTR
                        f_def = self.functions[var.name]._replace(name=expr.name)
                        var_type = var_type or var.var_type
                        indirection += var.indirection
                        struct_type = var.struct_type
            else:
                names = expr_type.type.names
                if len(names) == 1 and names[0] in self.state.typedefs:
                    typedef_info = self.state.typedefs[names[0]]
                    struct_type = typedef_info.struct_type
                    indirection += typedef_info.indirection
                    var_type = var_type or typedef_info.var_type
                else:
                    var_type = var_type or VariableType.SCALAR
        elif isinstance(expr_type, Struct):
            self._handle_struct(expr_type)
        elif isinstance(expr_type, (FuncDecl, FuncDeclExt)):
            var_type = VariableType.FUNCTION_PTR
            f_def = self._parse_func_ptr_decl(expr_type)
        elif isinstance(expr_type, Enum):
            self._handle_enum(expr_type)
        elif isinstance(expr_type, Union):
            struct_type = ""
            var_type = VariableType.OPAQUE_STRUCT
        else:
            raise ValueError("Unknown type of declaration '%s' at line %s" %
                             (str(expr_type), str(expr.coord)))

        return (Variable(name=expr.name, var_type=var_type, scope=self.state.scope_depth,
                         indirection=indirection, secret=False, struct_type=struct_type,
                         user_input=False),
                f_def)

    def _handle_decl(self, expr: Decl) -> VariableSet:
        """
        Handle a declaration statement, adding a new variable to the set of known
        variables, and setting the dependencies as defined by the initialization (if any).
        """
        # First, determine the type of variable (or function) that is being declared
        (new_var, f_def) = self._parse_decl(expr)
        if not new_var or not new_var.name:
            return VariableSet()

        if isinstance(new_var, Variable):
            # print("Add variable %s" % str(new_var))
            self.state.add_var(new_var, expr.coord, clear=True)
        if new_var.var_type == VariableType.FUNCTION_PTR and f_def is not None:
            funcs = self.state.functions
            if f_def.name not in funcs or not funcs[f_def.name].is_complete:
                self.state.add_function(f_def)
            if expr.init:
                # /!\ WARNING /!\
                # We only accept IDs for the init. No pointer arithmetic, no weird cast please
                func = self._get_function_by_name(expr.init)
                # noinspection PyProtectedMember
                self.state.add_function(func._replace(name=new_var.name))
            # noinspection PyProtectedMember
            self.state.add_var(Variable.new(new_var.name)._replace(
                scope=self.state.scope_depth,
                var_type=VariableType.FUNCTION_PTR,
                indirection=1), expr.coord)
            return VariableSet()

        # Update dependencies for new_var
        # Those are the dependencies of expr.init, if it is set, as well
        # as the extra dependencies induced by branches or loops.
        # InitList are handled in a special way, as several dependencies have
        # to be updated.

        dependencies = VariableSet()
        if expr.init:
            if isinstance(expr.init, InitList):
                member_dependencies = self._parse_struct_init(new_var, expr.init)
                self.state.add_dependencies(new_var, member_dependencies, expr.coord)
                # for (var, deps) in member_dependencies.items():
                #     # deps.update(self.state.extra_dependencies)
                #     dependencies.update(deps)
                #     # print("%s -> %s" % (str(var), str(deps)))
                #     self.state.add_dependencies(var, deps, expr.coord)
                return dependencies
            dependencies = self.depends_on(expr.init)

        # dependencies.update(self.state.extra_dependencies)
        if self.state.function_stack:
            # print("%s -> %s" % (new_var, dependencies))
            self.state.add_dependencies(new_var, dependencies, expr.coord)

        if expr.name.startswith(SECRET_PREFIX) and self.state.function_stack:
            # noinspection PyProtectedMember
            deref_new_var = new_var._replace(indirection=0)
            self.state.add_dependencies(deref_new_var, VariableSet([deref_new_var, SECRET]),
                                        expr.coord)
        return dependencies

    def _parse_named_initializer(self, expr: NamedInitializer) -> Tuple[str, int]:
        """Returns the tuple (member_name, indirection)"""
        indir_level = 0
        member_name = ""

        # if isinstance(expr, (ID, Constant, InitList, Cast, BinaryOp, UnaryOp, ArrayRef)):
        if not isinstance(expr, NamedInitializer):
            return "", -1

        for name in expr.name:
            if isinstance(name, Constant):
                # Corresponds to a list identifier
                indir_level += 1
            elif isinstance(name, ID):
                # Corresponds to a member name
                member_name = name.name if not member_name else member_name + "." + name.name
                indir_level = 0

        return member_name, indir_level

    def _handle_named_initializer(self, expr: NamedInitializer) -> VariableSet:
        return self.depends_on(expr.expr)

    def _parse_struct_init(self, var: Variable, init: InitList) -> VariableSet:
        """
        Parse a struct initializer, given as an InitList or an other expression,
        and returns a dictionary {field: dependent_vars}, where some fields may contain
        structure references.
        """

        # The algorithm is the following:
        # If the variable var is an opaque/unknown struct or a list, all dependencies
        # are combined into dependencies, else, for every element in the init list,
        # we determine which field it corresponds to, and add the dependencies to
        # the appropriate field.

        dependencies = VariableSet()
        if not var.struct_type or var.struct_type not in self.state.structs or var.var_type == VariableType.ARRAY:
            treat_as_list = True
        else:
            treat_as_list = False

        member_name = ""
        struct_odict = var.get_members_as_odict(self.state.structs)
        for initializer in init.exprs:
            # For every element in the InitList, we first try to determine the field name and indirection,
            # if provided
            alt_member_names = var.get_possible_next_fields(struct_odict, member_name) if not treat_as_list else \
                (None, None)

            member_name, indir_level = self._parse_named_initializer(initializer)
            # If no indirection is provided, and we are handling a list, the indirection is equal
            # to var's indirection
            if indir_level == -1:
                indir_level = var.indirection

            if not member_name and not treat_as_list:
                # choose the first or second alternative member name, depending on the type of initializer
                member_name = alt_member_names[0 if isinstance(initializer, InitList) else 1]

            expr = initializer.expr if isinstance(initializer, NamedInitializer) else initializer

            vs = self.depends_on(expr) if not isinstance(expr, InitList) else \
                self._parse_struct_init(
                    var.get_member(member_name, self.state.structs, init.coord).indirect(-indir_level), expr)
            # Finally, we get the dependencies for that field and it them to the result

            dependencies.add_vars_to_field(vs, member_name, indirection=-indir_level)

        return dependencies

    def _handle_enum(self, expr: Enum) -> VariableSet:
        """
        Handle enum type definitions. Create variables with the names of
        the different enum names.
        """
        if not expr.values:
            return VariableSet()

        for enum in expr.values.enumerators:
            # noinspection PyProtectedMember
            var = Variable.new(enum.name)._replace(scope=self.state.scope_depth,
                                                   var_type=VariableType.ENUM_VALUE)
            self.state.add_var(var, expr.coord)
        return VariableSet()

    def _handle_for(self, expr: For) -> VariableSet:
        """
        Handle a for-loop. The condition variables leak and will affect all variable
        dependencies in the loop body. All instructions of the body are parsed twice.
        """
        extra_deps = VariableSet()
        with self.state.new_scope():
            safe_branch = self.check_safe_branch()
            self.depends_on(expr.init)
            with self.state.ignore_potential_leakages(not safe_branch):
                cond = self.depends_on(expr.cond)
            # extra_deps.update(cond)
            self.state.add_extra_dependencies(cond,
                                              safe_branch=safe_branch,
                                              control_dependencies=self.code_analysis.control_dependencies)
            if not safe_branch:
                self._add_leaking_vars(cond, expr.coord, LeakageType.CondBranch)
            nb_iterations = 0
            while True:
                nb_iterations += 1
                old_dependency_graph = self.state.dependency_graph.copy()
                # old_state = old_dependency_graph.get_state()

                extra_deps.update(self.depends_on(expr.stmt))
                extra_deps.update(self.depends_on(expr.next))
                if not safe_branch:
                    self._add_leaking_vars(cond, expr.coord, LeakageType.CondBranch)
                self.state.dependency_graph.merge(old_dependency_graph)

                if self.state.dependency_graph.get_state() == old_dependency_graph.get_state():
                    # Fixed point was attained
                    return extra_deps

                if nb_iterations > 5:
                    warnings.warn("More than 5 iterations needed to attain fixed point at loop %s" % str(expr.coord))
                    # import pdb; pdb.set_trace()

    def _add_func_argument_dependencies(self, function_arguments: List[Variable], call_arguments: List[Set[Variable]],
                                        fun: FunctionDefinition, expr: FuncCall):
        dependency_variables = OrderedDict()

        def add_dependencies_on_dependent_var(dependent_var: Variable):
            # Dependencies are only created at one more indirection level (eg, for a pointer,
            # only the dereferenced value can change, not the pointer per se.)
            if dependent_var.var_type == VariableType.FUNCTION_PTR:
                return

            for indir in range(1, function_argument.indirection + 1):
                deref_var = dependent_var.indirect(-indir)
                for (dependent_var_related, fun_argument_related) in \
                        dep_graph.get_related_equivalent_vars(deref_var, function_argument.indirect(-indir)):
                    # We get directly all variables that are members of function_argument, and
                    # retrieve at the same time the ones that are equivalent members of
                    # deref_var
                    # dependent_var_related is the variable that might be modified.
                    # Let's see if it is the case
                    dependencies = dep_graph.get_dependency_set(fun_argument_related)

                    depends_on = self._translate_variables(
                        dependencies, fun.arguments, call_arguments, expr.coord)  # \
                    # .union(self.state.extra_dependencies)
                    # print("Fun. argument dependencies (%s): " % str(dependent_var_related))
                    # print(depends_on)
                    if dependencies == {fun_argument_related}:
                        continue
                    dependency_variables.setdefault(dependent_var_related, VariableSet())
                    dependency_variables[dependent_var_related].update(depends_on)

        for (call_vars, function_argument) in zip(call_arguments, function_arguments):
            # We will determine how the function call modifies the dependencies of the
            # variables the function was called with
            dependent_var = self._get_dependent_var(call_vars)

            # If no "dependent variable", then there can be no additional dependency.
            if not dependent_var:
                continue
            dep_graph = fun.dependency_graph  # type: DependencyGraph
            self.state.import_structs(dep_graph)
            # Finally, add the dependencies
            if len(dependent_var) > 1:
                with self.state.dependencies_add_only():
                    for dep in dependent_var:
                        add_dependencies_on_dependent_var(dep)
            else:
                add_dependencies_on_dependent_var(list(dependent_var)[0])

        # print("Function argument dependencies added: ")
        # print(dependency_variables)
        self.state.add_multiple_dependencies(dependency_variables, str(expr.coord))

    def _add_global_dependencies(self, deps, function_arguments, call_arguments, expr):
        """
        Handle dependencies induced on global variables. Since there might be issues related to
        ill-defined scopes (same variable name in different functions), this function only
        focuses on tagging SECRET the necessary variables.
        """
        global_deps = self.state.global_dependencies  # type: DependencyGraph
        for var in deps.variables():  # type: Variable
            if var.scope < 0 and var.indirection == 0:
                dependencies = self._translate_variables(deps.get_dependency_set(var), function_arguments,
                                                         call_arguments, expr.coord)
                for dep in dependencies:
                    if SECRET in self.state.dependency_graph.get_dependency_set(dep) or dep == SECRET:
                        global_deps.add_dependencies(var, VariableSet([var, SECRET]), str(expr.coord))
                        self.state.dependency_graph.add_dependencies(var, VariableSet([var, SECRET]), str(expr.coord))

    def _translate_variables(self, var_set, function_arguments, call_arguments, coords):
        """
        Translate the variables in `var_set`, which are function local variables, to variables
        in the caller scope. The arguments the function was called with are in `call_arguments`.

        Args:
            var_set (:obj:`Set[Variable]`): Function-local variables to translate
            function_arguments (:obj:`List[Variable]`): Arguments the called function was defined with
            call_arguments (:obj:`List[Set[Variable]]`): Variables the function was called with
            coords: coordinates of the corresponding call

        Returns:
            :obj:`VariableSet`: The translated variables
        """

        res = VariableSet()

        # Handle global variables once and for all.
        for var in var_set:
            if var.scope < 0:
                # res.update(self.state.global_dependencies.get_dependency_set(var))
                res.update({var})

        for (def_arg, call_args) in zip(function_arguments, call_arguments):
            for indir in range(def_arg.indirection + 1):
                if def_arg.indirect(-indir) in var_set:
                    res.update(VariableSet([v.indirect(-min(indir, v.indirection))
                                            for v in call_args]))

            for (member_name, variable_sets) in call_args.items():
                if member_name:
                    res.add_vars_to_field(variable_sets, member_name)
            for var in var_set:
                if var.scope < 0:
                    pass

                elif var.name.startswith(def_arg.name + "."):
                    # A member of def_arg is in var_set. def_arg is therefore a struct
                    # Get the element in call_args that is a struct of the same type as def_arg
                    if not def_arg.struct_type:
                        raise ValueError("Encountered struct reference for variable %s of "
                                         "unknown struct type at %s" % (def_arg, coords))
                    member_name = var.name.split(".")[1:][0]
                    member_found_in_call_args = False
                    if member_name in call_args:
                        member_found_in_call_args = True
                        res.update(call_args[member_name])

                    call_struct_var_candidates = \
                        [call_var for call_var in call_args
                         if call_var.struct_type == def_arg.struct_type]

                    if not call_struct_var_candidates:
                        if not member_found_in_call_args and call_args:
                            # call_args is empty if the function is called with NULL as argument
                            # This should not be reported as an error, as no structure variable is
                            # expected in this case.
                            warnings.warn("No structure variable in %s of type %s at %s."
                                          % (str(call_args), def_arg.struct_type, str(coords)))
                        continue
                    if len(call_struct_var_candidates) > 1:
                        warnings.warn("Could not determine unambiguously which structure "
                                      "reference %s references at %s." % (var, coords))
                    call_struct_var = call_struct_var_candidates[0]

                    # And add to res, the corresponding member of call_struct_var
                    call_var_name = ".".join([call_struct_var.name] + var.name.split(".")[1:])
                    # noinspection PyProtectedMember
                    call_var = var._replace(name=call_var_name, scope=call_struct_var.scope)
                    # self.state.add_var(call_var)
                    res.add(call_var)

        for global_var in [SECRET, USER_INPUT, SPECTRE_LEAKAGE]:
            if global_var in var_set:
                res.add(global_var)

        return res

    def _import_function(self, function_name, call_args=None):
        """Import the function `func_name` and preserve the scope and function stack."""
        if "." in function_name:
            import warnings
            # import ipdb;ipdb.set_trace()
            warnings.warn("Unknown function %s" % function_name)
            self.state.add_function(VariadicFunctionDefinition.new(function_name))
            return self.functions[function_name]

        f_def = self.code_analysis.get_func_definition(function_name)

        with self.state.saved_scopes():
            self.importing_functions.add(function_name)
            self._handle_func_def(f_def, call_args=call_args)
            self.importing_functions.remove(function_name)

        # print(self.state.function_stack)

        self.state.dependency_graph.structs = self.state.structs

        return self.functions[function_name]

    def _handle_func_call(self, expr):
        """Handle a function call. Returns the dependencies of the return value."""
        # Determine whether a definition of the called function is known. If not, raise an Exception

        function_name = self._parse_struct_ref(expr.name)

        calling_function = self.state.get_function().name
        self.function_call_graph.setdefault(calling_function, {})
        self.function_call_graph[calling_function][str(expr.coord)] = function_name

        call_arguments = [] if not expr.args else \
            list(reversed([self.depends_on(child) for child in reversed(expr.args.exprs)]))

        funcs = self.state.functions

        if function_name not in funcs or not funcs[function_name].is_complete:
            if function_name in self.importing_functions:
                warnings.warn("Recursive function detected: %s" % function_name)
                return VariableSet()
            elif function_name in funcs:
                self._import_function(function_name, call_args=call_arguments)
            else:
                # raise UnknownFunctionDefError(function_name, expr)
                self._import_function(function_name, call_args=call_arguments)

        fun = self.state.functions[function_name]


        # Compute the dependency sets for all function arguments
        # TODO refactor all of this. It starts to become too ugly
        if isinstance(fun, VariadicFunctionDefinition):
            # print("Variadic function : %s" % fun.name)

            # print(fun.leaking_vars)
            # noinspection PyProtectedMember
            fun.arguments = []
            for (i, call_argument) in enumerate(call_arguments):
                if not call_argument or len(call_argument.get_vars()) == 0:
                    indirection = 0
                else:
                    indirection = max([v.indirection for v in call_argument])

                fun.arguments.append(Variable.new("arg%d" % i)._replace(scope=0).indirect(indirection))

            f_object = fun.definition(fun.arguments)
            fun.return_vars = VariableSet(f_object.return_values())
            dependency_graph = DependencyGraph.from_dict(f_object.dependency_set(), expr.coord)
            fun.dependency_graph = dependency_graph
            fun.leaking_vars = LeakageEvents([
                LeakageEvent(variable=v, dependencies=set(), coord=LeakageCoord.from_coord(str(expr.coord)),
                             leakage_type=LeakageType.Other)
                for v in f_object.leaking_vars()])
            res_depends_on = VariableSet(
                self._translate_variables(f_object.return_values(),
                                          fun.arguments, call_arguments, expr.coord))
            ret_var = None
        else:
            ret_var = fun.get_ret_var()  # type: Variable
            return_structure = fun.get_ret_var_set()  # type: VariableSet
            res_depends_on = VariableSet(
                self._translate_variables(fun.dependency_graph.get_dependency_set(ret_var),
                                          fun.arguments, call_arguments, expr.coord))

            # Also, look at what happens at lower indirection levels:
            for indir_level in range(1, ret_var.indirection + 1):
                indir_dependencies = self._translate_variables(
                    fun.dependency_graph.get_dependency_set(ret_var.indirect(-indir_level)),
                    fun.arguments, call_arguments, expr.coord)
                for variable in indir_dependencies:
                    res_depends_on.add(variable, -indir_level)

        # Determine dependencies of the return values and leaking variables
        def handle_return_structures(res_depends_on, ret_var):
            # TODO this should be corrected some time
            # max_indirection = max(res_depends_on.vars.keys())

            if not ret_var:
                return
            # print('Return structure')
            # print(return_structure)
            for member_name in return_structure.keys():
                vs = return_structure[member_name]
                for indir_level in vs.vars:
                    for member_v in vs.get_vars(indir_level):
                        # if fun.name == "bar":
                        #     print(member_v)
                        #     print(fun.dependency_graph.get_dependency_set(member_v))
                        if member_v.secret or member_v.user_input:
                            res_depends_on.add_vars_to_field(VariableSet({member_v}), member_name, indir_level)
                        else:
                            dependencies = self._translate_variables(
                                fun.dependency_graph.get_dependency_set(member_v), fun.arguments,
                                call_arguments, expr.coord)

                            if member_v.scope == 0:
                                # TODO this is somewhat confusing
                                indirection_mod = indir_level - max(vs.vars.keys())
                                res_depends_on.add_vars_to_field(dependencies, member_name, indirection_mod)

        handle_return_structures(res_depends_on, ret_var)
        # print("Return  structures")
        # print(res_depends_on)
        for le in fun.leaking_vars:
            translated_vars = self._translate_variables(
                {le.variable}, fun.arguments, call_arguments, expr.coord)
            leaking_coords = le.coord.add_leaking_call(str(expr.coord), fun.name)
            self._add_leaking_vars(translated_vars, leaking_coords, le.leakage_type)
            # self._add_leaking_vars(translated_vars, "%s|(%s) %s" % (expr.coord, fun.name, le.coord), le.leakage_type)

        # Add the dependencies created during the function call
        self._add_func_argument_dependencies(fun.arguments, call_arguments, fun, expr)

        # And also, for global variables
        self._add_global_dependencies(fun.dependency_graph, fun.arguments, call_arguments, expr)
        # print(res_depends_on)
        # print(res_depends_on.vars)
        return res_depends_on

    @staticmethod
    def _transform_leaking_vars(leakage_events):
        """Transform a LeakageEvents, by eliminating all local leakage events, and
        only keeping one event per variable.

        Arguments:
            leakage_events (:obj:`LeakageEvents`): Leakage events to be transformed.
            
        Returns:
            :obj:`LeakageEvents`: A cleaned up list.
        """
        # When we store the information about variable leakage of that function, we are only
        # interested in the leakage of function arguments
        # They may leak via function-local variable. We thus have to look at all dependencies
        # of leaking variables (at the time of leakage), filter out the local variables, and
        # store the other leakage events.
        leaking_vars = set()
        # total_ptr = len([le for le in leakage_events if le.leakage_type == LeakageType.PointerDeref])
        # total_cond = len([le for le in leakage_events if le.leakage_type == LeakageType.CondBranch])
        # warnings.warn("Registered leaking events: %d / %d" % (total_ptr, total_cond))

        for le in leakage_events:
            # if le.leakage_type == LeakageType.CondBranch:
            #    print("var: %s, coord: %s" % (str(le.variable), str(le.coord)))
            local_coord = le.coord[-1][0]

            for gl in le.dependencies:
                if gl.variable.scope > 0:
                    continue

                le.coord[-1] = (local_coord, tuple(gl.coords))

                leaking_vars.add((gl.variable, le.coord.copy(), le.leakage_type))
            le.coord[-1] = (local_coord, ())

        le = LeakageEvents([
            LeakageEvent(variable=v, dependencies=VariableSet(), coord=coord, leakage_type=leakage_type)
            for (v, coord, leakage_type) in leaking_vars])

        # return le
        # Then filter out the leakage events. Just keep one per leaking variable, line and type
        variables_leaked = {}
        for event in le:  # type: LeakageEvent
            key = (event.variable, event.get_leakage_coord(), event.coord.coords[-1][0], event.leakage_type)
            if key not in variables_leaked or \
                    len(variables_leaked[key].coord) > len(event.coord):
                variables_leaked[key] = event

        return LeakageEvents([variables_leaked[v] for v in variables_leaked])

    def _prepare_function_scope(self, expr):
        """Set up the scope for a new function needing parsing. More precisely,
        add function parameters to the current scope, and return the `FunctionDefinition`
        stub that should be pushed using `self.state.new_function`

        Arguments:
            expr: A FuncDef node.

        Returns:
            :obj:`FunctionDefinition`: A `FunctionDefinition` stub with correct name
                and parameter list.
        """
        # Declare arguments if there are any
        if expr.decl.type.args is not None:
            self._handle_list(expr.decl.type.args)
            # Get the variables in a nice list
            params = expr.decl.type.args.params
            if len(params) == 1 and not params[0].name:
                func_args = []  # only argument: void
            else:
                func_args = [self._get_variable_by_name(ID(v.name))
                             for v in expr.decl.type.args.params]
        else:
            func_args = []

        # Also, determine return type so that the special \\RET variable can be set
        t_decl = expr.decl.type.type
        if "align" in Decl.__slots__:
            decl = Decl("\\RET", "", "", "", "", t_decl, "", 0)
        else:
            decl = Decl("\\RET", "", "", "", t_decl, "", 0)
        ret_var, _ = self._parse_decl(decl)
        if ret_var.var_type == VariableType.FUNCTION_PTR:
            ret_var = None

        if ret_var:
            return_vars = {ret_var}
        else:
            return_vars = set()

        fd = FunctionDefinition.new(name=expr.decl.name, arguments=func_args, is_complete=True,
                                    return_vars=return_vars)

        # Pass to the dependency graph, the set of ignored variables
        # Since the ignore list can depend on the function name and / or the
        # file name, we can filter out rules that won't be used for that graph and
        # only provide the variable names, which will speed up checking whether a variable
        # needs to be ignored
        function_name = expr.decl.name
        file_name = str(expr.coord).split(":", 0)[0]
        ignored_variables = self.get_option("ignored_variables") or {}
        sensitive_variables = self.get_option("sensitive_variables") or {}

        fd.dependency_graph.ignored_variables = self.filter_pattern_list(file_name, function_name, ignored_variables)
        fd.dependency_graph.sensitive_variables = self.filter_pattern_list(file_name,
                                                                           function_name, sensitive_variables)

        self.state.import_structs(fd.dependency_graph)
        # print("structs: %s " % str(fd.dependency_graph.structs))
        fd.dependency_graph.add_local_variable_dependencies(ret_var, "")

        return fd

    @staticmethod
    def filter_pattern_list(file_name: str, func_name: str, pattern_dict: Dict[str, Dict[str, Set[str]]]) -> Set[str]:

        return set(itertools.chain(*(pattern_dict.get(file_key, {}).get(func_key, set())
                                     for file_key in ("*", file_name) for func_key in ("*", func_name))))

    def prepare_local_function_scope(self, local_vars, call_args, coord):
        self.state.dependency_graph.merge(self.state.global_dependencies)

        # Add the return variable
        fd = self.state.get_function()
        if fd.return_vars:
            for v in fd.return_vars:
                self.state.add_var(v, coord)

        # If function pointers are passed as function arguments, we should try to determine which
        # function they point to. Most of the time, they will point to the same function, so the
        # behaviour of the called function can be easily analyzed - if the information about
        # function pointers is propagated across function calls.

        # The scopes and dependency graphs for successive function calls are stored in self.state.stack_saved_states.
        # No, how to perform function name translation between caller and callee ?

        for var in local_vars:
            for member_var in var.get_all_members(self.state.structs):
                if self.state.is_variable_concerned(member_var.name, self.state.dependency_graph.ignored_variables):
                    self.state.dependency_graph.ignored_variable_names.add(member_var.name)
                if self.state.is_variable_concerned(member_var.name, self.state.dependency_graph.sensitive_variables):
                    self.state.dependency_graph.sensitive_variable_names.add(member_var.name)

                self.state.add_var(member_var._replace(scope=1), coord)

            if var.var_type == VariableType.FUNCTION_PTR:
                verbose = self.state.parse_options.get("verbose", True)
                if call_args is None:
                    if verbose:
                        warnings.warn("Unknown function %s at %s." % (var.name, coord), UserWarning)
                    self.state.add_function(VariadicFunctionDefinition.new(var.name))
                else:
                    translated_vars = list(self._translate_variables({var}, fd.arguments, call_args, coord))
                    if len(translated_vars) > 1:
                        if verbose:
                            warnings.warn("Could not disambiguate between function pointers: %s at %s" %
                                          (", ".join(translated_vars), coord))
                        self.state.add_function(VariadicFunctionDefinition.new(var.name))
                    elif len(translated_vars) == 0:
                        if verbose:
                            warnings.warn("Unknown function %s at %s." % (var.name, coord), UserWarning)
                        self.state.add_function(VariadicFunctionDefinition.new(var.name))
                    else:
                        # search for function with name trans_fun_ptr in all defined functions, local and global
                        trans_fun_ptr = translated_vars[0]  # type: Variable
                        trans_fun_fd = None  # type: Optional[FunctionDefinition]
                        for fun_name, fd_ in self.state.functions.items():
                            if fun_name == trans_fun_ptr.name:
                                trans_fun_fd = fd_
                        for scope in self.state.stack_saved_structures[-1][0]:
                            for fun_name, fd_ in scope.functions.items():
                                if fun_name == trans_fun_ptr.name:
                                    trans_fun_fd = fd_

                        if not trans_fun_fd or not trans_fun_fd.is_complete:
                            # Maybe the function name of a not yet analyzed function is provided
                            # as function argument: try to import it now.
                            trans_fun_fd = self._import_function(trans_fun_ptr.name)

                        if trans_fun_ptr.var_type != VariableType.FUNCTION_PTR or trans_fun_fd is None:
                            if verbose:
                                warnings.warn("Unknown function %s at %s. (weird case)" %
                                              (var.name, coord), UserWarning)
                            self.state.add_function(VariadicFunctionDefinition.new(var.name))
                        else:
                            print("Bound %s to %s" % (var.name, trans_fun_ptr.name))
                            self.state.add_function(trans_fun_fd._replace(name=var.name))
            else:
                self.state.dependency_graph.add_function_argument_dependencies(var, coord)

    def _handle_func_def(self, expr, call_args=None):
        """
        Handle a function definition. Update the set of known functions, and return a
        FunctionDefinition corresponding to that function.
        """
        # if self.get_option("verbose"):
        #    print("Parsing %s at %s" % (expr.decl.name, expr.coord))
        with self.state.new_function_def_scope():
            # Process the FuncDef's arguments list and create a
            # FunctionDefinition stub accordingly.
            fd = self._prepare_function_scope(expr)
            local_vars = self.state.get_scope().vars
            with self.state.new_function(fd):
                # Add dependencies between local copies and function arguments
                self.prepare_local_function_scope(local_vars, call_args, expr.coord)

                # Become aware of labels
                self._parse_labels(expr.body)
                # And then, interprete the function body
                # noinspection PyProtectedMember
                self.depends_on(expr.body)

                self._check_leakage()

                # TODO eliminate the function-local variables instead
                self.state.dependency_graph.eliminate_local_vars(local_scope=0)

                # Finally, interprete the return values dependencies
                le = self._transform_leaking_vars(self.state.leaking_vars)
                self.state.leaking_vars.clear()
                self.state.leaking_vars.extend(le)

        function_name = expr.decl.name
        funcs = self.state.functions
        if function_name in funcs and not funcs[function_name].is_complete:
            # We already encountered the prototype. Since some function pointer might
            # point to the prototype, carefully update instead of overwriting
            f_prototype = funcs[function_name]
            f_prototype.arguments[:] = []  # same as f_prototype.arguments.clear()
            f_prototype.arguments.extend(fd.arguments)

            f_prototype.leaking_vars.extend(fd.leaking_vars)

            f_prototype.return_vars.update(fd.return_vars)
            f_prototype.is_complete.add(True)

            f_prototype.dependency_graph.replace_with_closed_graph(fd.dependency_graph)
        else:
            self.state.add_function(fd)

        # noinspection PyProtectedMember
        self.state.add_var(Variable.new(expr.decl.name)._replace(
            var_type=VariableType.FUNCTION_PTR,
            scope=self.state.scope_depth), expr.coord)
        # print(fd.dependency_graph)
        logger.info("Finished parsing " + expr.decl.name)
        # if self.get_option("verbose"):
        #    print("Finished parsing %s" % expr.decl.name)
        return fd

    def _parse_func_ptr_decl(self, expr):
        """
        The input expression is supposed to be a FuncDecl of either a Typedef'd
        function pointer, or a struct member. Return the according FunctionDefinition.

        Args:
            expr: FuncDecl, Typedef or struct member node

        Returns:
            Union[FunctionDefinition, VariadicFunctionDefinition]
        """
        expr_type = expr.type
        while not hasattr(expr_type, "declname"):
            expr_type = expr_type.type

        assert isinstance(expr, (FuncDecl, FuncDeclExt))

        name = expr_type.declname
        args = expr.args.params if expr.args else []
        arguments = []

        for (i, decl) in enumerate(args):
            if isinstance(decl, EllipsisParam):
                # warnings.warn("EllipsisParam for function %s at %s" % (name, expr.coord))
                if name not in self.state.functions:
                    # noinspection PyProtectedMember
                    return VariadicFunctionDefinition.new(name)._replace(is_complete=VariableSet())
                return None
            v, _ = self._parse_decl(decl)
            if v is None:
                return None
            # noinspection PyProtectedMember
            v = v._replace(name="arg%d" % i)
            arguments.append(v)
        res = FunctionDefinition.new(name=name, arguments=arguments, is_complete=False)
        return res

    def _parse_labels(self, expr):
        """
        Explore all statements in the Compound expr in order to extract
        all labeled expression groups. Updates state with a dictionary, indexed
        by label names, that contains the list of all expressions executed after
        a goto to the corresponding label. Only works in simple cases (mainly
        supports the "goto end;" paradigm), does not work with labels that are
        not defined at function top-level.
        """
        res = {}
        cur_labels = set()
        if not hasattr(expr, "block_items") or not expr.block_items:
            return res

        for e in expr.block_items:
            r = []
            if isinstance(e, Label):
                cur_labels.add(e.name)
                e = e.stmt

            elif isinstance(e, If):
                r = [self._parse_labels(e.iftrue), self._parse_labels(e.iffalse)]

            elif isinstance(e, Compound):
                r = [self._parse_labels(e)]

            for label in cur_labels:
                res.setdefault(label, [])
                res[label].append(e)

            for _r in r:
                cur_labels.update(_r.keys())
                res.update(_r)

        self.state.label_groups.update(res)
        return res

    def _handle_goto(self, expr):
        """Jump to the expressions in the label defined by `expr.name`."""
        name = expr.name
        if name in self.gotos_taken:
            warnings.warn("Recursive goto structure detected for label '%s'." % name)
            return VariableSet()

        if name not in self.state.label_groups:
            raise ValueError("Goto could not be resolved: unknown label %s at %s" %
                             (name, expr.coord))

        self.gotos_taken.append(name)
        for e in self.state.label_groups[name]:
            self.depends_on(e)
        self.gotos_taken.pop()

        return VariableSet()

    def _handle_id(self, expr):
        """Return the variable corresponding to the ID expr."""
        try:
            res = self._get_variable_by_name(expr)
        except UnknownVariableError as e:
            print("Unknown variable at %s" % expr.coord)
            raise e
        return VariableSet({res})

    def _handle_if(self, expr):
        safe_branch = self.check_safe_branch()

        cond_vars_are_leaking = (
                (not safe_branch)
                and (not isinstance(expr, TernaryOp) or not self.state.parse_options.get("cond_move", False))
        )
        cond = expr.cond
        with self.state.ignore_potential_leakages(cond_vars_are_leaking):
            cond_vars = self.depends_on(cond)

        res = set(cond_vars)  # type: Set[Variable]
        # If there is only one conditional variable which is a pointer and is checked against 0 or
        # NULL, then there is no leakage and no dependencies should be added
        if len(res) == 1 and all({var.indirection > 0 for var in res}):

            if isinstance(cond, BinaryOp) and cond.op == "==" and \
                    (is_null_check(cond.left) or is_null_check(cond.right)):
                res.clear()
                cond_vars = VariableSet()

        if cond_vars_are_leaking:
            self._add_leaking_vars(cond_vars, expr.coord, LeakageType.CondBranch)

        with self.state.new_scope():
            self.state.add_extra_dependencies(cond_vars,
                                              safe_branch=safe_branch,
                                              control_dependencies=self.code_analysis.control_dependencies)
            # print(self.state.extra_dependencies)
            old_dep_graph = self.state.dependency_graph.copy()
            res.update(self.depends_on(expr.iftrue))
            modified_graph = self.state.dependency_graph.copy()

            old_dep_graph.graft_extra_variables(modified_graph)

            self.state.replace_dependency_graph(old_dep_graph)
            res.update(self.depends_on(expr.iffalse))

            self.state.dependency_graph.merge(modified_graph)

        return VariableSet(res)

    def _handle_label(self, expr):
        return self.depends_on(expr.stmt)

    def _handle_list(self, expr):  # handles list type constructs (Compound, InitList)
        res = VariableSet()
        if expr is None:
            return res
        for (_, e) in expr.children():
            values = self.depends_on(e)
            res.update(values)
        return res

    def _handle_pragma(self, expr):
        """
        Pragmas are used during static analysis in two situations:

         * to indicate secret variables: STA secret var_name [var_name [var_name...]]

         * to give hints for function definition imports: STA import filename func_name [alias_name]
           or STA import_all filename

        """
        s = expr.string
        if not s.startswith("STA "):
            return VariableSet()  # Unknown pragma

        values = s.split()[1:]
        if values[0] == "secret":
            for var_name in values[1:]:
                try:
                    var = self._get_variable_by_name(ID(var_name))
                except UnknownVariableError as e:
                    # Variable has never been encountered yet. Try to reconstruct it.
                    # We suppose that this is the member from a struct variable. If not, there
                    # is nothing we can do
                    try:
                        if "." not in var_name:
                            # print(self.state.vars)
                            raise e
                        # var_name relates to a structure member. Let's get the structure variable
                        # and member path, so that we can get the correct variable identification
                        base_var_name, member_name = var_name.split(".", 1)
                        var = self._get_variable_by_name(ID(base_var_name)) \
                            .get_member(member_name, self.state.structs)
                    except UnknownVariableError:
                        raise ValueError("pragma STA secret: No variable with name %s at %s." %
                                         (var_name, str(expr.coord)))

                # noinspection PyProtectedMember
                current_func = self.state.get_function()
                func_name = current_func.name if current_func else ""

                for v in var.get_all_members(self.state.structs):
                    if v.var_type not in (VariableType.ENUM_VALUE, VariableType.FUNCTION_PTR):
                        deref_new_var = v._replace(indirection=0)
                        self.state.add_dependencies(deref_new_var, VariableSet([deref_new_var, SECRET]),
                                                    expr.coord)
                        self.tagged_variables["secret"].update([(deref_new_var.name, expr.coord, func_name)])

        # ---- Spectre Detection start ----
        elif values[0] == "user_input":
            for var_name in values[1:]:
                var = self._get_variable_by_name(ID(var_name))
                # noinspection PyProtectedMember
                deref_new_var = var._replace(indirection=0)
                self.state.add_dependencies(deref_new_var, VariableSet([deref_new_var, USER_INPUT]), expr.coord)
                current_func = self.state.get_function()
                func_name = current_func.name if current_func else ""
                self.tagged_variables["user_input"].update([(deref_new_var.name, expr.coord, func_name)])
        # ---- Spectre Detection end ----
        elif values[0] == "import":
            assert len(values) >= 3
            filename = values[1]
            defunction_name = values[2]
            if len(values) > 3:
                call_name = values[3]
            else:
                call_name = defunction_name

            func_def = self.code_analysis.get_func_definition(defunction_name, filename)
            func_def.decl.name = call_name

            with self.state.saved_scopes():
                self.depends_on(func_def)

        elif values[0] == "import_all":
            assert len(values) >= 2
            filename = values[1]
            self.code_analysis.import_file(filename)
        elif values[0] == "safe":
            for var_name in values[1:]:
                base_var = self._get_variable_by_name(ID(var_name))
                # noinspection PyProtectedMember
                var = base_var._replace(indirection=0)

                if var in self.state.dependency_graph.get_dependency_set(var):
                    self.state.dependency_graph.clear_dependencies(base_var, expr.coord)
                    self.state.add_dependencies(var, var, expr.coord)
                else:
                    self.state.dependency_graph.clear_dependencies(base_var, expr.coord)
        elif values[0] == "safe_branch":
            # The next branch (if / for / while / switch) is ignored as far as leakage is concerned
            self.state_modifiers["safe_branch"] = True
        elif values[0] == "independent_branch":
            # The next branch (if / for / while / switch) does not add any extra dependencies
            self.set_option("no_extra_dependencies", True)
        elif values[0] == "ignore_branch":
            # combines safe_branch and independent_branch
            self.state_modifiers["safe_branch"] = True
            self.set_option("no_extra_dependencies", True)
        elif values[0] == "array_declaration":
            self.state_modifiers.setdefault("array_pointers", set())
            self.state_modifiers["array_pointers"].update(values[1:])
        elif values[0] == "debug":
            import pdb
            pdb.set_trace()
        else:
            warnings.warn("Unknown pragma directive %s" % s)

        return VariableSet()

    def _handle_return(self, expr):
        if self.state.get_function().return_vars:
            # Add the dependencies to the special variable \\RET
            values = self.depends_on(expr.expr).union(self.state.extra_dependencies)
            # print(self.state.extra_dependencies)
            # We have saved indirection and struct type of that variable, let's get it
            ret_var = next(self.state.get_function().return_vars.__iter__())
            self.state.add_dependencies(ret_var, values, expr.coord)
        return VariableSet()

    def _handle_struct(self, expr, name=None):
        if not expr.decls:
            # Not a declaration. We should already know that structure
            # TODO If not, treat as "opaque" struct (or more likely, see in typedef handling code)
            # if expr.name not in self.state.structs:
            #     if expr.name not in \
            #             ["Display", "MirConnection", "MirSurface", "MirSurfaceSpec", "MirScreencast",
            #              "MirPromptSession", "MirBufferStream", "MirPersistentId", "MirBlob",
            #              "MirDisplayConfig", "xcb_connection_t"]:
            #         warnings.warn("Encountered unknown struct type %s." % expr.name)
            return expr.name
        members = [self._parse_decl(d)[0] for d in expr.decls]
        expr_name = expr.name or name
        self.state.add_struct(expr_name, members)

        return expr.name

    def _parse_struct_ref(self, expr):
        """Determine the name of the variable referenced by `expr`."""
        res = ""
        while not isinstance(expr, ID):
            if hasattr(expr, "field"):
                res = ".%s%s" % (expr.field.name, res)
            if hasattr(expr, "name"):
                expr = expr.name
            elif hasattr(expr, "expr"):
                expr = expr.expr
            else:
                vs = self._get_dependent_var(self.depends_on(expr))
                if len(vs) > 1:
                    import pdb;
                    pdb.set_trace()
                    raise ValueError("Unable to parse expression %s" % expr)
                else:
                    expr = ID(list(vs)[0].name)
        return expr.name + res

    def _get_full_struct_type(self, expr):
        res = []

        def get_element_by_name(name, elements):
            for el in elements:
                if el.name == name:
                    return el
            raise ValueError("Did not find variable or function with name %s" % name)

        if not isinstance(expr, str):
            expr_name = self._parse_struct_ref(expr)
        else:
            expr_name = expr
        variables = self.state.vars
        structs = self.state.structs
        for name in expr_name.split("."):
            if variables is None:
                raise ValueError("Could not determine struct type of %s" % expr_name)
            v = get_element_by_name(name, variables)
            if isinstance(v, Variable) and v.struct_type in structs:
                variables = structs[v.struct_type]
                res.append(v.struct_type)
            else:
                variables = None
                res.append(v.name)

        return ".".join(res)

    def _handle_struct_ref(self, expr):
        var_name = self._parse_struct_ref(expr)
        fields = var_name.split(".")
        # struct_var is the variable representing the struct
        struct_var = self._get_variable_by_name(ID(fields[0]))
        structs = self.state.structs
        # scope = struct_var.scope

        # Find the structure type definition

        member_name = var_name.split(".", 1)[1]
        return_var = struct_var.get_member(member_name, structs, expr.coord)  # type: Variable

        return VariableSet({return_var})

    def _handle_switch(self, expr):
        safe_branch = self.check_safe_branch()
        from .switch_cfg import SwitchControlFlowVisitor, SwitchBreakType

        paths = []
        stmts = expr.stmt.block_items  # Iterable[Node]
        for i in range(len(stmts)):
            path = []
            for case in stmts[i:]:
                path.append(case)
                break_status = SwitchControlFlowVisitor().visit(case.stmts)
                if break_status == SwitchBreakType.MUST:
                    break
                elif break_status == SwitchBreakType.NEVER:
                    continue
                else:  # break_status == SwitchBreakType.MAY
                    paths.append(list(path))
                    continue

            paths.append(path)

        with self.state.ignore_potential_leakages(not safe_branch):
            cond_vars = self.depends_on(expr.cond)
        if not safe_branch:
            self._add_leaking_vars(cond_vars, expr.coord, LeakageType.CondBranch)
        # paths contains all possible longest paths of the different switches
        # TODO Maybe use a more naive approach if there are too many paths

        with self.state.new_scope():
            self.state.add_extra_dependencies(cond_vars,
                                              safe_branch=safe_branch,
                                              control_dependencies=self.code_analysis.control_dependencies)
            original_dependency_graph = self.state.dependency_graph.copy()
            accumulated_dependency_graph = original_dependency_graph.copy()

            for path in paths:
                self.state.replace_dependency_graph(original_dependency_graph.copy())
                for c in path:
                    self.depends_on(c)
                accumulated_dependency_graph.merge(self.state.dependency_graph)
                original_dependency_graph.graft_extra_variables(self.state.dependency_graph)

            self.state.replace_dependency_graph(accumulated_dependency_graph)

        return VariableSet()

    def _handle_typedef(self, expr):
        """ A typedef can be followed by :
         * An identifier type (int, char, ...), or a pointer or array of those
         * A struct that was previously defined or that will be later defined
         * The type definition of a struct
         * Maybe a union, but we will ignore them for now
         """

        # The structure of a Decl and a TypeDef are similar. Let's use this fact
        typedef_var, _ = self._parse_decl(expr)
        typedef_info = TypedefInfo(struct_type=typedef_var.struct_type,
                                   indirection=typedef_var.indirection,
                                   var_type=typedef_var.var_type)
        struct_name = typedef_var.struct_type
        if struct_name and struct_name not in self.state.structs:
            # This is an opaque struct, but it is very likely that we need the concrete definition
            # for the analysis
            # th = self.code_analysis.tagsHandler
            # if th:
            #     filenames = th.get_struct_files(struct_name)
            #     if len(filenames) > 1:
            #         if not any([f in self.code_analysis.analyzed_files for f in filenames]):
            #             # If one of the proposals is already being processed, no need ta raise an exeption.
            #             raise ValueError("Multiple matches for structure %s, please run again by adding one of the "
            #                            "following source files to parse:\n - %s" % (struct_name, "\n - ".join(filenames)))
            #     elif len(filenames) == 0:
            #         warnings.warn("No definition found for stucture %s at %s." % (struct_name, expr.coord))
            #     else:
            #         self.code_analysis.import_file(filenames[0])
            # else:
            warnings.warn("No definition found for stucture %s at %s." % (struct_name, expr.coord))
        self.state.add_typedef(typedef_var.name, typedef_info)
        # print("Typedef %s parsed : %s" % (typedef_var.name, typedef_var))
        return VariableSet()

    def _handle_unary_op(self, expr) -> VariableSet:
        # sizeof is ignored
        if expr.op.lower() == "sizeof":
            return VariableSet()

        # Post / pre increment / decrement operators are handled in a special way
        if expr.op in ("p++", "++", "p--", "--"):
            affected_variable = self.depends_on(expr.expr)
            dependent_var = self._get_dependent_var(affected_variable)
            if len(dependent_var) > 1:
                raise ValueError("Pre/post decrement or increment operator used on ambiguous expression.")
            affected_variable = list(dependent_var)[0]
            dependencies = VariableSet({affected_variable})
            if affected_variable.var_type != VariableType.FUNCTION_PTR:
                dependencies.update(self.state.extra_dependencies)
            self.state.add_dependencies(affected_variable, dependencies, expr.coord)
            return VariableSet({affected_variable})

        if expr.op == "&":
            # handle everything here directly
            # array <-> &array
            # expr.expr.show()
            variables = self.depends_on(expr.expr)
            return variables \
                .map(lambda v: v if (v.var_type == VariableType.ARRAY and v.indirection > 0) or
                                    (v in variables.ignored_dependent_vars)  # &arr[i] depends on &arr and i, not &i
                     else +v)

        elif expr.op == "*":
            variables = self.depends_on(expr.expr)
            for var in variables:
                self._add_leaking_vars({var}, expr.coord, LeakageType.PointerDeref)
            return variables.map(lambda v: -v)
        elif expr.op in ("-", "+", "~", "!"):
            return self.depends_on(expr.expr)
        else:
            raise ValueError("Unknown operator %s" % expr.op)


    def _handle_while(self, expr):
        safe_branch = self.check_safe_branch()
        with self.state.ignore_potential_leakages(not safe_branch):
            extra_deps = self.depends_on(expr.cond)
        if not safe_branch:
            self._add_leaking_vars(extra_deps, expr.cond.coord, LeakageType.CondBranch)

        with self.state.new_scope():
            self.state.add_extra_dependencies(extra_deps,
                                              safe_branch=safe_branch,
                                              control_dependencies=self.code_analysis.control_dependencies)
            res = VariableSet()
            nb_iterations = 0
            while True:
                nb_iterations += 1
                old_dependency_graph = self.state.dependency_graph.copy()
                old_state = old_dependency_graph.get_state()
                res.update(self.depends_on(expr.stmt))
                self.state.dependency_graph.merge(old_dependency_graph)

                if self.state.dependency_graph.get_state() == old_state:
                    # The fixed point was attained, we can stop analyzing the loop
                    break
                if nb_iterations > 5:
                    warnings.warn("More than 5 iterations needed to attain fixed point at loop %s" % str(expr.coord))
                    # import pdb; pdb.set_trace()

            return res

    @staticmethod
    def _handle_missing_expr(expr):
        raise ValueError("Expression of unknown type %s at %s" %
                         (expr.__class__, expr.coord))

    def depends_on(self, expr) -> VariableSet:
        """
        Parse the node `expr`, update the analysis state by adding variables,
        functions and updates the dependency graph of the currently parsed
        function. Also, compute the set of variables that might affect the
        value of the statement defined by `expr`. This function mainly act as
        a dispatcher, calling a different function depending on the type of node
        `expr` represents.

        Args:
            expr: Node representing an expression.

        Returns:
            :obj:`VariableSet`: Set of variables that might affect the value of `expr`.
        """

        if expr is None:
            return VariableSet()
        # print("Expr: %s" % expr.coord)
        # Pattern matching
        if logger.getEffectiveLevel() <= logging.INFO and \
                not isinstance(expr, (ID, Compound, CompoundLiteral, StructRef, DeclList, ExprList, InitList, ParamList,
                                      ArrayRef, Constant, BinaryOp, UnaryOp)):
            logger.info(str(expr.coord) + ": " + SimpleCGenerator().visit(expr))
        expr_types = {
            ArrayRef: self._handle_array_ref,
            Asm: self._handle_asm,
            Assignment: self._handle_assignment,
            BinaryOp: self._handle_binary_op,
            (Break, Constant, Continue, EmptyStatement): (lambda *args, **kwargs: VariableSet()),
            (Case, Default): self._handle_case,
            Cast: lambda e: self.depends_on(e.expr),
            Compound: self._handle_compound,
            CompoundLiteral: self._handle_compound_literal,
            (DeclList, ExprList, InitList, ParamList): self._handle_list,
            Decl: self._handle_decl,
            Enum: self._handle_enum,
            For: self._handle_for,
            FuncCall: self._handle_func_call,
            FuncDef: self._handle_func_def,
            Goto: self._handle_goto,
            ID: self._handle_id,
            (If, TernaryOp): self._handle_if,
            Label: self._handle_label,
            NamedInitializer: self._handle_named_initializer,
            Pragma: self._handle_pragma,
            Return: self._handle_return,
            StructRef: self._handle_struct_ref,
            Switch: self._handle_switch,
            Typedef: self._handle_typedef,
            Typename: lambda _: VariableSet(),
            UnaryOp: self._handle_unary_op,
            (While, DoWhile): self._handle_while,
        }

        # For easier debugging
        type_key_candidates = list(filter(lambda v: isinstance(expr, v), expr_types.keys()))
        if type_key_candidates:
            return expr_types[type_key_candidates[0]](expr)
        return self._handle_missing_expr(expr)
        # for expr_type in expr_types:
        #     if isinstance(expr, expr_type):
        #         res = expr_types[expr_type](expr)
        #         return res


