import ipywidgets as widgets
from IPython.display import display, clear_output, FileLink
import numpy as np
from PyQt5.QtWidgets import QFileDialog, QApplication

from IPython.core.display import display, HTML
import pandas as pd
import qgrid

app = QApplication([dir])
out = widgets.Output()


class Controls(object):

    def __init__(self, df):
        box_layout = {"flex_flow": "row wrap"}
        # Widget for leakage type selection
        leakage_type_labels = ["Other", "PointerDeref", "CondBranch"]
        leakage_types_list = widgets.HBox(
            [widgets.Checkbox(value=True, description=l_name) for l_name in leakage_type_labels])
        max_depth = max(10, int(max(df["Max Depth"])))
        leakage_depth_limit = widgets.IntSlider(value=10, min=1, max=max_depth, description="Max depth")
        leakage_type_accordion = widgets.Accordion([leakage_types_list, leakage_depth_limit])

        leakage_type_accordion.set_title(0, "Leakage type filtering")
        leakage_type_accordion.set_title(1, "Depth cutoff ")
        # display(leakage_type_accordion)

        # Widget for public outputs

        # Widget for leakage location
        function_names = np.unique(df["Function"])
        self.function_names = function_names

        vnames_series = df["Dependency Chain"]  # .apply(lambda x: x[2:-2].split("', '"))
        variable_names = []
        for l in vnames_series:
            variable_names.extend(l)
        variable_names = np.unique(variable_names)
        self.variable_names = variable_names

        file_names = np.unique([s.rsplit("/")[-1].split(":")[0] for s in df["Reference"]])
        self.file_names = file_names
        self.file_links = list(map(FileLink, np.unique([s.split(":")[0] for s in df["Reference"]])))

        def get_select_button(widget_list):
            select_button = widgets.Button(description="Select All")

            def select_all(b):
                for ch in widget_list:
                    ch.value = True

            select_button.on_click(select_all)
            return select_button

        def get_remove_button(widget_list):
            remove_button = widgets.Button(description="Remove All")

            def remove_all(b):
                for ch in widget_list:
                    ch.value = False

            remove_button.on_click(remove_all)
            return remove_button

        def get_checkbox_set(values, extra_list=None):
            if not extra_list:
                widget_list = [widgets.Checkbox(value=True, description=value) for value in values]
            else:
                widget_list = [widgets.HBox([widgets.Checkbox(value=True, description=value), extra_widget]) for value, extra_widget in zip(values, extra_list)]

            select_button = get_select_button(widget_list)
            remove_button = get_remove_button(widget_list)

            leakage_names = widgets.VBox([widgets.Box(widget_list, layout=box_layout),
                                          widgets.HBox([select_button, remove_button],
                                          layout={"justify_content": "center"})])
            return leakage_names

        leakage_function_names = get_checkbox_set(function_names)

        leakage_variable_names = get_checkbox_set(variable_names)

        # TODO corriger le lien vers les fichiers
        leakage_file_names = get_checkbox_set(file_names, [widgets.HTML(
            '<a href="files/{fn}" data-commandlinker-command="docmanager:open" data-commandlinker-args="{{&quot;path&quot;:&quot;{fn}&quot;}}">file</a>'.format(fn=f)) for f in self.file_names])

        leakage_location_accordion = widgets.Accordion(
            [leakage_function_names, leakage_variable_names, leakage_file_names])

        leakage_location_accordion.set_title(0, "Function names")
        leakage_location_accordion.set_title(1, "Variable names")
        leakage_location_accordion.set_title(2, "File names")

        # Widget for leakage variables

        # Global tabs
        controls = widgets.Tab([leakage_location_accordion, leakage_type_accordion])
        controls.set_title(1, "Leakage type")
        controls.set_title(0, "Leakage location")

        self.controls = controls

    def display_controls(self):
        display(self.controls)

    def get_functions_to_show(self):
        checkboxes = self.controls.children[0].children[0].children[0].children
        return [cb.description for cb in checkboxes if cb.value]

    def get_variables_to_show(self):
        checkboxes = self.controls.children[0].children[1].children[0].children
        return [cb.description for cb in checkboxes if cb.value]

    def get_files_to_show(self):
        checkboxes = self.controls.children[0].children[2].children[0].children
        return [cb.children[0].description for cb in checkboxes if cb.children[0].value]

    def get_leakage_types_to_show(self):
        checkboxes = self.controls.children[1].children[0].children
        return [cb.description for cb in checkboxes if cb.value]

    def get_maxdepth_to_show(self):
        return self.controls.children[1].children[1].value


class SelectReportButton(widgets.Button):
    """A file widget that uses PyQt5."""

    def __init__(self):
        super(SelectReportButton, self).__init__()
        # Create the button.
        self.description = "Select report"
        self.icon = "file-text-o"
        # Set on click behavior.
        self.on_click(self.openfile_dialog)

    @staticmethod
    def openfile_dialog(b):
        filename, _ = QFileDialog.getOpenFileName(None, "Select a report to analyse.", "",
                                                  "CSV files (*.csv);;All Files (*)")
        update_analysis(filename)


class UpdateButton(widgets.Button):

    def __init__(self, qgrid_widget, df, controls):
        super(UpdateButton, self).__init__()
        self.qgrid_widget = qgrid_widget
        self.df = df
        self.controls = controls

        self.description = "Update"
        self.on_click(UpdateButton.on_button_clicked)

    @staticmethod
    def filter_fun(to_display, transform=lambda x: x):
        def mask_f(x):
            if isinstance(x, str):
                return transform(x) in to_display
            elif isinstance(x, (int, float)):
                return transform(x) <= to_display
            else:
                for el in x:
                    if transform(el) not in to_display:
                        return False
                return True

        return mask_f

    @staticmethod
    def on_button_clicked(b):

        mask_var = b.df["Dependency Chain"].apply(UpdateButton.filter_fun(b.controls.get_variables_to_show()))
        mask_fun = b.df["Function"].apply(UpdateButton.filter_fun(b.controls.get_functions_to_show()))
        mask_file = b.df["Reference"].apply(UpdateButton.filter_fun(b.controls.get_files_to_show(),
                                                                    transform=lambda x: x.rsplit("/")[-1].split(":")[
                                                                        0]))
        mask_type = b.df["Leakage Type"].apply(UpdateButton.filter_fun(b.controls.get_leakage_types_to_show()))
        mask_depth = b.df["Max Depth"].apply(UpdateButton.filter_fun(b.controls.get_maxdepth_to_show()))
        b.qgrid_widget.df = b.df[mask_var & mask_fun & mask_file & mask_type & mask_depth]


def update_analysis(filename):
    with out:
        clear_output()
        display(HTML("<style>.container { width:90% !important; }</style>"))
        b = SelectReportButton()
        display(b)

        if filename:
            df = pd.read_csv(filename, sep="\t",
                             usecols=lambda x: not x.startswith("Unnamed") and x != "Count",
                             comment="#")
            df["Dependency Chain"] = df["Dependency Chain"].apply(lambda x: x[2:-2].split("', '"))
            df["Code"] = df["Code"].apply(lambda x: str(x).replace("<", "&lt;"))
            c = Controls(df)
            c.display_controls()

            qgrid_widget = qgrid.show_grid(df, show_toolbar=True, grid_options={"editable": False})
            # display(df[mask])
            button = UpdateButton(qgrid_widget, df, c)
            display(button)

            display(qgrid_widget)

            print(str(filename))


def display_analysis(filename=""):
    update_analysis(filename)
    display(out)

