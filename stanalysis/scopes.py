"""
scopes
======
The module :mod:`scopes` contains classes that handle modifications to the analysis scope. For more
information, refer to the :class:`Scope` class.
"""

from collections import namedtuple
from contextlib import contextmanager
import enum
from typing import List, Tuple, Set, Union, Optional, Iterable, Dict
import warnings

from xml.dom import minidom
from xml.dom.minidom import getDOMImplementation
from xml.parsers.expat import ExpatError

from pycparser.plyparser import Coord


from .common import Variable, VariableType, XmlExportMixin, VariadicFunctionDefinition, VariableSet, TypedefInfo, \
    ControlDependenciesHandling
from .dependencygraph import DependencyGraph

FunctionType = Union[VariadicFunctionDefinition, "FunctionDefinition"]


class UnknownVariableError(ValueError):
    pass


class LeakageType(enum.Enum):
    Other = 1
    CondBranch = 2
    PointerDeref = 3
    SpectreVulnerability = 4
    PotentialBranchShortcut = 5
    PotentialBranchBitwise = 6


class LeakageCoord:
    def __init__(self):
        self.coords = []  # List[Tuple[str, Tuple]]

    @staticmethod
    def from_coords(coords: List[Tuple[str, Tuple]]) -> "LeakageCoord":
        res = LeakageCoord()
        res.coords = list(coords)
        return res

    @staticmethod
    def from_coord(coord: str) -> "LeakageCoord":
        return LeakageCoord.from_coords([(coord, ())])

    def add_leaking_call(self, coord: str, fun_name: str) -> "LeakageCoord":
        res = self.copy()
        res[-1] = ("(%s) %s" % (fun_name, res[-1][0]), res[-1][1])

        res.append((coord, ()))

        return res

    def finalize(self, function_name: str) -> "LeakageCoord":
        res = self.copy()
        res[-1] = ("(%s) %s" % (function_name, res[-1][0]), res[-1][1])
        return res

    def get_calls(self) -> tuple:
        return tuple([l[0] for l in self.coords])

    def get_leakage_coord(self) -> str:
        return self.coords[0][0]

    def get_leakage_call_coord(self) -> str:
        return self.coords[-1][0]

    @staticmethod
    def from_str(s: str) -> "LeakageCoord":
        res = LeakageCoord()
        res.coords = []
        for l in s.split("+"):
            v = l.split("|")
            res.coords.append((v[0], tuple(v[1:])))
        return res

    def to_str(self) -> str:
        return "+".join(["|".join([l[0]] + list(l[1])) for l in self.coords])

    def copy(self) -> "LeakageCoord":
        return LeakageCoord.from_coords(self.coords)

    def append(self, value: Tuple[str, Tuple]):
        self.coords.append(value)

    def __getitem__(self, item: int) -> Tuple[str, Tuple]:
        return self.coords[item]

    def __setitem__(self, key: int, value: Tuple[str, Tuple]):
        self.coords[key] = value

    def __len__(self) -> int:
        return len(self.coords)

    def __iter__(self):
        return self.coords.__iter__()

    def __reversed__(self):
        return reversed(self.coords)

    def asXml(self):
        return getDOMImplementation().createDocument(None, "some_tag", None).createTextNode(self.to_str())


class LeakageEvent(namedtuple("LeakageEvent", "variable dependencies coord leakage_type"),
                   XmlExportMixin):
    __slots__ = ()
    tag_to_class = {
        "variable": Variable,
        "at": DependencyGraph.GraphLink,
        "coord": LeakageCoord
    }

    @classmethod
    def fromXml(cls, element):
        coord = LeakageCoord.from_str(cls.importXmlString(element, "coord"))
        var_element = element.getElementsByTagName("variable")[0]. \
            getElementsByTagName("variable")[0]

        dependencies = cls.importXmlList(element, "dependencies")
        leakage_type_value = cls.importXmlString(element, "leakage_type")
        leakage_type = cls.enum_modifier(LeakageType)(leakage_type_value) if leakage_type_value\
            else LeakageType.Other

        var = Variable.fromXml(var_element)
        return LeakageEvent(variable=var, dependencies=dependencies, coord=coord,
                            leakage_type=leakage_type)

    def get_leakage_coord(self) -> str:
        return self.coord.get_leakage_coord()

    def get_leakage_call_coord(self) -> str:
        return self.coord.get_leakage_call_coord()

    def key(self) -> Tuple[Variable, str, str, LeakageType]:
        return self.variable, self.get_leakage_coord(), str(self.coord), self.leakage_type


class LeakageEvents:
    def __init__(self, leakages: List[LeakageEvent] = None):
        self.leaking_vars = {}
        if leakages:
            for el in leakages:
                k = el.key()
                self.leaking_vars.setdefault(k, [])
                self.leaking_vars[k].append(el)

    def get_vars(self, ignore_types=None) -> Set[Variable]:
        if ignore_types is None:
            ignore_types = []

        res = set()
        for elements in self.leaking_vars.values():
            for el in elements:
                if el.leakage_type not in ignore_types:
                    res.add(el.variable)
        return res

    def get_equivalent_leakage_events(self, leakage_event: LeakageEvent):
        return self.leaking_vars.get(leakage_event.key(), None)

    def clear(self):
        self.leaking_vars.clear()

    def extend(self, leakages: List[LeakageEvent]):
        for el in leakages:
            k = el.key()
            self.leaking_vars.setdefault(k, [])
            self.leaking_vars[k].append(el)

    def append(self, leakage: LeakageEvent):
        k = leakage.key()
        self.leaking_vars.setdefault(k, [])
        self.leaking_vars[k].append(leakage)

    def __iter__(self):
        for elements in self.leaking_vars.values():
            for element in elements:
                yield element


class LeakageTrace:
    class FunctionTrace(namedtuple("FunctionTrace", "name inst_coord dependency_chain")):
        def get_dependency_chain(self):
            res = []
            for dep in self.dependency_chain:
                if dep[1]:
                    var_name = dep[1][:dep[1].find("{")]
                    if var_name not in res:
                        res.append(var_name)
            return res

    """
    Holds the structured representation of a leakage event. It comprises
    the series of function calls that lead to the leakage, as well as the variable
    dependency chain inside each function that explains why the leaking variable
    depends on sensitive data.
    """

    def __init__(self, leakage_coord: LeakageCoord, dependencies: List[str]):
        """
        Parse the leakage text into a more structured format. The leakage text
        is supposed to respect the following format:
            Function calls are delimited via the pipe | character
            The function name is given between at the beginning of the string, or just after
            a |, between parantheses.
            Dependency-chain elements are delimited via the + character
            More formally, the format is:
             \(function_name\) leaking_inst_coord(+dependency_coord \(variable_name\{scope\}\[structure\] secret\)|)*
            with, well, no trailing "|"

        Args:
            leakage_coord (:obj`LeakageCoord`): The coordiantes to be parsed.
            dependencies(List): The list of dependencies
        """
        self.trace = []
        self.type = None

        for i, (inst, deps) in enumerate(reversed(leakage_coord)):
            dependency_chain = []
            if i == 0:
                deps = dependencies
            function_name, leaking_inst_coord = inst[1:].split(") ", 1)
            for dep in deps:
                if " (" in dep[:-8]:
                    dep_coord, variable = dep[:-8].split(" (")
                else:
                    dep_coord = dep[:-8]
                    variable = None
                dependency_chain.append((dep_coord, variable))
            self.trace.append(LeakageTrace.FunctionTrace(function_name, leaking_inst_coord, dependency_chain))

    def get_dependency_chain(self):
        """
        Get the dependency chain, that is, the list of variables, depending on each other, where
        the last variable leaks.
        """
        res = []
        for function_call in self.trace:  # type: LeakageTrace.FunctionTrace
            for dep in function_call.dependency_chain:
                var_name = dep[1][:dep[1].find("{")] if dep[1] else ""
                if var_name not in res:
                    res.append(var_name)
        return res


class FunctionDefinition(namedtuple("function", "name arguments dependency_graph "
                                                "leaking_vars return_vars is_complete "
                                                "label_groups"),
                         XmlExportMixin):
    """
    A class representing a function definition. This is used to determine how a
    function call affects variables. A `FunctionDefinition` is created at the
    start of a `FuncDef` node, and completed when all instructions have been
    parsed. It is represented by the following attributes:

    Attributes:
        name (:obj:`str`): function name.
        arguments (:obj:`List[Variable]`): the list of variables representing the
            function arguments.
        dependency_graph (:obj:`DependencyGraph`): dependencies between variables of
            that function.
        leaking_vars (:obj:`LeakageEvents`): leakage events happening during function
            execution.
        return_vars (:obj:`Set[Variable]`): variables affecting the return value.
        is_complete (:obj:`Set[bool]`): non empty if the function body has been correctly
            parsed.
        label_groups (:obj:`Dict[str, List]`): Instruction groups executed after a goto,
            indexed by label name
    """
    tag_to_class = {
        "variable": Variable,
        "dependency_graph": DependencyGraph,
        "leakage_event": LeakageEvent,
    }

    def asXml(self, fields_to_export=None):
        return super(FunctionDefinition, self).asXml(
            fields_to_export=["name", "arguments", "dependency_graph", "leaking_vars",
                              "return_vars"])

    @classmethod
    def fromXml(cls, element):
        func_name = cls.importXmlString(element, "name")
        arguments = cls.importXmlList(element, "arguments")
        leaking_vars = LeakageEvents(cls.importXmlList(element, "leaking_vars"))
        return_vars = set(cls.importXmlList(element, "return_vars"))
        dependency_graph_element = element.getElementsByTagName("dependency_graph")[0]. \
            getElementsByTagName("dependency_graph")[0]
        dependency_graph = DependencyGraph.fromXml(dependency_graph_element)
        return FunctionDefinition(func_name, arguments, dependency_graph, leaking_vars,
                                  return_vars, {True}, set())

    @classmethod
    def new(cls, name: str, arguments: List[Variable], is_complete: bool = True,
            return_vars: Optional[Set[Variable]] = None) -> "FunctionDefinition":
        """
        Return a new empty FunctionDefinition object

        Args:
            name (:obj:`str`): The name of the function.
            arguments (:obj:`List[Variable]`): List of arguments of the function.
            is_complete (:obj:`bool`): Set to true if the definition of the function is known
            return_vars(:obj:`Set[Variable]`): Set that either contains no variable, or exactly
                one which is the return variable, with name \\RET.

        Returns:
            :obj:`FunctionDefinition`: A stub function definition to be completed at a later point.
        """
        if not return_vars:
            return_vars = set()
        return FunctionDefinition(name=name, arguments=arguments, dependency_graph=DependencyGraph(),
                                  leaking_vars=LeakageEvents(), return_vars=return_vars,
                                  label_groups={}, is_complete={True} if is_complete else set())

    def get_ret_var(self) -> Union[None, Variable]:
        """
        Return the special variable representing the return value, or `None` if no return value
        is expected (for void functions, or functions returning function pointer)

        Returns:
            :obj:`Variable` that represents the return value, or `None`.
        """
        if self.return_vars:
            return next(self.return_vars.__iter__())
        return None

    def get_ret_var_set(self) -> "VariableSet":
        """Return the VariableSet corresponding to this function."""
        res = VariableSet()
        ret_var = self.get_ret_var()

        if not ret_var:
            return res

        for var in self.dependency_graph.variables():
            if not var.name.startswith(ret_var.name):
                continue
            # var is now either ret_var, at some indirection level,
            # or a member field
            field_name = "" if var.name == ret_var.name else var.name.replace(ret_var.name+".", "")
            res.add_vars_to_field(self.dependency_graph.get_dependency_set(var), field_name, var.indirection)
        return res

    def __str__(self) -> str:
        return "{name}\nArgs: {args}\nRet: {ret_value}\n{dep_graph}\n"\
            "Leaking variables: {leaking_vars}"\
            .format(
                name=self.name,
                args=", ".join(map(str, self.arguments)),
                ret_value=str(list(self.return_vars)[0]),
                dep_graph=str(self.dependency_graph),
                leaking_vars="\n  ".join(map(str, self.leaking_vars))
            )


class Scope(object):
    """
    A class representing a scope. A scope is defined by the following attributes:

    Attributes:
        vars (:obj:`Set[Variable]`): The set of all variables defined in this scope. Only a
            single instance of each variable may be present in this set at any
            time. The indirection level of that instance is the indirection
            level it was defined with. For every function, a corresponding
            variable of type `FunctionPointer` is also created.
        functions (:obj:`Dict[str, Union[FunctionDefinition, VariadicFunctionDefinition]]`):
            A dictionary, indexed by the function names,
            that contains all functions declared in that scope. The elements
            in the dictionary are either of type `FunctionDefinition`, or
            `VariadicFunctionDefinition` (for variadic functions).
        structs (:obj:`Dict[str, List[Variable]`): A dictionary, indexed by the struct names, that
            contains all structure definitions defined in that scope. A struct
            is represented by a list of Variables.
        typedefs (:obj:`Dict[str, str]`): A mapping between typedef names and struct names.
        extra_dependencies (:obj:`Set[Variable]`): The set of all variables that induce extra
            dependencies on the dependencies induced by expressions in the
            current scope (mainly, variables in the if and while condition,
            in the switch condition, etc.).
    """

    def __init__(self):
        self.vars = set()
        self.functions = {}
        self.structs = {}
        self.typedefs = {}
        self.extra_dependencies = set()

    def get_variable_by_name(self, name: str) -> Optional[Variable]:
        """Return the variable with name `name`, or `None` if not present."""
        for v in self.vars:
            if v.name == name:
                return v
        return None

    def get_function_by_name(self, name: str) -> Optional[FunctionType]:
        """Return the function with name `name`, or `None` if not present."""
        return self.functions.get(name, None)

    def import_functions(self, filename: str):
        """Import all the functions declared in `filename` into the current scope."""
        try:
            doc = minidom.parse(filename)
        except FileNotFoundError:
            # File does not exist yet, no worries, abort import
            return
        except ExpatError:
            # Emtpy file, also ignore import
            return

        # Now, import all functions and all variadic functions
        for fun_elem in doc.documentElement.getElementsByTagName("function_definition"):
            fd = FunctionDefinition.fromXml(fun_elem)
            self.functions[fd.name] = fd
            self.vars.add(Variable.new(fd.name)._replace(var_type=VariableType.FUNCTION_PTR,
                                                         indirection=1, scope=-1))

        for fun_elem in doc.documentElement.getElementsByTagName("variadic_function_definition"):
            fd = VariadicFunctionDefinition.fromXml(fun_elem)
            self.functions[fd.name] = fd

    def export_functions(self, filename: str):
        """Export all functions declared in the current scope."""
        impl = getDOMImplementation()
        doc = impl.createDocument(None, "functions", None)
        for fun in sorted(self.functions.values(), key=lambda x: x.name.lower()):
            if fun.is_complete:
                doc.documentElement.appendChild(fun.asXml())
        with open(filename, "w") as f:
            doc.writexml(f, indent="", addindent='\t', newl="\n")

    def __str__(self):
        return "Vars: {%s}, functions: {%s}" % (
            ", ".join(map(lambda v: v.name, self.vars)),
            ", ".join(self.functions.keys())
        )

    def __repr__(self):
        return self.__str__()


class AnalysisState(object):
    """
    The class representing the analysis state for a single file.

    Attributes:
        scopes (:obj:`List[Scope]`): The list of Scopes. The last element is the current
            scope, and after a scope has been parsed, it is popped from the
            list of current scopes. Can never be empty.
        function_stack (:obj:`List[Union[FunctionDefinition, VariadicFunctionDefinition]]`):
            The list of functions being parsed. The last element is the current function
            being parsed.
        global_dependencies (:obj:`DependencyGraph`): Dependencies between global
            variables (that do not belong to any function).
        parse_options (:obj:`Dict`): Other options relating to source code parsing.
            Options that can be used are:

                * ``functions_file`` (:obj:`str`): filename from which functions will be imported\
                and into which they will be exported.

                * ``filenames`` (:obj:`List[str]`): list of the names of the files being parsed.

                * ``cond_move`` (:obj:`bool`): if set to true, ternary conditions are supposed to be\
                implemented using conditional moves, and thus are not leaking\
                timing information.

                * ``add_only`` (:obj:`bool`): if set to true, dependencies are only added\
                (quickfix for dependency graph merging which takes far too\
                much time). Because this option needs to be potentially\
                retained across several scopes, it's better to declare\
                it here instead of Scope.

                * ``spectre_vulnerability`` (:obj:`bool`): if set to true, emit warnings when\
                potential spectre-type vulnerabilities are found in the code.

    """

    def __init__(self):
        self.potential_leakages_already_covered = False
        self.scopes = [Scope()]
        self.function_stack = []
        self.stack_saved_structures = []
        self.global_dependencies = DependencyGraph()
        self.parse_options = {}
        self.tagsHandler = None
        self.function_setup = False

    def push_scope(self, scope: Optional[Scope] = None):
        """
        Push the scope `scope` to the list of scopes. If no scope is provided,
        a new empty scope is pushed instead.
        """
        if scope is not None:
            self.scopes.append(scope)
        else:
            self.scopes.append(Scope())

    def pop_scope(self):
        """Remove the last scope from the list of scopes.

        Returns:
            :obj:`Scope`: The last scope of the stack
        """
        return self.scopes.pop()

    def get_scope(self) -> Scope:
        """Return the current scope object"""
        return self.scopes[-1]

    def push_function(self, fun: FunctionType):
        """Push the function `function` to the scope."""
        # print("Pushed %s" % fun.name)
        self.function_stack.append(fun)

    def pop_function(self) -> FunctionType:
        """Pop the latest function from the scope."""
        # print("Popped %s" % self.function_stack[-1].name)
        return self.function_stack.pop()

    def get_function(self) -> Optional[FunctionType]:
        """Return the currently parsed function."""
        if self.function_stack:
            return self.function_stack[-1]
        else:
            return None

    def save_scopes(self) -> Tuple[List[Scope], Optional[FunctionType]]:
        """
        Returns all scopes (except the global one) and the current function being parsed.
        These objects are removed from the scope stack and the function stack, respectively.
        This is used when an unknown (not yet parsed) function is being called and this
        function needs to be parsed.

        Returns:
            Tuple[List[Scope], FunctionDefinition]: The parsing structures being saved.
        """
        old_scopes = []
        while len(self.scopes) > 1:
            old_scopes.append(self.pop_scope())
        old_fdef = self.pop_function() if self.function_stack else None

        return old_scopes, old_fdef

    def restore_scopes(self, saved_structures: Tuple[List[Scope], Optional[FunctionType]]):
        """Restores saved scopes and function definitions being parsed.

        Args:
            saved_structures (Tuple[List[Scope], FunctionDefinition]): the structures returned by
                `save_scopes`.

        Raises:
            ValueError: `saved_structures` is not a tuple of an iterable of scopes, and a
                FunctionDefinition object.
        """

        try:
            (old_scopes, old_fdef) = saved_structures
        except ValueError:
            raise ValueError("`saved_structures` must be a Tuple or List of 2 elements.")

        if not isinstance(old_fdef, FunctionDefinition) and old_fdef is not None:
            raise ValueError("Second element must be of type FunctionDefintion.")

        try:
            for old_scope in reversed(old_scopes):
                self.push_scope(old_scope)
            if old_fdef is not None:
                self.push_function(old_fdef)
                # In this case, we might need to graft new global variables into its dependency graph
                dep_graph = self.function_stack[-1].dependency_graph  # DependencyGraph
                dep_graph.structs = self.structs
                for variable in self.scopes[0].vars:
                    if variable.is_global() and variable not in dep_graph.value_dependencies and\
                            variable.var_type != VariableType.FUNCTION_PTR:
                        if self.is_variable_concerned(variable.name, dep_graph.ignored_variables):
                            dep_graph.ignored_variable_names.add(variable.name)
                        if self.is_variable_concerned(variable.name, dep_graph.sensitive_variables):
                            dep_graph.sensitive_variable_names.add(variable.name)
                        dep_graph.graft_global_variable(variable)
        except TypeError:
            raise ValueError("First element must be an iterable.")

    @contextmanager
    def saved_scopes(self):
        saved_structures = self.save_scopes()
        self.stack_saved_structures.append(saved_structures)
        try:
            yield
        finally:
            self.restore_scopes(saved_structures)
            self.stack_saved_structures.pop()

    def get_option(self, option_name: str):
        return self.parse_options.get(option_name, None)

    def set_option(self, option_name: str, option_value):
        self.parse_options[option_name] = option_value

    def get_variable_by_name(self, name: str) -> Variable:
        """
        Search the variable that has name `name` in all known scopes. The
        variable of the innermost scope (that is, maximum scope property)
        is returned, with the correct indirection level.

        Args:
            name (:obj:`str`): Name of the variable to look up.

        Returns:
            :obj:`Variable`: A variable with name `name`.

        Raises:
            ValueError: No variable named `name` is found.
        """
        for scope in reversed(self.scopes):
            var = scope.get_variable_by_name(name)
            if var is not None:
                return var
        raise UnknownVariableError("Unknown variable with name %s" % name)

    def add_dependencies(self, var: Variable, dependencies: Iterable[Variable], coord: Union[Coord, str]):
        """
        Add a dependency between `var` and `v` for every `v` in `dependencies` to
        the relevant dependency graph, that is, that of the current parsing
        function, or `global_dependencies` if no function is being parsed. If var is
        a function pointer, a new function pointer is created instead, its definition
        being imported from the function pointer in `dependencies`, if present.

        Args:
            var (:obj:`Variable`): Variable to which dependencies will be added.
            dependencies (:obj:`Iterable[Variable]`): List or set of
                Variables upon which `var` depends on.
            coord (:obj:`str`): Source file, line and column which holds the operation that
                creates this dependency.

        Raises:
            ValueError: `var` is not a variable, or `var` is a function pointer and
                `dependencies` contains anything but one variable that is also a function
                pointer, pointing to a known function.
        """
        self.add_multiple_dependencies({var: dependencies}, coord)

    def import_structs(self, dependency_graph: DependencyGraph):
        structs = {struct_name: s
                   for scope in self.scopes
                   for (struct_name, s) in scope.structs.items()}
        dependency_graph.structs = structs

    def add_multiple_dependencies(self, dependency_vars: Dict[Variable, Iterable[Variable]], coord: Union[Coord, str]):
        """
        Add the dependencies described in `dependency_vars` to
        the relevant dependency graph, that is, that of the current parsing
        function, or `global_dependencies` if no function is being parsed. For function
        pointers, a new function pointer is created instead, its definition
        being imported from the function pointer in `dependencies`, if present.

        Args:
            dependency_vars (:obj:`Dict[Variable, Iterable[Variable]]`): Dependency as a dictionary,
             indexed by the dependent variables, and with the set of dependencies as values.
            coord (:obj:`str`): Source file, line and column which holds the operation that
                creates this dependency.

        Raises:
            ValueError: `var` is not a variable, or `var` is a function pointer and
                `dependencies` contains anything but one variable that is also a function
                pointer, pointing to a known function.
        """
        add_only = self.parse_options.get("add_only", False)

        # extended_dependency_vars = OrderedDict()

        # The dependency graph needs to be aware of all structs that
        # are defined during parsing

        self.import_structs(self.dependency_graph)
        structs = self.dependency_graph.structs
        extended_dependency_vars = {}  # contains the dependencies that do not involve function pointers
        for (var, dependencies) in dependency_vars.items():

            if not isinstance(var, Variable):
                raise ValueError("Argument var must be of type Variable, got %s instead." % type(var))

            if var.var_type == VariableType.FUNCTION_PTR and dependencies:
                # If var is a function pointer, we just create a new function
                # with the definition of the only element in dependencies
                self._add_function_pointer_dependencies(var, dependencies, coord)
                self.dependency_graph.add_dependencies(var, dependencies, coord)
            else:
                # If not, var is a standard variable, and we should add dependencies to
                # the appropriate dependency graph
                # TODO dependencies is a VariableSet, and it might contain dependencies at various indirection levels
                # TODO and variable members ! These must be taken into account !!!!
                # filtered_dependency_vars[var] = dependencies

                def add_member_dependencies(var, dependencies):
                    if var.var_type == VariableType.FUNCTION_PTR:
                        self._add_function_pointer_dependencies(var, dependencies, coord)
                        return

                    if isinstance(dependencies, set) or not var.struct_type or not dependencies.struct_vars \
                            or var.struct_type not in structs:
                        if isinstance(dependencies, set):
                            extended_dependency_vars.setdefault(var, set())
                            extended_dependency_vars[var].update(dependencies)
                        elif isinstance(dependencies, VariableSet):
                            for indirection_mod in dependencies.vars:
                                indir_var = var.indirect(indirection_mod)
                                extended_dependency_vars.setdefault(indir_var, set())
                                extended_dependency_vars[indir_var].update(dependencies.vars[indirection_mod])

                    else:
                        for member_var in structs[var.struct_type]:
                            if member_var.name in dependencies.struct_vars:
                                add_member_dependencies(
                                    member_var._replace(name=var.name+"."+member_var.name,
                                                        scope=var.scope),
                                    dependencies.struct_vars[member_var.name])

                add_member_dependencies(var, dependencies)

                # Also add dependencies for different indirection levels
                for indirection_mod in range(1, var.indirection+1):
                    indir_dependencies = dependencies.get_vars(-indirection_mod)
                    indir_var = var.indirect(-indirection_mod)
                    if indir_dependencies:
                        extended_dependency_vars.setdefault(indir_var, VariableSet())
                        extended_dependency_vars[indir_var].update(indir_dependencies)

        self.dependency_graph.add_multiple_dependencies(extended_dependency_vars, coord, add_only)

        # if extended_dependency_vars:
        #     dependency_graph.add_multiple_dependencies(extended_dependency_vars, coord, add_only)

    def _add_function_pointer_dependencies(self, var: Variable, dependencies: Set[Variable],
                                           coord: Union[Coord, str]):
        """ Assuming `var` is a function pointer, creates a new function with its name, which
        corresponds to the first function in `dependencies`. If there is more than one function or
        function pointer in `dependencies`, or there is a non-function pointer variable in
        `dependencies`, or the function in `dependencies` is not fully defined, emits a warning."""
        verbose = self.parse_options.get("verbose", True)

        if not var.var_type == VariableType.FUNCTION_PTR or var.indirection == 0:
            return

        if len(dependencies) > 1 and verbose:
            warnings.warn("Ambiguous function pointer dependency for pointer %s at %s" %
                          (var.name, coord))

        if not dependencies:
            warnings.warn("No dependency for function pointer %s at %s" % (str(var), str(coord)))
            return

        f_var = list(dependencies)[0]
        if f_var.var_type != VariableType.FUNCTION_PTR and verbose:
            warnings.warn("Trying to assign a function pointer to a non \
                function pointer-registered variable at %s" % coord)
            return
        # If no function corresponding to the element in dependencies has been encountered,
        # we must throw an error
        f_name = f_var.name
        f_def = self.functions.get(f_name, None)
        # If var is a global variable, add the function pointer info to the global scope. Else,
        # it is probably a local function pointer, and therefore add it to the local scope.
        if 0 <= var.scope+1 < len(self.scopes):
            dict_to_modify = self.scopes[var.scope+1].functions
        else:
            dict_to_modify = self.get_scope().functions
        if f_def is None or not f_def.is_complete:
            # Could be a function pointer as argument. Brrrr... but this exists
            # Emit a warning, and use a variadic function template, because
            # that's easier
            if verbose:
                warnings.warn("Unknown function %s at %s." % (f_name, coord), UserWarning)

            #dict_to_modify[var.name] = self.impor
            dict_to_modify[var.name] = VariadicFunctionDefinition.new(var.name)
        else:
            dict_to_modify[var.name] = f_def._replace(name=var.name)

    # Here, we define getters (but not setters) for the main attributes of
    # Scope and FunctionDefinition.
    def __getattr__(self, name):
        if name in self.__dict__:
            return self.__dict__[name]

        # For fields belonging to function definition, just return the field
        if name in ["leaking_vars", "return_vars", "is_complete", "label_groups"]:
            return getattr(self.get_function(), name)

        # For most fields belonging to scope, look a all scopes and add the elements
        if name in ["vars", "functions", "structs", "typedefs"]:
            el = getattr(self.scopes[0], name)
            if isinstance(el, set):
                res = set()
            elif isinstance(el, dict):
                res = {}
            else:
                raise ValueError(type(el))
            for scope in self.scopes:
                res.update(getattr(scope, name))
            return res
        else:
            raise ValueError("Unknown attribute %s" % name)

    def replace_dependency_graph(self, dep_graph: DependencyGraph):
        graph = self.get_function().dependency_graph if self.function_stack else self.global_dependencies

        graph.clear()
        graph.merge(dep_graph)

    @property
    def dependency_graph(self) -> DependencyGraph:
        if self.function_stack:
            return self.get_function().dependency_graph
        else:
            return self.global_dependencies

    @property
    def scope_depth(self) -> int:
        """
        Return the current scope depth. The initial scope has depth -1.
        """
        return len(self.scopes) - 2

    @property
    def extra_dependencies(self) -> Set[Variable]:
        """Return all extra dependencies contained in all currently parsed scopes."""
        return set([dep for scope in self.scopes for dep in scope.extra_dependencies])

    def add_extra_dependencies(self, extra_dependencies: Iterable[Variable], safe_branch: bool,
                               control_dependencies: ControlDependenciesHandling):
        """
        Add all variables in `extra_dependencies` to the set of variables that might affect
        variable assignments. Filter out pointers and function pointers, they should not be added.

        Args:
            extra_dependencies (:obj:`Set[Variable]`): Set of Variables to add
                to the additional dependencies.
            safe_branch (bool): whether or not the leakage of the current branch is suppressed
            control_dependencies: the setting used by the current analysis to determine the
                control dependency handling policy.
        """
        if control_dependencies == ControlDependenciesHandling.NONE or \
                (safe_branch and control_dependencies == ControlDependenciesHandling.LESS):
            self.set_option("no_extra_dependencies", True)

        if not self.get_option("no_extra_dependencies"):
            for v in extra_dependencies:
                if v.indirection == 0:
                    for dep in self.dependency_graph.get_dependency_set(v):
                        if dep.var_type != VariableType.FUNCTION_PTR and dep.indirection == 0:
                            self.get_scope().extra_dependencies.add(dep)
            # self.get_scope().extra_dependencies.update(
            #     dep for v in
            #     filter(lambda x: x.var_type != VariableType.FUNCTION_PTR and x.indirection == 0, extra_dependencies)
            #     for dep in self.dependency_graph.get_dependency_set(v)
            # )

        self.set_option("no_extra_dependencies", False)

    def add_struct(self, struct_name: str, members: List[Variable]):
        """
        Add a new structure named `struct_name` with `members` as structure members to
        the dictionary of currently known structs.

        Args:
            struct_name (:obj:`str`): Name of the structure
            members (:obj:`List[Variable]`): List of variables that define the structure

        """
        self.get_scope().structs[struct_name] = members

    @staticmethod
    def is_match(fields_and_structs, key_fields):
        # Test whether `key_fields` is a "prefix" of `fields_and_structs`
        if len(fields_and_structs) < len(key_fields):
            return False

        for (struct, field), key_field in zip(fields_and_structs, key_fields):
            if key_field not in ("[%s]" % struct, field, "*"):
                return False

        return True

    def is_variable_concerned(self, var_name: str, var_set: Iterable[str]) -> bool:
        var_name_fields = var_name.split(".")

        # Determine the intermediate structures
        var_name_fields_and_struct_types = []
        intermediary_name = ""
        for field_name in var_name_fields:
            intermediary_name += field_name + "."
            intermediary_variable = self.get_variable_by_name(intermediary_name[:-1])
            var_name_fields_and_struct_types.append((intermediary_variable.struct_type, field_name))

        for var_name_key in var_set:
            var_name_key_fields = var_name_key.split(".")
            for i, (struct_type, field_name) in enumerate(var_name_fields_and_struct_types):
                if var_name_key_fields[0] in ("*", "[%s]" % struct_type, field_name):
                    if self.is_match(var_name_fields_and_struct_types[i+1:], var_name_key_fields[1:]):
                        return True

        return False

    def add_var(self, var: Variable, coord: str = "", clear: bool = False):
        """
        Add the variable `var` to the set of known variables. Also update the list of variables
        to ignore.

        Args:
            var (:obj:`Variable`): Variable to add.
            coord (str): Coordinates of the instruction causing the variable to be added.
            clear (bool): If set to true, remove any existing dependencies
        """
        dep_graph = self.dependency_graph

        self.import_structs(dep_graph)

        for member_var in var.get_all_members(self.structs):
            self.get_scope().vars.add(member_var)

            # Update the set of ignored and sensitive variables
            if self.is_variable_concerned(member_var.name, dep_graph.ignored_variables) or \
                    self.is_variable_concerned(var.name, dep_graph.ignored_variables):
                dep_graph.ignored_variable_names.add(member_var.name)
            if self.is_variable_concerned(member_var.name, dep_graph.sensitive_variables) or \
                    self.is_variable_concerned(var.name, dep_graph.sensitive_variables):
                dep_graph.sensitive_variable_names.add(member_var.name)

        if var.var_type != VariableType.ENUM_VALUE and not self.function_setup:
            if var in dep_graph.variables() and var.scope > 0 and clear:
                dep_graph.clear_dependencies(var, coord)
            dep_graph.add_local_variable_dependencies(var, coord)

    def add_typedef(self, name: str, typedef_info: TypedefInfo):
        self.get_scope().typedefs[name] = typedef_info

    def add_function(self, f_def: FunctionType):
        self.get_scope().functions[f_def.name] = f_def

    def import_functions(self, filename: str):
        """Import functions from `filename` into the current scope."""
        self.get_scope().import_functions(filename)

    def export_functions(self, filename: str):
        """Export functions from current scope into `filename`."""
        self.get_scope().export_functions(filename)

    # This context manager is used by control flow expressions to signal that
    # only potential leakages (due to the use of binary boolean or bitwise expressions)
    # should not be reported, because the values leak anyway due to explicit control flow
    @contextmanager
    def ignore_potential_leakages(self, should_ignore: bool):
        if should_ignore:
            self.potential_leakages_already_covered = True
            yield
            self.potential_leakages_already_covered = False
        else:
            yield

    # The contextmanager functions. Useful to avoid any problems with cleanup for example
    @contextmanager
    def new_scope(self):
        """Context manager to be used when a new scope needs to be created. Yields that scope."""
        self.push_scope(None)
        yield self.get_scope()
        self.pop_scope()
        self.dependency_graph.eliminate_local_vars(local_scope=self.scope_depth, close_graph=False)

    @contextmanager
    def new_function_def_scope(self):
        """Context manager to be used when a new scope needs to be created as part of a
        function setup. Yields that scope."""
        self.push_scope(None)
        self.function_setup = True
        yield self.get_scope()
        self.pop_scope()
        self.function_setup = False

    @contextmanager
    def new_function(self, f_def: FunctionDefinition):
        """
        Context manager to be used when a new function is being parsed. Pushes a new scope
        and a new function and yields the tuple (scope, function).
        """
        self.push_scope(None)
        self.push_function(f_def)
        self.function_setup = False

        yield self.get_scope(), self.get_function()

        self.pop_scope()
        self.pop_function()

    @contextmanager
    def dependencies_add_only(self):
        """
        Context manager to be used when dependencies should only be added when
        parsing some code blocks.
        """
        if self.parse_options.get("add_only", False):
            yield
            return

        self.parse_options["add_only"] = True
        yield
        self.parse_options["add_only"] = False
