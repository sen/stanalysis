# ---------------------------------------------------------------------
# cache_eval: switch_cfg.py
#
# Determines whether a switch block must break, might break, or
# never breaks.
#
# Alexander Schaub
# License: BSD
# ---------------------------------------------------------------------

import enum
from typing import Optional, Union as TypeUnion

from pycparser.c_ast import *


class SwitchBreakType(enum.Enum):
    NEVER = 1
    MAY = 2
    MUST = 3

    def __or__(self, other):
        # Ternary OR:
        # MUST | x = MUST
        # MAY | MAY = MAY | NEVER = MAY
        # NEVER | NEVER = NEVER

        if SwitchBreakType.MUST in (self, other):
            return SwitchBreakType.MUST
        elif SwitchBreakType.MAY in (self, other):
            return SwitchBreakType.MAY
        else:
            return SwitchBreakType.NEVER


def truth_table_if_else(state_if: SwitchBreakType, state_else: SwitchBreakType) -> SwitchBreakType:
    # Determines the swith state after an If / Else branch has been analyzed
    # If one of both branches has state MAY, or if both branches have different
    # states, then the whole If / Else branch MAY break. If both branches have the same
    # state, then the whole branch has this same state
    if state_if != state_else:
        return SwitchBreakType.MAY
    else:
        return state_if


class SwitchControlFlowVisitor(NodeVisitor):
    """Return the first node with the correct filename and line number."""

    def __init__(self, break_types=(Break, Return)):
        super().__init__()
        self.break_types = break_types

    def visit(self, node: Node) -> SwitchBreakType:
        method = 'visit_' + node.__class__.__name__

        return getattr(self, method, self.generic_visit)(node)

    def visit_If(self, node: If) -> SwitchBreakType:
        state_if = SwitchControlFlowVisitor().visit(node.iftrue)
        state_else = SwitchControlFlowVisitor().visit(node.iffalse)
        return truth_table_if_else(state_if, state_else)

    def visit_While(self, node: While) -> SwitchBreakType:
        return SwitchControlFlowVisitor(break_types=(Return,)).visit(node.stmt)

    def visit_For(self, node: For) -> SwitchBreakType:
        return SwitchControlFlowVisitor(break_types=(Return,)).visit(node.stmt)

    def visit_DoWhile(self, node: DoWhile) -> SwitchBreakType:
        return SwitchControlFlowVisitor(break_types=(Return,)).visit(node.stmt)

    def visit_Switch(self, node: Switch) -> SwitchBreakType:
        # A nested switch is possible and requires extra care
        # The switch MUST break iff every case MUST break with a Return statement
        # Breaks inside cases are irrelevant in the analysis here (but they will
        # become relevant later on when the nested Switch will be analyzed by the
        # main class LeakageAnalysis)
        # In practice, compute the state as follows: accumulate
        # the state for each Case statement using truth_table_if_else as the
        # accumulator function

        res = None
        for case in node.stmt.block_items:
            case_state = SwitchControlFlowVisitor(break_types=(Return, )).visit(case.stmts)
            res = truth_table_if_else(res or case_state, case_state)
        return res

    def visit_Break(self, _: Break) -> SwitchBreakType:
        if Break in self.break_types:
            return SwitchBreakType.MUST
        else:
            return SwitchBreakType.NEVER

    def visit_Return(self, _: Return) -> SwitchBreakType:
        if Return in self.break_types:
            return SwitchBreakType.MUST
        else:
            return SwitchBreakType.NEVER

    def generic_visit(self, node: Optional[TypeUnion[Node, list]]) -> SwitchBreakType:
        # ~ print('generic:', type(node))
        if node is None:
            return SwitchBreakType.NEVER
        else:
            res = SwitchBreakType.NEVER
            it = enumerate(node) if isinstance(node, list) else node.children()
            for _, c in it:
                res |= self.visit(c)
                if res == SwitchBreakType.MUST:
                    return res
            return res
