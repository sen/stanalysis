import argparse
import os
import pickle
import shlex
import subprocess
import shutil
import tempfile
from typing import List, Callable, Dict, Tuple

from pycparser import parse_file
from pycparserext.ext_c_parser import GnuCParser
from pycparserext.ext_c_generator import GnuCGenerator, AsmAndAttributesMixin
from pycparser.c_ast import *


class MainAlreadyPresentError(ValueError):
    pass


class CorrectedAsmMixin(AsmAndAttributesMixin):
    def visit_Asm(self, n):
        components = [
                n.template,
                ]
        if (n.output_operands is not None
                or n.input_operands is not None
                or n.clobbered_regs is not None):
            components.extend([
                n.output_operands,
                n.input_operands,
                n.clobbered_regs,
                ])

        return " %s(%s);" % (
                n.asm_keyword,
                " : ".join(
                    self.visit(c) for c in components))


class Generator(GnuCGenerator, CorrectedAsmMixin):
    def __init__(self):
        super().__init__()

    def visit_Pragma(self, n):
        return ""


class NodeSearcher(NodeVisitor):

    def __init__(self, coord):
        super().__init__()
        self.target = coord
        self.parents = []

    def visit(self, node):
        if hasattr(node, "coord") and str(node.coord) == self.target:
            return node, list(self.parents)

        method = 'visit_' + node.__class__.__name__

        return getattr(self, method, self.generic_visit)(node)

    def generic_visit(self, node):
        # ~ print('generic:', type(node))
        if node is None:
            return None
        else:
            for c_name, c in node.children():
                self.parents.append((node, c_name))
                res = self.visit(c)
                self.parents.pop()
                if res is not None:
                    return res
            return None


class TimeResolution:

    def __init__(self, coord: str, n: int, command_before: str,
                 command_after: str, probe_file_name: str = "probe.c",
                 copy_filter: List[str] = None, parse_options: List[str] = None):
        """
        Performs a timing analysis for the instruction at coordinates `coord`, i.e. determines how
        often and at what interval this instruction is executed. Call the method
        `perform_timing_probe` to run the program.

        Args:
            coord: The coordinates of the instruction, in the format filename:lineno:colno, as
                reported by GnuCParser.
            n: number of times the probe will be executed
            command_before: command to execute before the target file is transformed
            command_after: command to be executed after the target file is transformed
            probe_file_name: name of the C file that will be executed
            copy_filter: glob-style filters to filter out the files that will *not* be copied
            parse_options: a list of options for gcc that will be used to pre-process the file to
                be modified
        """
        self.probe_file_name = probe_file_name
        self.copy_filter = copy_filter or []
        self.parse_options = parse_options or []
        self.command_after = command_after
        self.command_before = command_before
        self.coord = coord
        self.n = n

    @staticmethod
    def transform_function_time_resolution(ast, filename: str,
                                           transformation: Callable[[FileAST, Node], bool]) -> bool:
        # We need to determine whether __uint64/32t, uint64/t or none of these
        # were defined. Also, we need to determine whether printf was declared

        # Iterate over all external nodes. Put all typedefs into one set, funcdefs into another
        typedefs, funcdefs = set(), set()
        for inst in ast.ext:
            if isinstance(inst, FuncDef):
                funcdefs.add(inst.decl.name)
            elif isinstance(inst, Typedef):
                typedefs.add(inst.name)

        # Check which one was defined
        if "uint64_t" in typedefs:
            prefix = "uint"
        elif "__uint64_t" in typedefs:
            prefix = "__uint"
        else:
            (f_id, path) = tempfile.mkstemp()
            f = os.fdopen(f_id, mode="w+")
            f.write("#include <stdint.h>")
            # We need to include stdint.h
            stdint = parse_file(path, use_cpp=True, cpp_args=["-E"])
            try:
                f.close()
                os.remove(path)
            except FileNotFoundError:
                print("Could not delete temporary file - strange !")

            for inst in reversed(stdint.ext):
                ast.ext.insert(0, inst)
            prefix = "uint"

        source_code = """
        extern int printf(const char *__restrict __format, ...);
        typedef int {prefix}64_t;
        typedef int {prefix}32_t;
        void ___time_res_reset() {{
            ___time_res_counter = 0;
        }}
        void ___time_res_print() {{
            for ({prefix}64_t i = 0; i < ___time_res_counter; i++) {{
                printf("%lu\\n", ___time_res_table[i]);
            }}
        }}   
        static inline {prefix}64_t rdtscp() {{
          {prefix}32_t low, high;
          asm volatile ("rdtscp": "=a" (low), "=d" (high) :: "ecx");
          return ((({prefix}64_t)high) << 32) | low;
        }}
        static {prefix}64_t ___time_res_counter = 0;
        static {prefix}64_t ___time_res_table[10000000];
        void main() {{
            ___time_res_table[___time_res_counter++] = rdtscp();
        }}
        """.format(prefix=prefix)
        cparser = GnuCParser()
        _ast = cparser.parse(source_code)
        reset_function = _ast.ext[-6]
        print_function = _ast.ext[-5]
        init_counter = _ast.ext[-3]
        init_table = _ast.ext[-2]
        rdtscp_function = _ast.ext[-4]
        table_increment = _ast.ext[-1].body.block_items[0]
        printf = _ast.ext[0]

        # Find the instruction corresponding to the function name
        func_def = None
        i = 0

        abs_filename = os.path.abspath(filename)
        for inst in ast.ext:
            inst_filename, inst_lineno, _ = str(inst.coord).split(":")
            if isinstance(inst, FuncDef) and os.path.abspath(inst_filename) == abs_filename and inst_lineno != "0":
                func_def = inst
                break
            i += 1

        if not func_def:
            raise ValueError("No function declared in %s !" % filename)

        ret = transformation(ast, table_increment)

        ast.ext.insert(i, reset_function)
        ast.ext.insert(i, print_function)
        ast.ext.insert(i, init_table)
        ast.ext.insert(i, init_counter)
        ast.ext.insert(i, rdtscp_function)

        # Check whether printf was defined
        if "printf" not in funcdefs:
            ast.ext.insert(i, printf)

        return ret

    @staticmethod
    def get_transformation(coord: str, alternative: bool = False) -> Callable[[FileAST, Node], bool]:
        """If alternative is set to true, an alternative AST transformation is performed, i.e.:
            - If node: iffalse is modified instead of iftrue
            - For / While loop: the instruction is put before the loop instruction, as opposed to inside.
            - DoWhile: no effect
            - Switch: ???
        """
        def transform(ast: FileAST, node_to_add: Node) -> bool:
            alternative_path_taken = not alternative
            target_node, stack = NodeSearcher(str(coord)).visit(ast)
            alternative_possible = False
            for node, attr_name in reversed(stack+[(target_node, "")]):
                if isinstance(node, Compound):
                    if node == target_node:
                        i = 0
                    else:
                        i = node.block_items.index(target_node)
                    node.block_items.insert(i, node_to_add)
                    return alternative_possible
                elif isinstance(node, (If, While, For, DoWhile)):
                    if isinstance(node, If):
                        if attr_name not in ("iftrue", "iffalse"):
                            alternative_possible = True
                            if alternative_path_taken:
                                attr_name = "iftrue"
                            else:
                                alternative_path_taken = True
                                target_node = node
                                continue
                    else:
                        if alternative_path_taken or isinstance(node, DoWhile):
                            alternative_possible = not isinstance(node, DoWhile)
                            attr_name = "stmt"
                        else:
                            alternative_possible = True
                            alternative_path_taken = True
                            target_node = node
                            continue

                    if isinstance(node.__getattribute__(attr_name), Compound):
                        node.__getattribute__(attr_name).block_items.insert(0, node_to_add)
                    else:
                        node.__setattr__(attr_name,
                                         Compound(block_items=[node_to_add, node.__getattribute__(attr_name)]))
                    return alternative_possible
                else:
                    target_node = node
        return transform

    @staticmethod
    def perform_transformation(coord: str, parse_options: List[str] = None, alternative: bool = False) -> bool:
        if not parse_options:
            parse_options = []
        filename = coord.split(":")[0]

        ast = parse_file(filename, parser=GnuCParser(), use_cpp=True,
                         cpp_args=["-E", "-D__extension__=", "-std=c99"]+parse_options)
        has_main = False
        for inst in ast.ext:
            if isinstance(inst, FuncDef) and inst.decl.name == "main":
                has_main = True

        alternative_possible = TimeResolution\
            .transform_function_time_resolution(ast, filename, TimeResolution.get_transformation(coord, alternative))

        with open(filename, "w+") as f:
            f.write(Generator().visit(ast))

        if has_main:
            raise MainAlreadyPresentError("File to be transformed has main function - probe cannot be performed.")

        return alternative_possible

    @staticmethod
    def process_results(intervals: List[int]) -> Dict:
        # Instead of keeping all timing intervals, only keep the total number of hits, the first and
        # last deciles / centiles, as well as the median and average values
        ls = sorted(intervals)
        n = len(ls)
        if n == 0:
            return {key: 0 for key in ("n", "avg", 1, 10, 50, 90, 99)}

        return {
            "n": n,
            "avg": sum(ls) // n,
            1: ls[n // 100],
            10: ls[n // 10],
            50: ls[n // 2],
            90: ls[(90 * n) // 100],
            99: ls[(99 * n) // 100]
        }

    @staticmethod
    def probe() -> List[int]:
        res = subprocess.run(["./probe"], stdout=subprocess.PIPE).stdout.decode("utf-8").split("\n")
        intervals = []
        last_value = -1
        for i in range(1, len(res) - 1):
            try:
                r, l = int(res[i]), int(res[i-1])
                if r > last_value:
                    intervals.append(r-l)
                    last_value = r

            except ValueError:
                # Probably some spurious output ?
                pass
        return intervals

    @staticmethod
    def get_new_dir_name(dir_name: str) -> str:
        return "%s_dyn" % dir_name

    def perform_timing_probe(self, cleanup: bool = True, alternative: bool = False) -> Tuple[List[Dict], List[Dict]]:
        dir_name = os.path.basename(os.getcwd())
        # Copy the current directory
        new_dir_name = TimeResolution.get_new_dir_name(dir_name)

        if os.path.isdir(os.path.join("..", new_dir_name)):
            raise ValueError("A directory with the name %s already exists ! Please delete it first." % new_dir_name)

        os.chdir("..")
        abs_dir_name, abs_new_dir_name = os.path.abspath(dir_name), os.path.abspath(new_dir_name)

        try:
            shutil.copytree(dir_name, new_dir_name, symlinks=True, ignore=shutil.ignore_patterns(*self.copy_filter))
            os.chdir(new_dir_name)

            subprocess.run(self.command_before, shell=True)

            alternative_possible = self.perform_transformation(self.coord, self.parse_options, alternative)

            subprocess.run(self.command_after, shell=True)

            results = [
                TimeResolution.process_results(TimeResolution.probe()) for _ in range(self.n)
            ]

        finally:
            if cleanup:
                if os.path.isdir(abs_new_dir_name):
                    shutil.rmtree(abs_new_dir_name)
                os.chdir(abs_dir_name)

        if not alternative and alternative_possible and cleanup:
            # Not possible if in debug mode, i.e. cleanup deactivated
            alternative_results, _ = self.perform_timing_probe(cleanup=True, alternative=True)
        else:
            alternative_results = None
        # Return the results for this pass
        return results, alternative_results

    @staticmethod
    def save_probe_to_file(results, coord, filename, overwrite=False):
        try:
            with open(filename, "rb") as f:
                final_results = pickle.load(f)

                if overwrite or coord not in final_results:
                    final_results[coord] = results
                else:
                    final_results[coord].extend(results)
        except FileNotFoundError:
            # Output file does not exists yet
            final_results = {coord: results}

        # Finally, update / create the result file
        with open(filename, "wb") as f:
            pickle.dump(final_results, f)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("--coord", help="Coordinates of the instruction to target, format: filename:file_no:col_no")
    parser.add_argument("--n", help="Number of probes to take (default:100)", default=100, type=int)
    parser.add_argument("--output-filename", help="pickled file that will store the results")
    parser.add_argument("--command-before", help="command to execute before the target file is transformed")
    parser.add_argument("--command-after", help="command to execute after the target file is transformed")
    parser.add_argument("--probe-file-name", help="name of the C file that will be executed", default="probe.c")
    parser.add_argument("--copy-filter", help="glob-style filters to filter out the files that will *not* be copied",
                        nargs="+", default=[])
    parser.add_argument("--parse-options", help="a list of options for gcc that will be used to pre-process the file "
                                                "to be modified", default="")
    parser.add_argument("--overwrite", help="if set to True, the results from the current run will replace the results "
                        "from previous runs for the same coordinates. Otherwise, the new results are appended instead.",
                        action="store_true", default=False)

    opts = parser.parse_args()

    tr = TimeResolution(opts.coord, opts.n, opts.command_before, opts.command_after,
                        opts.probe_file_name, opts.copy_filter, shlex.split(opts.parse_options))
    res, _ = tr.perform_timing_probe()
    TimeResolution.save_probe_to_file(res, opts.coord, opts.output_filename, opts.overwrite)
