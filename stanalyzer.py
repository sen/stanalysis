#!/usr/bin/env python3
import argparse
import datetime
import os
import resource
import warnings
from pathlib import Path
import time
from shutil import copyfile
from sys import setrecursionlimit
from typing import Optional, Dict, List
from xml.dom import minidom


from pycparser.c_ast import FuncDef
from pycparserext.ext_c_parser import GnuCParser
from pycparser.plyparser import ParseError

from stanalysis.cli_helper import SilentLeakageAnalysis, write_leakages_to_csv, generate_function_call_graph, get_cpp_args_for
from stanalysis.common_analysis import get_ast

from stanalysis.codeanalysis import CodeAnalysis
from stanalysis.common import VariadicFunctionDefinition, ControlDependenciesHandling
from stanalysis.scopes import FunctionDefinition


def limit_memory(maxsize):
    soft, hard = resource.getrlimit(resource.RLIMIT_AS)
    resource.setrlimit(resource.RLIMIT_AS, (maxsize, hard))


def rec_limit(val):
    setrecursionlimit(val)


# To avoid crashes
limit_memory(int(4.5e9))
rec_limit(100000)


def _initialize(*args, **kwargs):
    p = Path(os.path.dirname(os.path.realpath(__file__)))

    # Copy the file containing the default functions (printf and similar)
    copyfile(str(p / "utils" / "default_functions.xml"), "functions.xml")

    # Copy the file containing (some) standard library and OpenSSL stub implementations
    copyfile(str(p / "utils" / "fake_lib_implem.c"), "fake_lib_implem.c")

    # Create the default configurations file
    with open(".stanalyzer-config", "w") as f:
        f.write("-nostdinc -E -I%s -Doffsetof(x,y)=1" % str(p / "utils" / "fake_libc_include"))


def _analyze_file(args):
    """Analyzes infile and finds all potential side channel leaks."""

    parser = argparse.ArgumentParser(prog="stanalyzer.py analyze")
    parser.add_argument('infiles', nargs="+", help="Input files")
    parser.add_argument("--function-names", nargs="*", help="Function to analyze")
    parser.add_argument("--functions-file", default="functions.xml", help="Filename containing the "
                                                                          "function definitions")
    parser.add_argument("--extensions", default=False, action="store_true",
                        help="Use parser able to parse Gnu-gcc extensions (but might be buggy !)")

    parser.add_argument("--cpp-command", default=None, help="Overwrite the command used by the C preprocessor. "
                                                            "Should include at least -E -nostdinc` for gcc or similar "
                                                            "options for other programs.")
    parser.add_argument("--cpp-extra-args", default="", help="Appends additional arguments to the preprocessor command")

    parser.add_argument("--compilation-database", default=None,
                        help="Overwrites the cpp command for those C files specified in the compilation database file. "
                             "This file can be generated by cmake or by the bear utility and is usually named "
                             " compile_commands.json. For all other files, a warning will be emitted and the default "
                             "options will be used instead."
                        )

    parser.add_argument("--output", default=None, help="Output report file")
    parser.add_argument("--append", default=None, help="Append to report file", action="store_true")

    parser.add_argument("--ignored-variables", default=[],
                        help="List of variables to ignore. Same syntax is accepted as for the list of sensitive "
                             "variables", nargs="*")
    parser.add_argument("--sensitive-variables", default=[],
                        help="List of variables to always  consider sensitive. Struct fields are indicated after dots, "
                        "like so: var_name.field_name.inner_field_name. Struct types can be indicated in brackets. If "
                        "you need to consider the field 'foo' of all structs of type 'bar' to be sensitive, pass "
                        "the value '[bar].foo' to this option. Does not work with typedefs, only struct names are "
                        "accepted, except for anonymous structs where the typedef'd name can be used. Furthermore, "
                        "you can optionally specify the filename and function name the pattern applies to "
                        "like so: file_name:function_name:variable_name. The file or function name might be "
                        "replaced by a wildcard * character. The short form variable_name is internally expanded "
                        "to, and is thus equivalent to, *:*:variable_name.", nargs="*")

    parser.add_argument("--cond-move", action="store_true",
                        help="Assume ternary condition operations are implemented by cond_move instructions")

    parser.add_argument("--cache-functions", action="store_true",
                        help="Export the function behaviour of analyzed functions as an XML file. Useful for "
                             "debugging, but the XML should be regenerated before performing a new analysis.")

    parser.add_argument("--tags", default="", help="ctags-generated file to help locate function definitions")

    parser.add_argument("--strict-pointers", action="store_true",
                        help="Assume that variables declared as pointers are never used as arrays. This improves "
                             "the accuracy of the dependency analysis if it is the case, but might miss potential "
                             "leakages if the option is present but some arrays are, in fact, declared as pointers.")

    parser.add_argument("--no-control-dependencies", action="store_true", default=False,
                        help="Remove tracking of control dependencies between values. Can reduce the number of "
                        "false positives. However, if you have safe branches in your code (such as those introduced "
                        "by rejection sampling for instance), then the analysis can miss some actual dependencies "
                        "and thus produce false negatives by missing actual leakages.")

    parser.add_argument("--less-control-dependencies", action="store_true", default=False,
                        help="""Remove tracking of control dependencies except for branches marked as safe.""")

    parser.add_argument("--log-level", default="warning", choices=["critical", "error", "warning", "info", "debug"],
                        help="Set the logging level of all modules to the selected level.")

    opts = parser.parse_args(args)

    import logging
    for module_name in ["dependencygraph", "leakageanalysis"]:
        logging.getLogger("stanalysis.%s" % module_name).setLevel(getattr(logging, opts.log_level.upper()))

    return analyze_file(opts.infiles, opts.function_names, opts.functions_file, opts.cache_functions,
                        opts.ignored_variables, opts.sensitive_variables, opts.output, opts.append,
                        opts.cond_move, opts.tags, opts.strict_pointers, opts.cpp_command, opts.cpp_extra_args,
                        opts.compilation_database, opts.no_control_dependencies, opts.less_control_dependencies)


def parse_compilation_database(file: Optional[str]) -> Optional[Dict[str, List[str]]]:
    if file is None:
        return None

    import json
    import shlex
    # the compilation database is a JSON file with a specific structure. It consists of a list of command objects,
    # each of which need the following fields (directly taken from the doc from clang.llvm.org) :

    # directory: The working directory of the compilation. All paths specified in the command or file fields must be
    # either absolute or relative to this directory.

    # file: The main translation unit source processed by this compilation step. This is used by tools as the key into
    # the compilation database. There can be multiple command objects for the same file, for example if the same source
    # file is compiled with different configurations.

    # arguments: The compile command argv as list of strings. This should run the compilation step for the translation
    # unit file. arguments[0] should be the executable name, such as clang++. Arguments should not be escaped,
    # but ready to pass to execvp().

    # command: The compile command as a single shell-escaped string. Arguments may be shell quoted and escaped
    # following platform conventions, with ‘"’ and ‘\’ being the only special characters. Shell expansion is not
    # supported.

    # Either arguments or command is required. arguments is preferred, as shell (un)escaping is a possible source of
    # errors.

    # output: The name of the output created by this compilation step. This field is optional. It can be used to
    # distinguish different processing modes of the same input file.

    # We won't be using the output field here. Also, there should be only one entry by file (the dictionary output by
    # this function won't be able to handle several entries for the same file)

    res = {}
    try:
        commands = json.load(open(file))
    except Exception:
        warnings.warn("Could not open and / or parse compilation database file %s" % file)
        return None

    def filter_preprocessing_commands(l: List[str]) -> List[str]:
        # Only keep -I / -D options + default options
        res = [l[0]] + ["-E", "-nostdinc", "-Dsizeof(x)=1", "-Doffsetof(x,y)=1"]
        i = 0
        while i < len(l):
            if l[i] in ("-D", "-I"):
                # keep the option
                res.append(l[i])
                res.append(l[i+1])
                i += 2
            elif l[i].startswith("-I") or l[i].startswith("-D"):
                res.append(l[i])
                i += 1
            else:
                # skip
                i += 1
        return res

    for command in commands:
        # Get the key: join file and dict
        if not os.path.isabs(command["file"]):
            filename = str(os.path.join(command["directory"], command["file"]))
        else:
            filename = command["file"]
        # Get the value: either command if present, or arguments

        if "command" in command:
            args = shlex.split(command["command"])
        else:
            args = command["arguments"]
        res[filename] = filter_preprocessing_commands(args) + [filename]

    return res


def analyze_file(infiles, def_names, functions_file, save_funcs, ignored_variables,
                 sensitive_variables, output, append, cond_move, tags, strict_pointers,
                 cpp_command, cpp_extra_args, compilation_database, no_control_dependencies,
                 less_control_dependencies):
    maindir = os.path.realpath(infiles[0])
    maindir = os.path.dirname(maindir)
    print(__file__)
    # Check for file existence
    for f in infiles:
        if not os.path.isfile(f):
            exit("Incorrect argument: file %s doesn't exist" % f)
    print("Main path is %s. Analyzing %d files..." % (maindir, len(infiles)))

    reportdir = os.path.dirname(os.path.realpath(output)) if output else None
    control_dependencies = ControlDependenciesHandling.NONE if no_control_dependencies else \
        ControlDependenciesHandling.LESS if less_control_dependencies else ControlDependenciesHandling.FULL

    ca = CodeAnalysis(GnuCParser(), save_funcs=save_funcs, functions_file=functions_file,
                      la_class=SilentLeakageAnalysis, ignored_variables=ignored_variables,
                      sensitive_variables=sensitive_variables, assume_cond_move=cond_move,
                      tags_file=tags, strict_pointers=strict_pointers,
                      cpp_command=cpp_command, cpp_extra_args=cpp_extra_args, compilation_database=
                      parse_compilation_database(compilation_database),
                      control_dependencies=control_dependencies)
    la = ca.analyze(infiles, def_name=def_names)  # type: SilentLeakageAnalysis

    if output:
        print("Output report: %s" % output)
        mode = "a+" if append else "w+"

        with open(output, mode) as report:
            report.write("CACHE-TIMING STATIC ANALYSIS\n" + str(datetime.datetime.now()) +
                         "\n\nSource folder: " + maindir +
                         "\n\n**************************\nLEAKAGE REPORT\n**************************\n")
            report.write("Tagged Secret variables:\n")
            for l in la.tagged_variables['secret']:
                report.write("\t" + l[0] + " [" + l[-1] + "]\n")
            for count, l in enumerate(la.leakages):
                report.write("\n %d. %s\n---------------------\n" % (count, l))

        #_id = str(int(time.time()))
        write_leakages_to_csv(la.leakages_df, str(Path(reportdir) / Path("report_raw_%.0f.csv" % time.time())),
                              delimiter="\t")
        #la.leakages_df.to_csv(reportdir + "/report_raw_" + _id + ".csv", sep='\t')

        with open("leaks.gv", "w+") as f:
            f.write(la.get_leakage_dot_file())
    else:
        for count, l in enumerate(la.leakages):
            print("%d. %s\n" % (count, l))

    return la.leakages_df


def _delete_func_def(args):
    """Removes the function definition of function `func_name` from file `infile`."""
    parser = argparse.ArgumentParser(prog="stanalyzer.py analyze")
    parser.add_argument('infile', help="Input file")
    parser.add_argument('func_name', nargs="+", help="Function definition to delete")

    opts = parser.parse_args(args)

    doc = minidom.parse(opts.infile)
    f_defs = list(doc.getElementsByTagName("function_definition")) + list(
        doc.getElementsByTagName("variadic_function_definition"))
    for f_def in f_defs:
        f_name = f_def.getElementsByTagName("name")[0].firstChild.wholeText
        if f_name in opts.func_name:
            doc.firstChild.removeChild(f_def)
            print("Removed function %s." % f_name)

    with open(opts.infile, "w") as f:
        doc.writexml(f, indent="", addindent='', newl="")


def _show_func(args):
    """Shows the behaviour of the function `func_name` from file `infile`."""
    parser = argparse.ArgumentParser(prog="stanalyzer.py show_function")
    parser.add_argument('infile', help="Input file")
    parser.add_argument('func_name', help="Function to show")

    opts = parser.parse_args(args)

    doc = minidom.parse(opts.infile)
    f_defs = list(doc.getElementsByTagName("function_definition"))
    for f_def in f_defs:
        f_name = f_def.getElementsByTagName("name")[0].firstChild.wholeText
        if f_name == opts.func_name:
            fd = FunctionDefinition.fromXml(f_def)
            print(fd)


def _find_function(args):
    import shlex

    """Outputs the (list of) filename which contains the definition for function func_name."""
    parser = argparse.ArgumentParser(prog="stanalyzer.py find_function")
    parser.add_argument("func_name", help="Function name")
    parser.add_argument("filenames", nargs="+", help="Files to look function definition for")
    parser.add_argument("--cpp-command", default=None, help="Overwrite the command used by the C preprocessor. "
                                                            "Should include at least -E -nostdinc` for gcc or similar "
                                                            "options for other programs.")
    parser.add_argument("--cpp-extra-args", default="", help="Appends additional arguments to the preprocessor command")

    parser.add_argument("--compilation-database", default=None,
                        help="Overwrites the cpp command for those C files specified in the compilation database file. "
                             "This file can be generated by cmake or by the bear utility and is usually named "
                             " compile_commands.json. For all other files, a warning will be emitted and the default "
                             "options will be used instead."
                        )
    opts = parser.parse_args(args)

    cpp_extra_args = shlex.split(opts.cpp_extra_args)
    compilation_database = parse_compilation_database(opts.compilation_database)
    cpp_command = None if opts.cpp_command is None else shlex.split(opts.cpp_command)

    for file in opts.filenames:
        try:
            ast = get_ast(file,
                          cpp_extra_args=cpp_extra_args,
                          override_cpp_args=get_cpp_args_for(
                              file,
                              cpp_command=cpp_command,
                              compilation_database=compilation_database
                          ))

        except ParseError as e:
            print("Failed file: %s" % file)
            continue
        else:
            for expr in ast.ext:
                if isinstance(expr, FuncDef):
                    if expr.decl.name == opts.func_name:
                        print(str(expr.coord))


def _add_function(args):
    """Adds the definition of a variadic function to the file `infile`"""
    parser = argparse.ArgumentParser(prog="stanalyzer.py add_function")
    parser.add_argument('infile', help="Input file")
    parser.add_argument('func_name', help="Function name to add")
    parser.add_argument("--no-leakage", action="store_true", default=False,
                        help="This function does not leak if set to True")
    parser.add_argument("--no-dependency", action="store_true", default=False,
                        help="This function does not depend on its arguments if set to True")
    opts = parser.parse_args(args)

    fd = VariadicFunctionDefinition.new(opts.func_name, leaking=not opts.no_leakage,
                                        dependencies=not opts.no_dependency)
    # print(fd.asXml().toxml())

    # First, check that no function with that name already exists
    doc = minidom.parse(opts.infile)
    f_defs = list(doc.getElementsByTagName("function_definition")) + list(
        doc.getElementsByTagName("variadic_function_definition"))
    for f_def in f_defs:
        f_name = f_def.getElementsByTagName("name")[0].firstChild.wholeText
        if f_name == opts.func_name:
            print("Function %s already exists. You can use remove_function to delete it first." % opts.func_name)
            return
    # The function with that name does not exist already
    doc.documentElement.appendChild(fd.asXml())
    with open(opts.infile, "w") as f:
        doc.writexml(f, indent="", addindent='', newl="")


def _function_call_graph(args):
    """Generated the function call graphs for the provided root functions"""

    parser = argparse.ArgumentParser(prog="stanalyzer.py analyze")
    parser.add_argument('infiles', nargs="+", help="Input files")
    parser.add_argument("--function-names", nargs="*", help="Function to analyze")
    parser.add_argument("--functions-file", default="functions.xml", help="Filename containing the "
                                                                          "function definitions")

    parser.add_argument("--cpp-command", default=None, help="Overwrite the command used by the C preprocessor. "
                                                            "Should include at least -E -nostdinc` for gcc or similar "
                                                            "options for other programs.")
    parser.add_argument("--cpp-extra-args", default="", help="Appends additional arguments to the preprocessor command")

    parser.add_argument("--compilation-database", default=None,
                        help="Overwrites the cpp command for those C files specified in the compilation database file. "
                             "This file can be generated by cmake or by the bear utility and is usually named "
                             " compile_commands.json. For all other files, a warning will be emitted and the default "
                             "options will be used instead."
                        )
    parser.add_argument("--output", default="function_calls.gv", help="Output function call graph file.")

    parser.add_argument("--tags", default="", help="ctags-generated file to help locate function definitions")

    opts = parser.parse_args(args)

    ca = CodeAnalysis(GnuCParser(), save_funcs=False, functions_file=opts.functions_file,
                      la_class=SilentLeakageAnalysis, ignored_variables=[],
                      sensitive_variables=[], assume_cond_move=True,
                      tags_file=opts.tags, strict_pointers=False,
                      cpp_command=opts.cpp_command, cpp_extra_args=opts.cpp_extra_args,
                      compilation_database=parse_compilation_database(opts.compilation_database))

    la = ca.analyze(opts.infiles, def_name=opts.function_names)  # type: SilentLeakageAnalysis

    with open(opts.output, "w+") as f:
        f.write(generate_function_call_graph(la.function_call_graph))



# For debugging non-deterministic behaviour:
# Trace all calls / lines, and see where the divergence happens
# Note: seems to be unexploitable for now, as divergences can
# lead to equivalent program executions.

def trace_calls(frame, event, _):
    if event != 'call':
        return
    co = frame.f_code
    filename = co.co_filename
    if not filename:
        return
    if os.path.basename(filename) in ("yacc.py", "plyparser.py", "c_parser.py", "lex.py",
                                      "c_lexer.py", "stanalyzer.py", "c_ast.py", "ext_c_parser.py"):
        return

    if os.path.dirname(filename).endswith("python3.5"):
        return

    fun_name = co.co_name

    bt = [os.path.basename(filename)+":"+fun_name]
    while frame.f_back:
        frame = frame.f_back
        co = frame.f_code
        filename = co.co_filename
        fun_name = co.co_name
        if fun_name == "analyze":
            break
        bt = [os.path.basename(filename)+":"+fun_name] + bt
    with open("trace", "a+") as f:
        f.write('->'.join(bt) + "\n")
    return trace_lines


def trace_lines(frame, event, _):
    if event != "line":
        return
    co = frame.f_code
    func_name = co.co_name
    line_no = frame.f_lineno
    filename = co.co_filename
    coord = frame.f_locals["expr"].coord if 'expr' in frame.f_locals and hasattr(frame.f_locals["expr"], "coord") else ""

    if filename.startswith("<"):
        return

    if os.path.basename(filename) == "leakageanalysis.py" and func_name == "depends_on":
        return
    if func_name == "get_variable_by_name":
        return

    with open("trace", "a+") as f:
        f.write('%s:%s:%s (%s)\n' % (os.path.basename(filename), func_name, line_no, coord))

# import sys; sys.settrace(trace_calls)


if __name__ == "__main__":

    tools = {
        "init": _initialize,
        "analyze": _analyze_file,
        "remove_function": _delete_func_def,
        "find_function": _find_function,
        "show_function": _show_func,
        "add_function": _add_function,
        "generate_call_graph": _function_call_graph
    }

    tools_list_string = "\t  - %s\n" * len(tools) % tuple(k for k in tools)
    tool_parser = argparse.ArgumentParser(
        usage="""python stanalyzer.py program_name ...

    program_name must be a value from the following list :
%s

    Run the desired program with the -h option to get help for that program.
    """ % tools_list_string)
    tool_parser.add_argument('program', choices=tools.keys())
    tool_parser.add_argument('args', nargs=argparse.REMAINDER)
    tool_opts = tool_parser.parse_args()

    print(tool_opts.args)
    if tool_opts.program in tools:
        tools[tool_opts.program](tool_opts.args)
