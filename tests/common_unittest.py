import unittest
import sys 
import inspect
from functools import wraps
import warnings
from contextlib import contextmanager
from pycparserext.ext_c_parser import GnuCParser, FuncDeclExt
from pycparser.c_ast import FuncDef

from stanalysis.codeanalysis import CodeAnalysis

sys.path.extend(['.', '..'])  # need this in order to import the modules

from stanalysis.leakageanalysis import LeakageAnalysis, SECRET, SECRET_PREFIX
from stanalysis.scopes import FunctionDefinition
# from stanalysis.dependencygraph import DependencyGraph
from stanalysis.dependencygraph import DependencyGraph
# Note : Unary + -> &, Unary - -> *

import stanalysis.leakageanalysis
stanalysis.leakageanalysis.SECRET_PREFIX = "_"


# For introspection
def passmein(func):
    @wraps(func)
    def wrapper(self, *args, **kwargs):
        return func(self, func, *args, **kwargs)
    return wrapper


class BaseTest(unittest.TestCase):
    @contextmanager
    def assertNoWarnings(self):
        try:
            warnings.simplefilter("error")
            yield
        finally:
            warnings.resetwarnings()

    def setUp(self):
        self.parser = GnuCParser()

    def shortDescription(self):
        return "--------\n" + inspect.cleandoc(self._testMethodDoc) + "\n--------\n"

    def helper_test_text(self, text, before="void dummy() {\n", after="\n}", verbosity=0):
        text = before+text+after
        ast = self.parser.parse(text, filename="<none>")
        code_analysis = CodeAnalysis(functions_file=None)
        la = code_analysis.prepare_leakage_object()
        la.set_option("ignored_variables", {})
        f_def = FunctionDefinition.new(name="dummy", arguments=[], is_complete=True).\
            _replace(dependency_graph=DependencyGraph(verbosity))

        la.state.push_function(f_def)
        la.state.push_scope()
        la.state.push_scope()
        la._parse_labels(ast.ext[0].body)
        for expr in ast.ext[0].body.block_items:
            la.depends_on(expr)

        la._check_leakage()

        d = {v.name: v for v in la.state.vars}
        return None, la.state, d

    def helper_func_text(self, func, verbosity=0, functions_file=None):
        text = func.__doc__
        filename = func.__name__
        ast = self.parser.parse(text, filename=filename)
        code_analysis = CodeAnalysis(functions_file=functions_file)
        la = code_analysis.prepare_leakage_object()
        la.set_option("verbose", False)
        la.set_option("ignored_variables", {})

        if functions_file:
            la.import_functions(functions_file)

        if isinstance(ast.ext[-1], FuncDef):
            for expr in ast.ext[:-1]:
                la.depends_on(expr)
            la.state.push_scope()
            f_def = la._prepare_function_scope(ast.ext[-1]).\
                _replace(dependency_graph=DependencyGraph(verbosity))
            local_vars = la.state.get_scope().vars

            la.state.push_function(f_def)
            la.state.push_scope()
            la.prepare_local_function_scope(local_vars, ast.ext[-1].coord, "")
            la._parse_labels(ast.ext[-1].body)

            for expr in ast.ext[-1].body.block_items or []:
                la.depends_on(expr)

            la._check_leakage()
            le = la._transform_leaking_vars(la.state.leaking_vars)
            la.state.leaking_vars.clear()  # same as self.state.leaking_vars.clear()
            la.state.leaking_vars.extend(le)

            la.state.add_function(f_def)
        else:
            for expr in ast.ext:
                la.depends_on(expr)

        d = {}
        for scope in reversed(la.state.scopes):
            d.update({v.name: v for v in scope.vars})

        return None, la.state, d

    def assertSecret(self, l, G):
        for v in l:
            self.assertIn(SECRET, G.get_dependency_set(v))

    def assertNotSecret(self, l, G):
        for v in l:
            self.assertNotIn(SECRET, G.get_dependency_set(v))


# pylint: disable=wrong-import-position
# pylint: disable-msg=e0107
