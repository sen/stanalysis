import unittest
import sys 
sys.path.append('..')  # need this in order to import the modules

# pylint: disable=wrong-import-position
#from stanalysis.dependencygraph import DependencyGraph
from stanalysis.dependencygraph import DependencyGraph
from stanalysis.common import Variable

# Note : Unary + -> &, Unary - -> *


class DependencyGraphFunctions(unittest.TestCase):

    def test_ptr_construct1(self):
        """ Tests the following C code:
        int a, b;  # b is secret
        int *ptr_a = &a;
        int *ptr_b = &b;
        *ptr_a = *ptr_b;
        """
        G = DependencyGraph()

        SECRET = Variable.new("\\SECRET")._replace(secret=True)
        a = Variable.new("a")
        b = Variable.new("b")

        G.add_local_variable_dependencies(a, "test.c:1:1")
        G.add_local_variable_dependencies(b, "test.c:1:7")

        G.add_dependencies(b, SECRET, "test.c:1:7")

        ptr_a = +Variable.new("ptr_a")
        ptr_b = +Variable.new("ptr_b")
        G.add_local_variable_dependencies(ptr_a, "test.c:2:1")
        G.add_local_variable_dependencies(ptr_b, "test.c:3:1")

        G.add_dependencies(ptr_a, +a, "test.c:2:1")
        G.add_dependencies(ptr_b, +b, "test.c:3:1")

        G.add_dependencies(-ptr_a, -ptr_b, "test.c:4:1")

        self.assertEqual(set([SECRET]), G.get_dependency_set(a))
        self.assertEqual(set([SECRET]), G.get_dependency_set(b))
        self.assertEqual(set([SECRET]), G.get_dependency_set(-ptr_a))
        self.assertEqual(set([SECRET]), G.get_dependency_set(-ptr_b))

        self.assertEqual(set([+a]), G.get_dependency_set(ptr_a)) 
        self.assertEqual(set([+b]), G.get_dependency_set(ptr_b))

    def test_ptr_construct2(self):
        """ Tests the following C code:
        int a, b;  # b is secret
        int *ptr_a, *ptr_b;
        int ptr_a = ptr_b;
        *ptr_b = b;
        """
        G = DependencyGraph()

        SECRET = Variable.new("\\SECRET")._replace(secret=True)
        a = Variable.new("a")
        b = Variable.new("b")
        G.add_local_variable_dependencies(a, "test.c:1:1")
        G.add_local_variable_dependencies(b, "test.c:1:7")
        G.add_dependencies(b, SECRET, "test.c:1:7")

        ptr_a = +Variable.new("ptr_a")
        ptr_b = +Variable.new("ptr_b")
        G.add_local_variable_dependencies(ptr_a, "test.c:2:1")
        G.add_local_variable_dependencies(ptr_b, "test.c:3:1")
        G.add_dependencies(ptr_a, ptr_b, "test.c:2:1")
        G.add_dependencies(-ptr_a, b, "test.c:4:1")

        self.assertEqual(set([SECRET]), G.get_dependency_set(b))
        self.assertEqual(set([SECRET]), G.get_dependency_set(-ptr_a))
        self.assertEqual(set([SECRET]), G.get_dependency_set(-ptr_b))

        self.assertEqual(set([ptr_b]), G.get_dependency_set(ptr_a)) 
        
    def test_double_ptr1(self):
        """ Tests the following C code:
        int a, b;  # b is secret
        int *ptr_a, *ptr_b;
        int ** ptr_ptr_a, ** ptr_ptr_b;
        ptr_a = &a; ptr_b = &b;
        ptr_ptr_a = &ptr_a; ptr_ptr_b = &ptr_b;
        **ptr_ptr_a = b;
        """
        G = DependencyGraph()

        SECRET = Variable.new("\\SECRET")._replace(secret=True)
        a = Variable.new("a")
        b = Variable.new("b")
        G.add_local_variable_dependencies(a, "test.c:1:1")
        G.add_local_variable_dependencies(b, "test.c:1:7")
        G.add_dependencies(b, SECRET, "test.c:1:7")

        ptr_a = +Variable.new("ptr_a")
        ptr_b = +Variable.new("ptr_b")
        G.add_local_variable_dependencies(ptr_a, "test.c:2:1")
        G.add_local_variable_dependencies(ptr_b, "test.c:2:1")
        G.add_dependencies(ptr_b, +b, "test.c:2:1")
        G.add_dependencies(ptr_a, +a, "test.c:2:1")
        # pylint: disable-msg=e0107
        ptr_ptr_a = ++Variable.new("ptr_ptr_a")
        ptr_ptr_b = ++Variable.new("ptr_ptr_b")
        G.add_local_variable_dependencies(ptr_ptr_a, "test.c:2:1")
        G.add_local_variable_dependencies(ptr_ptr_b, "test.c:2:1")
        G.add_dependencies(ptr_ptr_b, +ptr_b, "test.c:2:1")
        G.add_dependencies(ptr_ptr_a, +ptr_a, "test.c:2:1")
        G.add_dependencies(--ptr_ptr_a, --ptr_ptr_b, "test.c:4:1")

        self.assertIn(SECRET, G.get_dependency_set(b))
        self.assertIn(SECRET, G.get_dependency_set(-ptr_b))
        self.assertIn(SECRET, G.get_dependency_set(--ptr_ptr_b))
        self.assertIn(SECRET, G.get_dependency_set(a))
        self.assertIn(SECRET, G.get_dependency_set(-ptr_a))
        self.assertIn(SECRET, G.get_dependency_set(--ptr_ptr_a))

    def test_double_ptr2(self):
        """ Tests the following C code:
        int a, b;  # b is secret
        int *ptr_a, *ptr_b;
        int ** ptr_ptr_a, ** ptr_ptr_b;
        ptr_a = &a; ptr_b = &b;
        ptr_ptr_a = &ptr_a; ptr_ptr_b = &ptr_b;
        *ptr_ptr_a = ptr_b;
        """
        G = DependencyGraph()

        SECRET = Variable.new("\\SECRET")._replace(secret=True)
        a = Variable.new("a")
        b = Variable.new("b")

        G.add_local_variable_dependencies(a, "test.c:1:1")
        G.add_local_variable_dependencies(b, "test.c:1:7")
        G.add_dependencies(b, SECRET, "test.c:1:7")

        ptr_a = +Variable.new("ptr_a")
        ptr_b = +Variable.new("ptr_b")
        G.add_local_variable_dependencies(ptr_a, "test.c:2:1")
        G.add_local_variable_dependencies(ptr_b, "test.c:2:1")
        G.add_dependencies(ptr_b, +b, "test.c:2:1")
        G.add_dependencies(ptr_a, +a, "test.c:2:1")
        # pylint: disable-msg=e0107
        ptr_ptr_a = ++Variable.new("ptr_ptr_a")
        ptr_ptr_b = ++Variable.new("ptr_ptr_b")
        G.add_local_variable_dependencies(ptr_ptr_a, "test.c:3:1")
        G.add_local_variable_dependencies(ptr_ptr_b, "test.c:3:1")
        G.add_dependencies(ptr_ptr_b, +ptr_b, "test.c:3:1")
        G.add_dependencies(ptr_ptr_a, +ptr_a, "test.c:3:1")
        G.add_dependencies(-ptr_ptr_a, ptr_b, "test.c:4:1")

        self.assertIn(SECRET, G.get_dependency_set(b))
        self.assertIn(SECRET, G.get_dependency_set(-ptr_b))
        self.assertIn(SECRET, G.get_dependency_set(--ptr_ptr_b))
        self.assertNotIn(SECRET, G.get_dependency_set(a))
        self.assertIn(SECRET, G.get_dependency_set(-ptr_a))
        self.assertIn(SECRET, G.get_dependency_set(--ptr_ptr_a))

    def test_double_ptr3(self):
        """ Tests the following C code:
        int a, b;  # b is secret
        int *ptr_a, *ptr_b;
        int ** ptr_ptr_a, ** ptr_ptr_b;
        ptr_a = &a; ptr_b = &b;
        ptr_ptr_a = &ptr_a; ptr_ptr_b = &ptr_b;
        ptr_ptr_a = ptr_ptr_b;
        """
        G = DependencyGraph()

        SECRET = Variable.new("\\SECRET")._replace(secret=True)
        a = Variable.new("a")
        b = Variable.new("b")

        G.add_local_variable_dependencies(a, "test.c:1:1")
        G.add_local_variable_dependencies(b, "test.c:1:7")
        G.add_dependencies(b, SECRET, "test.c:1:7")

        ptr_a = +Variable.new("ptr_a")
        ptr_b = +Variable.new("ptr_b")
        G.add_local_variable_dependencies(ptr_a, "test.c:2:1")
        G.add_local_variable_dependencies(ptr_b, "test.c:2:1")
        G.add_dependencies(ptr_b, +b, "test.c:2:1")
        G.add_dependencies(ptr_a, +a, "test.c:2:1")
        # pylint: disable-msg=e0107
        ptr_ptr_a = ++Variable.new("ptr_ptr_a")
        ptr_ptr_b = ++Variable.new("ptr_ptr_b")
        G.add_local_variable_dependencies(ptr_ptr_a, "test.c:2:1")
        G.add_local_variable_dependencies(ptr_ptr_b, "test.c:2:1")
        G.add_dependencies(ptr_ptr_b, +ptr_b, "test.c:2:1")
        G.add_dependencies(ptr_ptr_a, +ptr_a, "test.c:2:1")
        G.add_dependencies(ptr_ptr_a, ptr_ptr_b, "test.c:4:1")

        self.assertIn(SECRET, G.get_dependency_set(b))
        self.assertIn(SECRET, G.get_dependency_set(-ptr_b))
        self.assertIn(SECRET, G.get_dependency_set(--ptr_ptr_b))
        self.assertNotIn(SECRET, G.get_dependency_set(a))
        self.assertNotIn(SECRET, G.get_dependency_set(-ptr_a))
        self.assertIn(SECRET, G.get_dependency_set(--ptr_ptr_a))


if __name__ == '__main__':
    unittest.main()
