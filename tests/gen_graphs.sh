#!/bin/bash

for filename in *.gv; do
    name=${filename##*/}
    base=${name%.gv}
    dot -T pdf $filename > "$base".pdf
done
