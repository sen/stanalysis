# pylint: disable=wrong-import-position
# pylint: disable-msg=e0107

import sys
import unittest

from stanalysis.scopes import LeakageType
from tests.common_unittest import BaseTest as LeakageAnalysisTests, passmein

sys.path.extend(['.', '..'])  # need this in order to import the modules


from stanalysis.common import Variable, VariableType, LeakageWarning
from stanalysis.leakageanalysis import SECRET, LeakageAnalysis
from stanalysis.dependencygraph import DependencyGraph


class LeakageAnalysisPointerTests(LeakageAnalysisTests):
    """Pointer indirection testing"""

    @passmein
    def test_ptr_construct1(self, me):
        """int a, _b;
        int *ptr_a = &a;
        int *ptr_b = &_b;
        *ptr_a = *ptr_b;"""
        DependencyGraph.set_strict_pointers()
        (_, state, d) = self.helper_test_text(me.__doc__,)

        G = state.dependency_graph
        (a, b, ptr_a, ptr_b) = (d["a"], d["_b"], d["ptr_a"], d["ptr_b"])

        self.assertIn(SECRET, G.get_dependency_set(a))
        self.assertIn(SECRET, G.get_dependency_set(b))
        self.assertIn(SECRET, G.get_dependency_set(-ptr_a))
        self.assertIn(SECRET, G.get_dependency_set(-ptr_b))
        #self.assertSecret([a, b, -ptr_a, -ptr_b], G)

        self.assertEqual({+a}, G.get_dependency_set(ptr_a))
        self.assertEqual({+b}, G.get_dependency_set(ptr_b))


    @passmein
    def test_ptr_construct2(self, me):
        """int _d;
        int *a, *b, *c;
        *c = _d;
        a = b;
        *b = *c;"""
        (_, state, d) = self.helper_test_text(me.__doc__)

        G = state.dependency_graph
        (a, b, c, _d) = (d["a"], d["b"], d["c"], d["_d"])

        self.assertSecret([_d, -c, -b, -a], G)

    @passmein
    def test_ptr_construct3(self, me):
        """int _d;
        int *a, *b, *c;
        *c = _d;
        a = b;
        b = c;"""
        (_, state, d) = self.helper_test_text(me.__doc__, verbosity=0)

        G = state.dependency_graph
        # print(G)
        (a, b, c, _d) = (d["a"], d["b"], d["c"], d["_d"])

        self.assertSecret([_d, -c, -b], G)
        self.assertNotSecret([-a], G)

    @passmein
    def test_double_ptr1(self, me):
        """int a, _b;
        int *ptr_a, *ptr_b;
        int ** ptr_ptr_a, ** ptr_ptr_b;
        ptr_a = &a; ptr_b = &_b;
        ptr_ptr_a = &ptr_a; ptr_ptr_b = &ptr_b;
        **ptr_ptr_a = _b;"""
        (_, state, d) = self.helper_test_text(me.__doc__)

        G = state.dependency_graph
        # print(G.asXml().toxml())
        (a, b, ptr_a, ptr_b) = (d["a"], d["_b"], d["ptr_a"], d["ptr_b"])
        (ptr_ptr_a, ptr_ptr_b) = (d["ptr_ptr_a"], d["ptr_ptr_b"])
        self.assertIn(SECRET, G.get_dependency_set(b))
        self.assertIn(SECRET, G.get_dependency_set(-ptr_b))
        self.assertIn(SECRET, G.get_dependency_set(--ptr_ptr_b))
        self.assertIn(SECRET, G.get_dependency_set(a))
        self.assertIn(SECRET, G.get_dependency_set(-ptr_a))
        self.assertIn(SECRET, G.get_dependency_set(--ptr_ptr_a))

    @passmein
    def test_double_ptr2(self, me):
        """int a, _b;
        int *ptr_a, *ptr_b;
        int ** ptr_ptr_a, ** ptr_ptr_b;
        ptr_a = &a; ptr_b = &_b;
        ptr_ptr_a = &ptr_a; ptr_ptr_b = &ptr_b;
        *ptr_ptr_a = ptr_b;"""
        (_, state, d) = self.helper_test_text(me.__doc__)

        G = state.dependency_graph
        (a, b, ptr_a, ptr_b) = (d["a"], d["_b"], d["ptr_a"], d["ptr_b"])
        (ptr_ptr_a, ptr_ptr_b) = (d["ptr_ptr_a"], d["ptr_ptr_b"])
        self.assertIn(SECRET, G.get_dependency_set(b))
        self.assertIn(SECRET, G.get_dependency_set(-ptr_b))
        self.assertIn(SECRET, G.get_dependency_set(--ptr_ptr_b))
        self.assertNotIn(SECRET, G.get_dependency_set(a))
        self.assertIn(SECRET, G.get_dependency_set(-ptr_a))
        self.assertIn(SECRET, G.get_dependency_set(--ptr_ptr_a))

    @passmein
    def test_double_ptr3(self, me):
        """int a, _b;
        int *ptr_a, *ptr_b;
        int ** ptr_ptr_a, ** ptr_ptr_b;
        ptr_a = &a; ptr_b = &_b;
        ptr_ptr_a = &ptr_a; ptr_ptr_b = &ptr_b;
        ptr_ptr_a = ptr_ptr_b;"""
        (_, state, d) = self.helper_test_text(me.__doc__)

        G = state.dependency_graph
        (a, b, ptr_a, ptr_b) = (d["a"], d["_b"], d["ptr_a"], d["ptr_b"])
        (ptr_ptr_a, ptr_ptr_b) = (d["ptr_ptr_a"], d["ptr_ptr_b"])

        self.assertIn(SECRET, G.get_dependency_set(b))
        self.assertIn(SECRET, G.get_dependency_set(-ptr_b))
        self.assertIn(SECRET, G.get_dependency_set(--ptr_ptr_b))
        self.assertNotIn(SECRET, G.get_dependency_set(a))
        self.assertNotIn(SECRET, G.get_dependency_set(-ptr_a))
        self.assertIn(SECRET, G.get_dependency_set(--ptr_ptr_a))

    @passmein
    def test_double_ptr4(self, me):
        """int a, _b;
        int *ptr_a, *ptr_b;
        int ** ptr_ptr_a, ** ptr_ptr_b;
        *ptr_b = _b;
        ptr_a = &a; ptr_ptr_a = &ptr_a;
        ptr_ptr_b = &ptr_a;
        *ptr_ptr_b = ptr_b;"""
        # Expected behaviour when pointers cannot be arrays
        DependencyGraph.set_array_pointers()
        (_, state, d) = self.helper_test_text(me.__doc__)
        G = state.dependency_graph
        # TODO finish this test

    @passmein
    def test_ptr_compare(self, me):
        """
        struct {
            int a;
        } bar;
        int foo(int* X) {
            if (X == 0) return 0;
            return 1;
        }

        void main() {
            struct bar x = {.a = 0};
            #pragma STA secret x.a
            foo(&x.a);
            if (&(x.a) == 0) return;
        }


        """
        with self.assertNoWarnings():
            (_, state, d) = self.helper_func_text(me, verbosity=0)

    @passmein
    def test_ptr_index(self, me):
        """
        typedef struct bar_s {
            int f[2];
        } bar;

        void foo(int * x) {
            int a;
        #pragma STA secret a
            x[0] += a;
        }

        void main() {
            bar a = {0};
            int i = 0, b[2] = {0};
            int *c = &a.f[i];
            foo(c);
            c = &b[i];
            foo(c);
            foo(&b[i]);
        }
        """
        (_, state, d) = self.helper_func_text(me, verbosity=0)
        G = state.dependency_graph
        self.assertNotSecret({d["i"]}, G)
        self.assertSecret({-d["a.f"], -d["b"]}, G)


class LeakageAnalysisPointerChainTests(LeakageAnalysisTests):
    """Pointer chaining testing"""

    @passmein
    def test_three_pointer_chain1(self, me):
        """int *a, *b, *_c;
        a = b;
        b = _c;"""
        (_, state, d) = self.helper_test_text(me.__doc__)

        G = state.dependency_graph
        # print(G)
        (a, b, _c) = (d["a"], d["b"], d["_c"])
        self.assertIn(SECRET, G.get_dependency_set(-b))
        self.assertIn(SECRET, G.get_dependency_set(-_c))
        self.assertNotIn(SECRET, G.get_dependency_set(-a))

    @passmein
    def test_three_pointer_chain2(self, me):
        """int *a, *b, *_c;
        a = b;
        b = _c;
        *b = *_c;"""

        DependencyGraph.set_strict_pointers()
        (_, state, d) = self.helper_test_text(me.__doc__)
        G = state.dependency_graph
        (a, b, _c) = (d["a"], d["b"], d["_c"])
        self.assertIn(SECRET, G.get_dependency_set(-b))
        self.assertIn(SECRET, G.get_dependency_set(-_c))
        self.assertNotIn(SECRET, G.get_dependency_set(-a))

    @passmein
    def test_three_pointer_chain3(self, me):
        """int **ptr_ptr;
        int e,f,_g;
        *ptr_ptr = &f;
        int *a=&e, *b=*ptr_ptr, *c=&_g;
        a = b;
        *b = *c;"""
        # Expected behaviour when pointers cannot be arrays
        DependencyGraph.set_strict_pointers()
        (_, state, d) = self.helper_test_text(me.__doc__)

        G = state.dependency_graph
        # print(G)
        (a, b, _c) = (d["a"], d["b"], d["c"])
        (e, f, g) = (d["e"], d["f"], d["_g"])
        ptr_ptr = d["ptr_ptr"]

        self.assertIn(SECRET, G.get_dependency_set(-b))
        self.assertIn(SECRET, G.get_dependency_set(-_c))
        self.assertIn(SECRET, G.get_dependency_set(-a))
        self.assertIn(SECRET, G.get_dependency_set(f))
        self.assertIn(SECRET, G.get_dependency_set(g))
        self.assertNotIn(SECRET, G.get_dependency_set(e))
        # for v in G.get_alias_set(--ptr_ptr):
        #     print(v)

        # Expected behaviour when pointers might be arrays
        DependencyGraph.set_array_pointers()
        (_, state, d) = self.helper_test_text(me.__doc__)

        G = state.dependency_graph
        # print(G)
        (a, b, _c) = (d["a"], d["b"], d["c"])
        (e, f, g) = (d["e"], d["f"], d["_g"])
        ptr_ptr = d["ptr_ptr"]

        self.assertIn(SECRET, G.get_dependency_set(-b))
        self.assertIn(SECRET, G.get_dependency_set(-_c))
        self.assertIn(SECRET, G.get_dependency_set(-a))
        self.assertIn(SECRET, G.get_dependency_set(f))
        self.assertIn(SECRET, G.get_dependency_set(g))
        self.assertNotIn(SECRET, G.get_dependency_set(e))
        # for v in G.get_alias_set(--ptr_ptr):
        #     print(v)


class LeakageAnalysisFuncTests(LeakageAnalysisTests):
    """Testing function analysis."""
    @passmein
    def test_simple_func1(self, me):
        """int dummy(int a, int b){
            return a+b;
        }"""
        (_, state, d) = self.helper_func_text(me, verbosity=0)

        G = state.dependency_graph
        ret_vars = G.get_dependency_set(state.get_function().get_ret_var())

        (a, b) = (d["a"], d["b"])
        self.assertEqual(ret_vars, {a, b})

    @passmein
    def test_simple_func2(self, me):
        """int dummy(int *a, int b){
            return *a+b;
        }"""
        (_, state, d) = self.helper_func_text(me)

        G = state.dependency_graph
        ret_vars = G.get_dependency_set(state.get_function().get_ret_var())

        (a, b) = (d["a"], d["b"])
        self.assertEqual(ret_vars, {-a, b})

    @passmein
    def test_double_ret_fun(self, me):
        """int dummy(int a, int b, int c) {
            if (a > 0) return c;
            return b;
        }"""
        (_, state, d) = self.helper_func_text(me)

        G = state.dependency_graph
        self.assertEqual(G.get_dependency_set(d["\\RET"]), {d["a"], d["b"], d["c"]})


    @passmein
    def test_dependency_func1(self, me):
        """int dummy(int *c, int d){
            *c = d;
            return *c+d;
        }"""
        # Expected behaviour when pointers cannot be arrays
        DependencyGraph.set_strict_pointers()
        (_, state, dic) = self.helper_func_text(me)

        (c, d) = (dic["c"], dic["d"])
        G = state.dependency_graph
        ret_vars = G.get_dependency_set(state.get_function().get_ret_var())
        self.assertEqual({d}, G.get_dependency_set(-c))
        self.assertEqual({d}, ret_vars)


        # Expected behaviour when pointers might represent arrays
        DependencyGraph.set_array_pointers()
        (_, state, dic) = self.helper_func_text(me)
        (c, d) = (dic["c"], dic["d"])
        G = state.dependency_graph
        ret_vars = G.get_dependency_set(state.get_function().get_ret_var())
        self.assertEqual({-c, d}, G.get_dependency_set(-c))
        self.assertEqual({-c, d}, ret_vars)

    @passmein
    def test_dependency_func2(self, me):
        """int dummy(int *c, int d){
            *c += d;
            return *c+d;
        }"""
        (_, state, dic) = self.helper_func_text(me)
        # print(state.dependency_graph)
        (c, d) = (dic["c"], dic["d"])
        G = state.dependency_graph
        ret_vars = G.get_dependency_set(state.get_function().get_ret_var())
        self.assertEqual({d, -c}, G.get_dependency_set(-c))
        self.assertEqual({d, -c}, ret_vars)


class LeakageAnalysisStructTests(LeakageAnalysisTests):
    """Testing struct handling."""

    @passmein
    def test_basic_struct1(self, me):
        """struct foo {
            int a;
            int b;
        };
        """
        (_, state, _) = self.helper_test_text(me.__doc__)

        self.assertIn("foo", state.structs)
        self.assertIn(Variable.new("a")._replace(scope=1), state.structs["foo"])
        self.assertIn(Variable.new("b")._replace(scope=1), state.structs["foo"])

    @passmein
    def test_basic_struct2(self, me):
        """struct foo {
            int a;
            int b;
            struct bar {
                int c;
                char d;
            } e;
        };
        """
        (_, state, _) = self.helper_test_text(me.__doc__)

        self.assertIn("foo", state.structs)
        self.assertIn(Variable.new("a")._replace(scope=1), state.structs["foo"])
        self.assertIn(Variable.new("b")._replace(scope=1), state.structs["foo"])
        self.assertIn(Variable.new("e")._replace(scope=1, struct_type="bar",
                                                 var_type=VariableType.STRUCT),
                      state.structs["foo"])

        self.assertIn("bar", state.structs)
        self.assertIn(Variable.new("c")._replace(scope=1), state.structs["bar"])
        self.assertIn(Variable.new("d")._replace(scope=1), state.structs["bar"])

    @passmein
    def test_basic_typedef1(self, me):
        """typedef struct foo {
            int a;
            int b;
        } FOO;"""
        (_, state, _) = self.helper_test_text(me.__doc__)

        self.assertIn("foo", state.structs)
        self.assertIn(Variable.new("a")._replace(scope=1), state.structs["foo"])
        self.assertIn(Variable.new("b")._replace(scope=1), state.structs["foo"])

        self.assertIn("FOO", state.typedefs)
        self.assertEqual("foo", state.typedefs["FOO"].struct_type)

    @passmein
    def test_basic_typedef2(self, me):
        """struct foo {
            int a;
            int b;
        };
        typedef struct foo FOO;"""
        (_, state, _) = self.helper_test_text(me.__doc__)

        self.assertIn("foo", state.structs)
        self.assertIn(Variable.new("a")._replace(scope=1), state.structs["foo"])
        self.assertIn(Variable.new("b")._replace(scope=1), state.structs["foo"])

        self.assertIn("FOO", state.typedefs)
        self.assertEqual("foo", state.typedefs["FOO"].struct_type)

    @passmein
    def test_struct_decl1(self, me):
        """typedef struct foo FOO;
        FOO a;
        """
        #with self.assertWarns(UserWarning):
        (_, state, _) = self.helper_test_text(me.__doc__)
        self.assertIn(Variable.new("a")._replace(scope=1, struct_type="foo",
                                                 var_type=VariableType.OPAQUE_STRUCT), state.vars)

    @passmein
    def test_struct_decl2(self, me):
        """struct foo {
            int a;
            int b;
        };
        struct foo f;
        f.a = 1;
        f.b = 2;
        """

        (_, state, _) = self.helper_test_text(me.__doc__)
        self.assertIn(Variable.new("f")._replace(scope=1, struct_type="foo",
                                                 var_type=VariableType.STRUCT), state.vars)
        self.assertIn(Variable.new("f.a")._replace(scope=1), state.vars)
        self.assertIn(Variable.new("f.b")._replace(scope=1), state.vars)

    @passmein
    def test_struct_decl3(self, me):
        """struct foo {
            int a;
            int *b;
            int c[5];
        };
        struct foo f;
        f.a = 1;
        f->b = 2;
        f.c[0] = 3;
        """

        (_, state, dic) = self.helper_test_text(me.__doc__)
        self.assertIn(Variable.new("f")._replace(scope=1, struct_type="foo",
                                                 var_type=VariableType.STRUCT), state.vars)
        self.assertEqual(Variable.new("f.a")._replace(scope=1), dic["f.a"])
        self.assertEqual(+Variable.new("f.b")._replace(scope=1, var_type=VariableType.PTR),
                         dic["f.b"])
        self.assertEqual(+Variable.new("f.c")._replace(scope=1, var_type=VariableType.ARRAY),
                         dic["f.c"])

    @passmein
    def test_struct_dep1(self, me):
        """struct foo {
            int a;
            int b;
        };
        struct foo f1;
        int _s;

        struct foo *f2;
        f2 = &f1;
        f2->a=0;
        f1.a = _s;
        """

        (_, state, dic) = self.helper_test_text(me.__doc__, verbosity=0)
        G = state.dependency_graph
        # print(G)
        (f1a, f2a) = (dic["f1.a"], dic["f2.a"])

        self.assertIn(SECRET, G.get_dependency_set(f1a))
        self.assertIn(SECRET, G.get_dependency_set(f2a))

    @passmein
    def test_struct_dep2(self, me):
        """struct foo {
            int a;
            int b;
        };
        struct foo f1;
        int _s;
        f1.a = 0;
        struct foo *f2;
        f2 = &f1;
        f2->a=_s;
        """

        (_, state, dic) = self.helper_test_text(me.__doc__, verbosity=0)
        G = state.dependency_graph
        (f1a, f2a) = (dic["f1.a"], dic["f2.a"])
        # print(G)
        self.assertIn(SECRET, G.get_dependency_set(f1a))
        self.assertIn(SECRET, G.get_dependency_set(f2a))

    @passmein
    def test_struct_dep3(self, me):
        """struct foo {
            int a;
            int b;
        };
        struct foo f1;
        int _s;
        f1.a = _s;
        f1.b = _s;
        struct foo *f2;
        f2 = &f1;
        f2->a = 0;

        """
        DependencyGraph.set_strict_pointers()
        (_, state, dic) = self.helper_test_text(me.__doc__, verbosity=0)
        G = state.dependency_graph
        # print(G)
        (f1a, f1b) = (dic["f1.a"], dic["f1.b"])
        f2a = f1a._replace(name="f2.a")
        f2b = f1b._replace(name="f2.b")
        # print(G.get_pointed_variables(f2a))
        # print(G.get_alias_set(f2a))
        # print()
        # for (l, r) in G.get_related_equivalent_vars(+dic["f2.a"], dic["f1.a"]):
        #    print("%s -> %s " % (l, r))

        # print(G)
        self.assertNotIn(SECRET, G.get_dependency_set(f1a))
        self.assertNotIn(SECRET, G.get_dependency_set(f2a))
        self.assertIn(SECRET, G.get_dependency_set(f1b))
        self.assertIn(SECRET, G.get_dependency_set(f2b))

    @passmein
    def test_struct_dep4(self, me):
        """typedef struct _s_state {
            int X0_s, reg_s, counter, X0_p;
            int* K_s, *X_s, *K_p, *X_p;
        } S_State;


        int encrypt(S_State *state, int delay) {
            int j;
            state->X0_s = 0;
            state->X0_p = 0;

            for (j = 0; j < delay; j++) {
                state->X0_s += state->K_s[j] * state->X_s[j];
                state->X0_p += state->K_p[j] * state->X_p[j];
            }

            return 0;
        }

        """
        (_, state, d) = self.helper_func_text(me, verbosity=0)
        G = state.dependency_graph
        #print(state.structs["_s_state"])
        print(G)

    @passmein
    def test_struct_fun_dep1(self, me):
        """struct foo {
            int a;
            int b;
        };
        int bar(struct foo f)
        {
            f.a = 1;
            #pragma STA secret f.a
            return f.a;
        }
        int main() {
            struct foo f = {.a = 0, .b=1};
            int secret = bar(f);
        }
        """
        (_, state, d) = self.helper_func_text(me, verbosity=0)
        G = state.dependency_graph
        self.assertIn(SECRET, state.dependency_graph.get_dependency_set(d["secret"]))

    @passmein
    def test_struct_fun_dep2(self, me):
        """struct foo {
            int a;
            int b;
        };
        int bar(struct foo f)
        {
            return f.a;
        }
        int main() {
            struct foo f = {.a = 0, .b=1};
            #pragma STA secret f.a
            int secret = bar(f);
        }
        """
        (_, state, d) = self.helper_func_text(me, verbosity=0)
        #G = state.dependency_graph
        #print(state.functions["bar"].dependency_graph)
        self.assertIn(SECRET, state.dependency_graph.get_dependency_set(d["secret"]))


class LeakageAnalysisRealWorld(LeakageAnalysisTests):
    """Testing real world examples from "Verifying constant time
    implementations" (Usenix 2016)"""

    @passmein
    def test_cond_leak(self, me):
        """typedef unsigned char uint8;
        typedef unsigned int uint32;
        void copy_subarray(uint8 *out, const uint8 *in,
            uint32 len, uint32 l_idx, uint32 sub_len) {
                #pragma STA secret l_idx
                uint32 i=0, j=0;
                for(i=0,j=0;i<len;i++) {
                    if (i >= l_idx && i < l_idx + sub_len) {
                        out[j] = in[i];
                        j++;
                    }
                }
        }"""

        with self.assertWarnsRegex(LeakageWarning, r'Leakage of l_idx'):
            (_, state, d) = self.helper_func_text(me, verbosity=0)


    @passmein
    def test_array_leak(self, me):
        """typedef unsigned char uint8;
        typedef unsigned int uint32;
        uint32 ct_lt(uint32 a, uint32 b) {
            uint32 c = a ^ ((a^b) | ((a-b)^b));
            return (0 - (c >> (4 * 8 -1)));
        }

        void cp_copy_subarray(uint8 *out, const uint8 *in,
            uint32 len, uint32 l_idx, uint32 sub_len, int j, int in_range) {
                #pragma STA secret l_idx
                uint32 i=0;
                for(i=0; i < sub_len; i++) out[i]=0;
                for(i=0;i<len;i++) {
                    in_range=0;
                    in_range |= ~ct_lt(i, l_idx);
                    in_range &= ct_lt(i, l_idx+sub_len);
                    out[j] |= in[i] & in_range;
                    j += (in_range % 2);
                }
        }"""

        with self.assertWarnsRegex(LeakageWarning, r'Leakage of'):
            (_, state, d) = self.helper_func_text(me)
        self.assertIn(d["l_idx"], state.functions["cp_copy_subarray"].leaking_vars.get_vars())
       # self.assertIn(SECRET, state.dependency_graph.get_dependency_set(d["j"]))

        # print(G)

    @passmein
    def test_no_leak(self, me):
        """typedef unsigned char uint8;
        typedef unsigned int uint32;
        uint32 ct_eq(uint32 a, uint32 b) {
            uint32 c = a^b;
            uint32 d = ~c & (c-1);
            return (0-(d>>4*8-1));
        }
        void cp_copy_subarray(uint8 *out, const uint8 *in,
            uint32 len, uint32 l_idx, uint32 sub_len) {
                #pragma STA secret l_idx
                uint32 i,j;
                for(i=0; i < sub_len; i++) out[i]=0;
                for(i=0;i<len;i++) {
                    for(j=0; j < sub_len; j++) {
                        out[j] |= in[i] & ct_eq(l_idx+j,i);
                    }
                }
        }"""

        with self.assertWarnsRegex(LeakageWarning, r"Leakage of"):
            (_, state, _) = self.helper_func_text(me)
        f = state.functions["cp_copy_subarray"]
        G = f.dependency_graph

        self.assertNotIn(SECRET, [dep for el in
                                  [G.get_dependency_set(v) for v in f.leaking_vars.get_vars(ignore_types=[LeakageType.PotentialBranchBitwise])]
                                  for dep in el])

    @passmein
    def test_external_leakage(self, me):
        """int leak(int a) {
            if(a > 0) {
                return 1;
            }
            return -1;
        }
        void do_stuff(int s, int t) {
            #pragma STA secret s
            int sign = leak(s);
            return sign+t;
        }"""
        with self.assertWarnsRegex(LeakageWarning, r'Leakage of s'):
            (_, state, _) = self.helper_func_text(me)

    @passmein
    def test_leakage_order(self, me):
        """int leak(int a) { if (a>0) return a; else return 0;}
        int leak2(int b){ int tmp = b+1; leak(tmp);}
        int main(int c){
            int tmp1 = c +1;
            int tmp2 = tmp1;
            leak2(tmp2);
        }
        """
        (_, state, _) = self.helper_func_text(me)
        # for le in state.functions["main"].leaking_vars:
        #    print(le.coord)
        #print(state.functions["main"].leaking_vars)

    @passmein
    def test_leakage_safe(self, me):
        """int leak(int a) { if (a>0) return a; else return 0;}
        int make_secret(void) {
            int s;
            #pragma STA secret s
            return s;
        }
        int main(int c){
            int secret = make_secret();
            #pragma STA safe secret
            int foo = secret + 1;

            leak(foo);
        }
        """
        (_, state, _) = self.helper_func_text(me)
        # for le in state.functions["main"].leaking_vars:
        #    print(le.coord)
        #print(state.functions["main"].leaking_vars)

    @passmein
    def test_secret_passing(self, me):
        """int randombytes(unsigned char* random_bytes, int size) {
            #pragma STA secret random_bytes
            return 1;
        }

        unsigned char *random_m(int size){
            unsigned char *r;;
            int i;
            randombytes(r, size);
            for (i = 0; i < size; i++)
            {
                r[i] = r[i] & 10;
            }

            return r;
        }


        int main(int size) {
            unsigned char *m = random_m(size);
        }
        """
        (_, state, d) = self.helper_func_text(me)
        G = state.dependency_graph
        #self.assertTrue(G.get_dependency_set(-d["m"]).issuperset())
        self.assertEqual(G.get_dependency_set(-d["m"]), {SECRET, d["size"]})

    @passmein
    def test_ternary_dependency(self, me):
        """
        int main() {
            int a;
            #pragma STA secret a
            int b = (a) ? 1 : 0;
        }
        """
        with self.assertWarnsRegex(LeakageWarning, r'Leakage of a'):
            (_, state, d) = self.helper_func_text(me)
            G = state.dependency_graph
            self.assertSecret({d["b"]}, G)
    @passmein
    def test_ternary1(self, me):
        """typedef struct _s_state {
            int X0_s, reg_s, counter, X0_p;
            int* K_s, X_s, K_p, X_p;
        } S_State;

        typedef struct _s_parameters{
            int delta_s, delay;
        } S_Parameters;

        int encrypt(S_State *state, S_Parameters *parameters) {
            int j;

            state->X0_s = 0;
            #pragma STA secret state.X0_s
            #pragma STA secret state.reg_s

            state->X0_p = 0;

            for (j = 0; j < parameters->delay; j++) {
                state->X0_s += state->K_s[j] * state->X_s[j];
                state->X0_p += state->K_p[j] * state->X_p[j];
            }

            if (state->counter % parameters->delta_s == 0) {
                state->X0_s = (state->X0_s == state->reg_s) ? state->X0_s : (state->X0_s ^ state->reg_s);
            }
        }
        """
        with self.assertWarnsRegex(LeakageWarning, r'Leakage of state.X0_s'):
            (_, state, d) = self.helper_func_text(me)

    @passmein
    def test_union_struct(self, me):
        """
        typedef int uint8_t;
        typedef int uint64_t;
        typedef int size_t;

        typedef struct {
            union {
                uint8_t b[200];
                uint64_t q[25];
            } st;
            int pt, rsiz, mdlen;
        } hila5_sha3_ctx_t;

        int hila5_sha3_update(hila5_sha3_ctx_t *c, const void *data)
        {
            c->st.b[0] += data[0];
            return 1;
        }
        """
        (_, state, d) = self.helper_func_text(me)
        G = state.functions["hila5_sha3_update"].dependency_graph
        self.assertEqual(G.get_dependency_set(-d["c.st"]._replace(scope=0)), {-d["c.st"]._replace(scope=0), -d["data"]})

    @passmein
    def test_toy_example(self, me):
        """
        int search1(int *array, int secret_index, int *result, int len) {
            #pragma STA secret secret_index
            if (secret_index < 0 || secret_index >= len) return -1;
            *result = array[secret_index];
            return 0;
        }

        int search2(int *array, int secret_index, int *result, int len) {
            #pragma STA secret secret_index
            for (int i = 0; i < len; i++)
            {
                if (i == secret_index) {
                    *result = array[i];
                }
            }
            return -(secret_index < 0 || secret_index >= len);
        }

        int search3(int *array, int secret_index, int *result, int len) {
            #pragma STA secret secret_index
            for (int i = 0; i < len; i++)
            {
                *result += array[i] * (secret_index==i);
            }
            return -(secret_index < 0 || secret_index >= len);
        }

        int main(int argc, char* argv[], int argc){
            int array[10] = {8,2,1,3,6,9,4,5,7,0};
            int res;
            search1(array, 4, &res, 10);
            search2(array, 4, &res, 10);
            search3(array, 4, &res, 10);
        }

        """
        with self.assertWarnsRegex(LeakageWarning, r'Leakage of secret_index'):
            (_, state, d) = self.helper_func_text(me)

    @passmein
    def test_dags(self, me):
        # Verifying the dependency between the secret and the decoding matrix H_alt
        """
        typedef struct matrix
        {
            unsigned int rown;
            unsigned int coln;
            int **coeff;
        } binmat_t;

        binmat_t read_sk(char * sk) {
            binmat_t H_alt;
            H_alt.coeff[0][0] = sk[0];
            return H_alt;
        }
        void main(){
            char * sk;
            binmat_t H_alt;
            #pragma STA secret sk
            H_alt = read_sk(sk);
        }
        """
        DependencyGraph.set_strict_pointers()
        (_, state, d) = self.helper_func_text(me)
        G = state.dependency_graph
        # print(G)
        self.assertEqual(G.get_dependency_set(--d["H_alt.coeff"]), {-d["sk"], SECRET})



    @passmein
    def test_mbedtls_set_key(self, me):
        """
        typedef unsigned int uint32_t;
        typedef struct mbedtls_aes_context
        {
            int nr;
            char rk[16];
            char buf[16];
        } mbedtls_aes_context;
        static unsigned char FSb[256];
        static unsigned int RCON[10];
        int mbedtls_aes_setkey_enc( mbedtls_aes_context *ctx, const unsigned char *key,
                            unsigned int keybits )
        {

            unsigned int i;
            uint32_t *RK;

            ctx->nr = 10;
            ctx->rk = RK = ctx->buf;


            for( i = 0; i < ( keybits >> 5 ); i++ )
            {
               RK[i] = key[(i<<2)];
            }



            for( i = 0; i < 10; i++, RK += 4 )
            {
                RK[4] = RK[0] ^ RCON[i] ^
                ( (uint32_t) FSb[ ( RK[3] >> 8 ) & 0xFF ] ) ^
                ( (uint32_t) FSb[ ( RK[3] >> 16 ) & 0xFF ] << 8 ) ^
                ( (uint32_t) FSb[ ( RK[3] >> 24 ) & 0xFF ] << 16 ) ^
                ( (uint32_t) FSb[ ( RK[3] ) & 0xFF ] << 24 );

                RK[5] = RK[1] ^ RK[4];
                RK[6] = RK[2] ^ RK[5];
                RK[7] = RK[3] ^ RK[6];
            }

            return( 0 );
        }

        int main() {
            mbedtls_aes_context ctx;
            unsigned int keybits;
            unsigned char key[32];
            #pragma STA secret key
            mbedtls_aes_setkey_enc( &ctx, key, keybits );

        }
        """
        with self.assertWarnsRegex(LeakageWarning, r'Leakage of key'):
            (G, state, d) = self.helper_func_text(me)
            G = state.dependency_graph
            self.assertIn(SECRET, G.get_dependency_set(-d["ctx.rk"]))

    @passmein
    def test_victor13(self, me):
        """
        static void cond_branch(int *a) {
            if(a[0]) {a[0] = 0;}
        }

        int test(int *a, int *b) {

            #pragma STA secret a b

            if(a[0]) {a[0] = 0;}

            if(a[0]) {a[0] = 0;}

            cond_branch(a);

            cond_branch(a);

            return 0;
        }
        """
        with self.assertWarns(LeakageWarning):
            (G, state, d) = self.helper_func_text(me)

        to_search = me.__name__+":16:13"
        for le in state.leaking_vars:
            if le.coord[-1][0] == to_search:
                return
        self.fail("Second leakage warning not triggered !")

    @passmein
    def test_victor14(self, me):
        """
        int main(int k, int p, int q) {

            int a = 0;

            #pragma STA secret k

            a = k;

            if(p == 0) {
                if(q == 0) {
                    p = 1;
                }
                a = 0;
            }
            int b = a;
            if(b == 0) {
                return 0;
            }
        }"""

        with self.assertWarnsRegex(LeakageWarning, r'Leakage of b'):
            self.helper_func_text(me)


class LeakageAnalysisPragmaTests(LeakageAnalysisTests):
    @passmein
    def test_secret1(self, me):
        """int a;
        int b;
        #pragma STA secret a"""
        (_, state, d) = self.helper_test_text(me.__doc__)
        G = state.dependency_graph

        self.assertEqual(state.vars, {d["a"], d["b"]})
        self.assertIn(SECRET, G.get_dependency_set(d["a"]))
        self.assertNotIn(SECRET, G.get_dependency_set(d["b"]))

    @passmein
    def test_secret2(self, me):
        """int a;
        int b;
        #pragma STA secret a b"""
        (_, state, d) = self.helper_test_text(me.__doc__)
        G = state.dependency_graph

        self.assertEqual(state.vars, {d["a"], d["b"]})
        self.assertIn(SECRET, G.get_dependency_set(d["a"]))
        self.assertIn(SECRET, G.get_dependency_set(d["b"]))

    @passmein
    def test_cond_leak_pragma(self, me):
        """typedef unsigned char uint8;
        typedef unsigned int uint32;
        void copy_subarray(uint8 *out, const uint8 *in,
            uint32 len, uint32 l_idx, uint32 sub_len) {
                #pragma STA secret l_idx
                uint32 i=0, j=0;
                for(i=0,j=0;i<len;i++) {
                    if (i >= l_idx && i < l_idx + sub_len) {
                        out[j] = in[i];
                        j++;
                    }
                }
        }"""

        with self.assertWarnsRegex(LeakageWarning, r'Leakage of'):
            (_, state, d) = self.helper_func_text(me)
        self.assertIn(d["l_idx"], state.leaking_vars.get_vars())
        #self.assertIn(SECRET, state.dependency_graph.get_dependency_set(d["l_idx"]))

    @passmein
    def test_implicit_flow_order_test(self, me):
        """
        int main() {
            int secret, e, a;
            #pragma STA secret secret
            if (e) {
                e = secret;
                a+= 1;
                if (a) return 0;
            }
        }
        """
        with self.assertNoWarnings():
            (_, state, d) = self.helper_func_text(me)
            G = state.dependency_graph

    @passmein
    def test_correct_if_analysis_test(self, me):
        """
        int main(int a, int b, int c) {
            int d1=0, d2=0, e1=0, e2=0;
            if (c > 0) {
                d1 = e2 + a;
                e1 = b;
            }
            else {
                d2 = e1 + a;
                e2 = b;
            }
        }
        """
        (_, state, d) = self.helper_func_text(me)
        G = state.dependency_graph
        self.assertEqual(G.get_dependency_set(d["d1"]), {d["c"], d["a"]})
        self.assertEqual(G.get_dependency_set(d["d2"]), {d["c"], d["a"]})

        self.assertEqual(G.get_dependency_set(d["e1"]), {d["c"], d["b"]})
        self.assertEqual(G.get_dependency_set(d["e2"]), {d["c"], d["b"]})

    @passmein
    def test_pointer_arithmetic_deref_test1(self, me):
        """
        int main() {
            int secret;
            #pragma STA secret secret
            int *ptr;
            ptr = ptr + secret;
            *ptr = 1;
        }"""
        with self.assertWarnsRegex(LeakageWarning, r'Leakage of'):
            (_, state, d) = self.helper_func_text(me)
            G = state.dependency_graph
            #print(G)

    @passmein
    def test_pointer_arithmetic_deref_test2(self, me):
        """
        int main (int k, int *a, int *b, int c) {
            int *l;
            int p;

            #pragma STA secret k

            l = (a & k) ^ (b & ~k);
            l[0] = c;
            p = l[0];

            return 0;
        }"""
        # import logging
        # logging.getLogger("stanalysis.dependencygraph").setLevel(logging.INFO)
        # logging.getLogger("stanalysis.leakageanalysis").setLevel(logging.INFO)
        with self.assertWarnsRegex(LeakageWarning, r'Leakage of'):
            (_, state, d) = self.helper_func_text(me)
            G = state.dependency_graph
            print(list(state.leaking_vars))
        # logging.getLogger("stanalysis.dependencygraph").setLevel(logging.WARNING)
        # logging.getLogger("stanalysis.leakageanalysis").setLevel(logging.WARNING)

    @passmein
    def test_pointer_arithmetic_deref_test3(self, me):
        """
        int unref2(int *l) {
            return l[0];
        }
        static int unref(int *l) {
            return unref2(l);
        }

        int test(int k, int *a, int *b) {
            int *l;
            int p;

            #pragma STA secret k

            l = (a & k) ^ (b & ~k);

            p = unref(l);

            return 0;
        }"""
        with self.assertWarnsRegex(LeakageWarning, r'Leakage of'):
            (_, state, d) = self.helper_func_text(me)
        #print(state.functions["unref"].leaking_vars)

    @passmein
    def test_pointer_arithmetic_deref_test4(self, me):
        """
        int test2(int *l) {
            return l[0];
        }
        int test1(int *l) {
            return test2(l);
        }


        int main() {
            int *l;
            int p;

            #pragma STA secret p

            test1(l+p);

            return 0;
        }"""
        with self.assertWarnsRegex(LeakageWarning, r'Leakage of'):
            (_, state, d) = self.helper_func_text(me)
        #print(state.functions["unref"].leaking_vars)

    @passmein
    def test_array_ref_test1(self, me):
        """
        typedef struct{int coeffs[4]; } poly;

        void cmov(unsigned char *r, const unsigned char *x, int len, unsigned char b)
        {
          b = -b;
          for(int i=0;i<len;i++)
            r[i] ^= b & (x[i] ^ r[i]);
        }

        int main() {
            int b = 1;
            #pragma STA secret b
            poly f1 = {.coeffs = {1, 2, 3, 4}}, f2 = {.coeffs = {5,6,7,8}};
            int foo1[4] = {0}, foo2[4] = {1};
            cmov((unsigned char *)&(f1.coeffs), (unsigned char *)&(f2.coeffs), sizeof(int) * 4, b);
            cmov((unsigned char *)&(foo1), (unsigned char *)&(foo2), sizeof(int) * 4, b);
            b = f2.coeffs[0] + foo2[0] + f1.coeffs[0] + foo1[0];
        }
        """
        # Tests whether '&array' is considered the same as plain 'array'
        with self.assertWarns(LeakageWarning):
            (_, state, d) = self.helper_func_text(me)
            G = state.dependency_graph
            self.assertSecret([-d["f1.coeffs"], -d["foo1"]], G)
            self.assertNotSecret([d["f1.coeffs"], d["foo1"], d["f2.coeffs"], d["foo2"], -d["f2.coeffs"], -d["foo2"]], G)


class LeakageAnalysisSwitchTests(LeakageAnalysisTests):
    @passmein
    def test_switch1(self, me):
        """int a, b, c;
            #pragma STA secret c
            switch(a) {
                case 1:
                    b = c;
                case 2:
                    if (b) {}
                    break;
            }
        """
        with self.assertWarnsRegex(LeakageWarning, r'Leakage of b{'):
            (_, _, _) = self.helper_test_text(me.__doc__)

    @passmein
    def test_switch2(self, me):
        """int a, b, c;
            #pragma STA secret c
            switch(a) {
                case 1:
                    b = c;
                    break;
                case 2:
                    if (b) {}
                    break;
            }
        """
        with self.assertNoWarnings():
            (_, _, _) = self.helper_test_text(me.__doc__)

    @passmein
    def test_switch3(self, me):
        """int a, b, c;
            #pragma STA secret c
            switch(a) {
                case 1:
                    if (b) { break; }
                    else {
                    b = c;
                    }
                case 2:
                    if (b) {}
                    break;
            }
        """
        with self.assertWarnsRegex(LeakageWarning, r'Leakage of b{'):
            (_, _, _) = self.helper_test_text(me.__doc__)

    @passmein
    def test_switch_while1(self, me):
        """int a, b, c;
            #pragma STA secret c
            switch(a) {
                case 1:
                    while(b) {
                        break;
                    }
                    b = c;
                case 2:
                    if (b) {}
                    break;
            }
        """
        with self.assertWarnsRegex(LeakageWarning, r'Leakage of b{'):
            (_, _, _) = self.helper_test_text(me.__doc__)

    @passmein
    def test_switch_while2(self, me):
        """int a, b, c;
            #pragma STA secret c
            switch(a) {
                case 1:
                    while(b) {
                        if (a) { return; }
                    }
                    b = c;
                case 2:
                    if (b) {}
                    break;
            }
        """
        with self.assertWarnsRegex(LeakageWarning, r'Leakage of b{'):
            (_, _, _) = self.helper_test_text(me.__doc__)

    @passmein
    def test_switch_while3(self, me):
        """int a, b, c;
            #pragma STA secret c
            switch(a) {
                case 1:
                    while(b) {
                        return;
                    }
                    b = c;
                case 2:
                    if (b) {}
                    break;
            }
        """
        with self.assertNoWarnings():
            (_, _, _) = self.helper_test_text(me.__doc__)

    @passmein
    def test_switch_may1(self, me):
        """int a, b, c;
            #pragma STA secret c
            switch(a) {
                case 1:
                    b = c;
                case 2:
                   b = 1;
                   break;
            }
            if (b) {};
        """
        with self.assertNoWarnings():
            (_, _, _) = self.helper_test_text(me.__doc__)

    @passmein
    def test_switch_may2(self, me):
        """int a, b, c;
            #pragma STA secret c
            switch(a) {
                case 1:
                    b = c;
                    break;
                case 2:
                   b = 1;
                    break;
            }
            if (b) {};
        """
        with self.assertWarnsRegex(LeakageWarning, r'Leakage of b{'):
            (_, _, _) = self.helper_test_text(me.__doc__)

    @passmein
    def test_switch_may3(self, me):
        """int a, b, c;
            #pragma STA secret c
            switch(a) {
                case 1:
                    b = c;
                    if (a) { break; }
                case 2:
                   b = 1;
                    break;
            }
            if (b) {};
        """
        with self.assertWarnsRegex(LeakageWarning, r'Leakage of b{'):
            (_, _, _) = self.helper_test_text(me.__doc__)


class LeakageAnalysisGotoTests(LeakageAnalysisTests):
    @passmein
    def test_simple_goto1(self, me):
        """int a = 0;
        if (a > 0) {
            goto end;
        }
        else {
            return 0;
        }
        end:
            return 1;"""
        (_, _, _) = self.helper_test_text(me.__doc__)

    @passmein
    def test_goto_branch(self, me):
        """int a = 0, b = 1;
        #pragma STA secret a
        if (b > 0) {
            continue;
        }
        else {
            goto clean;
        }
        if (a > 0) {
            b += 1;
        }
        clean:
            a = 0;
            b = 0;
            return;"""

        with self.assertWarnsRegex(LeakageWarning, r'Leakage of'):
            (_, _, _) = self.helper_test_text(me.__doc__)


class LeakageAnalysisPointerArithmeticTest(LeakageAnalysisTests):
    @passmein
    def test_simple_test1(self, me):
        """int secret;
        #pragma STA secret secret
        int *ptr;
        int res = *(ptr+secret);"""
        with self.assertWarnsRegex(LeakageWarning, r'Leakage of secret'):
            (_, state, d) = self.helper_test_text(me.__doc__)

    @passmein
    def test_simple_test2(self, me):
        """ int s = 1;
            #pragma STA secret s
            int *ptr;
            ptr += s;
            if (*ptr) { return 1;}
            return 0;
        """
        with self.assertWarnsRegex(LeakageWarning, r'Leakage of &ptr'):
            (_, state, d) = self.helper_test_text(me.__doc__)
            G = state.dependency_graph
            self.assertSecret({-d["ptr"]}, G)


class LeakageAnalysisGlobalsTest(LeakageAnalysisTests):
    @passmein
    def test_globals1(self, me):
        """char gl_secret[10];
        void set_secret(char *key) {
            gl_secret[0] = key[0];
        }
        void use_secret(char *r) {
            r[0] = gl_secret[0];
        }

        void main()
        {
            char *secret;
            #pragma STA secret secret
            set_secret(secret);
            char *r;
            use_secret(r);
        }
        """

        (_, state, d) = self.helper_func_text(me, verbosity=0)
        self.assertEqual(state.dependency_graph.get_dependency_set(-d["r"]), {SECRET, -d["gl_secret"], -d["r"]})
        self.assertEqual(state.global_dependencies.get_dependency_set(-d["gl_secret"]), {SECRET, -d["gl_secret"]})
    @passmein
    def test_global_array1(self, me):
        """
        static int values[5] = {0,1,2,3,4,5};
        void do_randomness(){
            int foo = 1;
            #pragma STA secret foo
            values[0] = foo;
        }

        int do_sign() {

            if (values[0]) return 1;
            return 2;
        }


        void main() {
            do_randomness();
            do_sign();
        }
        """
        with self.assertWarnsRegex(LeakageWarning, r'Leakage of values'):
            (_, state, d) = self.helper_func_text(me, verbosity=0)


class LeakageAnalysisBooleanOperatorTests(LeakageAnalysisTests):
    @passmein
    def test_warn_shortcut_and(self, me):
        """
        int a, b;
        #pragma STA secret a
        int c = a && b;
        """
        with self.assertWarnsRegex(LeakageWarning, r'Leakage of a'):
            (_, state, d) = self.helper_test_text(me.__doc__)

    @passmein
    def test_warn_shortcut_or(self, me):
        """
        int a, b;
        #pragma STA secret a
        int c = a || b;
        """
        with self.assertWarnsRegex(LeakageWarning, r'Leakage of a'):
            (_, state, d) = self.helper_test_text(me.__doc__)

    @passmein
    def test_warn_bitwise_and(self, me):
        """
        int a, b;
        #pragma STA secret a
        int c = a & b;
        """
        with self.assertWarnsRegex(LeakageWarning, r'Leakage of a'):
            (_, state, d) = self.helper_test_text(me.__doc__)

    @passmein
    def test_warn_bitwise_or(self, me):
        """
        int a, b;
        #pragma STA secret a
        int c = a | b;
        """
        with self.assertWarnsRegex(LeakageWarning, r'Leakage of a'):
            (_, state, d) = self.helper_test_text(me.__doc__)

    @passmein
    def test_warn_bitwise_assign_and(self, me):
        """
        int a, b;
        #pragma STA secret a
        b &= a;
        """
        with self.assertWarnsRegex(LeakageWarning, r'Leakage of a'):
            (_, state, d) = self.helper_test_text(me.__doc__)

    @passmein
    def test_warn_bitwise_assign_or(self, me):
        """
        int a, b;
        #pragma STA secret a
        b |= a;
        """
        with self.assertWarnsRegex(LeakageWarning, r'Leakage of a'):
            (_, state, d) = self.helper_test_text(me.__doc__)

    @passmein
    def test_no_duplicate_warn_shortcut_if(self, me):
        """
        int a, b, c;
        #pragma STA secret a
        if (a && b) {
            c = 1;
        }
        """
        with self.assertWarnsRegex(LeakageWarning, r'Leakage of a'):
            (_, state, d) = self.helper_test_text(me.__doc__)
            # Check the absence of potential leakage types
            self.assertListEqual([],
                                 [l for l in state.leaking_vars
                                  if l.leakage_type in
                                  (LeakageType.PotentialBranchShortcut, LeakageType.PotentialBranchBitwise)
                                  ])

    @passmein
    def test_no_duplicate_warn_bitwise_if(self, me):
        """
        int a, b, c;
        #pragma STA secret a
        if (a & b) {
            c = 1;
        }
        """
        with self.assertWarnsRegex(LeakageWarning, r'Leakage of a'):
            (_, state, d) = self.helper_test_text(me.__doc__)
            # Check the absence of potential leakage types
            self.assertListEqual([],
                                 [l for l in state.leaking_vars
                                  if l.leakage_type in
                                  (LeakageType.PotentialBranchShortcut, LeakageType.PotentialBranchBitwise)
                                  ])

    @passmein
    def test_warn_bitwise_if_on_safe_branch(self, me):
        """
        int a, b, c;
        #pragma STA secret a
        #pragma STA safe_branch
        if (a & b) {
            c = 1;
        }
        """
        with self.assertWarnsRegex(LeakageWarning, r'Leakage of a'):
            (_, state, d) = self.helper_test_text(me.__doc__)
            # Check the presence of potential leakage types
            self.assertNotEqual([],
                                 [l for l in state.leaking_vars
                                  if l.leakage_type in
                                  (LeakageType.PotentialBranchShortcut, LeakageType.PotentialBranchBitwise)
                                  ])


if __name__ == "__main__":
    unittest.main()
