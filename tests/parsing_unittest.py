import sys
import unittest

from stanalysis.scopes import UnknownVariableError

sys.path.extend(['.', '..'])  # need this in order to import the modules
from tests.common_unittest import BaseTest, passmein

from stanalysis.common import Variable, VariableType, LeakageWarning
from stanalysis.codeanalysis import UnknownFunctionDefError, LeakageAnalysis, CodeAnalysis
from stanalysis.leakageanalysis import SECRET
# from stanalysis.dependencygraph import DependencyGraph
from stanalysis.dependencygraph import DependencyGraph

# pylint: disable=wrong-import-position
# pylint: disable-msg=e0107


class ParsingSimpleTests(BaseTest):
    """Simple tests to verify variable declaration."""

    @passmein
    def test_decl(self, me):
        """int a;"""
        (_, state, d) = self.helper_test_text(me.__doc__)

        self.assertEqual(state.vars, {d["a"]})

    @passmein
    def test_multi_decl(self, me):
        """int a;
        int b;"""
        (_, state, d) = self.helper_test_text(me.__doc__)
        self.assertEqual(state.vars, {d["a"], d["b"]})

    @passmein
    def test_multi_dimension_array(self, me):
        """int a[5][5];
        a[0][0] = 1;
        """
        (_, state, d) = self.helper_test_text(me.__doc__)

    @passmein
    def test_fct_ptr1(self, me):
        """int foo(int a) { return a+1;}
        static int(*b)(int) = foo;
        """
        (_, state, _) = self.helper_func_text(me)
        self.assertIn("foo", state.functions.keys())
        self.assertIn("b", state.functions.keys())

    @passmein
    def test_fct_ptr2(self, me):
        """int foo(int a) { return a+1;}
        void main() {
            int(*b)(int);
            b = &foo;
            b = foo;
        }
        """
        (_, state, _) = self.helper_func_text(me)
        self.assertIn("foo", state.functions.keys())
        self.assertIn("b", state.functions.keys())

    @passmein
    def test_fct_ptr3(self, me):
        """int foo(int a) { return a+1;}
        void main() {
            int(*b)(int);
            int(*c)(int);
            b = &foo;
            c = b;
        }
        """
        (_, state, _) = self.helper_func_text(me)
        self.assertIn("foo", state.functions.keys())
        self.assertIn("b", state.functions.keys())
        self.assertIn("c", state.functions.keys())


class ConditionalParsingTests(BaseTest):
    @passmein
    def test_for_decl(self, me):
        """for(int i = 0; i < 10; i++){
            i+= 1;
        }"""
        (_, state, d) = self.helper_test_text(me.__doc__)

    @passmein
    def test_bool_return(self, me):
        """int foo(int a, int b) {
            if (a*b > 0) return 1;
            else { return 0;}
           }
        """
        (_, state, d) = self.helper_func_text(me)
        ret_var = Variable.new("\\RET")._replace(scope=0)
        self.assertEqual(state.functions["foo"].dependency_graph.get_dependency_set(ret_var),
                         {d["a"], d["b"]})

    @passmein
    def test_enum(self, me):
        """enum state1{ONE, TWO};
        typedef enum state2{THREE, FOUR};
        typedef enum state3{FIVE, SIX} state3b;
        extern enum state1 e1 = ONE;
        extern enum state2 e2 = THREE;
        extern enum state3 e3 = FIVE;
        extern state3b e3b = SIX;

        void main() {
            if (e1 == ONE) { e2 = FOUR;}
            return;
        }"""
        (_, state, d) = self.helper_func_text(me)
        self.assertTrue(state.vars.issuperset({d["e1"], d["e2"], d["e3"], d["e3b"]}))

    @passmein
    def test_increment(self, me):
        """
        void main(int a) {
            int b = 0;
            if (a > 0) {
                b++;
            }
        }
        """
        (_, state, d) = self.helper_func_text(me)
        G = state.dependency_graph
        self.assertEqual(G.get_dependency_set(d["b"]), {d["a"]})

    @passmein
    def test_while(self, me):
        """
        void main(int a) {
            int i = 0;
            while(--a > 0) {
                i += 1;
            }
            return i;
        }
        """
        (_, state, d) = self.helper_func_text(me)
        G = state.dependency_graph
        self.assertEqual(G.get_dependency_set(d["i"]), {d["a"]})

    @passmein
    def test_switch(self, me):
        """
        void main(int a, int b, int c, int d) {
            int e = 0, f = 0, g = 0;
            switch(d) {
                case 0:
                    e += a;
                    break;
                case 1:
                    e += b;
                    f = e;
                case 2:
                    e += c;
                    g = e;
            }
        }
        """
        (_, state, d) = self.helper_func_text(me)
        G = state.dependency_graph
        self.assertEqual(G.get_dependency_set(d["e"]), {d["a"], d["b"], d["c"], d["d"]})
        self.assertEqual(G.get_dependency_set(d["f"]), {d["b"], d["d"]})
        self.assertEqual(G.get_dependency_set(d["g"]), {d["b"], d["c"], d["d"]})

    @passmein
    @unittest.expectedFailure
    def test_union_struct(self, me):
        """
        typedef struct seed_s
        {
            union {
                int  raw[32];
                int qwords[4];
            };
        } seed_t;
        typedef struct double_seed_s
        {
            union {
                struct {
                    seed_t s1;
                    seed_t s2;
                };
                int raw[10];
            };
        } double_seed_t;

        int main() {
            double_seed_t ds = {0};
        }
        """
        (_, state, d) = self.helper_func_text(me)
        print(state.structs)


class InitializerParsingTests(BaseTest):

    @passmein
    def test_struct_initialize1(self, me):
        """struct s {
            int a;
            int b;
        };
        int i, j;
        #pragma STA secret i
        struct s v1 = {i};
        struct s v2 = {.a = i, .b = j};
        struct s v3 = {.b = i, .a = j};
        """
        (_, state, d) = self.helper_test_text(me.__doc__, verbosity=0)
        G = state.dependency_graph
        self.assertIn(SECRET, G.get_dependency_set(d["i"]))
        self.assertIn(SECRET, G.get_dependency_set(d["v1.a"]))
        self.assertIn(SECRET, G.get_dependency_set(d["v2.a"]))
        self.assertIn(SECRET, G.get_dependency_set(d["v3.b"]))

    @passmein
    def test_struct_initialize2(self, me):
        """struct s {
            int a;
            int b;
        };
        struct s1 {
            struct s sa;
            struct s sb;
        };
        int i, j;
        #pragma STA secret i
        struct s v1 = {i};
        struct s v2 = {.a = i, .b = j};
        struct s v3 = {.b = i, .a = j};

        struct s1 vv1 = {.sa = v1, .sb = v3};
        struct s1 vv2 = {.sa = {.b = i}, .sb = v2};
        struct s1 vv3 = {.sa = {.a = j, .b = i}, .sb = {.a = j}};
        struct s1 vv4 = {.sa = {.a = vv1.sa.a, .b = j}, .sb = {.a = i}};
        """
        (_, state, d) = self.helper_test_text(me.__doc__, verbosity=0)
        G = state.dependency_graph

        self.assertIn(SECRET, G.get_dependency_set(d["i"]))
        self.assertIn(SECRET, G.get_dependency_set(d["v1.a"]))
        self.assertIn(SECRET, G.get_dependency_set(d["v2.a"]))
        self.assertIn(SECRET, G.get_dependency_set(d["v3.b"]))

        self.assertIn(SECRET, G.get_dependency_set(d["vv4.sa.a"]))
        self.assertIn(SECRET, G.get_dependency_set(d["vv4.sb.a"]))
        self.assertIn(SECRET, G.get_dependency_set(d["vv2.sa.b"]))
        self.assertIn(SECRET, G.get_dependency_set(d["vv3.sa.b"]))

    @passmein
    def test_array_initializer1(self, me):
        """
        int precomp[P_MAX_INDEX][3][MAX_ECC_SIZE_IN_WORDS_SUPPORTED] = {{{0}}};
        """
        (_, state, d) = self.helper_test_text(me.__doc__, verbosity=0)


class StructParsingTests(BaseTest):
    @passmein
    def test_struct_typedef1(self, me):
        """typedef struct
            {
              int a;
              int b;
            } __mpz_struct;

            typedef __mpz_struct mpz_t[1];
        """
        (_, state, d) = self.helper_func_text(me, verbosity=0)

    @passmein
    def test_struct_init1(self, me):
        """typedef struct
            {
              int a;
              int b;
            } foo;

           typedef struct {
            foo f1;
            int e;
            foo g[10];
            foo g2;
            int h[10];
           } bar;


        typedef struct {foo a;foo b;} bar2;
        typedef struct {bar2 a;bar2 b;} bar3;


            int main(int c, int d, int i, int j) {
                int v4[10][5][2] = {[3][2][0]=c, [0]={i,1}, [1][2] = {1,d}};
                bar v2[5] = {[2].e=c,  [9].g[0].a=d, [4].h[0] = d};
                int v3[10] = {[1] = c, d};
                foo v[5] = {[1].a=d,[3].b=c, [4].b=d};
                int v5[3] = {1,j,i};
                foo w = {d,i};
                bar w2 = {c,d,i, j, c};

            }
        """
        (_, state, d) = self.helper_func_text(me, verbosity=0)
        G = state.dependency_graph
        #print(G)
        #print(odict)
        # print(odict["v2.g"])
        # print(d["v2"].get_possible_next_fields(odict, "e"))
        self.assertEqual(G.get_dependency_set(---d["v4"]), {---d["v4"], d["c"], d["i"], d["d"]})
        self.assertEqual(G.get_dependency_set(d["v.a"]), {d["v.a"], d["d"]})
        self.assertEqual(G.get_dependency_set(d["v.b"]), {d["v.b"], d["d"], d["c"]})
        self.assertEqual(G.get_dependency_set(-d["v5"]), {-d["v5"], d["i"], d["j"]})
        self.assertEqual(G.get_dependency_set(d["w.a"]), {d["d"]})
        self.assertEqual(G.get_dependency_set(d["w.b"]), {d["i"]})
        self.assertEqual(G.get_dependency_set(d["w2.f1.a"]), {d["c"]})
        self.assertEqual(G.get_dependency_set(d["w2.f1.b"]), {d["d"]})
        self.assertEqual(G.get_dependency_set(d["w2.e"]), {d["i"]})
        self.assertEqual(G.get_dependency_set(d["w2.g.a"]), {d["j"], d["w2.g.a"]})
        self.assertEqual(G.get_dependency_set(d["w2.g.b"]), {d["c"], d["w2.g.b"]})



class TypeofParsingTests(BaseTest):
    @passmein
    def test_fct_ptr1(self, me):
        """int foo(int a) { return a+1;}
        void main() {
            int(*b)(int);
            __typeof__(foo) c;
            b = &foo;
            c = b;
        }
        """
        (_, state, _) = self.helper_func_text(me, verbosity=0)
        self.assertIn("foo", state.functions.keys())
        self.assertIn("b", state.functions.keys())
        self.assertIn("c", state.functions.keys())

    @passmein
    def test_fct_ptr2(self, me):
        """int foo(int a) { return a+1;}
        void main() {
            int(*b)(int);
            __typeof__(foo) c = foo;
            b = &foo;
            c = b;
        }
        """
        (_, state, _) = self.helper_func_text(me, verbosity=0)
        self.assertIn("foo", state.functions.keys())
        self.assertIn("b", state.functions.keys())
        self.assertIn("c", state.functions.keys())

    @passmein
    def test_fct_ptr3(self, me):
        """int foo(int a) { return a+1;}
        void main() {
            int(*b)(int);
            __typeof__(foo) c = foo;
            b = &foo;
            c = b;
        }
        """
        (_, state, _) = self.helper_func_text(me, verbosity=0)
        self.assertIn("foo", state.functions.keys())
        self.assertIn("b", state.functions.keys())
        self.assertIn("c", state.functions.keys())

    @passmein
    def test_fct_ptr4(self, me):
        """int foo(int a) { return a+1;}
        void main() {
            int(*b)(int);
            __typeof__(foo) c = foo;
            b = &foo;
        }
        """
        (_, state, _) = self.helper_func_text(me, verbosity=0)
        self.assertIn("foo", state.functions.keys())
        self.assertIn("b", state.functions.keys())
        self.assertIn("c", state.functions.keys())

    @passmein
    def test_fct_ptr_arg1(self, me):
        """int ptr_func(int a, int b, int(*cmp)(int, int))
        {
            return cmp(a,b);
        }
        """
        with self.assertWarnsRegex(UserWarning, r'Unknown function cmp'):
            ast = self.parser.parse(me.__doc__, filename="<none>")
            la = LeakageAnalysis()
            la.depends_on(ast.ext[0])


    @passmein
    def test_prototype_fct1(self, me):
        """int foo(int);
        int foo(int a) {return a+1;}
        static __typeof__(foo) *b = (__typeof__(foo)*)foo;
        """
        (_, state, d) = self.helper_func_text(me, verbosity=0)
        self.assertIn("foo", state.functions.keys())
        self.assertIn("b", state.functions.keys())

    @passmein
    def test_prototype_fct2(self, me):
        """int foo(int);
        static __typeof__(foo) *b = (__typeof__(foo)*)foo;
        int foo(int a) {return a+1;}
        """
        (_, state, d) = self.helper_func_text(me, verbosity=0)
        self.assertIn("foo", state.functions.keys())
        self.assertIn("b", state.functions.keys())

    @passmein
    def test_prototype_fct3(self, me):
        """int foo(union{int i; char c;});
        static __typeof__(foo) *b = (__typeof__(foo)*)foo;
        int foo(int a) {return a+1;}
        """
        (_, state, d) = self.helper_func_text(me, verbosity=0)
        self.assertIn("foo", state.functions.keys())
        self.assertIn("b", state.functions.keys())

    @passmein
    def test_prototype_fct4(self, me):
        """int foobar(int i);
        int foo(int a) {return foobar(a)+1;}
        """
        with self.assertRaises(UnknownFunctionDefError):
            (_, state, d) = self.helper_func_text(me, verbosity=0)

    @passmein
    def test_prototype_fct5(self, me):
        """struct bar_t{
            int(*field)(int);
            int stuff;
        };
        int foo(int a) {return a+1;}
        void main() {
            struct bar_t bar = {.field = foo, .stuff=0};
            bar.field(1);
        }
        """
        (_, state, d) = self.helper_func_text(me, verbosity=0)
        self.assertIn("foo", state.functions.keys())
        #self.assertIn("b", state.functions.keys())

    @passmein
    def test_allocated_struct1(self, me):
        """struct bar{int a; int b;};
        struct bar instanciate(int c, int d) {
            struct bar sb;
            sb.a = d;
            sb.b = c;
            return sb;
        }

        int use_struct(struct bar sb) {
            return sb.a + sb.b;
        }
        int main(int e, int f) {
            return use_struct(instanciate(e, f));
        }
        """
        (_, state, d) = self.helper_func_text(me, verbosity=0)
        self.assertEqual(state.dependency_graph.get_dependency_set(d["\\RET"]), {d["e"], d["f"]})

    @passmein
    def test_opaque_union1(self, me):
        """typedef unsigned uint64_t;typedef unsigned size_t;
        typedef struct {
            union {
                    unsigned char d[4096];
                    uint64_t dummy_u64;
            } buf;
            size_t ptr;
            union {
                    unsigned char d[256];
                    uint64_t dummy_u64;
            } state;
            int type;
        } prng;

        int main (prng *p) {
            #pragma STA secret p.buf
            if (p.buf.d[0] > 0) return 0;
            return 1;
        }
        """
        with self.assertWarnsRegex(LeakageWarning, r'Leakage of p.buf'):
            (_, state, d) = self.helper_func_text(me, verbosity=0)
            print(state.structs)

    @passmein
    def test_opaque_union2(self, me):
        """typedef char uint8_t;
        typedef long long uint64_t;
        typedef struct seed_s
        {
            union {
                uint8_t  raw[32];
                uint64_t qwords[4];
            };
        } seed_t;

        typedef struct double_seed_s
        {
            union {
                struct {
                    seed_t s1;
                    seed_t s2;
                };
                uint8_t raw[sizeof(seed_t) * 2ULL];
            };
        } double_seed_t;

        int foo(double_seed_t *seeds) {
            return 0;
        }

        int main()
        {
            double_seed_t seeds;
            seeds.raw[0] = 1;
            return 0;
        }

        """
        (_, state, d) = self.helper_func_text(me, verbosity=0)


class VariadicFunctionTests(BaseTest):
    @passmein
    def test_printf1(self, me):
        """int printf( const char *restrict format, ... );
        int main(){
            printf("%d %d", 1, 2);
        }
        """
        (_, state, d) = self.helper_func_text(me, verbosity=0, functions_file="functions.xml")

    @passmein
    def test_printf2(self, me):
        """int printf( const char *restrict format, ... );
        int main(){
            int i = 1;
            #pragma STA secret i
            int ret = printf("%d %d", 1, i);
        }
        """
        with self.assertWarnsRegex(LeakageWarning, r'Leakage of i'):
            (_, state, d) = self.helper_func_text(me, verbosity=0, functions_file="functions.xml")
            G = state.dependency_graph
            self.assertNotIn(SECRET, G.get_dependency_set(d["ret"]))


class StrucFuncPtrTests(BaseTest):
    @passmein
    def test_struct_func1(self, me):
        """struct foo {int a; int b;};
        struct foo bar(int x, int y) {
            struct foo res = {.a = x, .b = y};
            return res;
        }
        void foo2(struct foo *f, int x, int y)
        {
            f->a = x;
            f->b = y;
        }

        void foo3(struct foo *f, int x, int y)
        {
            f[0]->a = x;
            f[0]->b = y;
        }
        void main(int c, int d) {
            struct foo f = bar(c, d);
            struct foo f2;
            foo2(&f2, c, d);
            struct foo f3[1];
            foo3(f, c, d);
        }
        """
        (_, state, d) = self.helper_func_text(me, verbosity=0, functions_file="functions.xml")
        G = state.dependency_graph
        self.assertEqual(G.get_dependency_set(d["f.a"]), {d["c"]})
        self.assertEqual(G.get_dependency_set(d["f.b"]), {d["d"]})
        self.assertEqual(G.get_dependency_set(d["f2.a"]), {d["c"]})
        self.assertEqual(G.get_dependency_set(d["f2.b"]), {d["d"]})

        #print(state.functions["foo2"].dependency_graph)

    @passmein
    def test_struct_func1b(self, me):
        """struct foo {int a; int b;};
        struct foo bar(int x, int y) {
            struct foo res = {.a = x, .b = y};
            return res;
        }
        void main(int c, int d) {
            struct foo f = bar(c, d);
        }
        """
        (_, state, d) = self.helper_func_text(me, verbosity=0, functions_file="functions.xml")
        G = state.dependency_graph
        self.assertEqual(G.get_dependency_set(d["f.a"]), {d["c"]})
        self.assertEqual(G.get_dependency_set(d["f.b"]), {d["d"]})


    @passmein
    def test_struct_func2(self, me):
        """typedef unsigned int size_t;
        void *malloc(size_t size) {
            for (int i = 0; i < 0; i++) {}
            return (void *)0;
        }
        struct foo {int a; int b;};
        struct foo *bar(int e, int f) {
            struct foo *res = malloc(sizeof(struct foo));
            res->a = f;
            res->b = e;
            return res;
        }
        void main(int c, int d) {
            struct foo *f = bar(c, d);
        }
        """
        (_, state, d) = self.helper_func_text(me, verbosity=0, functions_file="functions.xml")
        G = state.dependency_graph
        self.assertEqual(G.get_dependency_set(d["f.a"]), {d["d"]})
        self.assertEqual(G.get_dependency_set(d["f.b"]), {d["c"]})

    @passmein
    def test_struct_func3(self, me):
        """struct foo {
            int a;
            int b;
        };
        int main(struct foo f) {
            return f.a;
        }"""
        (_, state, d) = self.helper_func_text(me, verbosity=0, functions_file="functions.xml")
        G = state.dependency_graph
        self.assertEqual(G.get_dependency_set(Variable.new("\\RET")._replace(scope=0)), {d["f.a"]._replace(scope=0)})

    @passmein
    def test_struct_func4(self, me):
        """
        struct foo {int a; int b;};
        struct foo bar(int e, int f) {
            struct foo res;
            res->a = f;
            res->b = e;
            #pragma STA secret res.a
            return res;
        }
        void main(int c, int d) {
            struct foo f = bar(c, d);
        }
        """
        (_, state, d) = self.helper_func_text(me, verbosity=0, functions_file="functions.xml")
        G = state.dependency_graph
        self.assertEqual(G.get_dependency_set(d["f.a"]), {d["d"], SECRET})
        self.assertEqual(G.get_dependency_set(d["f.b"]), {d["c"]})

    @passmein
    def test_struct_internal_ptr1(self, me):
        """struct foo {
            int *ptr;
           };
        int main(struct foo f, int a) {
            struct foo b;
            *(b.ptr) = a;
            f.ptr = b.ptr;
        }
        """
        # Expected behaviour when pointers cannot be arrays
        DependencyGraph.set_strict_pointers()
        (_, state, d) = self.helper_func_text(me, verbosity=0, functions_file="functions.xml")
        G = state.dependency_graph
        self.assertEqual(G.get_dependency_set(-d["f.ptr"]._replace(scope=1)), {d["a"]})
        self.assertEqual(G.get_dependency_set(-d["b.ptr"]), {d["a"]})

        # Expected behaviour when pointers might represent arrays
        DependencyGraph.set_array_pointers()
        (_, state, d) = self.helper_func_text(me, verbosity=0, functions_file="functions.xml")
        G = state.dependency_graph
        self.assertEqual(G.get_dependency_set(-d["f.ptr"]._replace(scope=1)), {-d["b.ptr"], d["a"], -d["f.ptr"]._replace(scope=0)})
        self.assertEqual(G.get_dependency_set(-d["b.ptr"]), {-d["b.ptr"], d["a"]})

    @passmein
    def test_ptr_func1(self, me):
        """int *bar(int a, int *ptr_b) {
            int *res = ptr_b;
            *res = a;
            return res;
        }
        void main(int c, int d) {
            int * ptr;
            *ptr = c;
            int *ptr2 = bar(d, ptr);
        }
        """
        # Expected behaviour when pointers cannot be arrays
        DependencyGraph.set_strict_pointers()
        (_, state, d) = self.helper_func_text(me, verbosity=0, functions_file="functions.xml")
        G = state.dependency_graph
        #print(state.functions["bar"].dependency_graph)
        #print(state.dependency_graph)
        self.assertEqual(G.get_dependency_set(d["ptr2"]), {d["ptr"]})
        self.assertEqual(G.get_dependency_set(-d["ptr2"]), {d["d"]})
        self.assertEqual(G.get_dependency_set(-d["ptr"]), {d["d"]})

        # Expected behaviour when pointers might represent arrays
        DependencyGraph.set_array_pointers()
        (_, state, d) = self.helper_func_text(me, verbosity=0, functions_file="functions.xml")
        G = state.dependency_graph
        #print(state.functions["bar"].dependency_graph)
        #print(state.dependency_graph)
        self.assertEqual(G.get_dependency_set(d["ptr2"]), {d["ptr"], d["ptr2"]})
        self.assertEqual(G.get_dependency_set(-d["ptr2"]), {d["d"], d["c"], -d["ptr"], -d["ptr2"]})
        self.assertEqual(G.get_dependency_set(-d["ptr"]), {-d["ptr"], d["d"], d["c"]})

    @passmein
    def test_ptr_func2(self, me):
        """void swap(int *ptr_a, int *ptr_b) {
           int interm = *ptr_a;
           *ptr_a = *ptr_b;
           *ptr_b = interm;
        }
        void main(int *ptr_a, int *ptr_b) {
            swap(ptr_a, ptr_b);
        }
        """
        # Expected behaviour when pointers cannot be arrays
        DependencyGraph.set_strict_pointers()

        (_, state, d) = self.helper_func_text(me, verbosity=0, functions_file="functions.xml")
        G = state.dependency_graph

        self.assertEqual(G.get_dependency_set(-d["ptr_a"]._replace(scope=0)), {-d["ptr_b"]})
        self.assertEqual(G.get_dependency_set(-d["ptr_b"]._replace(scope=0)), {-d["ptr_a"]})
        self.assertEqual(G.get_dependency_set(d["ptr_a"]), {d["ptr_a"]})
        self.assertEqual(G.get_dependency_set(d["ptr_b"]), {d["ptr_b"]})

    @passmein
    def test_ptr_func3(self, me):
        """void main(int **ptr_a, int **ptr_b) {
            int interm = **ptr_a;
           **ptr_a = **ptr_b;
           **ptr_b = interm;
        }
        """
        # Expected behaviour when pointers cannot be arrays
        DependencyGraph.set_strict_pointers()

        (_, state, d) = self.helper_func_text(me, verbosity=1, functions_file="functions.xml")
        G = state.dependency_graph
        #print(G)
        self.assertEqual(G.get_dependency_set(--d["ptr_a"]._replace(scope=1)), {--d["ptr_b"]})
        self.assertEqual(G.get_dependency_set(--d["ptr_b"]._replace(scope=1)), {--d["ptr_a"]})
        self.assertEqual(G.get_dependency_set(d["ptr_a"]), {d["ptr_a"]})
        self.assertEqual(G.get_dependency_set(d["ptr_b"]), {d["ptr_b"]})

    @passmein
    def test_ptr_func4(self, me):
        """void main(int *ptr_a, int *ptr_b) {
           int interm = *ptr_a;
           *ptr_a = *ptr_b;
           *ptr_b = interm;
           int res = *ptr_a;
        }
        """
        # Expected behaviour when pointers cannot be arrays
        DependencyGraph.set_strict_pointers()

        (_, state, d) = self.helper_func_text(me, verbosity=0, functions_file="functions.xml")
        G = state.dependency_graph
        self.assertEqual(G.get_dependency_set((-d["ptr_b"])._replace(scope=1)), {-d["ptr_a"]})
        self.assertEqual(G.get_dependency_set((-d["ptr_a"])._replace(scope=1)), {-d["ptr_b"]})
        self.assertEqual(G.get_dependency_set(d["ptr_a"]), {d["ptr_a"]})
        self.assertEqual(G.get_dependency_set(d["ptr_b"]), {d["ptr_b"]})

    @passmein
    def test_ptr_ret1(self, me):
        """
        typedef struct {
            int a;
        } bar_t;
        char * foo(bar_t val) {
            char *res = "0";
            if (val.a > 0) { res = "";}
            res[0] = val.a;
            return res;
        }
        void main() {
            bar_t secret, secret2;
            int a = 0;
            #pragma STA secret secret.a secret2.a
            char ** vals;
            vals[0] = foo((a > 0 ? secret: secret2));
        }"""
        with self.assertWarnsRegex(LeakageWarning, r'Leakage of secret2?.a'):
            (_, state, d) = self.helper_func_text(me, verbosity=0, functions_file="functions.xml")
            G = state.dependency_graph
            self.assertIn(SECRET, G.get_dependency_set(--d["vals"]))

    @passmein
    def test_ptr_struct_crystal(self, me):
        """int N = 100, QINV=45, Q=25;
        typedef int uint64_t;
        typedef int uint32_t;
        typedef struct {
          int coeffs[N];
        } poly;
        uint32_t montgomery_reduce(uint64_t a) {
          const uint64_t qinv = QINV;
          uint64_t t;

          t = a * qinv;
          t &= (1UL << 32) - 1;
          t *= Q;
          t = a + t;
          return t >> 32;
        }
        void main(poly *c, const poly *a, const poly *b) {
            unsigned int i;
            for(i = 0; i < N; ++i)
                c->coeffs[i] = montgomery_reduce((uint64_t)a->coeffs[i] * b->coeffs[i]);
        }
        """
        (_, state, d) = self.helper_func_text(me, verbosity=0, functions_file="functions.xml")
        G = state.dependency_graph  # type: DependencyGraph
        self.assertEqual(G.get_dependency_set(-d["c.coeffs"]),
                         {-v._replace(scope=0) for v in {d["a.coeffs"], d["b.coeffs"], d["c.coeffs"]}}.union({
                             d["N"], d["Q"], d["QINV"]
                         }))

    @passmein
    @unittest.expectedFailure
    def test_recursive_goto(self, me):
        """static void
        poly_small_mkgauss(int *f, unsigned logn)
        {
            unsigned n, u;
            unsigned mod2;

            n = logn;
            mod2 = 0;
            for (u = 0; u < n; u ++) {
                int s;

            restart:
                s = 1;
                if (u == n - 1) {
                    if ((mod2 ^ (unsigned)(s & 1)) == 0) {
                        goto restart;
                    }
                } else {
                    mod2 ^= (unsigned)(s & 1);
                }
                f[u] = s;
            }
        }
        """
        # Recursive goto's are not supported yet.
        (_, state, d) = self.helper_func_text(me, verbosity=0)

    @passmein
    def test_function_ret(self, me):
        """typedef struct { double v; } fpr;

        static inline fpr
        FPR(double y)
        {
            fpr x;

            x.v = y;
            return x;
        }

        static inline fpr
        fpr_of(long i)
        {
            return FPR((double)i);
        }
        int main(int a) {
            fpr v = FPR(a);
            fpr v2 = fpr_of(a);

        }
        """
        (_, state, d) = self.helper_func_text(me, verbosity=0, functions_file="functions.xml")
        G = state.dependency_graph  # type: DependencyGraph
        self.assertEqual(G.get_dependency_set(d["v.v"]), {d["a"]._replace(scope=0)})
        self.assertEqual(G.get_dependency_set(d["v2.v"]), {d["a"]._replace(scope=0)})

    @passmein
    def test_function_rec(self, me):
        """int rec(int n, int p) {
            if(n==0) return 1;
            return rec(n-1, p-1)*n*p;
        }
        int main(void) {return 0;}"""
        with open("tmp.c", "w+") as f:
            f.write(me.__doc__)
        ca = CodeAnalysis()
        ca.analyze("tmp.c")

    @passmein
    def test_luov_function(self, me):
        """
        typedef int f16FELT;
        typedef struct {
           f16FELT coef[4];
           char test[10];
        } f64FELT;

        f16FELT f16deserialize_FELT(char *R) { return (int) R[0]; }

        f64FELT f64deserialize_FELT(char *R) {
            f64FELT new;

            new.coef[3] = f16deserialize_FELT(R);
            new.coef[2] = f16deserialize_FELT(R);
            new.coef[1] = f16deserialize_FELT(R);
            new.coef[0] = f16deserialize_FELT(R);
            new.test = R;
            return new;
            }

            int main(char *input) {
                f64FELT foo = f64deserialize_FELT(input);
            return 0;}
        """
        DependencyGraph.set_array_pointers()
        (_, state, d) = self.helper_func_text(me, verbosity=0)
        G = state.dependency_graph
        self.assertIn(-d["input"], G.get_dependency_set(-d["foo.coef"]))
        self.assertIn(-d["input"], G.get_dependency_set(-d["foo.test"]))
        self.assertIn(d["input"], G.get_dependency_set(d["foo.test"]))


class FunctionPtrTests(BaseTest):
    @passmein
    def test_func_ptr1(self, me):
        """
        typedef int (*func_ptr)(int);

        int bar(func_ptr f) {
            return f(1);
        }
        int foo(void) {


        }


        """
        (_, state, d) = self.helper_func_text(me, verbosity=0)


class ScopeTests(BaseTest):
    @passmein
    def test_faulty_scope1(self, me):
        """
        int main(int a, int b) {
            int d = 0;

            {
                int c = b;
            }

            c+= a;
        }
        """
        try:
            (_, state, d) = self.helper_func_text(me, verbosity=0)
        except UnknownVariableError:
            return
        self.fail("Wrong scope for variable c not detected.")


if __name__ == "__main__":
    unittest.main()
