import sys
import unittest

sys.path.extend(['.', '..'])  # need this in order to import the modules

from tests.common_unittest import BaseTest, passmein

from stanalysis.common import Variable, VariableType, LeakageWarning
from stanalysis.leakageanalysis import SECRET
from stanalysis.common_analysis import UnknownFunctionDefError

# pylint: disable=wrong-import-position
# pylint: disable-msg=e0107


class SpectreSimpleTests(BaseTest):
    """Simple tests to verify Spectre-type vulnerability detection."""
    @passmein
    def test_spectre_simple1(self, me):
        """int array1[], array2[], array_length = 10, x, y;
        #pragma STA user_input x
        if(x < array_length){
            y = array2[array1[x]];
        }
        """
        with self.assertWarnsRegex(LeakageWarning, r'Leakage of array1'):
            (_, state, d) = self.helper_test_text(me.__doc__, verbosity=0)


if __name__ == "__main__":
    unittest.main()
