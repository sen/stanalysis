import sys
import unittest

sys.path.extend(['.', '..'])  # need this in order to import the modules

from stanalysis.time_resolution import TimeResolution, Generator
from pycparserext.ext_c_parser import GnuCParser


class AstTransformationTest(unittest.TestCase):
    code = """int main() {
    for(int i =  0; i<1; i++){
        i++;
    }
    int foo = 1;
    while(foo < 5) 
    {
        foo++;
    }
    while(foo < 10) 
        foo++;
    if (foo > 0)
        i--;
    
    if (foo < 0)
        i--;
    else
        i++;
    }"""

    def transform(self, coords: str, alternative: bool = False):
        ast = GnuCParser().parse(self.code, filename="file.c")
        callback = TimeResolution.get_transformation("file.c:"+coords, alternative=alternative)
        TimeResolution.transform_function_time_resolution(ast, "file.c", callback)
        return Generator().visit(ast.ext[6])

    def test_for1(self):
        res = self.transform("2:9", alternative=False)
        self.assertEqual(res, "int main()\n{\n  for (int i = 0; i < 1; i++)\n  {\n    ___time_res_table["
                              "___time_res_counter++] = rdtscp();\n    i++;\n  }\n\n  int foo = 1;\n  while (foo < "
                              "5)\n  {\n    foo++;\n  }\n\n  while (foo < 10)\n    foo++;\n\n  if (foo > 0)\n    "
                              "i--;\n\n  if (foo < 0)\n    i--;\n  else\n    i++;\n\n}\n\n")

    def test_for2(self):
        res = self.transform("2:9", alternative=True)
        self.assertEqual(res, "int main()\n{\n  ___time_res_table[___time_res_counter++] = rdtscp();\n  for (int i = "
                              "0; i < 1; i++)\n  {\n    i++;\n  }\n\n  int foo = 1;\n  while (foo < 5)\n  {\n    "
                              "foo++;\n  }\n\n  while (foo < 10)\n    foo++;\n\n  if (foo > 0)\n    i--;\n\n  if (foo "
                              "< 0)\n    i--;\n  else\n    i++;\n\n}\n\n")

    def test_inst(self):
        res = self.transform("5:9", alternative=True)
        self.assertEqual(res, "int main()\n{\n  for (int i = 0; i < 1; i++)\n  {\n    i++;\n  }\n\n  "
                              "___time_res_table[___time_res_counter++] = rdtscp();\n  int foo = 1;\n  while (foo < "
                              "5)\n  {\n    foo++;\n  }\n\n  while (foo < 10)\n    foo++;\n\n  if (foo > 0)\n    "
                              "i--;\n\n  if (foo < 0)\n    i--;\n  else\n    i++;\n\n}\n\n")

    def test_while1(self):
        res1 = self.transform("10:11", alternative=False)
        res2 = self.transform("10:5", alternative=False)
        res3 = self.transform("10:17", alternative=False)
        self.assertEqual(res1, res2)
        self.assertEqual(res1, res3)
        self.assertEqual(res2, "int main()\n{\n  for (int i = 0; i < 1; i++)\n  {\n    i++;\n  }\n\n  int foo = 1;\n  "
                               "while (foo < 5)\n  {\n    foo++;\n  }\n\n  while (foo < 10)\n  {\n    "
                               "___time_res_table[___time_res_counter++] = rdtscp();\n    foo++;\n  }\n\n  if (foo > "
                               "0)\n    i--;\n\n  if (foo < 0)\n    i--;\n  else\n    i++;\n\n}\n\n")

    def test_while2(self):
        res1 = self.transform("10:11", alternative=True)
        res2 = self.transform("10:5", alternative=True)
        res3 = self.transform("10:17", alternative=True)
        self.assertEqual(res1, res2)
        self.assertEqual(res1, res3)
        self.assertEqual(res2, "int main()\n{\n  for (int i = 0; i < 1; i++)\n  {\n    i++;\n  }\n\n  int foo = 1;\n  "
                               "while (foo < 5)\n  {\n    foo++;\n  }\n\n  ___time_res_table[___time_res_counter++] = "
                               "rdtscp();\n  while (foo < 10)\n    foo++;\n\n  if (foo > 0)\n    i--;\n\n  if (foo < "
                               "0)\n    i--;\n  else\n    i++;\n\n}\n\n")

    def test_if1(self):
        res = self.transform("12:9", alternative=False)
        self.assertEqual(res,
                         "int main()\n{\n  for (int i = 0; i < 1; i++)\n  {\n    i++;\n  }\n\n  int foo = 1;\n  while "
                         "(foo < 5)\n  {\n    foo++;\n  }\n\n  while (foo < 10)\n    foo++;\n\n  if (foo > 0)\n  {\n  "
                         "  ___time_res_table[___time_res_counter++] = rdtscp();\n    i--;\n  }\n\n  if (foo < 0)\n   "
                         " i--;\n  else\n    i++;\n\n}\n\n")

    def test_if2(self):
        res = self.transform("12:9", alternative=True)
        res2 = self.transform("12:5", alternative=True)
        self.assertEqual(res, res2)
        self.assertEqual(res, "int main()\n{\n  for (int i = 0; i < 1; i++)\n  {\n    i++;\n  }\n\n  int foo = 1;\n  "
                              "while (foo < 5)\n  {\n    foo++;\n  }\n\n  while (foo < 10)\n    foo++;\n\n  "
                              "___time_res_table[___time_res_counter++] = rdtscp();\n  if (foo > 0)\n    i--;\n\n  if "
                              "(foo < 0)\n    i--;\n  else\n    i++;\n\n}\n\n")


