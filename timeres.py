#!/usr/bin/env python3
import argparse
import os
import subprocess
import warnings

from pycparser import parse_file
from pycparserext.ext_c_parser import GnuCParser
from pycparser.c_ast import *
from elftools.common.exceptions import ELFError

from stanalysis.asmtools import get_functions
from stanalysis.time_resolution import TimeResolution, MainAlreadyPresentError


SIGN_PROBE_FILE = """#include "rng.h"
#include "api.h"
#include <stdint.h>

void ___time_res_print(void);

static inline uint64_t rdtscp() {
  uint32_t low, high;
  asm volatile ("rdtscp": "=a" (low), "=d" (high) :: "ecx");
  return (((uint64_t)high) << 32) | low;
}

int main(int argc, char* argv[]) {

    char seed[48];
    char msg[] = "azertyuiopqsdfghjklmwxcvbn";
    unsigned char pk[CRYPTO_PUBLICKEYBYTES], sk[CRYPTO_SECRETKEYBYTES];

    uint64_t t = rdtscp();
    for (int i = 0; i < 8; i++)
        seed[i] = (t>>(8*i)) % 256;
    for (int i = 8; i < 48; i++)
        seed[i] = i;

    randombytes_init(seed, NULL, 256);
    crypto_sign_keypair(pk, sk);

    unsigned long long smlen = 0;
    unsigned char sm[CRYPTO_BYTES];

    crypto_sign(sm, &smlen, msg, sizeof(msg), sk);

    ___time_res_print();

}
"""

KEM_PROBE_FILE = """#include "rng.h"
#include "api.h"
#include <stdint.h>

void ___time_res_print(void);
void ___time_res_reset(void);

static inline uint64_t rdtscp() {
  uint32_t low, high;
  asm volatile ("rdtscp": "=a" (low), "=d" (high) :: "ecx");
  return (((uint64_t)high) << 32) | low;
}

int main(int argc, char* argv[]) {

    char seed[48];
    char ss[CRYPTO_BYTES];
    unsigned char pk[CRYPTO_PUBLICKEYBYTES], sk[CRYPTO_SECRETKEYBYTES];

    uint64_t t = rdtscp();
    for (int i = 0; i < 8; i++)
        seed[i] = (t>>(8*i)) % 256;
    for (int i = 8; i < 48; i++)
        seed[i] = i;

    randombytes_init(seed, NULL, 256);
    crypto_kem_keypair(pk, sk);

    for (int i = 0; i < CRYPTO_BYTES; i++) {
        ss[i] = i%256;
    }
    unsigned long long smlen = 0;
    unsigned char ct[CRYPTO_CIPHERTEXTBYTES];

    crypto_kem_enc(ct, ss, pk);

    ___time_res_reset();
    
    crypto_kem_dec(ss, ct, sk);

    ___time_res_print();

}
"""


def is_signature_scheme():
    if not os.path.isfile("api.h"):
        raise RuntimeError("No api.h file, cannot determine whether a signature or KEM scheme is being analyzed.")
    with open("api.h") as f:
        return "crypto_sign(" in f.read()


def has_main_function(fn):
    """Return True if the C file fn has a main function."""
    ast = parse_file(fn, parser=GnuCParser(), use_cpp=True, cpp_args=["-E", "-D__extension__=", "-std=c99"])
    for inst in ast.ext:
        if isinstance(inst, FuncDef) and inst.decl.name == "main":
            return True
    return False


def get_obj_files_without_main():
    files = []
    for fn in os.listdir("."):
        if not os.path.isdir(fn):
            try:
                if "main" not in get_functions(fn):
                    files.append(fn)
            except ELFError:
                pass
    return files


def get_c_files_without_main():
    import glob
    c_files = []
    for pathname in glob.glob("*.c"):
        if not has_main_function(pathname):
            c_files.append(pathname)
    return c_files


def determine_compile_command():
    # Idea: first compile all files. If there are compiled object files with no main,
    # use these to compile the probe.c file. Else, look at all .c files, and compile all
    # that have no main function. Then, remove all files created by main (we keep a diff between the
    # files before and after make was called). Finally, create the correct probe.c file
    
    # Step 1: compile and look at the files
    old_files = set(os.listdir("."))
    subprocess.call("make")

    obj_files = get_obj_files_without_main()
    if obj_files:
        # Every file is compiled independently
        gcc_cmd = "make; gcc -o probe.o -I. -c probe.c; gcc -I. -o probe %s probe.o -lm -lkeccak -lcrypto" % (" ".join(obj_files))
    else:
        c_files = get_c_files_without_main()
        # All files are compiled in one go. Let's do the same
        gcc_cmd = "gcc -I. -o probe probe.c %s -lm -lkeccak -lcrypto" % (" ".join(c_files))

    print(gcc_cmd)
    # Step 2: Now, remove the created files
    for f in set(os.listdir(".")).difference(old_files):
        if os.path.isfile(f):
            os.remove(f)

    # Step 3: Create the correct probe file
    if is_signature_scheme():
        with open("probe.c", "w") as f:
            f.write(SIGN_PROBE_FILE)
    else:
        with open("probe.c", "w") as f:
            f.write(KEM_PROBE_FILE)

    return gcc_cmd


if __name__ == "__main__":
    # parser = argparse.ArgumentParser(usage="""python timeres.py coord
    # where coord are the coordinates of the leaking instruction, in the format filename:line_no:col_no,
    # as provided by stanalyzer.py.""")

    parser = argparse.ArgumentParser()

    parser.add_argument("nb", help="Number of probes to take (default:100)", default=100, type=int)
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument("--coord", help="Coordinates of the instruction to target, format: filename:file_no:col_no")
    group.add_argument("--from-csv", help="Perform probes for all relevant locations from the provided CSV file "
                                          "(it should be tab separated and have a column with header Reference. This "
                                          "is the case for the CSV files generated by stanalyzer.py).")

    opts = parser.parse_args()

    if opts.from_csv:
        import csv
        references = set()
        with open(opts.from_csv) as f:
            reader = csv.DictReader(f, delimiter="\t")
            for row in reader:
                if "Reference" not in row:
                    raise ValueError("No column with header Reference in CSV file %s." % opts.from_csv)
                ref = row["Reference"]  # type: str
                if not ref.startswith(("/", ".")) and ref.count(":") == 2:
                    references.add(ref)
    else:
        references = [opts.coord]

    command_after = determine_compile_command()
    print(command_after)
    for ref in references:
        print(ref)
        try:
            tr = TimeResolution(ref, opts.nb, "", command_after,
                                copy_filter=["*.txt", "*.csv"], parse_options=["-I."])
            results, alternative_results = tr.perform_timing_probe()
            TimeResolution.save_probe_to_file(results, ref, "probe_res.txt")
            if alternative_results is not None:
                TimeResolution.save_probe_to_file(alternative_results, ref+":alt", "probe_res.txt")
        except MainAlreadyPresentError:
            warnings.warn("Could not probe for reference %s: file already has a main function." % ref)
