#include <_fake_typedefs.h>
#include <openssl/evp.h>
#include <openssl/aes.h>
#include <openssl/sha.h>
#include <openssl/bn.h>
#include <openssl/ec.h>
#include <openssl/hmac.h>
#include <emmintrin.h>
typedef int FILE;
typedef int locale_t;

int toupper(int c) { return c; }
int tolower(int c) { return c; } 

int toupper_l(int c, locale_t locale) { return c; }
int tolower_l(int c, locale_t locale) { return c; }

char *strdup(const char *s);


void *malloc(size_t size) {
//    for (int i = 0; i < 0; i++) {/*allocate memory, presumably*/}
    return (void *)0;
}

int posix_memalign(void **memptr, size_t alignment, size_t size) {
    return 1;
}

void free(void *ptr) {
    if(ptr != (void*)0){
        /* deallocate memory, presumabely */
    }
}

void *calloc(size_t nmemb, size_t size) {
    return malloc(nmemb*size);
}

void *realloc(void *ptr, size_t size){
    void* new_ptr = malloc(size);
    *new_ptr = *ptr;
    //while(*ptr != '\0') // not really but whatever
    //{
    //    *new_ptr++ = *ptr;
    //    *ptr++;
    //}
    return new_ptr;
}

void *memcpy(void *dest, const void *src, size_t n) {
    for(int i = 0; i < n; i++) {
        dest[i] = src[i];
    }
    return dest;
}

void *memmove(void *dest, const void *src, size_t n) {
    return memcpy(dest, src, n);
}

char *strcpy(char *dest, const char *src) {
    while(*src != '\0'){
        *dest++ = *src++;
    }
    *src = '\0';
    return dest;
}

int strlen(char *s)
{
    int res;
    for(res = 0; *s++; res++);
    return res;
}

char *strncpy(char *dest, const char *src, size_t n)
{
    size_t i;

    for (i = 0; i < n && src[i] != '\0'; i++)
        dest[i] = src[i];
    for ( ; i < n; i++)
        dest[i] = '\0';

    return dest;
}

int strncmp(const char *s1, const char *s2, size_t n){
    size_t i = 0;
    for (;i < n; i++){
        if (s1[i] == '\0' && s2[i] == '\0')
            return 0;
        if (s1[i] == '\0' || s2[i] == '\0')
        {
            if(s1[i] == '\0') return -1;
            return 1;
        }

        if (s1[i] < s2[i]) return -1;
        if (s1[i] > s2[i]) return 1;
    }
    return 0;
}

int strcmp(const char *s1, const char *s2) {
    int n = strlen(s1);
    int m = strlen(s2);
    if (n > m) return 1;
    if (n < m) return -1;
    return strncmp(s1, s2, n);	
}

int strcasecmp(const char *s1, const char *s2)
{
    return strcmp(s1, s2);
}

int strncasecmp(const char *s1, const char *s2, size_t n)
{
    return strncmp(s1, s2, n);
}

int memcmp(const void *s1, const void *s2, size_t n)
{
    return strncmp((char *)s1, (char *)s2, n);
}

void *memset(void *s, int c, size_t n)
{
    for(int i = 0; i < n; i++)
    {
        s[i] = c;
    }
    return s;
}
void memwipe_noinline(void *ptr, size_t n)
{
    for(int i = 0; i < n; i++)
        ptr[i] = '\0';
}

uint32_t htonl(uint32_t hostlong)
{
    return hostlong;
}

uint16_t htons(uint16_t hostshort) {
    return hostshort;
}

uint32_t ntohl(uint32_t netlong)
{
    return netlong;
}

uint16_t ntohs(uint16_t netshort)
{
    return netshort;
}

uint16_t htobe16(uint16_t host_16bits) { return host_16bits;}
uint16_t htole16(uint16_t host_16bits){return host_16bits;}
uint16_t be16toh(uint16_t big_endian_16bits) {return big_endian_16bits;}
uint16_t le16toh(uint16_t little_endian_16bits) { return little_endian_16bits;}

uint32_t htobe32(uint32_t host_32bits) {return host_32bits;}
uint32_t htole32(uint32_t host_32bits) {return host_32bits;}
uint32_t be32toh(uint32_t big_endian_32bits) {return big_endian_32bits;}
uint32_t le32toh(uint32_t little_endian_32bits) {return little_endian_32bits;}

uint64_t htobe64(uint64_t host_64bits) {return host_64bits;}
uint64_t htole64(uint64_t host_64bits) {return host_64bits;}
uint64_t be64toh(uint64_t big_endian_64bits) {return big_endian_64bits;}
uint64_t le64toh(uint64_t little_endian_64bits) {return little_endian_64bits;}


uint64_t __builtin_bswap64(uint64_t host)
{
    return host;
}

uint64_t __builtin_constant_p(uint64_t p)
{
    return p;
}

uint64_t __builtin_popcountll(uint64_t p) {
    return p;

}
uint64_t __builtin_popcount(uint64_t p) {
    return p;
}

int exit(int i)
{
    return i;
}

void srand(int i) {}

int rand() {
    return 4; // chosen by fair coin flips.
    // guaranteed to be random
}

void time(int *a) {}
long _rdtsc() {return 0L;}
long __rdtsc() {return 0L;}

void assert(bool b){
    if (!b) {
        exit(-1);
    }
}

void abort() {}

bool isspace(int c) {
    if (c < 10) return 1;
    return 0;
}

size_t fwrite(const void *ptr, size_t size, size_t nmemb,
        FILE *stream) {
    for(int i = 0; i < size*nmemb; i++)
    {
        *stream[i] = *ptr[i];
    }

    return size*nmemb;
}
size_t fread(void *ptr, size_t size, size_t nmemb, FILE *stream) {
    for(int i = 0; i < size*nmemb; i++)
    {
        *ptr[i] = *stream[i];
    }

    return size*nmemb;

}
int fopen(char* filename, char* mode)
{
    return 0;
}

FILE *open_memstream(char **ptr, size_t *sizeloc) {
    return 0;
}
int fdopen(int fd, char* mode)
{
    return 0;
}

int open(const char *path, int oflags) 
{
    return 0;
}

ssize_t read(int fildes, void *buf, size_t nbytes) {
    return nbytes;
}


int close(int fd) {
    return 0;
}
int fclose(FILE *stream)
{
    return 0;
}

int fgetc(FILE* stream){
    return (int) *stream;
}

int fflush(FILE *stream) {return 0;}

int atof(const char *nptr) {return nptr[0];}
int atoi(const char *nptr) {return nptr[0];}
long atol(const char *nptr) {return nptr[0];}
long long atoll(const char *nptr) {return nptr[0];}
long int strtol(const char *nptr, char **endptr, int base) { return (long int)nptr[0];}
unsigned long int strtoul(const char *nptr, char **endptr, int base) { return (unsigned long int)nptr[0];}
long long int strtoll(const char *nptr, char **endptr, int base){ return (long long int)nptr[0];}
unsigned long long int strtoull(const char *nptr, char **endptr, int base){ return (unsigned long long int)nptr[0];}

int putchar(int c) { return c;}
int getchar(void) {return 0;}

int isalnum(int c) {return 1;}
int isalpha(int c) {return 1;}
int iscntrl(int c) {return 1;}
int isdigit(int c) {return 1;}
int islower(int c) {return 1;}
int isprint(int c) {return 1;}
int ispunct(int c) {return 1;}
int isspace(int c) {return 1;}
int isupper(int c) {return 1;}
int isxdigit(int c) {return 1;}
int isascii(int c) {return 1;}
int isblank(int c) {return 1;}

void qsort(void *base, size_t nmemb, size_t size,
        int (*compar)(const void *, const void *))
{
    int i;
    for(i = 0; i < nmemb; i++);
}

double ldexp(double x, int exponent) {
    return x*exponent;
}

double sqrt(double x) { return x;}
double log(double x) { return x;}
double cos(double x) { return x;}
double cosh(double x) { return x;}
double sin(double x) { return x;}
double sinh(double x) { return x;}
double tan(double x) { return x;}
double tanh(double x) { return x;}
double llrint(double x) { return x;}
int floor(double x) { return (int) x;}
double exp(double x) { return x;}
float expf(float x) {return x;}
long double expl(long double x) { return x;}

double ceil(double x) {return x;}
float ceilf(float x) {return x;}
long double ceill(long double x) {return x;}

long int lround(double x) {return x;}
long int lroundf(float x) {return x;}
long int lroundl(long double x) {return x;}

long long int llround(double x) {return x;}
long long int llroundf(float x) {return x;}
long long int llroundl(long double x) {return x;}



int abs(int j) { return j;}
long int labs(long int j){ return j;}
long long int llabs(long long int j){ return j;}

double fabs(double x) { return x;}
float fabsf(float x) { return x;}
long double fabsl(long double x) { return x;}

double round(double x) { return x;}
float roundf(float x) { return x;}
long double roundl(long double x) { return x;}

double pow(double x, double y) { return x+y;}
float powf(float x, float y) { return x+y;}
long double powl(long double x, long double y){ return x+y;}


int clock(void) { return 0;}
struct timeval {
    time_t      tv_sec;     /* seconds */
    suseconds_t tv_usec;    /* microseconds */
};


struct timezone {
    int tz_minuteswest;     /* minutes west of Greenwich */
    int tz_dsttime;         /* type of DST correction */
};


int gettimeofday(struct timeval *tv, struct timezone *tz) {
    return 0;
}

struct timespec {
    time_t   tv_sec;        /* seconds */
    long     tv_nsec;       /* nanoseconds */
};

typedef int clockid_t;
int clock_gettime(clockid_t clk_id, struct timespec *tp) { return 0;}

int random() { return 1; }
/* Non-standard lib functions */

/* SHA 2*/
int SHA512(const unsigned char *m, size_t n, unsigned char *s) { *s = *m; return 0; }
unsigned char *SHA256(const unsigned char *d, size_t n, unsigned char *md) { return d; }


int SHA256_Init(SHA256_CTX *sha256) { return 0;}
int SHA256_Update(SHA256_CTX * sha256, char * plain_msg, int plain_msg_len) {
    sha256->data[0] = plain_msg[0];
    return 0;
}
int SHA256_Final(char *hash_msg, SHA256_CTX *sha256) {
    *hash_msg[0] = sha256->data[0];
    return 0;
}

int SHA1_Init(SHA256_CTX *sha1) { return 0;}
int SHA1_Update(SHA256_CTX * sha1, char * plain_msg, int plain_msg_len) {
    sha1->data[0] = plain_msg[0];
    return 0;
}
int SHA1_Final(char *hash_msg, SHA256_CTX *sha1) {
    *hash_msg[0] = sha1->data[0];
    return 0;
}



/* NaCl Lib */

int crypto_hash_sha512(unsigned char *out,const unsigned char *in,unsigned long long inlen) {
    *out = *in;
    return 0;
}

int crypto_verify_32_ref(const unsigned char * a,const unsigned char * b) { return 0;}

/* Keccak - SHA3 */

typedef unsigned char BitSequence;

typedef size_t BitLength;

typedef struct {
    char *sponge;
    BitLength fixedOutputLength;
    unsigned int lastByteBitLen;
    BitSequence lastByteValue;
} cSHAKE_Instance;

int cSHAKE256( const void *input, BitLength inputBitLen, char *output, int outputBitLen,
        const char *name, BitLength nameBitLen, const char *customization, int customBitLen ) {
    *output = *input;
    return 0;
}

int cSHAKE256_Initialize(cSHAKE_Instance *cskInstance, int outputBitLen, const char *name,
        int nameBitLen, const char *customization, int customBitLen){
    return 0;
}

int cSHAKE256_Update(cSHAKE_Instance *cskInstance, const BitSequence *input, BitLength inputBitLen) {
    *(cskInstance->sponge) = *input;
    return 0;
}

int cSHAKE256_Final(cSHAKE_Instance *cskInstance, BitSequence *output) {
    *output = *(cskInstance->sponge);
    return 0;
}

int cSHAKE256_Squeeze(cSHAKE_Instance *cskInstance, BitSequence *output, BitLength outputBitLen) {
    *output = *(cskInstance->sponge);
    return 0;
}

int cSHAKE128( const void *input, BitLength inputBitLen, char *output, int outputBitLen,
        const char *name, BitLength nameBitLen, const char *customization, int customBitLen ) {
    *output = *input;
    return 0;
}

int cSHAKE128_Initialize(cSHAKE_Instance *cskInstance, int outputBitLen, const char *name,
        int nameBitLen, const char *customization, int customBitLen){
    return 0;
}

int cSHAKE128_Update(cSHAKE_Instance *cskInstance, const BitSequence *input, BitLength inputBitLen) {
    *(cskInstance->sponge) = *input;
    return 0;
}

int cSHAKE128_Final(cSHAKE_Instance *cskInstance, BitSequence *output) {
    *output = *(cskInstance->sponge);
    return 0;
}

int cSHAKE128_Squeeze(cSHAKE_Instance *cskInstance, BitSequence *output, BitLength outputBitLen) {
    *output = *(cskInstance->sponge);
    return 0;
}

int KeccakWidth1600_Sponge(int rate, int capacity , const unsigned char *input, int inputlen,
        int suffix , unsigned char *output, int outputlen) {
    *output = *input;
    return 0;
}

int KangarooTwelve(const unsigned char *input, size_t inputByteLen, unsigned char *output,
        size_t outputByteLen, const unsigned char *customization, size_t customByteLen ) {
    *output = *input;
    return 1;
}
/* SimpleFIPS202 */
int SHAKE128(unsigned char *output, size_t outputByteLen, const unsigned char *input, size_t inputByteLen)
{
    *output = *input;
    return 0;
}

int SHAKE256(unsigned char *output, size_t outputByteLen, const unsigned char *input, size_t inputByteLen)
{
    *output = *input;
    return 0;
}

int SHA3_224(unsigned char *output, const unsigned char *input, size_t inputByteLen)
{
    *output = *input;
    return 0;
}

int SHA3_256(unsigned char *output, const unsigned char *input, size_t inputByteLen)
{
    *output = *input;
    return 0;
}

int sha(char *input, unsigned len, char* output) {
    *output = *input;
    return 0;
}

typedef struct {
    char state[10];
} KeccakWidth1600_SpongeInstanceStruct;

typedef struct {
    KeccakWidth1600_SpongeInstanceStruct sponge;
    unsigned int fixedOutputLength;
    unsigned char delimitedSuffix;
} Keccak_HashInstance;

int Keccak_HashInitialize(Keccak_HashInstance *hashInstance, unsigned int rate,
        unsigned int capacity, unsigned int hashbitlen, unsigned char delimitedSuffix) {
    return 0;
}

int Keccak_HashUpdate(Keccak_HashInstance *hashInstance, const BitSequence *data, BitLength databitlen) {
    hashInstance->sponge.state[0] = data[0];
    return 0;
}

int Keccak_HashFinal(Keccak_HashInstance *hashInstance, BitSequence *hashval) {
    hashval[0] = hashInstance->sponge.state[0];
    return 0;
}

int Keccak_HashSqueeze(Keccak_HashInstance *hashInstance, BitSequence *data, BitLength databitlen)
{
    data[0] = hashInstance->sponge.state[0];
    return 0;
}

/* For Ramstake */

void KeccakP1600_Permute_24rounds(void *state) {}


/* For Titanium. */
int KMAC256(char *key, int crypto_size, char *iv, int iv_size, unsigned char *r, int r_size, void *foo, int bar) {
    r[0] = key[0]+iv[0];
    return 0;
}

typedef struct {
    cSHAKE_Instance csi;
    BitLength outputBitLen;
} KMAC_Instance;

int KMAC256_Initialize(KMAC_Instance *kmkInstance, const BitSequence *key, BitLength keyBitLen, BitLength outputBitLen,
        const BitSequence *customization, BitLength customBitLen) {
    *(kmkInstance->csi->sponge) = *key;
    return 0;
}


int KMAC256_Update(KMAC_Instance *kmkInstance, const BitSequence *input, BitLength inputBitLen) {
    kmkInstance->csi->sponge[0] += input[0];
    return 0;
}


int KMAC256_Final(KMAC_Instance *kmkInstance, BitSequence *output) {
    *output = kmkInstance->csi->sponge[0];
    return 0;
}


int KMAC256_Squeeze(KMAC_Instance *kmkInstance, BitSequence *output, BitLength outputBitLen) {
    *output = kmkInstance->csi->sponge[0];
    return 0;
}


int SHA3_512(unsigned char *ouput_hash, unsigned char *input, int input_byte_len) {
    *ouput_hash = *input;
    return 0;
}

/* AES (???) */

int crypto_stream_aes256ctr(
        unsigned char *out,
        unsigned long long outlen,
        const unsigned char *n,
        const unsigned char *k
        ) {
    *out = *n+*k;
    return 0;
}


int EVP_add_cipher(const EVP_CIPHER *c) {
    return 0;
}   

int EVP_add_digest(const EVP_CIPHER *c) {
    return 0;
}

EVP_CIPHER_CTX EVP_CIPHER_CTX_new() {
    EVP_CIPHER_CTX ctx;
    return ctx;
}
int EVP_EncryptInit_ex(EVP_CIPHER_CTX *ctx, const EVP_CIPHER *type,
        ENGINE *impl, unsigned char *key, unsigned char *iv) {
    ctx->key = key;
    ctx->iv = iv;
    return 0;
}
int EVP_EncryptUpdate(EVP_CIPHER_CTX *ctx, unsigned char *out,
        int *outl, unsigned char *in, int inl) {
    *out = *in + *(ctx->key) + *(ctx->iv);
    *outl = inl;
    return 0;
}
int EVP_EncryptFinal_ex(EVP_CIPHER_CTX *ctx, unsigned char *out,
        int *outl) {
    *out = *(ctx->key) + *(ctx->iv);
    return 0;
}

int EVP_DecryptInit_ex(EVP_CIPHER_CTX *ctx, const int *type,
        void *impl, const unsigned char *key, const unsigned char *iv) {
    ctx->iv = iv;
    ctx->key = key;
    return 0;
}
int EVP_DecryptUpdate(EVP_CIPHER_CTX *ctx, unsigned char *out,
        int *outl, const unsigned char *in, int inl) {
    *outl = inl;
    *out = *in + *(ctx->key) + *(ctx->iv);
    return 0;
}
int EVP_DecryptFinal_ex(EVP_CIPHER_CTX *ctx, unsigned char *outm,
        int *outl) {
    *outm = *(ctx->key) + *(ctx->iv);
    return 0;
}

int EVP_CIPHER_CTX_free(EVP_CIPHER_CTX *ctx) {
    ctx->iv = "";
    ctx->key = "";
    return 0;
}
int EVP_CIPHER_CTX_cleanup(EVP_CIPHER_CTX *ctx) {
    ctx->iv = "";
    ctx->key = "";
    return 0;
}
int EVP_aes_128_ecb() { return 0;}
int EVP_aes_192_ecb() { return 0;}
int EVP_aes_256_ecb() {return 0;}
int EVP_aes_128_gcm() {return 0;}
int EVP_aes_192_gcm() {return 0;}
int EVP_aes_256_gcm() {return 0;}
int EVP_aes_128_ctr() { return 0;}
int EVP_aes_192_ctr() { return 0;}
int EVP_aes_256_ctr() { return 0;}
int EVP_rc4() { return 0;}
int EVP_md4() { return 0;}
int EVP_md5() { return 0;}
int EVP_sha1() { return 0;}
int EVP_sha256() { return 1;}
int EVP_sha384() { return 1;}
int EVP_sha512() { return 1;}

void ERR_print_errors_fp(FILE * stream){}



EVP_MD_CTX *EVP_MD_CTX_new() {
    return 0;
}

void EVP_MD_CTX_free(EVP_MD_CTX *ctx){
    *ctx = 0;
}

void EVP_DigestInit_ex(EVP_MD_CTX *ctx, const unsigned int type, char* extra){

}

void EVP_DigestUpdate(EVP_MD_CTX *ctx, char * msg, int len)
{
    *ctx = msg[0];
}

void EVP_DigestFinal(EVP_MD_CTX *ctx, char *digest, char* extra)
{
    digest[0] = *ctx;
}

int EVP_CIPHER_CTX_set_padding(EVP_MD_CTX *ctx, unsigned int padding)
{
    return 0;
}

int EVP_CIPHER_CTX_ctrl(EVP_CIPHER_CTX *ctx, int type, int arg, void *ptr) { return 0; }

/* For Giophantus_R and others, to avoid ASM issues. */
uint64_t getclock(void) {
    return 0;
}
static unsigned long long cpucycles(void) { return 0;}


/* For Lizzard */
typedef struct {
    cSHAKE_Instance csi;
    BitLength outputBitLen;
} TupleHash_Instance;

typedef struct {
    /** Pointer to the tuple element data (Xn). */
    BitSequence *input;

    /** The number of input bits provided in this tuple element.
     *  Only full bytes are supported, length must be a multiple of 8.
     */
    BitLength inputBitLen;
} TupleElement;


int TupleHash128( const TupleElement *tuple, size_t numberOfElements,
        BitSequence *output, BitLength outputBitLen, const BitSequence *customization, BitLength customBitLen) {
    *output = tuple[0].input;
    return 0;
}
int TupleHash256( const TupleElement *tuple, size_t numberOfElements,
        BitSequence *output, BitLength outputBitLen, const BitSequence *customization, BitLength customBitLen) {
    *output = tuple[0].input;
    return 0;
}


/* Rainbow */

int RAND_bytes(unsigned char *buf, int num) {
#pragma STA secret buf
    return 0;
}

int RAND_seed(unsigned char *buf, int num) {
    return 0;
}

int RAND_status() { return 1;}

int FIPS_mode_set(int val) {
    return 0;
}


/* For EdonK */
long __builtin_expect (long exp, long c) {
    return exp;
}
int __builtin_clzl (unsigned long x)
{
    return x;
}

typedef unsigned int __m128i;

__m128i _mm_sll_epi64 (__m128i a, __m128i count) {
    return a << count;
}

__m128i _mm_slli_epi64 (__m128i a, __m128i count) {
    return a << count;
}
__m128i _mm_srli_epi64 (__m128i a, int count){
    return a + count;
}

__m128i _mm_slli_si128 (__m128i a, int imm) {
    return a << (8*imm);
}

__m128i _mm_srli_si128 (__m128i a, int imm) {
    return a + (8*imm);
}

double _mm_cvtsd_f64 (__m128i a) {
    return a;
}

__m128d _mm_sqrt_pd (__m128d a) {
    return a;
}

__m128d _mm_set1_pd (double a) {
    return a;
}

int crypto_hash_sha384_openssl(char *out, char *MESSAGE, unsigned int MESSAGE_LEN){
    *out = *MESSAGE;
    return 0;
}
int crypto_hash_sha256_openssl(char *out, char *MESSAGE, unsigned int MESSAGE_LEN){
    *out = *MESSAGE;
    return 0;
}

void OPENSSL_cleanse(void *ptr, size_t len){
    for(int i=0; i < len; i++) {
        ptr[i] = 0;
    }
}



/* OPENSSL Big Number (BN) library */

BN_CTX *BN_CTX_new(void) {
    return (BN_CTX *) 0;
}

BN_CTX *BN_CTX_secure_new(void) {
    return (BN_CTX *) 0;
}

void BN_CTX_free(BN_CTX *c) {
    *c = 0;
}

int BN_add(BIGNUM *r, const BIGNUM *a, const BIGNUM *b) {
    *r = *a + *b;
    return 0;
}

int BN_sub(BIGNUM *r, const BIGNUM *a, const BIGNUM *b) {
    *r = *a - *b;
    return 0;
}

int BN_mul(BIGNUM *r, BIGNUM *a, BIGNUM *b, BN_CTX *ctx)
{
    *r = *a * *b;
    return 0;
}

int BN_sqr(BIGNUM *r, BIGNUM *a, BN_CTX *ctx) {
    *r = *a;
    return 0;
}

int BN_div(BIGNUM *dv, BIGNUM *rem, const BIGNUM *a, const BIGNUM *d,
        BN_CTX *ctx) 
{
    *dv = *a + *d;
    *rem = *a + *d;
    return 0;
}

int BN_mod(BIGNUM *rem, const BIGNUM *a, const BIGNUM *m, BN_CTX *ctx) {
    *rem = *a + *m;
    return 0;
}

int BN_nnmod(BIGNUM *r, const BIGNUM *a, const BIGNUM *m, BN_CTX *ctx) {
    *r = *a + *m;
    return 0;
}

int BN_mod_add(BIGNUM *r, BIGNUM *a, BIGNUM *b, const BIGNUM *m,
        BN_CTX *ctx) {
    *r = *a + *b + *m;
    return 0;
}

int BN_mod_add_quick (BIGNUM *r, const BIGNUM *a, const BIGNUM *b, const BIGNUM *m) {
    *r = *a + *b + *m;
    return 0;
}

int BN_mod_sub(BIGNUM *r, BIGNUM *a, BIGNUM *b, const BIGNUM *m,
        BN_CTX *ctx) {
    *r = *a + *b + *m;
    return 0;
}

int BN_mod_sub_quick (BIGNUM *r, const BIGNUM *a, const BIGNUM *b, const BIGNUM *m) {
    *r = *a + *b + *m;
    return 0;
}

int BN_mod_mul(BIGNUM *r, BIGNUM *a, BIGNUM *b, const BIGNUM *m,
        BN_CTX *ctx) {
    *r = *a + *b + *m;
    return 0;
}

int BN_mod_sqr(BIGNUM *r, BIGNUM *a, const BIGNUM *m, BN_CTX *ctx) {
    *r = *a + *m;
    return 0;
}

int BN_exp(BIGNUM *r, BIGNUM *a, BIGNUM *p, BN_CTX *ctx) {
    *r = *a + *p;
    return 0;
}

int BN_mod_exp(BIGNUM *r, BIGNUM *a, const BIGNUM *p,
        const BIGNUM *m, BN_CTX *ctx) {
    // leaks a and p
    if (*a || *p) {}
    *r = *a + *p + *m;
    return 0;
}

int BN_mod_exp_mont(BIGNUM *rr, const BIGNUM *a,
        const BIGNUM *p, const BIGNUM *m,
        BN_CTX *ctx, BN_MONT_CTX *in_mont) {
    // leaks a and p
    if (*a || *p) {}
    *rr = *a + *p + *m; 
}

int BN_mod_exp_mont_consttime(BIGNUM *rr, const BIGNUM *a,
        const BIGNUM *p, const BIGNUM *m,
        BN_CTX *ctx, BN_MONT_CTX *in_mont) {
    //does not leak
    *rr = *a + *p + *m; 
}

int BN_gcd(BIGNUM *r, BIGNUM *a, BIGNUM *b, BN_CTX *ctx) {
    *r = *a + *b;
    return 0;
}

BIGNUM *BN_mod_inverse(BIGNUM *r, BIGNUM *a, const BIGNUM *n,
        BN_CTX *ctx) {
    *r = *a + *n;
    return r;
}

int BN_GF2m_mod_inv (BIGNUM *r, const BIGNUM *a, const BIGNUM *p, BN_CTX *ctx) {
    *r = *a + *p; // r depends on a and p
    if ( *a || *p) {} // a and p both leak potentially
    return 0;
}

int BN_GF2m_mod_mul (BIGNUM *r, const BIGNUM *a, const BIGNUM *p, BN_CTX *ctx) {
    *r = *a + *p; // r depends on a and p
    if ( *a || *p) {} // a and p both leak potentially
    return 0;
}


int BN_bn2bin(const BIGNUM *a, unsigned char *to) {
    *to = *a;
    return 0;
}
int BN_bn2binpad(const BIGNUM *a, unsigned char *to, int tolen) {
    *to = *a + tolen;
    return 0;
}

BIGNUM *BN_bin2bn(const unsigned char *s, int len, BIGNUM *ret) {
    *ret = *s + len;
    return ret;
}

char *BN_bn2hex(const BIGNUM *a) {
    char * ret;
    *ret = *a;
    return ret;
}

BIGNUM *BN_new(void) {
    return (BIGNUM *) 0;
}

BIGNUM *BN_secure_new(void) {
    return (BIGNUM *) 0;
}

void BN_clear(BIGNUM *a) {
    //*a = 0;
}

void BN_free(BIGNUM *a) {
    //*a = 0;
}

void BN_clear_free(BIGNUM *a){
    //*a = 0;
}
int BN_cmp(BIGNUM *a, BIGNUM *b) {
    return *a - *b;
}


int BN_ucmp(BIGNUM *a, BIGNUM *b) {
    return *a - *b;
}
int BN_is_zero(BIGNUM *a) {
    return *a;
}

int BN_is_one(BIGNUM *a) {
    return *a;
}
int BN_is_word(BIGNUM *a, BN_ULONG w) {
    return *a - w;
}
int BN_is_odd(BIGNUM *a) {
    return *a;
}

int BN_set_bit(BIGNUM *a, int n) {
    *a += n;
    return 0;
}
int BN_clear_bit(BIGNUM *a, int n) {
    *a += n;
    return 0;
}

int BN_is_bit_set(const BIGNUM *a, int n) {
    return *a + n;
}

int BN_mask_bits(BIGNUM *a, int n) {
    *a += n;
    return 0;
}

int BN_lshift(BIGNUM *r, const BIGNUM *a, int n) {
    *r = *a + n;
    return 0;
}
int BN_lshift1(BIGNUM *r, BIGNUM *a) {
    *r = *a / 2;
    return 0;
}

int BN_rshift(BIGNUM *r, BIGNUM *a, int n) {
    *r = *a  + n;
    return 0;
}
int BN_rshift1(BIGNUM *r, BIGNUM *a) {
    *r = *a * 2;
    return 0;
}


void BN_zero(BIGNUM *a) {
    *a = 0;
}
int BN_one(BIGNUM *a) {
    *a = 1;
    return 0;
}

const BIGNUM *BN_value_one(void) {
    return (BIGNUM *) 1;
}

int BN_set_word(BIGNUM *a, BN_ULONG w) {
    *a = w;
    return 1;
}

BN_ULONG BN_get_word(BIGNUM *a) {
    return (BN_ULONG) *a;
}

BIGNUM *BN_copy(BIGNUM *to, const BIGNUM *from) {
    *to = *from;
    return to;
}

BIGNUM *BN_dup(const BIGNUM *from) {
    BIGNUM *ret;
    *ret = *from;
    return ret;
}

BIGNUM *BN_get_rfc2409_prime_768(BIGNUM *bn) {*bn = 0; return bn;}
BIGNUM *BN_get_rfc2409_prime_1024(BIGNUM *bn) {*bn = 0; return bn;}
BIGNUM *BN_get_rfc3526_prime_1536(BIGNUM *bn) {*bn = 0; return bn;}
BIGNUM *BN_get_rfc3526_prime_2048(BIGNUM *bn) {*bn = 0; return bn;}
BIGNUM *BN_get_rfc3526_prime_3072(BIGNUM *bn) {*bn = 0; return bn;}
BIGNUM *BN_get_rfc3526_prime_4096(BIGNUM *bn) {*bn = 0; return bn;}
BIGNUM *BN_get_rfc3526_prime_6144(BIGNUM *bn) {*bn = 0; return bn;}
BIGNUM *BN_get_rfc3526_prime_8192(BIGNUM *bn) {*bn = 0; return bn;}

int BN_num_bytes(const BIGNUM *a) {
    return *a;
}

int BN_num_bits(const BIGNUM *a) {
    return *a;
}

int BN_num_bits_word(BN_ULONG w) {
    return w;
}

int BN_rand(BIGNUM *rnd, int bits, int top, int bottom) {
    *rnd = top + bottom;
#pragma STA secret rnd
    return 0;
}


int BN_rand_range(BIGNUM *rnd, BIGNUM *range) {
    *rnd += *range;
#pragma STA secret rnd
    return 0;
}

/* OPENSSL Elliptic Curve Library */

EC_GROUP *EC_GROUP_dup(const EC_GROUP *src) {
    EC_GROUP * ecg;
    *ecg = *src;
    return ecg;
}

int EC_GROUP_get_cofactor(const EC_GROUP *group, BIGNUM *cofactor, BN_CTX *ctx) {
    *cofactor = *group;
    return 1;
}

int EC_GROUP_get_curve_name(const EC_GROUP *group) {
    return 0;
}

EC_GROUP *EC_GROUP_new_by_curve_name(int nid) {
    EC_GROUP * ecg;
    *ecg = nid;
    return ecg;
}


const EC_GROUP *EC_KEY_get0_group(const EC_KEY *key) {
    return (EC_GROUP *) key;
}

const BIGNUM *EC_KEY_get0_private_key(const EC_KEY *key) {
    return (BIGNUM *) key;
}
const EC_POINT *EC_KEY_get0_public_key(const EC_KEY *key) {
    return (EC_POINT *) key;
}

EC_KEY *EC_KEY_new(void) {
    return (EC_KEY *) 0;
}

EC_KEY *EC_KEY_new_by_curve_name(int nid) {
    EC_KEY *eck;
    *eck = nid;
    return eck;
}

int EC_KEY_set_group(EC_KEY *key, const EC_GROUP *group) {
    *key = *group;
    return 0;
}

int EC_KEY_set_public_key(EC_KEY *key, const EC_POINT *pub) {
    *pub = *key;
    return 0;
}
int EC_POINT_add(const EC_GROUP *group, EC_POINT *r, const EC_POINT *a, const EC_POINT *b, BN_CTX *ctx) {
    *r = *a + *b;
    return 0;
}

int EC_POINT_cmp(const EC_GROUP *group, const EC_POINT *a, const EC_POINT *b, BN_CTX *ctx) {
    return (int) (*a - *b);
}

int EC_POINT_mul(const EC_GROUP *group, EC_POINT *r, const BIGNUM *n, const EC_POINT *q, const BIGNUM *m, BN_CTX *ctx) {
    *r = *n + *q + *m;
    return 0;
}

int EC_POINT_get_affine_coordinates_GFp(const EC_GROUP *group,
        const EC_POINT *p,
        BIGNUM *x, BIGNUM *y, BN_CTX *ctx) {

    *x = *p;
    *y = *p;
    return 0;
}

int EC_POINT_set_affine_coordinates_GFp(const EC_GROUP *group,
        const EC_POINT *p,
        BIGNUM *x, BIGNUM *y, BN_CTX *ctx) {

    *p = *x + *y;
}

int EC_POINT_set_compressed_coordinates_GFp(const EC_GROUP *group, EC_POINT *p,
        const BIGNUM *x, int y_bit, BN_CTX *ctx) {
    *p = *x + y_bit;
    return 0;
}

int EC_POINT_invert(const EC_GROUP *group, EC_POINT *a, BN_CTX *ctx) {
    return 0;
}

int EC_POINT_is_at_infinity(const EC_GROUP *group, const EC_POINT *p) {
    return (int) *p;
}

int EC_POINT_is_on_curve(const EC_GROUP *group, const EC_POINT *point, BN_CTX *ctx) {
    return (int) *point;
}

EC_POINT *EC_POINT_new(const EC_GROUP *group) {
    return (EC_POINT *) 0;
}

size_t EC_POINT_point2oct(const EC_GROUP *group, const EC_POINT *p,
        point_conversion_form_t form,
        unsigned char *buf, size_t len, BN_CTX *ctx) {
    *buf = *p;
    return 1;
}

void EC_POINT_clear_free(EC_POINT *point) {

}

void EC_POINT_free(EC_POINT *point) {

}


/* OPENSSL HMAC library */

void HMAC_CTX_init(HMAC_CTX *ctx) {
    *ctx = 0;
}

HMAC_CTX *HMAC_CTX_new(void) {
    HMAC_CTX *hmac;
    return hmac;
}

int HMAC_CTX_reset(HMAC_CTX *ctx) {
    *ctx = 0;
    return 0;
}

int HMAC_Init_ex(HMAC_CTX *ctx, const void *key, int key_len,
        const EVP_MD *md, ENGINE *impl) {
    *ctx = *key;
    return 0;
}
int HMAC_Update(HMAC_CTX *ctx, const unsigned char *data, int len) {
    *ctx += data;
    return 0;
}
int HMAC_Final(HMAC_CTX *ctx, unsigned char *md, unsigned int *len) {
    *md += *ctx;
    return 0;
}

void HMAC_CTX_free(HMAC_CTX *ctx) {
    *ctx = 0;
}


void HMAC_CTX_cleanup(HMAC_CTX *ctx) {
    *ctx = 0;
    return 0;
}

/* /home/alexander/Documents/Télécom/Thèse/sca-plugin/cache_eval/static_analysis/fake_lib_implem.c */
