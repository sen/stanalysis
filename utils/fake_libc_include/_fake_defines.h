#ifndef _FAKE_DEFINES_H
#define _FAKE_DEFINES_H

#define	NULL	0
#define	BUFSIZ		1024
#define	FOPEN_MAX	20
#define	FILENAME_MAX	1024

#ifndef SEEK_SET
#define	SEEK_SET	0	/* set file offset to offset */
#endif
#ifndef SEEK_CUR
#define	SEEK_CUR	1	/* set file offset to current plus offset */
#endif
#ifndef SEEK_END
#define	SEEK_END	2	/* set file offset to EOF plus offset */
#endif

#define __BYTE_ORDER __LITTLE_ENDIAN
#define BYTE_ORDER __BYTE_ORDER

#define EXIT_FAILURE 1
#define EXIT_SUCCESS 0


#define UCHAR_MAX 255
#define USHRT_MAX 65535
#define UINT_MAX 4294967295U
#define RAND_MAX 32767
//#define INT_MAX 32767
//#define INT_MAX 32767

/* va_arg macros and type*/
typedef int va_list;
#define va_start(_ap, _type) __builtin_va_start((_ap))
#define va_arg(_ap, _type) __builtin_va_arg((_ap))
#define va_end(_list)

/* Max and min values for various types. */
#define SCHAR_MAX __SCHAR_MAX__
#define WCHAR_MAX __WCHAR_MAX__
#define SHRT_MAX __SHRT_MAX__
#define INT_MAX __INT_MAX__
#define LONG_MAX __LONG_MAX__
#define LONG_LONG_MAX __LONG_LONG_MAX__
#define WINT_MAX __WINT_MAX__
#define SIZE_MAX __SIZE_MAX__
#define PTRDIFF_MAX __PTRDIFF_MAX__
#define INTMAX_MAX __INTMAX_MAX__
#define UINTMAX_MAX __UINTMAX_MAX__
#define SIG_ATOMIC_MAX __SIG_ATOMIC_MAX__
#define INT8_MAX __INT8_MAX__
#define INT16_MAX __INT16_MAX__
#define INT32_MAX __INT32_MAX__
#define INT64_MAX __INT64_MAX__
#define UINT8_MAX __UINT8_MAX__
#define UINT16_MAX __UINT16_MAX__
#define UINT32_MAX __UINT32_MAX__
#define UINT64_MAX __UINT64_MAX__
#define INT_LEAST8_MAX __INT_LEAST8_MAX__
#define INT_LEAST16_MAX __INT_LEAST16_MAX__
#define INT_LEAST32_MAX __INT_LEAST32_MAX__
#define INT_LEAST64_MAX __INT_LEAST64_MAX__
#define UINT_LEAST8_MAX __UINT_LEAST8_MAX__
#define UINT_LEAST16_MAX __UINT_LEAST16_MAX__
#define UINT_LEAST32_MAX __UINT_LEAST32_MAX__
#define UINT_LEAST64_MAX __UINT_LEAST64_MAX__
#define INT_FAST8_MAX __INT_FAST8_MAX__
#define INT_FAST16_MAX __INT_FAST16_MAX__
#define INT_FAST32_MAX __INT_FAST32_MAX__
#define INT_FAST64_MAX __INT_FAST64_MAX__
#define UINT_FAST8_MAX __UINT_FAST8_MAX__
#define UINT_FAST16_MAX __UINT_FAST16_MAX__
#define UINT_FAST32_MAX __UINT_FAST32_MAX__
#define UINT_FAST64_MAX __UINT_FAST64_MAX__
#define INTPTR_MAX __INTPTR_MAX__
#define UINTPTR_MAX __UINTPTR_MAX__
#define WCHAR_MIN __WCHAR_MIN__
#define WINT_MIN __WINT_MIN__
#define SIG_ATOMIC_MIN __SIG_ATOMIC_MIN__

/* Conversion functions, I guess ? */
#define INT8_C __INT8_C
#define INT16_C __INT16_C
#define INT32_C __INT32_C
#define INT64_C __INT64_C
#define UINT8_C __UINT8_C
#define UINT16_C __UINT16_C
#define UINT32_C __UINT32_C
#define UINT64_C __UINT64_C
#define INTMAX_C __INTMAX_C
#define UINTMAX_C __UINTMAX_C


#define __volatile__
#define __WORDSIZE 64

#define ULLONG_MAX UINT64_MAX
#define PRId64 "%lld"
#define PRIuPTR ""

#define __func__ ""


#define EINTR 0
#define CLOCK_REALTIME 0

#endif
