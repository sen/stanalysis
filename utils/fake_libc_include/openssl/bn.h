/*
 * Copyright 1995-2016 The OpenSSL Project Authors. All Rights Reserved.
 * Copyright (c) 2002, Oracle and/or its affiliates. All rights reserved
 *
 * Licensed under the OpenSSL license (the "License").  You may not use
 * this file except in compliance with the License.  You can obtain a copy
 * in the file LICENSE in the source distribution or at
 * https://www.openssl.org/source/license.html
 */

#include "_fake_defines.h"
#include "_fake_typedefs.h"

#define BN_MP_DR_SETUP_C 1
#define BN_MP_REDUCE_2K_L_C 1
#define BN_MP_REDUCE_2K_SETUP_C 1
#define BN_MP_REDUCE_IS_2K_L_C 1
#define BN_FLG_CONSTTIME 1

#define BN_set_flags(b,n)   ((b)->flags|=(n))
#define BN_get_flags(b,n)   ((b)->flags&(n))


typedef int BIGNUM;
typedef int BN_CTX;
typedef int BN_ULONG;
typedef int BN_MONT_CTX;
