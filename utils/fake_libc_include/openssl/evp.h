#include "_fake_defines.h"
#include "_fake_typedefs.h"

#define EVP_CTRL_GCM_SET_TAG 1
#define EVP_CTRL_GCM_GET_TAG 1

typedef struct {
    char *key;
    char *iv;
} EVP_CIPHER_CTX;

typedef int EVP_CIPHER;
typedef int ENGINE;

typedef int EVP_MD_CTX;

