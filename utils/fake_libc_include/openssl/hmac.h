#include "_fake_defines.h"
#include "_fake_typedefs.h"

typedef int HMAC_CTX;
typedef int EVP_MD_CTX;
typedef int EVP_MD;
typedef int DES_key_schedule;
typedef int DES_cblock;
typedef int DH;
typedef int EVP_PKEY;
typedef int EVP_PKEY_CTX;