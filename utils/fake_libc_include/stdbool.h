#include "_fake_defines.h"
#include "_fake_typedefs.h"

/* C99 stdbool.h defines */
#define __bool_true_false_are_defined 1
#define false 0
#define true 1